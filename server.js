// @flow
require('dotenv').config();

const express = require('express');
const fs = require('fs');
const path = require('path');
const del = require('del');
const cors = require('cors');
const multer = require('multer');
const next = require('next');
const axios = require('axios');
const FormData = require('form-data');
const cookieParser = require('cookie-parser');
const map = require('lodash/map');
const omit = require('lodash/omit');
const crypto = require('crypto');
const bugsnag = require('@bugsnag/js');
const bugsnagExpress = require('@bugsnag/plugin-express');

const dev = process.env.NODE_ENV !== 'production';
const app = next({dev});
const handle = app.getRequestHandler();

const isBugsnagEnabled = !dev && process.env.NEXT_SERVER_BUGSNAG_CLIENT_ID;
let bugsnagClient;

if (isBugsnagEnabled) {
    bugsnagClient = bugsnag({
        apiKey: process.env.NEXT_SERVER_BUGSNAG_CLIENT_ID,
        beforeSend: report =>
            new Promise((resolve, reject) => {
                if (process.env.NODE_ENV === 'development') {
                    resolve(false);
                } else {
                    resolve();
                }
            }),
        releaseStage: process.env.NODE_ENV
    });
    bugsnagClient.use(bugsnagExpress);
}

const UPLOAD_PATH = '/tmp';
const storage = multer.diskStorage({
    dest: `${UPLOAD_PATH}/`,
    filename: (req, file, cb) => {
        crypto.pseudoRandomBytes(16, function(err, raw) {
            cb(null, `${raw.toString('hex')}.${path.extname(file.originalname).replace('.', '')}`);
        });
    },
    fileFilter: (req, file, cb) => {
        if (!file.originalname.match(/\.(jpg|jpeg|png)$/)) {
            return cb(new Error('Only image files are allowed'), false);
        }

        cb(null, true);
    }
});
const upload = multer({storage});

const startServer = () => {
    const server = express();

    if (isBugsnagEnabled && bugsnagClient) {
        const middleware = bugsnagClient.getPlugin('express');

        server.use(middleware.requestHandler);
        server.use(middleware.errorHandler);
    }

    server.use(cookieParser());
    server.get('/_next/*', (req, res) => handle(req, res));

    server.post('/upload-api/image', upload.single('file'), async (req, res) => {
        try {
            const form = new FormData();

            form.append('avatar', fs.createReadStream(req.file.path));
            axios
                .post(`${process.env.NEXT_SERVER_API_URL}${req.body.apiUrl}`, form, {
                    headers: {
                        ...{Authorization: `Bearer ${req.cookies.token}`},
                        ...form.getHeaders()
                    }
                })
                .then(response => {
                    // console.log({response});
                    // console.log(response.data.data);
                    res.status(200).json(response.data.data);
                })
                .catch(({response}) => {
                    // console.log(response.data.errors);
                    res.status(response.status).json(response.data);
                });
        } catch (err) {
            console.log({err});
            res.sendStatus(400);
        }
    });

    server.post('/upload-api/file', upload.single('file'), async (req, res) => {
        try {
            const form = new FormData();
            // $FlowFixMe
            const additionalFields = omit(req.body, ['file', 'apiUrl']);

            form.append('file', fs.createReadStream(req.file.path));
            map(additionalFields, (value, key) => {
                form.append(key, value);
            });

            axios
                .post(`${process.env.NEXT_SERVER_API_URL}${req.body.apiUrl}`, form, {
                    headers: {
                        ...{Authorization: `Bearer ${req.cookies.token}`},
                        ...form.getHeaders()
                    }
                })
                .then(response => {
                    console.log({response});
                    console.log(response.data.data);
                    res.status(200).json(response.data.data);
                })
                .catch(({response}) => {
                    console.log(response.data.errors);
                    res.status(response.status).json(response.data);
                });
        } catch (err) {
            console.log({err});
            res.sendStatus(400);
        }
    });

    server.all('*', handle);

    server.listen(process.env.PORT, err => {
        if (err) {
            throw err;
        }

        console.log(`> Ready on http://localhost:${process.env.PORT}`);
    });
};

app.prepare()
    .then(startServer)
    .catch(err => {
        console.error(err.stack);
        process.exit(1);
    });
