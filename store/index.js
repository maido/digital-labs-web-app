// @flow
import {createStore, applyMiddleware} from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension';
import thunkMiddleware from 'redux-thunk';
import {appReducer} from './reducers';

export const user = {
    isAuthenticated: false,
    team: {}
};

export const form = {fields: {}, name: '', state: 'default', step: 1};

export const pagination = {
    activePage: 1,
    currentItems: 0,
    itemsCountPerPage: 50,
    fetchedPages: {},
    onChange: null,
    pageRangeDisplayed: 20,
    skip: 0,
    totalItemsCount: 0,
    type: ''
};

export const syllabus = {
    courses: {
        hasFetched: false,
        allIds: [],
        byId: {}
    },
    labs: {
        hasFetched: false,
        allIds: [],
        byId: {}
    },
    modules: {
        hasFetched: false,
        allIds: [],
        byId: {}
    },
    topics: {
        hasFetched: false,
        allIds: [],
        byId: {}
    }
};

export const network = {
    isLoading: false
};

export const notifications = {
    allIds: {
        read: [],
        unread: []
    },
    byId: {},
    hasFetched: false
};

export const resources = {
    allIds: [],
    byId: {},
    hasFetched: false,
    type: 'All'
};

export const teams = {
    allIds: [],
    byId: {},
    hasFetched: false,
    filter: {name: '', country: ''}
};

export const mentors = {
    allIds: [],
    byId: {},
    hasFetched: false
};

export const initialStates = {
    user,
    form,
    mentors,
    network,
    notifications,
    pagination,
    resources,
    syllabus,
    teams
};

export const initializeStore = (initialState: Object = initialStates) => {
    return createStore(
        appReducer,
        initialState,
        composeWithDevTools(applyMiddleware(thunkMiddleware))
    );
};
