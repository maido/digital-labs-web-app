// @flow
import map from 'lodash/map';
import * as initialStates from './';
import {actions} from './types';

export const appReducer = (state: Object = initialStates, action: Function) => {
    switch (action.type) {
        case actions.UPDATE_FORM:
            return Object.assign({}, state, {
                ...state,
                form: {
                    ...state.form,
                    ...action.form
                }
            });
        case actions.UPDATE_USER:
            return Object.assign({}, state, {
                ...state,
                user: {
                    ...state.user,
                    ...action.user,
                    team: {
                        ...state.user.team,
                        ...action.user.team
                    }
                }
            });
        case actions.UPDATE_NETWORK:
            return Object.assign({}, state, {
                ...state,
                network: {
                    ...state.network,
                    ...action.network
                }
            });
        case actions.UPDATE_SYLLABUS:
            return Object.assign({}, state, {
                ...state,
                syllabus: {
                    ...state.syllabus,
                    ...action.content
                }
            });
        case actions.UPDATE_SYLLABUS_FETCHES:
            return Object.assign({}, state, {
                ...state,
                syllabus: {
                    ...state.syllabus,
                    [action.key]: {
                        ...state.syllabus[action.key],
                        ...{hasFetched: action.content}
                    }
                }
            });
        case actions.UPDATE_SYLLABUS_CONTENT: {
            const allIds = [];
            const byId = {};

            map(action.content, c => {
                let currentContent = {};

                if (state.syllabus[action.key] && state.syllabus[action.key].byId[c.id]) {
                    currentContent = state.syllabus[action.key].byId[c.id];
                }

                byId[c.id] = {...currentContent, ...c};

                if (!state.syllabus[action.key].allIds.includes(c.id)) {
                    allIds.push(c.id);
                }
            });

            return Object.assign({}, state, {
                ...state,
                syllabus: {
                    ...state.syllabus,
                    [action.key]: {
                        hasFetched: state.syllabus[action.key].hasFetched,
                        byId: {...state.syllabus[action.key].byId, ...byId},
                        allIds: [...state.syllabus[action.key].allIds, ...allIds]
                    }
                }
            });
        }
        case actions.UPDATE_NOTIFICATIONS: {
            const allIds = {
                read: [],
                unread: []
            };
            const byId = {};

            map(action.items, i => {
                byId[i.id] = i;

                const isRead = i.read_at;

                if (isRead && !state.notifications.allIds.read.includes(i.id)) {
                    allIds.read.push(i.id);
                } else if (!state.notifications.allIds.unread.includes(i.id)) {
                    allIds.unread.push(i.id);
                }
            });

            return Object.assign({}, state, {
                ...state,
                notifications: {
                    byId: {...state.notifications.byId, ...byId},
                    allIds: {
                        read: [...state.notifications.allIds.read, ...allIds.read],
                        unread: [...state.notifications.allIds.unread, ...allIds.unread]
                    },
                    hasFetched: true
                }
            });
        }
        case actions.UPDATE_RESOURCES: {
            const allIds = [];
            const byId = {};

            map(action.items, i => {
                byId[i.id] = i;

                if (!state.resources.allIds.includes(i.id)) {
                    allIds.push(i.id);
                }
            });

            return Object.assign({}, state, {
                ...state,
                resources: {
                    type: state.resources.type,
                    byId: {...state.resources.byId, ...byId},
                    allIds: [...state.resources.allIds, ...allIds],
                    hasFetched: true
                }
            });
        }
        case actions.UPDATE_RESOURCES_TYPE:
            return Object.assign({}, state, {
                ...state,
                resources: {
                    ...state.resources,
                    ...{type: action.resourceType}
                }
            });
        case actions.UPDATE_TEAMS: {
            const allIds = [];
            const byId = {};

            map(action.items, i => {
                byId[i.id] = i;

                if (!state.teams.allIds.includes(i.id)) {
                    allIds.push(i.id);
                }
            });

            return Object.assign({}, state, {
                ...state,
                teams: {
                    filter: state.teams.filter,
                    byId: {...state.teams.byId, ...byId},
                    allIds: [...state.teams.allIds, ...allIds],
                    hasFetched: true
                }
            });
        }
        case actions.UPDATE_TEAMS_FILTER:
            return Object.assign({}, state, {
                ...state,
                teams: {
                    ...state.teams,
                    ...{filter: {...state.teams.filter, ...action.filter}}
                }
            });
        case actions.UPDATE_PAGINATION:
            return Object.assign({}, state, {
                ...state,
                pagination: {
                    ...state.pagination,
                    ...action.pagination,
                    ...{
                        fetchedPages: {
                            ...state.pagination.fetchedPages,
                            ...action.pagination.fetchedPages
                        }
                    }
                }
            });
        case actions.UPDATE_MENTORS: {
            const allIds = [];
            const byId = {};

            map(action.items, i => {
                byId[i.id] = i;

                if (!state.mentors.allIds.includes(i.id)) {
                    allIds.push(i.id);
                }
            });

            return Object.assign({}, state, {
                ...state,
                mentors: {
                    byId: {...state.mentors.byId, ...byId},
                    allIds: [...state.mentors.allIds, ...allIds],
                    hasFetched: true
                }
            });
        }
        default:
            return state;
    }
};
