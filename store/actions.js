// @flow
import {actions} from './types';

export const updateForm = (form: Object) => {
    return {type: actions.UPDATE_FORM, form};
};

//

export const updateUser = (user: Object) => {
    return {type: actions.UPDATE_USER, user};
};

//

export const updateNetwork = (network: Object) => {
    return {type: actions.UPDATE_NETWORK, network};
};

//

export const updateResources = (items: Object) => {
    return {type: actions.UPDATE_RESOURCES, items};
};

export const updateResourcesType = (type: Object) => {
    return {type: actions.UPDATE_RESOURCES_TYPE, resourceType: type};
};

//

export const updateTeams = (items: Object) => {
    return {type: actions.UPDATE_TEAMS, items};
};

export const updateTeamsFilter = (filter: Object) => {
    return {type: actions.UPDATE_TEAMS_FILTER, filter};
};

//

export const updateNotifications = (items: Object) => {
    return {type: actions.UPDATE_NOTIFICATIONS, items};
};

//

export const updateSyllabus = (content: Object) => {
    return {type: actions.UPDATE_SYLLABUS, content};
};

export const updateSyllabusFetches = (key: string, content: Object) => {
    return {type: actions.UPDATE_SYLLABUS_FETCHES, key, content};
};

export const updateSyllabusContent = (key: string, content: Object) => {
    return {type: actions.UPDATE_SYLLABUS_CONTENT, key, content};
};

//

export const updatePagination = (pagination: Object) => {
    return {type: actions.UPDATE_PAGINATION, pagination};
};

//

export const updateMentors = (items: Object) => {
    return {type: actions.UPDATE_MENTORS, items};
};
