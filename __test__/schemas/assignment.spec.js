// @flow
import {isValidFileType} from '../../schemas/assignment';

describe('Schemas: Assignment', () => {
    it('validates jpeg files', () => {
        expect(isValidFileType('image/jpg')).toEqual(true);
        expect(isValidFileType('image/jpeg')).toEqual(true);
    });

    it('validates png files', () => {
        expect(isValidFileType('image/png')).toEqual(true);
    });

    it('validates gif files', () => {
        expect(isValidFileType('image/gif')).toEqual(true);
    });

    it('validates heic files', () => {
        expect(isValidFileType('image/heic')).toEqual(true);
        expect(isValidFileType('image/heif')).toEqual(true);
    });

    it('validates mp3 files', () => {
        expect(isValidFileType('audio/mp3')).toEqual(true);
        expect(isValidFileType('audio/mpeg3')).toEqual(true);
        expect(isValidFileType('audio/x-mpeg-3')).toEqual(true);
        expect(isValidFileType('audio/mp4')).toEqual(true);
        expect(isValidFileType('video/mpeg')).toEqual(true);
        expect(isValidFileType('video/x-mpeg')).toEqual(true);
    });

    it('validates csv files', () => {
        expect(isValidFileType('application/csv')).toEqual(true);
        expect(isValidFileType('application/x-csv')).toEqual(true);
        expect(isValidFileType('text/csv')).toEqual(true);
        expect(isValidFileType('text/comma-separated-values')).toEqual(true);
        expect(isValidFileType('text/x-comma-separated-values')).toEqual(true);
        expect(isValidFileType('text/tab-separated-values')).toEqual(true);
    });

    it('validates pdf files', () => {
        expect(isValidFileType('application/pdf')).toEqual(true);
    });

    it('validates zip files', () => {
        expect(isValidFileType('application/x-rar-compressed')).toEqual(true);
        expect(isValidFileType('application/octet-stream')).toEqual(true);
        expect(isValidFileType('application/x-compressed')).toEqual(true);
        expect(isValidFileType('application/x-zip-compressed')).toEqual(true);
        expect(isValidFileType('application/zip')).toEqual(true);
        expect(isValidFileType('multipart/x-zip')).toEqual(true);
        expect(isValidFileType('application/x-tar')).toEqual(true);
        expect(isValidFileType('application/x-7z-compressed')).toEqual(true);
    });

    it('validates spreadsheet files', () => {
        expect(isValidFileType('application/vnd.ms-excel')).toEqual(true);
        expect(isValidFileType('application/xls')).toEqual(true);
        expect(isValidFileType('application/xlsx')).toEqual(true);
        expect(isValidFileType('application/vnd.ms-excel')).toEqual(true);
        expect(
            isValidFileType('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
        ).toEqual(true);
        expect(
            isValidFileType('application/vnd.openxmlformats-officedocument.spreadsheetml.template')
        ).toEqual(true);
    });

    it('validates document files', () => {
        expect(isValidFileType('application/docx')).toEqual(true);
        expect(isValidFileType('application/msword')).toEqual(true);
        expect(
            isValidFileType(
                'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
            )
        ).toEqual(true);
    });

    it('validates presentation files', () => {
        expect(isValidFileType('application/ppt')).toEqual(true);
        expect(isValidFileType('application/vnd.ms-powerpoint')).toEqual(true);
        expect(
            isValidFileType(
                'application/vnd.openxmlformats-officedocument.presentationml.presentation'
            )
        ).toEqual(true);
    });
});
