import configureStore from 'redux-mock-store';
import {initialStates} from '../../store';
import {appReducer} from '../../store/reducers';
import * as actions from '../../store/actions';
import {actions as actionTypes} from '../../store/types';

const mockStore = configureStore();
const store = mockStore();

describe('Store: Reducers', () => {
    describe('Form', () => {
        beforeEach(() => {
            store.clearActions();
        });

        test('returns a default form', () => {
            const action = {type: ''};
            const initialState = {
                fields: {},
                name: '',
                state: 'default',
                step: 1
            };

            expect(appReducer(undefined, action).form).toEqual(initialState);
        });

        test('returns an updated form', () => {
            const action = {
                type: actionTypes.UPDATE_FORM,
                form: {step: 2, fields: {first_name: 'Foo', last_name: 'Bar'}, name: 'foo'}
            };
            const modifiedState = {
                fields: {
                    first_name: 'Foo',
                    last_name: 'Bar'
                },
                state: 'default',
                name: 'foo',
                step: 2
            };

            expect(appReducer(undefined, action).form).toEqual(modifiedState);
        });
    });

    describe('Network Status', () => {
        beforeEach(() => {
            store.clearActions();
        });

        test('returns a default status', () => {
            const action = {type: ''};
            const initialState = {
                isLoading: false
            };

            expect(appReducer(undefined, action).network).toEqual(initialState);
        });

        test('returns an updated status', () => {
            const action = {
                type: actionTypes.UPDATE_NETWORK,
                network: {isLoading: true}
            };
            const modifiedState = {
                isLoading: true
            };

            expect(appReducer(undefined, action).network).toEqual(modifiedState);
        });
    });

    describe('Syllabus', () => {
        beforeEach(() => {
            store.clearActions();
        });

        test('returns a default syllabus', () => {
            const action = {type: ''};
            const initialState = {
                labs: {
                    hasFetched: false,
                    byId: {},
                    allIds: []
                },
                modules: {
                    hasFetched: false,
                    byId: {},
                    allIds: []
                },
                courses: {
                    hasFetched: false,
                    byId: {},
                    allIds: []
                },
                topics: {
                    hasFetched: false,
                    byId: {},
                    allIds: []
                }
            };

            expect(appReducer(undefined, action).syllabus).toEqual(initialState);
        });

        test('returns an updated syllabus', () => {
            const action = {
                type: actionTypes.UPDATE_SYLLABUS,
                content: {title: 'Foo', slug: 'foo'}
            };
            const modifiedState = {
                title: 'Foo',
                slug: 'foo',
                labs: {
                    hasFetched: false,
                    byId: {},
                    allIds: []
                },
                modules: {
                    hasFetched: false,
                    byId: {},
                    allIds: []
                },
                courses: {
                    hasFetched: false,
                    byId: {},
                    allIds: []
                },
                topics: {
                    hasFetched: false,
                    byId: {},
                    allIds: []
                }
            };

            expect(appReducer(undefined, action).syllabus).toEqual(modifiedState);
        });

        test('returns an updated syllabus with normalised labs content', () => {
            const action = {
                type: actionTypes.UPDATE_SYLLABUS_CONTENT,
                key: 'labs',
                content: [{id: 1, title: 'Foo'}]
            };
            const modifiedState = {
                labs: {
                    hasFetched: false,
                    byId: {
                        1: {
                            title: 'Foo',
                            id: 1
                        }
                    },
                    allIds: [1]
                },
                modules: {
                    hasFetched: false,
                    byId: {},
                    allIds: []
                },
                courses: {
                    hasFetched: false,
                    byId: {},
                    allIds: []
                },
                topics: {
                    hasFetched: false,
                    byId: {},
                    allIds: []
                }
            };

            expect(appReducer(undefined, action).syllabus).toEqual(modifiedState);
        });

        test('returns an updated syllabus with normalised courses content', () => {
            const action = {
                type: actionTypes.UPDATE_SYLLABUS_CONTENT,
                key: 'courses',
                content: [{title: 'Foo', id: 1}]
            };
            const modifiedState = {
                labs: {
                    hasFetched: false,
                    byId: {},
                    allIds: []
                },
                modules: {
                    hasFetched: false,
                    byId: {},
                    allIds: []
                },
                courses: {
                    hasFetched: false,
                    byId: {
                        1: {
                            title: 'Foo',
                            id: 1
                        }
                    },
                    allIds: [1]
                },
                topics: {
                    hasFetched: false,
                    byId: {},
                    allIds: []
                }
            };

            expect(appReducer(undefined, action).syllabus).toEqual(modifiedState);
        });

        test('returns an updated syllabus with normalised modules content', () => {
            const action = {
                type: actionTypes.UPDATE_SYLLABUS_CONTENT,
                key: 'modules',
                content: [{title: 'Foo', id: 1}]
            };
            const modifiedState = {
                labs: {
                    hasFetched: false,
                    byId: {},
                    allIds: []
                },
                modules: {
                    hasFetched: false,
                    byId: {
                        1: {
                            title: 'Foo',
                            id: 1
                        }
                    },
                    allIds: [1]
                },
                courses: {
                    hasFetched: false,
                    byId: {},
                    allIds: []
                },
                topics: {
                    hasFetched: false,
                    byId: {},
                    allIds: []
                }
            };

            expect(appReducer(undefined, action).syllabus).toEqual(modifiedState);
        });

        test('returns an updated syllabus with normalised topics content', () => {
            const action = {
                type: actionTypes.UPDATE_SYLLABUS_CONTENT,
                key: 'topics',
                content: [{title: 'Foo', id: 1}]
            };
            const modifiedState = {
                labs: {
                    hasFetched: false,
                    byId: {},
                    allIds: []
                },
                modules: {
                    hasFetched: false,
                    byId: {},
                    allIds: []
                },
                courses: {
                    hasFetched: false,
                    byId: {},
                    allIds: []
                },
                topics: {
                    hasFetched: false,
                    byId: {
                        1: {
                            title: 'Foo',
                            id: 1
                        }
                    },
                    allIds: [1]
                }
            };

            expect(appReducer(undefined, action).syllabus).toEqual(modifiedState);
        });

        test('returns an updated syllabus with merged normalised  content', () => {
            const action = {
                type: actionTypes.UPDATE_SYLLABUS_CONTENT,
                key: 'topics',
                content: [{title: 'Foo', id: 1}]
            };
            const initialState = {
                labs: {
                    hasFetched: false,
                    byId: {},
                    allIds: []
                },
                modules: {
                    hasFetched: false,
                    byId: {},
                    allIds: []
                },
                courses: {
                    hasFetched: false,
                    byId: {},
                    allIds: []
                },
                topics: {
                    hasFetched: false,
                    byId: {
                        1: {
                            title: 'Foo',
                            id: 1
                        }
                    },
                    allIds: [1]
                }
            };

            const modifiedState = {
                labs: {
                    hasFetched: false,
                    byId: {},
                    allIds: []
                },
                modules: {
                    hasFetched: false,
                    byId: {},
                    allIds: []
                },
                courses: {
                    hasFetched: false,
                    byId: {},
                    allIds: []
                },
                topics: {
                    hasFetched: false,
                    byId: {
                        1: {
                            title: 'Foo',
                            id: 1,
                            id: 1
                        }
                    },
                    allIds: [1]
                }
            };

            expect(appReducer(undefined, action).syllabus).toEqual(modifiedState);
        });
    });

    describe('Resources', () => {
        beforeEach(() => {
            store.clearActions();
        });

        test('returns a default resource list', () => {
            const action = {type: ''};
            const initialState = {
                allIds: [],
                byId: {},
                type: 'All',
                hasFetched: false
            };

            expect(appReducer(undefined, action).resources).toEqual(initialState);
        });

        test('returns an updated resource list with normalised content', () => {
            const action = {
                type: actionTypes.UPDATE_RESOURCES,
                items: [{id: 1, title: 'Foo'}]
            };
            const modifiedState = {
                byId: {
                    1: {
                        title: 'Foo',
                        id: 1
                    }
                },
                allIds: [1],
                type: 'All',
                hasFetched: true
            };

            expect(appReducer(undefined, action).resources).toEqual(modifiedState);
        });

        test('returns an updated resource list with a new type', () => {
            const action = {
                type: actionTypes.UPDATE_RESOURCES_TYPE,
                resourceType: 'bar'
            };
            const modifiedState = {
                byId: {},
                allIds: [],
                type: 'bar',
                hasFetched: false
            };

            expect(appReducer(undefined, action).resources).toEqual(modifiedState);
        });
    });

    describe('Notifications', () => {
        beforeEach(() => {
            store.clearActions();
        });

        test('returns a default notification list', () => {
            const action = {type: ''};
            const initialState = {
                allIds: {
                    read: [],
                    unread: []
                },
                byId: {},
                hasFetched: false
            };

            expect(appReducer(undefined, action).notifications).toEqual(initialState);
        });

        test('returns an updated notification list with normalised content', () => {
            const action = {
                type: actionTypes.UPDATE_NOTIFICATIONS,
                items: [{id: 1, title: 'Foo'}]
            };
            const modifiedState = {
                byId: {
                    1: {
                        title: 'Foo',
                        id: 1
                    }
                },
                allIds: {
                    unread: [1],
                    read: []
                },
                hasFetched: true
            };

            expect(appReducer(undefined, action).notifications).toEqual(modifiedState);
        });
    });

    describe('Teams', () => {
        beforeEach(() => {
            store.clearActions();
        });

        test('returns a default team list', () => {
            const action = {type: ''};
            const initialState = {
                allIds: [],
                byId: {},
                filter: {name: '', country: ''},
                hasFetched: false
            };

            expect(appReducer(undefined, action).teams).toEqual(initialState);
        });

        test('returns an updated team list with normalised content', () => {
            const action = {
                type: actionTypes.UPDATE_TEAMS,
                items: [{id: 1, title: 'Foo'}]
            };
            const modifiedState = {
                byId: {
                    1: {
                        title: 'Foo',
                        id: 1
                    }
                },
                allIds: [1],
                filter: {name: '', country: ''},
                hasFetched: true
            };

            expect(appReducer(undefined, action).teams).toEqual(modifiedState);
        });

        test('returns an updated team list with a new filter', () => {
            const action = {
                type: actionTypes.UPDATE_TEAMS_FILTER,
                filter: {name: 'foo'}
            };
            const modifiedState = {
                byId: {},
                allIds: [],
                filter: {name: 'foo', country: ''},
                hasFetched: false
            };

            expect(appReducer(undefined, action).teams).toEqual(modifiedState);
        });
    });

    describe('Pagination', () => {
        beforeEach(() => {
            store.clearActions();
        });

        test('returns a default pagination', () => {
            const action = {type: ''};
            const initialState = {
                activePage: 1,
                currentItems: 0,
                itemsCountPerPage: 50,
                fetchedPages: {},
                onChange: null,
                pageRangeDisplayed: 20,
                skip: 0,
                totalItemsCount: 0,
                type: ''
            };

            expect(appReducer(undefined, action).pagination).toEqual(initialState);
        });

        test('returns updated pagination', () => {
            const action = {
                type: actionTypes.UPDATE_PAGINATION,
                pagination: {
                    activePage: 2,
                    currentItems: 50,
                    skip: 50,
                    totalItemsCount: 100
                }
            };
            const modifiedState = {
                activePage: 2,
                currentItems: 50,
                itemsCountPerPage: 50,
                fetchedPages: {},
                onChange: null,
                pageRangeDisplayed: 20,
                skip: 50,
                totalItemsCount: 100,
                type: ''
            };

            expect(appReducer(undefined, action).pagination).toEqual(modifiedState);
        });

        test('returns updated pagination content ids', () => {
            const action = {
                type: actionTypes.UPDATE_PAGINATION,
                pagination: {fetchedPages: {1: [1, 2, 3]}}
            };
            const modifiedState = {
                activePage: 1,
                currentItems: 0,
                itemsCountPerPage: 50,
                fetchedPages: {1: [1, 2, 3]},
                onChange: null,
                pageRangeDisplayed: 20,
                skip: 0,
                totalItemsCount: 0,
                type: ''
            };

            expect(appReducer(undefined, action).pagination).toEqual(modifiedState);

            const action2 = {
                type: actionTypes.UPDATE_PAGINATION,
                pagination: {fetchedPages: {2: [4, 5, 6]}}
            };
            const modifiedState2 = {
                activePage: 1,
                currentItems: 0,
                itemsCountPerPage: 50,
                fetchedPages: {
                    1: [1, 2, 3],
                    2: [4, 5, 6]
                },
                onChange: null,
                pageRangeDisplayed: 20,
                skip: 0,
                totalItemsCount: 0,
                type: ''
            };

            expect(
                appReducer(
                    {
                        pagination: {
                            activePage: 1,
                            currentItems: 0,
                            itemsCountPerPage: 50,
                            fetchedPages: {
                                1: [1, 2, 3]
                            },
                            onChange: null,
                            pageRangeDisplayed: 20,
                            skip: 0,
                            totalItemsCount: 0,
                            type: ''
                        }
                    },
                    action2
                ).pagination
            ).toEqual(modifiedState2);
        });
    });

    describe('Mentors', () => {
        beforeEach(() => {
            store.clearActions();
        });

        test('returns a default mentor list', () => {
            const action = {type: ''};
            const initialState = {
                allIds: [],
                byId: {},
                hasFetched: false
            };

            expect(appReducer(undefined, action).mentors).toEqual(initialState);
        });

        test('returns an updated mentor list with normalised content', () => {
            const action = {
                type: actionTypes.UPDATE_MENTORS,
                items: [{id: 1, title: 'Foo'}]
            };
            const modifiedState = {
                byId: {
                    1: {
                        title: 'Foo',
                        id: 1
                    }
                },
                allIds: [1],
                hasFetched: true
            };

            expect(appReducer(undefined, action).mentors).toEqual(modifiedState);
        });
    });
});
