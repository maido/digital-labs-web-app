import configureStore from 'redux-mock-store';
import * as actions from '../../store/actions';
import {actions as actionsTypes} from '../../store/types';

const mockStore = configureStore();
const store = mockStore();

describe('Store: Actions', () => {
    describe('Form', () => {
        beforeEach(() => {
            store.clearActions();
        });

        test('updates the form', () => {
            const expected = [
                {
                    form: {email: 'foo@foo.com'},
                    type: actionsTypes.UPDATE_FORM
                }
            ];

            store.dispatch(actions.updateForm({email: 'foo@foo.com'}));
            expect(store.getActions()).toEqual(expected);
            expect(store.getActions()).toMatchSnapshot();
        });
    });

    describe('Network', () => {
        beforeEach(() => {
            store.clearActions();
        });

        test('updates the network', () => {
            const expected = [
                {
                    network: {isLoading: true},
                    type: actionsTypes.UPDATE_NETWORK
                }
            ];

            store.dispatch(actions.updateNetwork({isLoading: true}));
            expect(store.getActions()).toEqual(expected);
            expect(store.getActions()).toMatchSnapshot();
        });
    });

    describe('Syllabus', () => {
        beforeEach(() => {
            store.clearActions();
        });

        test('updates a syllabus', () => {
            const expected = [
                {
                    content: {title: 'Foo'},
                    type: actionsTypes.UPDATE_SYLLABUS
                }
            ];

            store.dispatch(actions.updateSyllabus({title: 'Foo'}));
            expect(store.getActions()).toEqual(expected);
            expect(store.getActions()).toMatchSnapshot();
        });

        test('updates syllabus content modules', () => {
            const expected = [
                {
                    content: {title: 'Foo', id: 1},
                    key: 'labs',
                    type: actionsTypes.UPDATE_SYLLABUS_CONTENT
                }
            ];

            store.dispatch(actions.updateSyllabusContent('labs', {title: 'Foo', id: 1}));
            expect(store.getActions()).toEqual(expected);
            expect(store.getActions()).toMatchSnapshot();
        });
    });

    describe('Resources', () => {
        beforeEach(() => {
            store.clearActions();
        });

        test('updates resources', () => {
            const expected = [
                {
                    items: {title: 'Foo'},
                    type: actionsTypes.UPDATE_RESOURCES
                }
            ];

            store.dispatch(actions.updateResources({title: 'Foo'}));
            expect(store.getActions()).toEqual(expected);
            expect(store.getActions()).toMatchSnapshot();
        });

        test('updates resources type', () => {
            const expected = [
                {
                    resourceType: 'foo',
                    type: actionsTypes.UPDATE_RESOURCES_TYPE
                }
            ];

            store.dispatch(actions.updateResourcesType('foo'));
            expect(store.getActions()).toEqual(expected);
            expect(store.getActions()).toMatchSnapshot();
        });
    });

    describe('Notifications', () => {
        beforeEach(() => {
            store.clearActions();
        });

        test('updates notifications', () => {
            const expected = [
                {
                    items: {title: 'Foo', id: 1, read_at: null},
                    type: actionsTypes.UPDATE_NOTIFICATIONS
                }
            ];

            store.dispatch(actions.updateNotifications({title: 'Foo', id: 1, read_at: null}));
            expect(store.getActions()).toEqual(expected);
            expect(store.getActions()).toMatchSnapshot();
        });
    });

    describe('Teams', () => {
        beforeEach(() => {
            store.clearActions();
        });

        test('updates teams', () => {
            const expected = [
                {
                    items: {title: 'Foo'},
                    type: actionsTypes.UPDATE_TEAMS
                }
            ];

            store.dispatch(actions.updateTeams({title: 'Foo'}));
            expect(store.getActions()).toEqual(expected);
            expect(store.getActions()).toMatchSnapshot();
        });

        test('updates teams filter', () => {
            const expected = [
                {
                    filter: {title: 'foo'},
                    type: actionsTypes.UPDATE_TEAMS_FILTER
                }
            ];

            store.dispatch(actions.updateTeamsFilter({title: 'foo'}));
            expect(store.getActions()).toEqual(expected);
            expect(store.getActions()).toMatchSnapshot();
        });
    });

    describe('Pagination', () => {
        beforeEach(() => {
            store.clearActions();
        });

        test('updates pagination', () => {
            const expected = [
                {
                    pagination: {contentIds: [1, 2, 3], page: 2},
                    type: actionsTypes.UPDATE_PAGINATION
                }
            ];

            store.dispatch(actions.updatePagination({contentIds: [1, 2, 3], page: 2}));
            expect(store.getActions()).toEqual(expected);
            expect(store.getActions()).toMatchSnapshot();
        });
    });

    describe('Mentors', () => {
        beforeEach(() => {
            store.clearActions();
        });

        test('updates mentors', () => {
            const expected = [
                {
                    items: {title: 'Foo'},
                    type: actionsTypes.UPDATE_MENTORS
                }
            ];

            store.dispatch(actions.updateMentors({title: 'Foo'}));
            expect(store.getActions()).toEqual(expected);
            expect(store.getActions()).toMatchSnapshot();
        });
    });
});
