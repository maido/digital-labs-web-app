import {advanceTo} from 'jest-date-mock';
import {getPitchActions} from '../../globals/functions';

describe('Globals: Functions', () => {
    describe('getPitchActions()', () => {
        test('returns correct details if today is before the pitch deadline or countdown', async () => {
            advanceTo(new Date('2020/01/07 21:12:00'));

            const pitchActions = await getPitchActions('2020/01/24');
            const expected = {
                canSubmit: true,
                daysRemaining: 17,
                deadline: '2020/01/24'
            };

            expect(pitchActions).toEqual(expected);
        });

        test('returns correct details if today is the day before the pitch deadline or countdown', async () => {
            advanceTo(new Date('2020/01/23 21:12:00'));

            const pitchActions = await getPitchActions('2020/01/24');
            const expected = {
                canSubmit: true,
                daysRemaining: 1,
                deadline: '2020/01/24'
            };

            expect(pitchActions).toEqual(expected);
        });

        test('returns correct details if today is after the pitch deadline or countdown', async () => {
            advanceTo(new Date('2020/01/25 00:00:00'));

            const pitchActions = await getPitchActions('2020/01/24');
            const expected = {canSubmit: true, daysRemaining: 0};

            expect(pitchActions).toEqual(expected);
        });

        test('returns correct details if today is during the pitch deadline countdown', async () => {
            advanceTo(new Date('2020/01/24 21:12:00'));

            const pitchActions = await getPitchActions('2020/01/24');
            const expected = {
                canSubmit: true,
                deadline: '2020/01/24',
                daysRemaining: 0
            };

            expect(pitchActions).toEqual(expected);
        });

        test('returns correct details if today is on the pitch deadline', async () => {
            advanceTo(new Date('2020/01/24 21:12:00'));

            const pitchActions = await getPitchActions('2020/01/24');
            const expected = {
                canSubmit: true,
                deadline: '2020/01/24',
                daysRemaining: 0
            };

            expect(pitchActions).toEqual(expected);
        });
    });
});
