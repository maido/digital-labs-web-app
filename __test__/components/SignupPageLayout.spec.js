import React from 'react';
import {render, fireEvent, wait, findAllByTestId} from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import {Provider} from 'react-redux';
import mockAxios from 'axios';
import mockCookies from 'js-cookie';
import faker from 'faker';
import dayjs from 'dayjs';
import {RouterContext} from 'next/dist/next-server/lib/router-context';
import selectEvent from 'react-select-event';
import {initializeStore} from '../../store/';
import {areasOfInterest} from '../../globals/content';
import {getSchemaDefaultFieldValues} from '../../globals/functions';
import accountCreationSchema from '../../schemas/create-account';
import SignupPageLayout from '../../components/SignupPageLayout';

describe('Component: SignupPageLayout', () => {
    const fields = getSchemaDefaultFieldValues({
        ...accountCreationSchema.step1.fields,
        ...accountCreationSchema.step2.fields
    });
    const invite = {
        email: 'foo@bar.com',
        invited_by: {
            first_name: 'Foo',
            last_name: 'Bar'
        },
        team: {
            members: [],
            name: 'Bar Baz'
        }
    };

    test('renders step 1 as expected', async () => {
        const {asFragment} = render(
            <Provider
                store={initializeStore({
                    form: {
                        fields,
                        name: 'signup',
                        state: 'default',
                        step: 1
                    }
                })}
            >
                <SignupPageLayout />
            </Provider>
        );
        expect(asFragment()).toMatchSnapshot();
    });

    test('renders an error for incorrect or missing fields on step 1 submit', async () => {
        const {findByTestId, getByLabelText, getByRole} = render(
            <Provider
                store={initializeStore({
                    form: {
                        fields,
                        name: 'signup',
                        state: 'default',
                        step: 1
                    }
                })}
            >
                <SignupPageLayout />
            </Provider>
        );

        const $inputFirstName = getByLabelText(/first name/i);
        const $inputLastName = getByLabelText(/last name/i);
        const $inputEmail = getByLabelText(/email/i);
        const $inputDob = getByLabelText(/date/i);
        const $inputPassword = getByLabelText('Password');
        const $inputPasswordConfirm = getByLabelText(/confirm password/i);
        const $form = getByRole('form');

        fireEvent.change($inputFirstName, {target: {value: 'a'}});
        fireEvent.change($inputLastName, {target: {value: 'a'}});
        fireEvent.change($inputEmail, {target: {value: 'foo@'}});
        fireEvent.change($inputDob, {target: {value: '1000-01-01'}});
        fireEvent.change($inputPassword, {target: {value: 'a'}});
        fireEvent.change($inputPasswordConfirm, {target: {value: 'ab'}});
        fireEvent.submit($form);

        expect(await findByTestId('error-first_name')).toBeInTheDocument();
        expect(await findByTestId('error-last_name')).toBeInTheDocument();
        expect(await findByTestId('error-email')).toBeInTheDocument();
        expect(await findByTestId('error-birthdate')).toBeInTheDocument();
        expect(await findByTestId('error-password')).toBeInTheDocument();
        expect(await findByTestId('error-password_confirmation')).toBeInTheDocument();
    });

    test.skip('renders an error if an email is already in use', async () => {
        mockAxios.post.mockImplementationOnce(() =>
            Promise.reject({response: {data: {email: 'Already exists'}}})
        );

        const {findByTestId, getByLabelText, getByRole} = render(
            <Provider
                store={initializeStore({
                    form: {
                        fields,
                        name: 'signup',
                        state: 'default',
                        step: 1
                    }
                })}
            >
                <SignupPageLayout />
            </Provider>
        );

        const $inputFirstName = getByLabelText(/first name/i);
        const $inputLastName = getByLabelText(/last name/i);
        const $inputEmail = getByLabelText(/email/i);
        const $inputDob = getByLabelText(/date/i);
        const $inputPassword = getByLabelText('Password');
        const $inputPasswordConfirm = getByLabelText(/confirm password/i);
        const $form = getByRole('form');

        const date = dayjs(faker.date.between(new Date(1919, 1, 1), new Date(2019, 1, 1))).format(
            'YYYY-MM-DD'
        );
        const password = faker.internet.password();

        fireEvent.change($inputFirstName, {target: {value: faker.name.firstName()}});
        fireEvent.change($inputLastName, {target: {value: faker.name.lastName()}});
        fireEvent.change($inputEmail, {target: {value: faker.internet.email()}});
        fireEvent.change($inputDob, {target: {value: date}});
        fireEvent.change($inputPassword, {target: {value: password}});
        fireEvent.change($inputPasswordConfirm, {target: {value: password}});
        fireEvent.submit($form);

        await wait();

        expect(await findByTestId('error-email')).toBeInTheDocument();
    });

    test('renders step 2 as expected', async () => {
        const {asFragment} = render(
            <Provider
                store={initializeStore({
                    form: {
                        fields,
                        name: 'signup',
                        state: 'default',
                        step: 2
                    }
                })}
            >
                <SignupPageLayout />
            </Provider>
        );
        expect(asFragment()).toMatchSnapshot();
    });

    test('renders an error for incorrect or missing fields on step 2 submit', async () => {
        const {findByTestId, getByDisplayValue, getByLabelText, getByRole} = render(
            <Provider
                store={initializeStore({
                    form: {
                        fields,
                        name: 'signup',
                        state: 'default',
                        step: 2
                    }
                })}
            >
                <SignupPageLayout />
            </Provider>
        );

        const $inputOccupation = getByDisplayValue(/Founder/i);
        const $inputTerms = getByLabelText(/terms/i);
        const $form = getByRole('form');

        fireEvent.change($inputOccupation, {target: {value: 'foo@'}});
        fireEvent.change($inputTerms, {target: {value: 'a'}});
        fireEvent.submit($form);

        expect(await findByTestId('error-country_origin')).toBeInTheDocument();
        expect(await findByTestId('error-country_residence')).toBeInTheDocument();
        expect(await findByTestId('error-occupation')).toBeInTheDocument();
        expect(await findByTestId('error-areas_of_interest')).toBeInTheDocument();
        expect(await findByTestId('error-areas_of_learning')).toBeInTheDocument();
        expect(await findByTestId('error-heard_via')).toBeInTheDocument();
        expect(await findByTestId('error-terms')).toBeInTheDocument();
    });

    test('renders degree and graduations dates if Occupation is Student', async () => {
        const {getByLabelText, getByDisplayValue, queryByLabelText} = render(
            <Provider
                store={initializeStore({
                    form: {
                        fields,
                        name: 'signup',
                        state: 'default',
                        step: 2
                    }
                })}
            >
                <SignupPageLayout />
            </Provider>
        );

        expect(queryByLabelText(/degree date/i)).toBeNull();
        expect(queryByLabelText(/graduation date/i)).toBeNull();

        const $inputOccupation = getByDisplayValue(/student/i);

        fireEvent.click($inputOccupation);

        expect(getByLabelText(/degree date/i)).toBeInTheDocument();
        expect(getByLabelText(/graduation date/i)).toBeInTheDocument();
    });

    test('renders an error if Occupation is Student and required dates are missing', async () => {
        const {findByTestId, getByDisplayValue, getByRole} = render(
            <Provider
                store={initializeStore({
                    form: {
                        fields,
                        name: 'signup',
                        state: 'default',
                        step: 2
                    }
                })}
            >
                <SignupPageLayout />
            </Provider>
        );

        const $inputOccupation = getByDisplayValue(/student/i);
        const $form = getByRole('form');

        fireEvent.click($inputOccupation);
        fireEvent.submit($form);

        expect(await findByTestId('error-degree_date')).toBeInTheDocument();
        expect(await findByTestId('error-graduation_date')).toBeInTheDocument();
    });

    test('renders a checkbox to copy Country of Origin to Country of Residence once it has been selected', async () => {
        const {getByLabelText} = render(
            <Provider
                store={initializeStore({
                    form: {
                        fields,
                        name: 'signup',
                        state: 'default',
                        step: 2
                    }
                })}
            >
                <SignupPageLayout />
            </Provider>
        );

        const $inputCountryOfOrigin = getByLabelText(/country of origin/i);

        await selectEvent.select($inputCountryOfOrigin, 'United Kingdom');

        expect(getByLabelText(/same as country of origin/i)).toBeInTheDocument();
    });

    test('copies Country of Origin to Country of Residence if selected', async () => {
        const {getByLabelText, getByTestId} = render(
            <Provider
                store={initializeStore({
                    form: {
                        fields,
                        name: 'signup',
                        state: 'default',
                        step: 2
                    }
                })}
            >
                <SignupPageLayout />
            </Provider>
        );

        const $inputCountryOfOrigin = getByLabelText(/country of origin/i);

        await selectEvent.select($inputCountryOfOrigin, 'United Kingdom');

        const $inputCopyCountry = getByLabelText(/same as country of origin/i);

        fireEvent.click($inputCopyCountry);

        expect(getByTestId('formstep2')).toHaveFormValues({country_residence: 'United Kingdom'});
    });

    test('handles `date` input types', async () => {
        const {getByLabelText, getByRole, queryByTestId} = render(
            <Provider
                store={initializeStore({
                    form: {
                        fields,
                        name: 'signup',
                        state: 'default',
                        step: 1
                    }
                })}
            >
                <SignupPageLayout />
            </Provider>
        );

        const $inputDob = getByLabelText(/date/i);
        const $form = getByRole('form');
        const date = dayjs(faker.date.between(new Date(1919, 1, 1), new Date(2019, 1, 1))).format(
            'YYYY-MM-DD'
        );

        fireEvent.change($inputDob, {target: {value: date}});
        fireEvent.submit($form);

        expect(queryByTestId('error-dob')).toBeNull();
    });

    test("handles fallback manual input when `date` input types aren't supported", async () => {
        const {getByLabelText, getByRole, queryByTestId} = render(
            <Provider
                store={initializeStore({
                    form: {
                        fields,
                        name: 'signup',
                        state: 'default',
                        step: 1
                    }
                })}
            >
                <SignupPageLayout />
            </Provider>
        );

        const $inputDob = getByLabelText(/date/i);
        const $form = getByRole('form');
        const date = dayjs(faker.date.between(new Date(1919, 1, 1), new Date(2019, 1, 1))).format(
            'DD-MM-YYYY'
        );

        fireEvent.change($inputDob, {target: {value: date}});
        fireEvent.submit($form);

        expect(queryByTestId('error-dob')).toBeNull();
    });

    test('renders a related team if signup is via an invitation', async () => {
        const {getByText} = render(
            <Provider
                store={initializeStore({
                    form: {
                        fields,
                        name: 'signup',
                        state: 'default',
                        step: 1
                    }
                })}
            >
                <SignupPageLayout invite={invite} />
            </Provider>
        );

        expect(getByText('Sign up to join Foo', {exact: false})).toBeInTheDocument();
        expect(getByText('Bar Baz', {exact: false})).toBeInTheDocument();
    });

    test('saves the invite token in the form if signup is via an invitation', async () => {
        const {findByTestId} = render(
            <Provider
                store={initializeStore({
                    form: {
                        fields: {
                            ...fields,
                            invite_token: 'abc123'
                        },
                        name: 'signup',
                        state: 'default',
                        step: 1
                    }
                })}
            >
                <SignupPageLayout invite={invite} />
            </Provider>
        );

        expect(await findByTestId('invitetoken')).toHaveValue('abc123');
    });

    test('disables email input if signup is via an invitation', async () => {
        const {findByTestId} = render(
            <Provider
                store={initializeStore({
                    form: {
                        fields: {
                            ...fields,
                            invite_token: 'abc123'
                        },
                        name: 'signup',
                        state: 'default',
                        step: 1
                    }
                })}
            >
                <SignupPageLayout invite={invite} />
            </Provider>
        );

        expect(document.querySelector('input[name="email"]')).toBeDisabled();
    });
});
