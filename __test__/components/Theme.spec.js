import React from 'react';
import {render} from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Theme from '../../components/Theme';

describe('Component: Theme', () => {
    test('renders as expected', async () => {
        const {asFragment} = render(<Theme theme="primary" />);
        expect(asFragment()).toMatchSnapshot();
    });

    test('renders a themed wrapper element with child components', async () => {
        const {getByText} = render(
            <Theme theme="primary">
                <span>foo</span>
            </Theme>
        );

        expect(getByText('foo')).toBeInTheDocument();
    });
});
