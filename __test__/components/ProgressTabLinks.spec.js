import React from 'react';
import {render} from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import ProgressTabLinks from '../../components/ProgressTabLinks';

describe('Component: ProgressTabLinks', () => {
    test('renders as expected', async () => {
        const {asFragment} = render(<ProgressTabLinks />);
        expect(asFragment()).toMatchSnapshot();
    });

    test('renders a button for each step', () => {
        const {getByText} = render(
            <ProgressTabLinks
                steps={[
                    {label: 'Step 1', to: '/team/create/1'},
                    {label: 'Step 2', to: '/team/create/2'},
                    {label: 'Step 3', to: '/team/create/3'}
                ]}
            />
        );

        expect(getByText('Step 1')).toBeInTheDocument();
        expect(getByText('Step 2')).toBeInTheDocument();
        expect(getByText('Step 3')).toBeInTheDocument();
    });

    test('renders a `completed` state for completed steps (before current step)', () => {
        const {container} = render(
            <ProgressTabLinks
                currentStep={2}
                steps={[
                    {label: 'Step 1', to: '/team/create/1'},
                    {label: 'Step 2', to: '/team/create/2'},
                    {label: 'Step 3', to: '/team/create/3'}
                ]}
            />
        );
        const $els = container.querySelectorAll('[data-complete="true"]');

        expect($els.length).toBe(1);
        expect($els[0]).toHaveTextContent('Step 1');
    });

    test('renders a `completed` state for completed steps even after moving to previous steps', () => {
        const {container} = render(
            <ProgressTabLinks
                currentStep={2}
                steps={[
                    {label: 'Step 1', to: '/team/create/1'},
                    {label: 'Step 2', to: '/team/create/2'},
                    {label: 'Step 3', to: '/team/create/3'}
                ]}
            />
        );
        const $els = container.querySelectorAll('[data-complete="true"]');

        expect($els.length).toBe(1);
        expect($els[0]).toHaveTextContent('Step 1');
    });

    test('renders an `active` state for current step (current step)', () => {
        const {container} = render(
            <ProgressTabLinks
                currentStep={2}
                steps={[
                    {label: 'Step 1', to: '/team/create/1'},
                    {label: 'Step 2', to: '/team/create/2'},
                    {label: 'Step 3', to: '/team/create/3'}
                ]}
            />
        );
        const $els = container.querySelectorAll('[aria-current="step"]');

        expect($els.length).toBe(1);
        expect($els[0]).toHaveTextContent('Step 2');
    });

    test('renders a `disabled` state for future steps (after curren step)', () => {
        const {container} = render(
            <ProgressTabLinks
                currentStep={1}
                steps={[
                    {label: 'Step 1', to: '/team/create/1'},
                    {label: 'Step 2', to: '/team/create/2'},
                    {label: 'Step 3', to: '/team/create/3'}
                ]}
            />
        );
        const $els = container.querySelectorAll('[disabled]');

        expect($els.length).toBe(2);
    });
});
