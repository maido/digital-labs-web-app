import React from 'react';
import {fireEvent, render} from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import {advanceTo} from 'jest-date-mock';
import __ from '../../globals/strings';
import NotificationsPageLayout from '../../components/NotificationsPageLayout';

describe('Component: NotificationsPageLayout', () => {
    test('renders as expected', () => {
        const {asFragment} = render(
            <NotificationsPageLayout
                network={{isLoading: false}}
                notifications={{hasFetched: false, allIds: {read: [], unread: []}, byId: {}}}
            />
        );

        expect(asFragment()).toMatchSnapshot();
    });

    test('renders placeholders if network is loading and there has been no fetches', () => {
        const {getAllByTestId} = render(
            <NotificationsPageLayout
                network={{isLoading: true}}
                notifications={{hasFetched: false, allIds: {read: [], unread: []}, byId: {}}}
            />
        );

        expect(getAllByTestId('placeholdertextcard').length).toBe(4);
    });

    test("doesn't render placeholders if network is loading and there has been fetches", () => {
        const {queryAllByTestId} = render(
            <NotificationsPageLayout
                network={{isLoading: true}}
                notifications={{hasFetched: true, allIds: {read: [], unread: []}, byId: {}}}
            />
        );

        expect(queryAllByTestId('placeholdertextcard').length).toBe(0);
    });

    test("doesn't render feedback if there has been no fetches", () => {
        const {queryByText} = render(
            <NotificationsPageLayout
                network={{isLoading: true}}
                notifications={{hasFetched: false, allIds: {read: [], unread: []}, byId: {}}}
            />
        );

        expect(queryByText(__('notifications.emptyUnread'))).not.toBeInTheDocument();
    });

    test('renders feedback if there has been fetches and there is no unread notifications', () => {
        const {getByText} = render(
            <NotificationsPageLayout
                network={{isLoading: false}}
                notifications={{hasFetched: true, allIds: {read: [], unread: []}, byId: {}}}
            />
        );

        expect(getByText(__('notifications.emptyUnread'))).toBeInTheDocument();
    });

    test('renders feedback if there has been fetches and there is no read notifications', () => {
        const {getByText} = render(
            <NotificationsPageLayout
                network={{isLoading: false}}
                notifications={{hasFetched: true, allIds: {read: [], unread: []}, byId: {}}}
            />
        );

        expect(getByText(__('notifications.emptyRead'))).toBeInTheDocument();
    });

    test('renders a card for each unread notification', () => {
        const {getAllByTestId} = render(
            <NotificationsPageLayout
                network={{isLoading: false}}
                notifications={{
                    hasFetched: true,
                    allIds: {read: [], unread: [1, 2]},
                    byId: {
                        1: {id: 1, title: 'Title text'},
                        2: {id: 2, title: 'Title text'}
                    }
                }}
            />
        );

        expect(getAllByTestId('textcard').length).toBe(2);
    });

    test('renders a card for each read notification', () => {
        const {getAllByTestId} = render(
            <NotificationsPageLayout
                network={{isLoading: false}}
                notifications={{
                    hasFetched: true,
                    allIds: {read: [1, 2], unread: []},
                    byId: {
                        1: {id: 1, title: 'Title text'},
                        2: {id: 2, title: 'Title text'}
                    }
                }}
            />
        );

        expect(getAllByTestId('textcard').length).toBe(2);
    });

    test('renders the correct content for a notification', () => {
        const {getByText} = render(
            <NotificationsPageLayout
                network={{isLoading: false}}
                notifications={{
                    hasFetched: true,
                    allIds: {read: [1], unread: []},
                    byId: {1: {id: 1, title: 'Title text', content: 'Content text'}}
                }}
            />
        );

        expect(getByText('Title text')).toBeInTheDocument();
        expect(getByText('Content text')).toBeInTheDocument();
    });

    test('renders the correct relative timestamp for a notification (within minute)', () => {
        advanceTo(new Date('2019/12/21 21:12:00'));

        const {getByText} = render(
            <NotificationsPageLayout
                network={{isLoading: false}}
                notifications={{
                    hasFetched: true,
                    allIds: {read: [1], unread: []},
                    byId: {
                        1: {id: 1, title: 'Title text', created_at: '2019-12-21T21:11:00.000000Z'}
                    }
                }}
            />
        );

        expect(getByText('a minute ago', {exact: false}, {exact: false})).toBeInTheDocument();
    });

    test('renders the correct relative timestamp for a notification (within minutes)', () => {
        advanceTo(new Date('2019/12/21 21:12:00'));

        const {getByText} = render(
            <NotificationsPageLayout
                network={{isLoading: false}}
                notifications={{
                    hasFetched: true,
                    allIds: {read: [1], unread: []},
                    byId: {
                        1: {id: 1, title: 'Title text', created_at: '2019-12-21T21:10:00.000000Z'}
                    }
                }}
            />
        );

        expect(getByText('2 minutes ago', {exact: false})).toBeInTheDocument();
    });

    test('renders the correct relative timestamp for a notification (within a day)', () => {
        advanceTo(new Date('2019/12/22 21:12:00'));

        const {getByText} = render(
            <NotificationsPageLayout
                network={{isLoading: false}}
                notifications={{
                    hasFetched: true,
                    allIds: {read: [1], unread: []},
                    byId: {
                        1: {id: 1, title: 'Title text', created_at: '2019-12-21T21:10:00.000000Z'}
                    }
                }}
            />
        );

        expect(getByText('a day ago', {exact: false})).toBeInTheDocument();
    });

    test('renders the correct relative timestamp for a notification (within days)', () => {
        advanceTo(new Date('2019/12/26 21:12:00'));

        const {getByText} = render(
            <NotificationsPageLayout
                network={{isLoading: false}}
                notifications={{
                    hasFetched: true,
                    allIds: {read: [1], unread: []},
                    byId: {
                        1: {id: 1, title: 'Title text', created_at: '2019-12-21T21:10:00.000000Z'}
                    }
                }}
            />
        );

        expect(getByText('5 days ago', {exact: false})).toBeInTheDocument();
    });

    test('renders the correct relative timestamp for a notification (within a month)', () => {
        advanceTo(new Date('2020/01/26 21:12:00'));

        const {getByText} = render(
            <NotificationsPageLayout
                network={{isLoading: false}}
                notifications={{
                    hasFetched: true,
                    allIds: {read: [1], unread: []},
                    byId: {
                        1: {id: 1, title: 'Title text', created_at: '2019-12-21T21:10:00.000000Z'}
                    }
                }}
            />
        );

        expect(getByText('a month ago', {exact: false})).toBeInTheDocument();
    });

    test('renders the correct relative timestamp for a notification (within months)', () => {
        advanceTo(new Date('2020/04/26 21:12:00'));

        const {getByText} = render(
            <NotificationsPageLayout
                network={{isLoading: false}}
                notifications={{
                    hasFetched: true,
                    allIds: {read: [1], unread: []},
                    byId: {
                        1: {id: 1, title: 'Title text', created_at: '2019-12-21T21:10:00.000000Z'}
                    }
                }}
            />
        );

        expect(getByText('4 months ago', {exact: false})).toBeInTheDocument();
    });

    test('renders the correct relative timestamp for a notification (within a year)', () => {
        advanceTo(new Date('2021/01/26 21:12:00'));

        const {getByText} = render(
            <NotificationsPageLayout
                network={{isLoading: false}}
                notifications={{
                    hasFetched: true,
                    allIds: {read: [1], unread: []},
                    byId: {
                        1: {id: 1, title: 'Title text', created_at: '2019-12-21T21:10:00.000000Z'}
                    }
                }}
            />
        );

        expect(getByText('a year ago', {exact: false})).toBeInTheDocument();
    });

    test('renders the correct relative timestamp for a notification (within years)', () => {
        advanceTo(new Date('2029/12/21 21:12:00'));

        const {getByText} = render(
            <NotificationsPageLayout
                network={{isLoading: false}}
                notifications={{
                    hasFetched: true,
                    allIds: {read: [1], unread: []},
                    byId: {
                        1: {id: 1, title: 'Title text', created_at: '2019-12-21T21:10:00.000000Z'}
                    }
                }}
            />
        );

        expect(getByText('10 years ago', {exact: false})).toBeInTheDocument();
    });

    test('renders the correct link for a Lab', () => {
        const {debug, getByTestId} = render(
            <NotificationsPageLayout
                network={{isLoading: false}}
                notifications={{
                    hasFetched: true,
                    allIds: {read: [1], unread: []},
                    byId: {
                        1: {id: 1, model_type: 'lab', model_id: 1}
                    }
                }}
            />
        );

        expect(getByTestId('textcard')).toHaveAttribute('href', expect.stringContaining('/labs/1'));
    });

    test('renders the correct link for a Module', () => {
        const {getByTestId} = render(
            <NotificationsPageLayout
                network={{isLoading: false}}
                notifications={{
                    hasFetched: true,
                    allIds: {read: [1], unread: []},
                    byId: {
                        1: {id: 1, model_type: 'module', model_id: 1}
                    }
                }}
            />
        );

        expect(getByTestId('textcard')).toHaveAttribute(
            'href',
            expect.stringContaining('/labs/modules/1')
        );
    });

    test('renders the correct link for a Courses', () => {
        const {getByTestId} = render(
            <NotificationsPageLayout
                network={{isLoading: false}}
                notifications={{
                    hasFetched: true,
                    allIds: {read: [1], unread: []},
                    byId: {
                        1: {id: 1, model_type: 'course', model_id: 1}
                    }
                }}
            />
        );

        expect(getByTestId('textcard')).toHaveAttribute(
            'href',
            expect.stringContaining('/labs/courses/1')
        );
    });

    test('renders the correct link for a Topic', () => {
        const {getByTestId} = render(
            <NotificationsPageLayout
                network={{isLoading: false}}
                notifications={{
                    hasFetched: true,
                    allIds: {read: [1], unread: []},
                    byId: {
                        1: {id: 1, model_type: 'topic', model_id: 1}
                    }
                }}
            />
        );

        expect(getByTestId('textcard')).toHaveAttribute(
            'href',
            expect.stringContaining('/labs/topics/1')
        );
    });

    test('renders the correct link for a Resource', () => {
        const {getByTestId} = render(
            <NotificationsPageLayout
                network={{isLoading: false}}
                notifications={{
                    hasFetched: true,
                    allIds: {read: [1], unread: []},
                    byId: {
                        1: {id: 1, model_type: 'resource', model_id: 1}
                    }
                }}
            />
        );

        expect(getByTestId('textcard')).toHaveAttribute(
            'href',
            expect.stringContaining('/resources/1')
        );
    });

    test('renders the correct link for a Team', () => {
        const {getByTestId} = render(
            <NotificationsPageLayout
                network={{isLoading: false}}
                notifications={{
                    hasFetched: true,
                    allIds: {read: [1], unread: []},
                    byId: {
                        1: {id: 1, model_type: 'team', model_id: 1}
                    }
                }}
            />
        );

        expect(getByTestId('textcard')).toHaveAttribute(
            'href',
            expect.stringContaining('/teams/1')
        );
    });

    test("doesn't render a notification once has been read", () => {
        const {queryByText} = render(
            <NotificationsPageLayout
                network={{isLoading: false}}
                notifications={{
                    hasFetched: true,
                    allIds: {read: [1], unread: [1]},
                    byId: {
                        1: {id: 1, title: 'Title text', model_type: 'team', model_id: 1}
                    }
                }}
            />
        );

        expect(
            queryByText('Title text', {selector: '[data-testid="readnotifications"] *'})
        ).toBeInTheDocument();
        expect(
            queryByText('Title text', {selector: '[data-testid="unreadnotifications"] *'})
        ).toBeNull();
    });

    test('renders notifications in the correct time order', () => {
        /**
         * Notifications are stored in order they've been fetched, but this isn't necesssarily time-ordered.
         * When we display an unread/read list it should be time-ordered, latest first.
         */
        const {queryAllByTestId} = render(
            <NotificationsPageLayout
                network={{isLoading: false}}
                notifications={{
                    hasFetched: true,
                    allIds: {read: [], unread: [1, 2, 3]},
                    byId: {
                        1: {
                            id: 1,
                            title: 'Title text 1',
                            model_type: 'team',
                            model_id: 1,
                            created_at: new Date('2019/10/01 02:00:00')
                        },
                        2: {
                            id: 2,
                            title: 'Title text 2',
                            model_type: 'team',
                            model_id: 1,
                            created_at: new Date('2019/10/01 01:00:00')
                        },
                        3: {
                            id: 3,
                            title: 'Title text 3',
                            model_type: 'team',
                            model_id: 1,
                            created_at: new Date('2019/10/01 03:00:00')
                        }
                    }
                }}
            />
        );

        expect(queryAllByTestId('textcardtitle')[0]).toHaveTextContent('Title text 3');
        expect(queryAllByTestId('textcardtitle')[1]).toHaveTextContent('Title text 1');
        expect(queryAllByTestId('textcardtitle')[2]).toHaveTextContent('Title text 2');
    });
});
