import React from 'react';
import {render} from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import {Provider} from 'react-redux';
import faker from 'faker';
import {RouterContext} from 'next/dist/next-server/lib/router-context';
import {network, notifications, initializeStore} from '../../store';
import SiteHeader from '../../components/SiteHeader';

describe('Component: SiteHeader', () => {
    const loggedInUserState = {
        avatar: null,
        first_name: faker.name.firstName(),
        isAuthenticated: true,
        pitch: {canSubmit: true},
        team: {
            id: 1,
            name: faker.internet.userName()
        }
    };

    test('renders as expected when a user is logged out', async () => {
        const router = {
            pathname: '/dashboard',
            route: '/dashboard',
            replace: jest.fn().mockImplementationOnce(),
            query: {}
        };
        const {asFragment} = render(
            <Provider store={initializeStore()}>
                <RouterContext.Provider value={router}>
                    <SiteHeader />
                </RouterContext.Provider>
            </Provider>
        );
        expect(asFragment()).toMatchSnapshot();
    });

    test('renders as expected when a user is logged in', async () => {
        const router = {
            pathname: '/dashboard',
            route: '/dashboard',
            replace: jest.fn().mockImplementationOnce(),
            query: {}
        };
        const {asFragment} = render(
            <Provider store={initializeStore({network, notifications, user: loggedInUserState})}>
                <RouterContext.Provider value={router}>
                    <SiteHeader />
                </RouterContext.Provider>
            </Provider>
        );
        expect(asFragment()).toMatchSnapshot();
    });

    test("renders a link to pitch submission if the user can submit their team's pitch", async () => {
        const router = {
            pathname: '/dashboard',
            route: '/dashboard',
            replace: jest.fn().mockImplementationOnce(),
            query: {}
        };
        const {findByTestId} = render(
            <Provider store={initializeStore({network, notifications, user: loggedInUserState})}>
                <RouterContext.Provider value={router}>
                    <SiteHeader />
                </RouterContext.Provider>
            </Provider>
        );
        expect(await findByTestId('nav-pitch-link')).toBeInTheDocument();
    });
});
