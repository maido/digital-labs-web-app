import React from 'react';
import {render, fireEvent} from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import faker from 'faker';
import Accordion from '../../components/Accordion';

describe('Component: Accordion', () => {
    test('renders as expected', async () => {
        const {asFragment} = render(<Accordion />);
        expect(asFragment()).toMatchSnapshot();
    });

    test('renders a toggle with custom header content', () => {
        const header = faker.lorem.lines(1);
        const {getByText} = render(<Accordion header={header}></Accordion>);

        expect(getByText(header)).toBeInTheDocument();
    });

    test("it doesn't render child content by default", () => {
        const {queryByTestId} = render(
            <Accordion header="Foo bar baz">
                <span data-testid="child-text">Baz bar foo</span>
            </Accordion>
        );

        expect(queryByTestId('child-text')).toBeNull();
    });

    test("renders child content if it's toggled", async () => {
        const {queryByTestId} = render(
            <Accordion header="Foo bar baz">
                <span data-testid="child-text">Baz bar foo</span>
            </Accordion>
        );

        expect(queryByTestId('child-text')).toBeNull();
        fireEvent.click(queryByTestId('accordion'));
        expect(queryByTestId('child-text')).not.toBeNull();
    });

    test("renders child content if it's active by default", async () => {
        const {queryByTestId} = render(
            <Accordion header="Foo bar baz" isActive={true}>
                <span data-testid="child-text">Baz bar foo</span>
            </Accordion>
        );

        expect(queryByTestId('child-text')).not.toBeNull();
    });

    test("doesn't render child content if it's disabled", async () => {
        const {queryByTestId} = render(
            <Accordion header="Foo bar baz" isDisabled={true}>
                <span data-testid="child-text">Baz bar foo</span>
            </Accordion>
        );

        expect(queryByTestId('child-text')).toBeNull();
        fireEvent.click(queryByTestId('accordion'));
        expect(queryByTestId('child-text')).toBeNull();
    });
});
