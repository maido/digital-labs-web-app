import React from 'react';
import {render} from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import {RouterContext} from 'next/dist/next-server/lib/router-context';
import __ from '../../globals/strings';
import LearningContentListPageLayout from '../../components/LearningContentListPageLayout';

describe('Component: LearningContentListPageLayout', () => {
    const router = {
        pathname: '/labs',
        route: '/labs',
        replace: jest.fn().mockImplementationOnce(),
        query: {}
    };

    test('renders as expected', () => {
        const {asFragment} = render(
            <RouterContext.Provider value={router}>
                <LearningContentListPageLayout hasFetched={true} network={{isLoading: true}} />{' '}
            </RouterContext.Provider>
        );

        expect(asFragment()).toMatchSnapshot();
    });

    test('renders placeholders when content is loading', () => {
        const {getAllByTestId} = render(
            <RouterContext.Provider value={router}>
                <LearningContentListPageLayout
                    hasFetched={false}
                    network={{isLoading: true}}
                    items={[]}
                />{' '}
            </RouterContext.Provider>
        );

        expect(getAllByTestId('placeholderlearningcontent').length).toBe(6);
    });

    test('renders placeholders if network is loading and there has been no fetches', () => {
        const {getAllByTestId} = render(
            <RouterContext.Provider value={router}>
                <LearningContentListPageLayout
                    hasFetched={false}
                    network={{isLoading: true}}
                    items={[]}
                />{' '}
            </RouterContext.Provider>
        );

        expect(getAllByTestId('placeholderlearningcontent').length).toBe(6);
    });

    test("doesn't render feedback if there has been no fetches", () => {
        const {queryByText} = render(
            <RouterContext.Provider value={router}>
                <LearningContentListPageLayout
                    hasFetched={false}
                    network={{isLoading: true}}
                    items={[]}
                />{' '}
            </RouterContext.Provider>
        );

        expect(queryByText(__('learningContent.empty'))).not.toBeInTheDocument();
    });

    test('renders feedback if there has been fetches and there is no unread notifications', () => {
        const {getByText} = render(
            <RouterContext.Provider value={router}>
                <LearningContentListPageLayout
                    hasFetched={true}
                    network={{isLoading: false}}
                    items={[]}
                />{' '}
            </RouterContext.Provider>
        );

        expect(getByText(__('learningContent.empty'))).toBeInTheDocument();
    });

    test('renders a page intro', () => {
        const {getByText} = render(
            <RouterContext.Provider value={router}>
                <LearningContentListPageLayout
                    hasFetched={true}
                    network={{isLoading: false}}
                    content={{title: 'Title text', headline: 'Headline text'}}
                    items={[{id: 1}]}
                />{' '}
            </RouterContext.Provider>
        );

        expect(getByText('Title text')).toBeInTheDocument();
        expect(getByText('Headline text')).toBeInTheDocument();
    });

    test('renders child item count in page intro', () => {
        const {getByText} = render(
            <RouterContext.Provider value={router}>
                <LearningContentListPageLayout
                    hasFetched={true}
                    network={{isLoading: false}}
                    content={{title: 'Title text', headline: 'Headline text'}}
                    items={[{id: 1}, {id: 2}]}
                    itemsType="lab"
                />{' '}
            </RouterContext.Provider>
        );

        expect(getByText('2 labs')).toBeInTheDocument();
    });

    test('renders a card for each child item', () => {
        const {getAllByTestId} = render(
            <RouterContext.Provider value={router}>
                <LearningContentListPageLayout
                    hasFetched={true}
                    network={{isLoading: false}}
                    items={[
                        {id: 1, title: 'Title text'},
                        {id: 2, title: 'Title text'},
                        {id: 3, title: 'Title text'},
                        {id: 4, title: 'Title text'},
                        {id: 5, title: 'Title text'}
                    ]}
                />{' '}
            </RouterContext.Provider>
        );

        expect(getAllByTestId('learningcontentcard').length).toBe(5);
    });

    test('renders correct child card links for a Syllabus', () => {
        const {getByTestId} = render(
            <RouterContext.Provider value={router}>
                <LearningContentListPageLayout
                    hasFetched={true}
                    network={{isLoading: false}}
                    contentType="syllabus"
                    items={[{id: 1, title: 'Title text'}]}
                />{' '}
            </RouterContext.Provider>
        );

        expect(getByTestId('learningcontentcard')).toHaveAttribute(
            'href',
            expect.stringContaining('/labs/1')
        );
    });

    test('renders correct child card links for a Lab', () => {
        const {getByTestId} = render(
            <RouterContext.Provider value={router}>
                <LearningContentListPageLayout
                    hasFetched={true}
                    network={{isLoading: false}}
                    contentType="lab"
                    items={[{id: 1, title: 'Title text'}]}
                />{' '}
            </RouterContext.Provider>
        );

        expect(getByTestId('learningcontentcard')).toHaveAttribute(
            'href',
            expect.stringContaining('/labs/modules/1')
        );
    });

    test('renders correct child card links for a Module', () => {
        const {getByTestId} = render(
            <RouterContext.Provider value={router}>
                <LearningContentListPageLayout
                    hasFetched={true}
                    network={{isLoading: false}}
                    contentType="module"
                    items={[{id: 1, title: 'Title text'}]}
                />{' '}
            </RouterContext.Provider>
        );

        expect(getByTestId('learningcontentcard')).toHaveAttribute(
            'href',
            expect.stringContaining('/labs/courses/1')
        );
    });

    test('renders correct child card links for a Course', () => {
        const {getByTestId} = render(
            <RouterContext.Provider value={router}>
                <LearningContentListPageLayout
                    hasFetched={true}
                    network={{isLoading: false}}
                    contentType="course"
                    items={[{id: 1, title: 'Title text'}]}
                />{' '}
            </RouterContext.Provider>
        );

        expect(getByTestId('learningcontentcard')).toHaveAttribute(
            'href',
            expect.stringContaining('/labs/topics/1')
        );
    });

    test('renders a difficulty rating for a Topic', () => {
        const {getByText} = render(
            <RouterContext.Provider value={router}>
                <LearningContentListPageLayout
                    hasFetched={true}
                    network={{isLoading: false}}
                    contentType="course"
                    items={[{id: 1, title: 'Title text', difficulty: 'Beginner'}]}
                />{' '}
            </RouterContext.Provider>
        );

        expect(getByText('Beginner')).toBeInTheDocument();
    });

    test('renders 2 columns for Syllabus and Lab content and 1 colunn for Module and Course content', () => {
        let {container} = render(
            <RouterContext.Provider value={router}>
                <LearningContentListPageLayout
                    hasFetched={true}
                    network={{isLoading: false}}
                    contentType="syllabus"
                    items={[{id: 1, title: 'Title text'}, {id: 2, title: 'Title text'}]}
                />{' '}
            </RouterContext.Provider>
        );
        const $twoColLayoutItems = container.querySelector('[class*="LayoutItem"]');
        const twoColClass = $twoColLayoutItems.className;

        container = render(
            <RouterContext.Provider value={router}>
                <LearningContentListPageLayout
                    hasFetched={true}
                    network={{isLoading: false}}
                    contentType="course"
                    items={[{id: 1, title: 'Title text'}, {id: 2, title: 'Title text'}]}
                />{' '}
            </RouterContext.Provider>
        ).container;
        const $oneColLayoutItems = container.querySelector('[class*="LayoutItem"]');
        const oneColClass = $oneColLayoutItems.className;

        expect(twoColClass).not.toBe(oneColClass);
    });
});
