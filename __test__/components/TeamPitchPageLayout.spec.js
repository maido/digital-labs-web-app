import React from 'react';
import {fireEvent, render, wait} from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import {Provider} from 'react-redux';
import {RouterContext} from 'next/dist/next-server/lib/router-context';
import {team, user} from '../stubs';
import __ from '../../globals/strings';
import {initializeStore} from '../../store';
import pitchSchema from '../../schemas/pitch';
import TeamPitchPageLayout from '../../components/TeamPitchPageLayout';

describe('Component: TeamPitchPageLayout', () => {
    const pitchFormState = {
        fields: {
            ...pitchSchema.files.fields,
            ...pitchSchema.details.fields
        },
        name: 'pitch-submit',
        state: 'default'
    };
    const router = {
        pathname: '/team/pitch',
        route: '/team/pitch',
        replace: jest.fn().mockImplementationOnce(),
        query: {}
    };

    test('renders as expected', () => {
        const {asFragment} = render(
            <RouterContext.Provider value={router}>
                <Provider
                    store={initializeStore({
                        form: pitchFormState
                    })}>
                    <TeamPitchPageLayout
                        form={pitchFormState}
                        network={{isLoading: false}}
                        team={team()}
                        user={user()}
                    />
                </Provider>
            </RouterContext.Provider>
        );

        expect(asFragment()).toMatchSnapshot();
    });

    test('does not render pitch details by default', () => {
        const {queryByTestId} = render(
            <RouterContext.Provider value={router}>
                <Provider
                    store={initializeStore({
                        form: pitchFormState
                    })}>
                    <TeamPitchPageLayout
                        form={pitchFormState}
                        network={{isLoading: false}}
                        team={team()}
                        user={user()}
                    />
                </Provider>
            </RouterContext.Provider>
        );

        expect(queryByTestId('pitch-details')).not.toBeInTheDocument();
    });

    test('renders pitch details form when team details are confirmed', async () => {
        const {getByLabelText, queryByTestId} = render(
            <RouterContext.Provider value={router}>
                <Provider
                    store={initializeStore({
                        form: pitchFormState
                    })}>
                    <TeamPitchPageLayout
                        form={pitchFormState}
                        network={{isLoading: false}}
                        team={team()}
                        user={user()}
                    />
                </Provider>
            </RouterContext.Provider>
        );

        const $teamConfirmation = getByLabelText(__('pitch.team.confirmation'));

        fireEvent.click($teamConfirmation);

        await wait();

        expect(queryByTestId('pitch-details')).toBeInTheDocument();
    });
});
