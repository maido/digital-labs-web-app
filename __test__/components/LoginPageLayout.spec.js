import React from 'react';
import {render, fireEvent, wait} from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import {Provider} from 'react-redux';
import mockAxios from 'axios';
import mockCookies from 'js-cookie';
import faker from 'faker';
import {RouterContext} from 'next/dist/next-server/lib/router-context';
import __ from '../../globals/strings';
import {initializeStore} from '../../store/';
import LoginPageLayout from '../../components/LoginPageLayout';

describe('Component: LoginPageLayout', () => {
    const loginFormState = {
        fields: {type: 'login', email: '', password: ''},
        name: 'login',
        state: 'default'
    };

    test('renders as expected', async () => {
        const router = {
            pathname: '/login',
            route: '/login',
            replace: jest.fn().mockImplementationOnce(),
            query: {}
        };
        const {asFragment} = render(
            <Provider store={initializeStore()}>
                <RouterContext.Provider value={router}>
                    <LoginPageLayout />
                </RouterContext.Provider>
            </Provider>
        );
        expect(asFragment()).toMatchSnapshot();
    });

    test('renders an error for incorrect fields on submit', async () => {
        const router = {
            pathname: '/login',
            route: '/login',
            replace: jest.fn().mockImplementationOnce(),
            query: {}
        };
        const {findByTestId, getByLabelText, getByRole} = render(
            <Provider store={initializeStore({form: loginFormState, user: {}})}>
                <RouterContext.Provider value={router}>
                    <LoginPageLayout />
                </RouterContext.Provider>
            </Provider>
        );

        const $input = getByLabelText(/your email/i);
        const $inputPassword = getByLabelText(/your password/i);
        const $form = getByRole('form');

        fireEvent.change($input, {target: {value: 'abcd'}});
        fireEvent.change($inputPassword, {target: {value: ' '}});
        fireEvent.submit($form);

        expect(await findByTestId('error-email')).toBeInTheDocument();
        expect(await findByTestId('error-password')).toBeInTheDocument();
    });

    test('renders an error for an empty email', async () => {
        const router = {
            pathname: '/login',
            route: '/login',
            replace: jest.fn().mockImplementationOnce(),
            query: {}
        };
        const {findByTestId, getByLabelText} = render(
            <Provider store={initializeStore({form: loginFormState, user: {}})}>
                <RouterContext.Provider value={router}>
                    <LoginPageLayout />
                </RouterContext.Provider>
            </Provider>
        );

        const $input = getByLabelText(/your email/i);

        fireEvent.change($input, {target: {value: ' '}});
        fireEvent.blur($input);

        expect(await findByTestId('error-email')).toBeInTheDocument();
    });

    test('renders an error for incorrect email', async () => {
        const router = {
            pathname: '/login',
            route: '/login',
            replace: jest.fn().mockImplementationOnce(),
            query: {}
        };
        const {findByTestId, getByLabelText} = render(
            <Provider store={initializeStore({form: loginFormState, user: {}})}>
                <RouterContext.Provider value={router}>
                    <LoginPageLayout />
                </RouterContext.Provider>
            </Provider>
        );

        const $input = getByLabelText(/your email/i);

        fireEvent.change($input, {target: {value: 'foo@bar'}});
        fireEvent.blur($input);

        expect(await findByTestId('error-email')).toBeInTheDocument();
    });

    test('validates a correct email', async () => {
        const router = {
            pathname: '/login',
            route: '/login',
            replace: jest.fn().mockImplementationOnce(),
            query: {}
        };
        const {queryByTestId, getByLabelText} = render(
            <Provider store={initializeStore({form: loginFormState, user: {}})}>
                <RouterContext.Provider value={router}>
                    <LoginPageLayout />
                </RouterContext.Provider>
            </Provider>
        );

        const $input = getByLabelText(/your email/i);

        fireEvent.change($input, {target: {value: faker.internet.email()}});
        fireEvent.blur($input);

        expect(await queryByTestId('error-email')).toBeNull();
    });

    test('renders an error for an empty password', async () => {
        const router = {
            pathname: '/login',
            route: '/login',
            replace: jest.fn().mockImplementationOnce(),
            query: {}
        };
        const {findByTestId, getByLabelText} = render(
            <Provider store={initializeStore({form: loginFormState, user: {}})}>
                <RouterContext.Provider value={router}>
                    <LoginPageLayout />
                </RouterContext.Provider>
            </Provider>
        );

        const $input = getByLabelText(/your password/i);

        fireEvent.change($input, {target: {value: ' '}});
        fireEvent.blur($input);

        expect(await findByTestId('error-password')).toBeInTheDocument();
    });

    test('renders an error for incorrect password', async () => {
        const router = {
            pathname: '/login',
            route: '/login',
            replace: jest.fn().mockImplementationOnce(),
            query: {}
        };
        const {findByTestId, getByLabelText} = render(
            <Provider store={initializeStore({form: loginFormState, user: {}})}>
                <RouterContext.Provider value={router}>
                    <LoginPageLayout />
                </RouterContext.Provider>
            </Provider>
        );

        const $input = getByLabelText(/your password/i);

        fireEvent.change($input, {target: {value: 'abcd'}});
        fireEvent.blur($input);

        expect(await findByTestId('error-password')).toBeInTheDocument();
    });

    test('validates a correct password', async () => {
        const router = {
            pathname: '/login',
            route: '/login',
            replace: jest.fn().mockImplementationOnce(),
            query: {}
        };
        const {queryByTestId, getByLabelText} = render(
            <Provider store={initializeStore({form: loginFormState, user: {}})}>
                <RouterContext.Provider value={router}>
                    <LoginPageLayout />
                </RouterContext.Provider>
            </Provider>
        );

        const $input = getByLabelText(/your password/i);

        fireEvent.change($input, {target: {value: 'abcdefghijklmnop'}});
        fireEvent.blur($input);

        expect(await queryByTestId('error-password')).toBeNull();
    });

    test('renders an error if the login was unsuccessful', async () => {
        mockAxios.post.mockImplementationOnce(() =>
            Promise.reject({
                response: {status: 422},
                errors: {email: 'Sorry, your email is blocked.'}
            })
        );

        const router = {
            pathname: '/login',
            route: '/login',
            replace: jest.fn().mockImplementationOnce(),
            query: {}
        };
        const {getByLabelText, getByRole, findByTestId} = render(
            <Provider store={initializeStore({form: loginFormState, user: {}})}>
                <RouterContext.Provider value={router}>
                    <LoginPageLayout />
                </RouterContext.Provider>
            </Provider>
        );

        const $input = getByLabelText(/your email/i);
        const $inputPassword = getByLabelText(/your password/i);
        const $form = getByRole('form');

        fireEvent.change($input, {target: {value: faker.internet.email()}});
        fireEvent.change($inputPassword, {target: {value: 'abcdefghijklmnop'}});
        fireEvent.submit($form);

        expect(await findByTestId('error-password')).toBeInTheDocument();
    });

    test('renders a generic error if the login was unsuccessful but no error was returned', async () => {
        mockAxios.post.mockImplementationOnce(() =>
            Promise.reject({response: {status: 422}, data: {}})
        );

        const router = {
            pathname: '/login',
            route: '/login',
            replace: jest.fn().mockImplementationOnce(),
            query: {}
        };
        const {debug, getByLabelText, getByRole, findByTestId} = render(
            <Provider store={initializeStore({form: loginFormState, user: {}})}>
                <RouterContext.Provider value={router}>
                    <LoginPageLayout />
                </RouterContext.Provider>
            </Provider>
        );

        const $input = getByLabelText(/your email/i);
        const $inputPassword = getByLabelText(/your password/i);
        const $form = getByRole('form');

        fireEvent.change($input, {target: {value: faker.internet.email()}});
        fireEvent.change($inputPassword, {target: {value: 'abcdefghijklmnop'}});
        fireEvent.submit($form);

        expect(await findByTestId('error-password')).toHaveTextContent(
            __('account.login.errors.login')
        );
    });

    test("saves the user's information and then redirects to their dashboard when login is successful", async () => {
        mockAxios.post.mockImplementationOnce(() =>
            Promise.resolve({data: {access_token: 'abc123'}})
        );
        /**
         * Mock of /me request
         */
        mockAxios.get.mockImplementationOnce(() =>
            Promise.resolve({
                data: {
                    team: {
                        name: 'Foo bar'
                    }
                }
            })
        );
        mockCookies.set.mockImplementationOnce();

        const router = {
            pathname: '/login',
            route: '/login',
            replace: jest.fn().mockImplementationOnce(),
            query: {}
        };
        const {getByLabelText, getByRole} = render(
            <Provider store={initializeStore({form: loginFormState, user: {}})}>
                <RouterContext.Provider value={router}>
                    <LoginPageLayout />
                </RouterContext.Provider>
            </Provider>
        );

        const $input = getByLabelText(/your email/i);
        const $inputPassword = getByLabelText(/your password/i);
        const $form = getByRole('form');

        fireEvent.change($input, {target: {value: faker.internet.email()}});
        fireEvent.change($inputPassword, {target: {value: 'abcdefghijklmnop'}});
        fireEvent.submit($form);

        await wait();

        expect(mockCookies.set).toHaveBeenCalledWith('token', 'abc123', {expires: 1, path: '/'});
        expect(router.replace).toHaveBeenCalledWith({pathname: '/dashboard'});
    });

    test('redirects the user to the team create page if they do not have a team (or a completed team)', async () => {
        mockAxios.post.mockImplementationOnce(() =>
            Promise.resolve({data: {access_token: 'abc123'}})
        );
        /**
         * Mock of /me request
         */
        mockAxios.get.mockImplementationOnce(() =>
            Promise.resolve({
                data: {
                    team: {
                        name: null
                    }
                }
            })
        );
        mockCookies.set.mockImplementationOnce();

        const router = {
            pathname: '/login',
            route: '/login',
            replace: jest.fn().mockImplementationOnce(),
            query: {}
        };
        const {getByLabelText, getByRole} = render(
            <Provider store={initializeStore({form: loginFormState, user: {}})}>
                <RouterContext.Provider value={router}>
                    <LoginPageLayout />
                </RouterContext.Provider>
            </Provider>
        );

        const $input = getByLabelText(/your email/i);
        const $inputPassword = getByLabelText(/your password/i);
        const $form = getByRole('form');

        fireEvent.change($input, {target: {value: faker.internet.email()}});
        fireEvent.change($inputPassword, {target: {value: 'abcdefghijklmnop'}});
        fireEvent.submit($form);

        await wait();

        expect(mockCookies.set).toHaveBeenCalledWith('token', 'abc123', {expires: 1, path: '/'});
        expect(router.replace).toHaveBeenCalledWith({pathname: '/team/create'});
    });

    test('redirects the user to where the first visited before required to login', async () => {
        mockAxios.post.mockImplementationOnce(() =>
            Promise.resolve({data: {access_token: 'abc123'}})
        );
        /**
         * Mock of /me request
         */
        mockAxios.get.mockImplementationOnce(() =>
            Promise.resolve({
                data: {
                    team: {
                        name: 'tst'
                    }
                }
            })
        );
        mockCookies.set.mockImplementationOnce();

        const router = {
            pathname: '/login',
            route: '/login',
            replace: jest.fn().mockImplementationOnce(),
            query: {redirect: '/foo/bar'}
        };
        const {getByLabelText, getByRole} = render(
            <Provider store={initializeStore({form: loginFormState, user: {}})}>
                <RouterContext.Provider value={router}>
                    <LoginPageLayout />
                </RouterContext.Provider>
            </Provider>
        );

        const $input = getByLabelText(/your email/i);
        const $inputPassword = getByLabelText(/your password/i);
        const $form = getByRole('form');

        fireEvent.change($input, {target: {value: faker.internet.email()}});
        fireEvent.change($inputPassword, {target: {value: 'abcdefghijklmnop'}});
        fireEvent.submit($form);

        await wait();

        expect(mockCookies.set).toHaveBeenCalledWith('token', 'abc123', {expires: 1, path: '/'});
        expect(router.replace).toHaveBeenCalledWith({pathname: '/foo/bar'});
    });

    test('sends a password reset request when forgot password is toggled and the form is submitted', async () => {
        mockAxios.post.mockImplementationOnce(() => Promise.resolve({errors: {email: ''}}));

        const router = {
            pathname: '/login',
            route: '/login',
            replace: jest.fn().mockImplementationOnce(),
            query: {}
        };
        const {getByLabelText, getByRole, getByText, findByTestId} = render(
            <Provider store={initializeStore({form: loginFormState, user: {}})}>
                <RouterContext.Provider value={router}>
                    <LoginPageLayout />
                </RouterContext.Provider>
            </Provider>
        );

        const $input = getByLabelText(/your email/i);
        const email = faker.internet.email();
        fireEvent.change($input, {target: {value: email}});

        const $forgotPasswordCTA = getByText(/forgot your password/i);
        fireEvent.click($forgotPasswordCTA);

        const $form = getByRole('form');
        fireEvent.submit($form);

        await wait();

        expect(mockAxios.post).toBeCalledWith('/api/account/forgot-password', {email});
    });

    test.skip('renders feedback after a forgot password form is submitted', async () => {
        mockAxios.post.mockImplementationOnce(() => Promise.resolve({errors: {email: ''}}));

        const router = {
            pathname: '/login',
            route: '/login',
            replace: jest.fn().mockImplementationOnce(),
            query: {}
        };
        const {getByLabelText, getByRole, getByText, getByTestId} = render(
            <Provider store={initializeStore({form: loginFormState, user: {}})}>
                <RouterContext.Provider value={router}>
                    <LoginPageLayout />
                </RouterContext.Provider>
            </Provider>
        );

        const $input = getByLabelText(/your email/i);
        const email = faker.internet.email();
        fireEvent.change($input, {target: {value: email}});

        const $forgotPasswordCTA = getByText(/forgot your password/i);
        fireEvent.click($forgotPasswordCTA);

        const $form = getByRole('form');
        fireEvent.submit($form);

        await wait();

        expect(getByTestId('fullscreenmessage')).toBeInTheDocument();
    });

    test('renders an error if the forgot password request was unsuccessful', async () => {
        mockAxios.post.mockImplementationOnce(() => Promise.reject({errors: {email: 'Sorry'}}));

        const router = {
            pathname: '/login',
            route: '/login',
            replace: jest.fn().mockImplementationOnce(),
            query: {}
        };
        const {getByLabelText, getByRole, getByText, findByTestId} = render(
            <Provider store={initializeStore({form: loginFormState, user: {}})}>
                <RouterContext.Provider value={router}>
                    <LoginPageLayout />
                </RouterContext.Provider>
            </Provider>
        );

        const $input = getByLabelText(/your email/i);
        const email = faker.internet.email();
        fireEvent.change($input, {target: {value: email}});

        const $forgotPasswordCTA = getByText(/forgot your password/i);
        fireEvent.click($forgotPasswordCTA);

        const $form = getByRole('form');
        fireEvent.submit($form);

        await wait();

        expect(await findByTestId('error-email')).toBeInTheDocument();
    });
});
