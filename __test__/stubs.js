import faker from 'faker';

export const team = () => {
    return {
        id: faker.random.number(),
        name: faker.internet.userName(),
        mission: faker.lorem.paragraph(),
        avatar: {},
        pitch: {},
        members: [...Array(3).keys()].map(() => ({
            id: faker.random.number(),
            first_name: faker.name.firstName(),
            last_name: faker.name.lastName(),
            avatar: {},
            country: faker.address.country()
        })),
        created_at: faker.date.recent(),
        updated_at: faker.date.recent()
    };
};

export const user = () => {
    return {
        isAuthenticated: true,
        team: team(),
        id: faker.random.number(),
        first_name: faker.name.firstName(),
        last_name: faker.name.lastName(),
        birthdate: faker.date.past(),
        email: faker.internet.email(),
        country_origin: faker.address.country(),
        country_residence: faker.address.country(),
        location: faker.address.city(),
        university: faker.lorem.words(),
        field_of_expertise: [faker.lorem.word()],
        occupation: [faker.lorem.word()],
        linkedin: faker.internet.url(),
        avatar: {},
        degree_date: null,
        graduation_date: null,
        areas_of_interest: [faker.lorem.word()],
        areas_of_learning: [faker.lorem.word()],
        heard_via: faker.lorem.words(),
        pitch: {
            daysRemaining: faker.random.number(),
            canSubmit: true
        }
    };
};
