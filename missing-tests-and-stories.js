const FileHound = require('filehound');

const files = {
    containers: [],
    components: [],
    stories: [],
    tests: []
};

const containers = FileHound.create()
    .paths('./components')
    .ignoreHiddenDirectories()
    .ignoreHiddenFiles()
    .ext('.js')
    .match('*PageLayout*')
    .findSync();

files.containers = containers.map(r => {
    let folder = r.split('/');
    return folder[1];
});

const components = FileHound.create()
    .paths('./components')
    .ignoreHiddenDirectories()
    .ignoreHiddenFiles()
    .ext('.js')
    .match('index*')
    .findSync();

files.components = components.map(r => {
    let folder = r.split('/');
    return folder[1];
});

const stories = FileHound.create()
    .paths('./components')
    .ignoreHiddenDirectories()
    .ignoreHiddenFiles()
    .ext('.js')
    .match('stories*')
    .findSync();

files.stories = stories.map(r => {
    let folder = r.split('/');
    return folder[1];
});

const tests = FileHound.create()
    .paths('./__test__/components')
    .ignoreHiddenDirectories()
    .ignoreHiddenFiles()
    .ext('.js')
    .match('.spec')
    .findSync();

files.tests = tests.map(r => {
    let folder = r.split('/');
    return folder[1];
});

////

if (files.stories.length < files.components.length) {
    const missingStories = files.components.filter(
        c => !files.stories.includes(c) && !c.includes('PageLayout')
    );

    console.log(`${missingStories.length} stories missing:`);
    console.log(missingStories);
}

if (files.tests.length < files.components.length) {
    const missingTests = files.components.filter(c => !files.tests.includes(c));

    console.log(`${missingTests.length} tests missing:`);
    console.log(missingTests);
}
