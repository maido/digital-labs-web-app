// @flow
import React from 'react';
import {withAuthSync} from '../globals/auth';
import Container from '../components/Container';
import Layout from '../components/Layout';
import SEO from '../components/SEO';
import * as UI from '../components/UI/styles';

const PitchTermsAndConditionsPage = () => (
    <Layout theme="tertiary">
        <SEO title="Terms and Conditions" />
        <Container size="medium" paddingVertical="l">
            <UI.Heading2 as="h1" css={UI.centerText}>
                2019-2020 Thought For Food Challenge
            </UI.Heading2>

            <UI.Heading5 as="h2" css={UI.centerText}>
                Pitch Terms and Conditions
            </UI.Heading5>
            <UI.Spacer />

            <UI.OrderedList>
                <li></li>
            </UI.OrderedList>
        </Container>
    </Layout>
);

export default withAuthSync(PitchTermsAndConditionsPage);
