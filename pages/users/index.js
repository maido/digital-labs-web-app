// @flow
import React from 'react';
import Router from 'next/router';
import {withAuthSync} from '../../globals/auth';

const UsersListingPage = () => <div />;

UsersListingPage.getInitialProps = ctx => {
    if (ctx.req) {
        ctx.res.writeHead(302, {Location: '/teams'});
        ctx.res.end();
    } else {
        Router.replace('/teams');
    }
};

export default withAuthSync(UsersListingPage);
