// @flow
import React, {useEffect, useState} from 'react';
import {useRouter} from 'next/router';
import axios from 'axios';
import {connect} from 'react-redux';
import {withAuthSync} from '../../globals/auth';
import {updateNetwork} from '../../store/actions';
import Layout from '../../components/Layout';
import UserDetailPageLayout from '../../components/UserDetailPageLayout';
import SEO from '../../components/SEO';

type Props = {
    dispatch: Function,
    network: NetworkStatus
};

const UserDetailPage = ({dispatch, network}: Props) => {
    const [user, setUser] = useState(null);
    const router = useRouter();
    const {userId} = router.query;

    let breadcrumbNav;

    if (user) {
        breadcrumbNav = [
            {label: 'Teams', url: '/teams'},
            {label: user.team.name, url: {href: '/teams/[teamId]', as: `/teams/${user.team.id}`}},
            {
                label: `${user.first_name} ${user.last_name}`,
                url: {href: '/users/[userId]', as: `/users/${user.id}`}
            }
        ];
    }

    useEffect(() => {
        async function fetchData() {
            dispatch(updateNetwork({isLoading: true}));

            try {
                const response = await axios.get(`/api/users/${userId}`);
                const {data} = response;

                setUser(data);
                dispatch(updateNetwork({isLoading: false}));
            } catch (e) {
                dispatch(updateNetwork({isLoading: false}));
            }
        }

        fetchData();
    }, []);

    return (
        <Layout>
            <SEO title={user ? `${user.first_name} ${user.last_name}` : ''} />
            {user ? (
                <UserDetailPageLayout breadcrumbNav={breadcrumbNav} network={network} user={user} />
            ) : null}
        </Layout>
    );
};

const mapStateToProps = state => ({
    network: state.network
});

export default withAuthSync(connect(mapStateToProps)(UserDetailPage));
