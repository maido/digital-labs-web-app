// @flow
import React from 'react';
import {withAuthSync} from '../globals/auth';
import Container from '../components/Container';
import Layout from '../components/Layout';
import SEO from '../components/SEO';
import * as UI from '../components/UI/styles';

const TermsAndConditionsPage = () => (
    <Layout theme="tertiary">
        <SEO title="Terms and Conditions" />
        <Container size="medium" paddingVertical="l">
            <UI.Heading2 as="h1" css={UI.centerText}>
                Rising Star Agri-Food Innovation Challenge 2021{' '}
            </UI.Heading2>

            <UI.Heading5 as="h2" css={UI.centerText}>
                Terms and conditions
            </UI.Heading5>
            <UI.Spacer />

            <UI.OrderedList>
                <li>
                    <p>
                        NO PURCHASE NECESSARY TO WIN. A PURCHASE DOES NOT INCREASE YOUR CHANCE OF
                        WINNING. VOID WHERE PROHIBITED OR RESTRICTED BY LAW. By submitting an entry
                        to the 2021 Rising Star Agri-Food Innovation Challenge, you are agreeing to
                        be bound by the following terms and conditions listed herein (the
                        &ldquo;Terms and Conditions&rdquo;). &nbsp;
                    </p>
                </li>
                <li>
                    <p>
                        SPONSORSHIP: The Rising Star Agri-Food Innovation Challenge is paid for and
                        sponsored by Green Growth Asia Foundation (&ldquo;GGAF&rdquo;), a Malaysian
                        non-profit Foundation, and&nbsp; UNINET, a strategic component for IMT-GT.
                        &nbsp;It is supported by the Indonesia-Malaysia-Thailand Growth Triangle
                        (&ldquo;IMT-GT&rdquo;) and powered by the Thought For Food Foundation
                        (&ldquo;Foundation&rdquo;), a non-profit charity incorporated in Delaware,
                        USA.
                    </p>
                </li>
                <li>
                    <p>
                        PARTICIPATION: Participation in the Rising Star Agri-Food Innovation
                        Challenge is voluntary and subject to all applicable federal, state,
                        provincial and local laws and regulations in the participating countries of
                        Indonesia, Malaysia and Thailand. Participants are responsible for checking
                        and complying with all applicable laws in their jurisdiction before
                        participating in the Rising Star Agri-Food Innovation Challenge.
                        Participants also are responsible for obtaining all permits, passports,
                        visas, and other government-required documents and permissions needed to
                        participate in all elements of the program, and any federal, state,
                        provincial or local taxes that may be levied on any prize monies.&nbsp;
                    </p>
                </li>
                <li>
                    <p>
                        ELIGIBILITY: The Rising Star Agri-Food Innovation Challenge is open to
                        individuals who are registered at university (undergraduate and graduate) in
                        Indonesia, Malaysia and Thailand at time of entry (each, an &ldquo;eligible
                        participant&rdquo;). In order to participate, teams must consist of at least
                        2 and no more than 5 eligible participants.&nbsp; A team consisting of all
                        local country students is allowed. International students are allowed for
                        each team but limited to a team of 2 or 3, only 1 international student is
                        allowed; a team of 4 or 5, only 2 international students are allowed.
                        &nbsp;Teams may comprise eligible participants from the same or different
                        disciplines, communities, countries, universities, etc. Only one entry is
                        allowed per team. Employees, vendors and contractors of Foundation and of
                        Foundation donors or their affiliates are not eligible to enter or win.
                    </p>
                </li>
                <li>
                    <p>
                        HOW TO ENTER: The Rising Star Agri-Food Innovation Challenge will begin on
                        January 25, 2021 and end on May 28th, 2021. To enter, participants should
                        sign up on the TFF Digital Labs Platform via www.tffdigitallabs.org
                        beginning at 5:00 PM Greenwich Mean Time on January 25, 2021 and ending at
                        12:00 AM Greenwich Mean Time on May 28th, 2021. Interested participants can
                        sign up at any time in this period but are encouraged to sign up as early as
                        possible to take advantage of content and experiences offered on the TFF
                        Digital Labs platform.&nbsp;
                    </p>
                </li>
                <li>
                    <p>
                        JUDGING: Entries will be judged based on Judging Criteria publicly available
                        on the TFF Digital Labs platform and in the Rising Star Agri-Food Innovation
                        Challenge Participant Pack. From the submissions received, a panel of
                        judges, selected by GGAF,&nbsp; UNINET , and the Foundation in its sole
                        discretion, will identify a select number of teams (&ldquo;Rising Star
                        Agri-Food Innovation Challenge Winners&rdquo;) to be presented via social
                        media and press releases, where the winners and the names of the winners
                        will be displayed. Rising Star Agri-Food Innovation Challenge Winners will
                        be announced via the global Thought For Food website, email and social
                        media&nbsp; no later than April 30, 2021.
                    </p>
                </li>
                <li>
                    <p>
                        NOTIFICATION: Rising Star Agri-Food Innovation Challenge Winners will be
                        notified directly via email. Failure to fully comply with these Terms and
                        Conditions will result in disqualification. In the event of disqualification
                        of any Rising Star Agri-Food Innovation Challenge Winner, the prize will be
                        forfeited by that Rising Star Agri-Food Innovation Challenge Winner and the
                        next highest ranked submission, as determined by the judges, will be
                        selected as a new Rising Star Agri-Food Innovation Challenge Winner. Neither
                        Foundation nor the Rising Star Agri-Food Innovation Challenge are
                        responsible for notifications that are misdirected, addresses that are no
                        longer correct, or for any other reason beyond the control of Foundation or
                        the Rising Star Agri-Food Innovation Challenge. Judges&rsquo; decisions are
                        final.
                    </p>
                </li>
                <li>
                    <p>
                        OPTION TO INVEST: Foundation seeks to assist teams in the advancement of
                        their promising business opportunities resulting from participation in the
                        Rising Star Agri-Food Innovation Challenge. This can be accomplished in
                        several ways, including through assistance in obtaining investment capital
                        and/or through direct investment by Foundation or another entity. Therefore,
                        as a Rising Star Agri-Food Innovation Challenge entrant you may be offered
                        the opportunity to enter into an agreement with Foundation or another entity
                        identified by Foundation (collectively, &ldquo;Investor&rdquo;) granting an
                        option (&ldquo;Option&rdquo;) that, upon being exercised by Investor, grants
                        Investor the right to represent the entrants and assist the entrants in
                        securing additional funding to further develop and market products, ideas,
                        concepts, findings, research, markets, software, and business entities that
                        are included in, derived from, or develop as a result of the entry. This
                        could also include the right to present entry-related information to other
                        potential investors and other sources of funding. In addition, the Option,
                        upon being exercised by Investor, could grant Investor an equity share in
                        any business, corporation, company or other entity arising from the entry as
                        may be agreed upon between the entrant and Investor.
                    </p>
                </li>
                <li>
                    <p>
                        OWNERSHIP OF ENTRIES: Foundation does not claim any ownership rights in your
                        entry. By submitting your entry, you agree to be bound by these Terms and
                        Conditions and grant Foundation a non-exclusive, fully paid-up and
                        royalty-free, worldwide license to use, modify, delete from, add to,
                        publicly perform, publicly display, reproduce and translate your entry for
                        promotional purposes, including without limitation the right to distribute
                        all or part of your entry in any media formats through any media channels.
                        You consent to the use, by Foundation, its affiliates, subsidiaries,
                        parents, assigns and licensees, of your name, likeness, and image, in
                        connection with the Rising Star Agri-Food Innovation Challenge and
                        TFF&rsquo;s related marketing activities, in any media or format now known
                        or hereafter invented, in any and all locations, without any payment to or
                        further approval from you. You agree that this consent is perpetual and
                        cannot be revoked.
                    </p>
                </li>
                <li>
                    <p>
                        NO USE OF TRADEMARKS: You acknowledge that your entry is being provided to
                        Foundation. You agree that nothing in these Terms and Conditions grants you
                        a right or license to use the terms Thought For Food, Thought For Food
                        Foundation, TFF, or any Thought for Food, TFF, or Foundation trademark or
                        service mark.
                    </p>
                </li>
                <li>
                    <p>
                        TRUTHFUL AND CORRECT ENTRY: The representations and warranties made by you
                        in this entry are true, complete and correct in all material respects as of
                        the date of your submission and do not contain any untrue statements of
                        material fact or omit to state a material fact required to be stated therein
                        or necessary in order to make the statements made therein, in light of the
                        circumstances under which they were made, not misleading. In the event that
                        changes occur as to any material information, documents or exhibits referred
                        to in this entry, of which you have knowledge, you will immediately disclose
                        the same to Foundation when first available to you; and, in the event of any
                        such material change, Foundation may, at its election, terminate your entry
                        to the Rising Star Agri-Food Innovation Challenge and invoice you for any of
                        Foundation&rsquo;s incurred costs.
                    </p>
                </li>
                <li>
                    <p>
                        INTELLECTUAL PROPERTY OWNERSHIP: You acknowledge and agree that all
                        intellectual property required or currently used by you or your organization
                        is legally owned or licensed to you or your organization. &nbsp;The
                        operation of your business as currently conducted, including the design,
                        development, use, publication, distribution and sale of any products or
                        services, does not (1) infringe or misappropriate the intellectual property
                        of any third party, (2) violate the intellectual property rights of any
                        person or entity, including, without limitation, rights of privacy and
                        publicity, or (3) constitute an unfair competition or an unfair trade
                        practice under any law.
                    </p>
                </li>
                <li>
                    <p>
                        INTELLECTUAL PROPERTY WARRANTIES: By submitting an entry, you represent and
                        warrant that your entry: 1) is your own original work; 2) does not contain
                        material or images that is, in Foundation&rsquo;s sole judgment, obscene,
                        inappropriate, offensive, or defamatory; 3) does not violate or infringe
                        upon the copyrights, trademarks, rights of privacy, publicity or other
                        rights of any person or entity; any form of plagiarism is strictly
                        prohibited, 4) does not contain malicious code, such as malware, viruses,
                        timebombs, cancelbots, worms, Trojan horses or other potentially harmful
                        programs or other material or information; 5) does not and will not violate
                        any applicable law, statute, ordinance, rule or regulation; and 6) does not
                        trigger any reporting or royalty obligation to any third party.
                    </p>
                </li>
                <li>
                    <p>
                        ROYALTIES, FEES AND CONFLICTS: Foundation is not responsible for paying any
                        royalties in the event that a team submits copyrighted materials. Any
                        violation of the above terms will result in the potential revocation of
                        participation in the Rising Star Agri-Food Innovation Challenge and/or any
                        rewards. You further represent and warrant that the rights that you are
                        granting under these Terms and Conditions do not conflict in any way with
                        any other agreement to which you are a party, or with any commitments,
                        restrictions, or obligations that you are under to any other person or
                        entity. If your entry contains an image or representation of an identifiable
                        feature of a person other than yourself (for example, if enough of someone
                        else&rsquo;s face were shown so that that person could be identified), then
                        you represent and warrant that you obtained the express approval of that
                        person before submitting your entry.
                    </p>
                </li>
                <li>
                    <p>
                        LIMITATION OF LIABILITY: By participating in the Rising Star Agri-Food
                        Innovation Challenge, you agree to release, indemnify and hold harmless
                        Foundation and its donors, respective partners, affiliates, subsidiaries,
                        advertising and promotions agencies, and each of their respective agents,
                        representatives, officers, directors, shareholders, and employees, as well
                        as each judge (collectively &ldquo;Released Entities&rdquo;) from and
                        against any injuries, losses, damages, claims, actions and any liability of
                        any kind resulting from or arising out of your participation in the Rising
                        Star Agri-Food Innovation Challenge or inability to participate in the
                        Rising Star Agri-Food Innovation Challenge. Released Entities are not
                        responsible for any miscommunications such as technical failures related to
                        computer, telephone, mobile device, cable, wireless, or unavailable network
                        or server connections, related technical failures, or other failures related
                        to hardware, software or virus, or incomplete, late or misdirected entries.
                        Any compromise to the fair and proper conduct of this Rising Star Agri-Food
                        Innovation Challenge may result in the disqualification of an entry,
                        termination of the Rising Star Agri-Food Innovation Challenge, or other
                        remedial action, at the sole discretion of GGAF, IMT-GT, and/or the
                        Foundation. Foundation and its agents shall have the right to remove any
                        entry from the Rising Star Agri-Food Innovation Challenge webpage at any
                        time, if, in their sole discretion, an entry is determined to be obscene,
                        offensive, inappropriate, or to infringe or violate the rights of any person
                        or entity.
                    </p>
                </li>
                <li>
                    <p>
                        WAIVER: By entering the Rising Star Agri-Food Innovation Challenge, you
                        waive all rights to seek injunctive or equitable relief, or to claim
                        punitive, incidental or consequential damages, or attorneys&rsquo; fees.
                    </p>
                </li>
                <li>
                    <p>
                        LAW &amp; JURISDICTION: You agree that all matters arising out of or
                        relating to this sweepstakes and these Terms and Conditions are governed by,
                        and construed in accordance with, the laws of Delaware, without giving
                        effect to any of its conflict of laws provisions thereof. You further agree
                        that any legal suit, action, or proceeding arising out of or relating to
                        this sweepstakes and these Terms and Conditions shall be brought exclusively
                        in the applicable federal or state courts located in Wilmington, Delaware.
                    </p>
                </li>
                <li>
                    <p>
                        REMEDIES: If any of your statements in herein are false, you agree to return
                        to Foundation any prize awarded to you in addition to any other remedies
                        that Foundation may seek to enforce against you.
                    </p>
                </li>
                <li>
                    <p>
                        RELEASE: You hold harmless, release, and discharge, on behalf of yourself,
                        and your heirs, executors, and assigns, Foundation and its respective
                        officers, directors, employees, agents, parent, subsidiaries, affiliates,
                        shareholders, successors, and assigns from any and all claims, demands,
                        lawsuits, expenses, injuries (including death), losses, damages, and any
                        other liability of any kind, of or to you or any other person, your
                        property, or your rights of privacy or publicity, directly or indirectly
                        arising out of or in connection with the Rising Star Agri-Food Innovation
                        Challenge (including the conduct of the Rising Star Agri-Food Innovation
                        Challenge), the awarding of any prizes, your participation in the Rising
                        Star Agri-Food Innovation Challenge, your acceptance, use, misuse, loss, or
                        damage of or to any prize, and use of your name, biographical information,
                        and, even if due to the negligence, omission, or other fault of Foundation.
                    </p>
                </li>
            </UI.OrderedList>
        </Container>
    </Layout>
);

export default withAuthSync(TermsAndConditionsPage, false);
