// @flow
import React, {useEffect} from 'react';
import {useRouter} from 'next/router';
import axios from 'axios';
import get from 'lodash/get';
import {connect} from 'react-redux';
import {updateNetwork, updateResources, updateResourcesType} from '../store/actions';
import {withAuthSync} from '../globals/auth';
import {resourceCategories} from '../globals/content';
import {filterAllowedStrings} from '../globals/functions';
import Layout from '../components/Layout';
import ResourcesListPageLayout from '../components/ResourcesListPageLayout';
import SEO from '../components/SEO';

type Props = {
    dispatch: Function,
    network: NetworkStatus,
    resources: Resources
};

const ResourcesListingPage = ({dispatch, network, resources}: Props) => {
    const router = useRouter();
    const type = filterAllowedStrings(get(router.query, 'type', 'All'), resourceCategories);

    const handleTypeChange = newType => {
        const newPath = `/resources?type=${newType}`;

        dispatch(updateResourcesType(newType));

        /**
         * A shallow replace allows us to 'update' the page without running getInitialProps.
         */
        router.push(newPath, newPath, {shallow: true});
    };

    useEffect(() => {
        async function fetchData() {
            dispatch(updateNetwork({isLoading: true}));

            try {
                const response = await axios.get(`/api/resources?type=${type}`);
                const {data} = response;

                dispatch(updateResources(data.data));
                dispatch(updateNetwork({isLoading: false}));
            } catch (e) {
                dispatch(updateNetwork({isLoading: false}));
            }
        }

        fetchData();

        if (type !== resources.type) {
            dispatch(updateResourcesType(type));
        }

        window.scrollTo({behavior: 'smooth', top: 0});
    }, [type, resources.type]);

    return (
        <Layout>
            <SEO title={`${type} Resources`} />
            <ResourcesListPageLayout
                network={network}
                handleTypeChange={handleTypeChange}
                resources={resources}
            />
        </Layout>
    );
};

const mapStateToProps = state => ({
    network: state.network,
    resources: state.resources
});

export default withAuthSync(connect(mapStateToProps)(ResourcesListingPage));
