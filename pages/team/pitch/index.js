// @flow
import React, {useEffect} from 'react';
import Router from 'next/router';
import {connect} from 'react-redux';
import get from 'lodash/get';
import {updateForm} from '../../../store/actions';
import pitchSchema from '../../../schemas/pitch';
import {withAuthSync} from '../../../globals/auth';
import __ from '../../../globals/strings';
import {getPitchActions, getSchemaDefaultFieldValues} from '../../../globals/functions';
import Container from '../../../components/Container';
import CTAButton from '../../../components/CTAButton';
import Layout from '../../../components/Layout';
import TeamPitchPageLayout from '../../../components/TeamPitchPageLayout';
import SEO from '../../../components/SEO';
import * as UI from '../../../components/UI/styles';

type Props = {
    dispatch: Function,
    form: Form,
    pitchStatus: {
        canSubmitPitch?: boolean,
        deadline?: string,
        daysRemaining?: string
    },
    team: Team
};

const TeamPitchPage = ({dispatch, form, pitchStatus, team}: Props) => {
    useEffect(() => {
        const cachedDetails = localStorage.getItem('pitch.details');

        if (cachedDetails) {
            dispatch(
                updateForm({
                    fields: JSON.parse(cachedDetails),
                    name: 'submit-pitch',
                    state: 'default',
                    step: 3
                })
            );
        } else if (form.name !== 'submit-pitch') {
            dispatch(
                updateForm({
                    fields: getSchemaDefaultFieldValues({
                        ...pitchSchema.details.fields,
                        ...pitchSchema.details.files
                    }),
                    name: 'submit-pitch',
                    state: 'default',
                    step: 1
                })
            );
        }
    }, []);

    let view = 'pitch';

    if (pitchStatus) {
        if (pitchStatus.canSubmit && pitchStatus.daysRemaining < 0) {
            view = 'passed';
        } else if (!pitchStatus.canSubmit) {
            view = 'other';
        }
    }

    return (
        <Layout theme="tertiary">
            <SEO title="Submit your pitch" />
            {view === 'edit-team' && (
                <Container size="small" paddingVertical="l">
                    <UI.Box>
                        <UI.Heading3>{__('pitch.blocked.title')}</UI.Heading3>
                        <p dangerouslySetInnerHTML={{__html: __('pitch.blocked.text')}} />
                        <CTAButton href="/team/edit">Edit your team</CTAButton>
                    </UI.Box>
                </Container>
            )}
            {view === 'passed' && (
                <Container size="small" paddingVertical="l">
                    <UI.Box>
                        <UI.Heading3>{__('pitch.passed.title')}</UI.Heading3>
                        <p dangerouslySetInnerHTML={{__html: __('pitch.passed.text')}} />
                        <CTAButton href="/dashboard">Back to dashboard</CTAButton>
                    </UI.Box>
                </Container>
            )}
            {view === 'other' && (
                <Container size="small" paddingVertical="l">
                    <UI.Box>
                        <UI.Heading3>{__('pitch.other.title')}</UI.Heading3>
                        <p dangerouslySetInnerHTML={{__html: __('pitch.other.text')}} />
                        <CTAButton href="/dashboard">Back to dashboard</CTAButton>
                    </UI.Box>
                </Container>
            )}
            {view === 'pitch' && <TeamPitchPageLayout team={team} />}
        </Layout>
    );
};

TeamPitchPage.getInitialProps = async ({reduxStore, req, res}) => {
    const state = reduxStore.getState();
    const hasPitched = false; //get(state, 'user.team.pitch.submitted_at');

    if (hasPitched) {
        const teamPageUrl = `/teams/${get(state, 'user.team.id')}`;

        if (req) {
            res.writeHead(302, {Location: teamPageUrl});
            res.end();
        } else {
            Router.replace(teamPageUrl);
        }
    } else {
        const pitchStatus = await getPitchActions('2021/05/28');

        return {pitchStatus};
    }
};

const mapStateToProps = state => ({form: state.form, team: state.user.team});

export default withAuthSync(connect(mapStateToProps)(TeamPitchPage));
