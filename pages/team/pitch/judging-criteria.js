// @flow
import React from 'react';
import {withAuthSync} from '../../../globals/auth';
import Container from '../../../components/Container';
import Layout from '../../../components/Layout';
import SEO from '../../../components/SEO';
import * as UI from '../../../components/UI/styles';

const TeamPitchPage = ({dispatch, form, pitchStatus, team}: Props) => (
    <Layout theme="tertiary">
        <SEO title="Pitch - Judging Criteria" />
        <Container size="medium" paddingVertical="l">
            <UI.Heading2 as="h1" css={UI.centerText}>
                Judging Criteria
            </UI.Heading2>
            <UI.Spacer />
            <p>Submissions will be evaluated according to the following criteria:</p>
            <br />
            <UI.Heading5 as="h2" css={UI.margin('bottom', 's')}>
                Innovation
            </UI.Heading5>
            <p>
                TFF commits to being on the cusp of new idea generation. Leverage cutting-edge
                technologies and business models to create a project that is fresh and exciting, in
                a way that has never been seen before.
            </p>
            <br />
            <UI.Heading5 as="h2" css={UI.margin('bottom', 's')}>
                Implementation & Scalability
            </UI.Heading5>
            <p>
                Food security solutions require short, medium, and long-term goals that shouldn’t
                end when our competition is over. The project should be implementable, with
                scalability potential that puts sustainability first.
            </p>
            <br />
            <UI.Heading5 as="h2" css={UI.margin('bottom', 's')}>
                Uniqueness
            </UI.Heading5>
            <p>
                The project should stand out among the wide-array of proposals we see, providing
                game-changing solutions and embodying a “wow” factor in terms of presentation, so
                that it gets noticed in a noisy world of innovation and startups.
            </p>
            <br />
            <UI.Heading5 as="h2" css={UI.margin('bottom', 's')}>
                Collaborative Spirit
            </UI.Heading5>
            <p>
                Entrepreneurship is a team effort. Being an innovator and an entrepreneur is one of
                the most exciting and rewarding things one can do. However, it is also a long and
                demanding journey. The team needs to demonstrate that it is able to withstand these
                challenges and deliver a solution that will make a real difference.
            </p>
            <br />
            <UI.Heading5 as="h2" css={UI.margin('bottom', 's')}>
                Impact
            </UI.Heading5>
            <p>
                The world of business is built around value capture and the competitive race for big
                investments. We want to create an innovation culture that dismantles the current
                “winner takes all” mentality and focuses on broader sharing of the benefits to
                tackle environmental and social challenges in profound ways. Submitted ideas should
                be a positive force for good that help tackle food systems and climate change
                challenges.
            </p>
            <br />
            <UI.Heading5 as="h2" css={UI.margin('bottom', 's')}>
                Multispectral Thinking
            </UI.Heading5>
            <p>
                The global challenges we face are extremely complex and involve lots of nuance. We
                are looking for teams that embrace the complexity and learn to better understand the
                intertwinement of systems. By adopting a multispectral mindset, we can unleash more
                creativity and innovation into the world and create a wider array of possible
                solutions than we ever thought possible.
            </p>
            <br />
        </Container>
    </Layout>
);

export default withAuthSync(TeamPitchPage);
