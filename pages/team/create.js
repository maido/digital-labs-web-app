// @flow
import React from 'react';
import {connect} from 'react-redux';
import nextCookie from 'next-cookies';
import Router from 'next/router';
import get from 'lodash/get';
import {updateForm} from '../../store/actions';
import {withAuthSync} from '../../globals/auth';
import Layout from '../../components/Layout';
import TeamCreatePageLayout from '../../components/TeamCreatePageLayout';
import SEO from '../../components/SEO';

type Props = {
    dispatch: Function,
    draftName: string,
    form: Form,
    team: Team
};

const TeamCreatePage = ({dispatch, draftName = '', form, team}: Props) => {
    const isEmpty = Object.keys(form.fields).length === 0;

    if (isEmpty || form.name !== 'team-create') {
        dispatch(
            updateForm({
                fields: {name: draftName},
                name: 'team-create',
                state: 'default',
                step: 1
            })
        );
    }

    const teams = [
        {
            title: 'Coating+',
            avatar: {url: '/avatars/coating+.jpg'},
            mission: 'Nigeria',
            footnote: '🌟 Grand Prize Winner'
        },
        {
            title: 'Safi Organics',
            mission: 'Kenya',
            avatar: {url: '/avatars/safi-organics.jpg'},
            footnote: '🌟 Runner-Up'
        },
        {
            title: 'AEROPOWDER',
            mission: 'United Kingdom',
            avatar: {url: '/avatars/aeropowder.jpg'},
            footnote: '🌟 Kirchner Food Prize Winner'
        },
        {
            title: 'Rice Inc.',
            mission: 'United Kingdom, Malaysia',
            avatar: {url: '/avatars/rice-inc.jpg'},
            footnote: '🌟 Take it to the Farmer Prize Winner'
        },
        {
            title: 'Sweetpot Yoghurt',
            mission: 'Ghana',
            avatar: {url: '/avatars/sweetpot-yoghurt.jpg'}
        },
        {
            title: 'Nutricandies',
            mission: 'Brazil',
            avatar: {url: '/avatars/nutricandies.jpg'}
        },
        {
            title: 'Laticin',
            mission: 'Brazil',
            avatar: {url: '/avatars/latacin.jpg'}
        },
        {
            title: 'Likabs',
            mission: 'Ghana',
            avatar: {url: '/avatars/likabs.jpg'}
        },
        {
            title: 'IoT Water Control Management',
            mission: 'Brazil, Jordan, Syria',
            avatar: {url: '/avatars/iot-water-control-management.jpg'}
        },
        {
            title: 'RiseHarvest',
            mission: 'Australia, Myanmar',
            avatar: {url: '/avatars/rise-harvest.jpg'}
        }
    ];

    return (
        <Layout theme="tertiary">
            <SEO title="Create your team" />
            <TeamCreatePageLayout team={team} teams={teams} />
        </Layout>
    );
};

TeamCreatePage.getInitialProps = ctx => {
    const {team_name_draft} = nextCookie(ctx);
    const state = ctx.reduxStore.getState();
    const name = get(state, 'user.team.name');

    /**
     * If the user is signed-in and already has created a team, redict back
     * to the dashboard
     */
    if (name) {
        if (ctx.req) {
            ctx.res.writeHead(302, {Location: '/dashboard'});
            ctx.res.end();
        } else {
            Router.replace('/dashboard');
        }
    }

    return {
        draftName: team_name_draft
    };
};

const mapStateToProps = state => ({form: state.form, team: state.user.team});

export default withAuthSync(connect(mapStateToProps)(TeamCreatePage));
