// @flow
import React, {useEffect} from 'react';
import {connect} from 'react-redux';
import map from 'lodash/map';
import schema from '../../schemas/edit-team';
import {updateForm} from '../../store/actions';
import {withAuthSync} from '../../globals/auth';
import Layout from '../../components/Layout';
import TeamEditPageLayout from '../../components/TeamEditPageLayout';
import SEO from '../../components/SEO';

type Props = {
    dispatch: Function,
    form: Form,
    team: Team,
    user: User
};

const TeamEditPage = ({dispatch, form, team, user}) => {
    const isEmpty = Object.keys(form.fields).length === 0;

    if (isEmpty || form.name !== 'team-edit') {
        const fields = {};

        map(schema.fields, (value, key) => {
            fields[key] = team[key] ? team[key] : '';
        });

        dispatch(
            updateForm({
                fields,
                name: 'team-edit',
                state: 'default',
                step: 1
            })
        );
    }

    return (
        <Layout theme="tertiary">
            <SEO title="Edit your team" />
            <TeamEditPageLayout team={team} user={user} />
        </Layout>
    );
};

const mapStateToProps = state => ({form: state.form, team: state.user.team, user: state.user});

export default withAuthSync(connect(mapStateToProps)(TeamEditPage));
