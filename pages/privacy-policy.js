// @flow
import React from 'react';
import {withAuthSync} from '../globals/auth';
import Container from '../components/Container';
import Layout from '../components/Layout';
import SEO from '../components/SEO';
import * as UI from '../components/UI/styles';

const PrivacyPolicyPage = () => (
    <Layout theme="tertiary">
        <SEO title="Privacy Policy" />
        <Container size="medium" paddingVertical="l">
            <UI.Heading2 as="h1" css={UI.centerText}>
                TFF Digital Labs Privacy Policy
            </UI.Heading2>
            <UI.Spacer />
            <UI.Heading5 as="h2">Who we are</UI.Heading5>
            <p>
                Thought For Food&trade; (hereinafter the ‘Foundation’) is the world’s leading
                organization dedicated to engaging, empowering and investing in the next generation
                of innovators in food and agriculture. Working in partnership with major
                corporations, startup accelerators and global non-profits, the Foundation has to
                date worked with more than 15,000 young people in 160+ countries - inspiring
                thousands of creative new business ideas and launching more than 50 impact-focused
                startups across the value chain that solve key challenges such as productivity,
                sustainability, transparency, nutrition and waste, and which leverage cutting-edge
                technologies such as biotech, AI/ machine learning and Robotics.
            </p>
            <p>
                The Foundation’s comprehensive programs create an end-to-end innovation engine for
                food and agriculture, and include:
            </p>

            <UI.UnorderedList>
                <li>
                    the TFF Digital Labs, a first-of-its-kind digital startup accelerator optimized
                    for next-gen food and agriculture entrepreneurs in every part of the world;{' '}
                </li>
                <li>the TFF Academy, an in-person incubator for world-class startups; </li>
                <li>
                    and the TFF Summit, an unconventional event that unites startups, corporations,
                    investors and the TFF Community.
                </li>
            </UI.UnorderedList>

            <p>
                Established as a 501(c)(3) non-profit in 2015, the Foundation has a small core team,
                as well as 13 Regional Coordinators located in 7 regions of the world, 400
                hand-selected volunteer Ambassadors, and a global community that is 15,000 strong.
            </p>
            <p>
                The Foundation’s Main Websites features a Content Hub, a blog for news and updates
                relating to the Foundation initiatives, curated articles from the Foundation
                Community and other content. The Foundation also runs the TFF Digital Labs platform
                which features learning units, mentoring, and peer-to-peer learning tools.
            </p>

            <UI.Spacer />
            <UI.Heading5 as="h2">Our general policy</UI.Heading5>
            <p>
                For the purposes of this Privacy Policy, the Foundation is the Data Controller for
                the Foundation Websites (
                <a href="https://thoughtforfood.org/" target="noopener">
                    thoughtforfood.org
                </a>{' '}
                and{' '}
                <a href="https://www.tffdigitallabs.org" target="noopener">
                    tffdigitallabs.org
                </a>
                ) the Foundation initiatives and websites relating to the Foundation initiatives.
            </p>
            <p>
                The Foundation recognizes the importance of protecting information we collect from
                visitors to our websites, participants of the Foundation initiatives, partners and
                sponsors, Regional Coordinators members, Ambassadors and broader community of
                Foundation members. It is the Foundation’s policy to respect your privacy regarding
                any information we may collect from you across our Main Websites and other websites
                we own and operate.
            </p>
            <p>
                This Privacy Policy concerns primarily the Main Websites (
                <a href="https://thoughtforfood.org/" target="noopener">
                    thoughtforfood.org
                </a>{' '}
                and{' '}
                <a href="https://www.tffdigitallabs.org" target="noopener">
                    tffdigitallabs.org
                </a>
                ), including services relating to the TFF Academy and the TFF Summit. Where
                appropriate and necessary this Privacy Policy will make reference to the
                correspondent initiative or services provided by the Foundation. In the meantime,
                unless provided otherwise, websites the Foundation owns and operates, other than the
                Main Websites, as well as the services the Foundation may provide in relation to
                them, are subject to separate and independent privacy policies.
            </p>
            <p>
                This Privacy Policy describes how we use and protect the information we acquire from
                visitors (also referred to as ‘users’) to our Main Websites.
            </p>
            <p>
                Users can visit the website without revealing any personal information. If one
                chooses to give us personal information via the Internet for the purposes of
                correspondence, processing an order, special website features and applications, or
                providing a subscription to a publication, then it is our intent to let you know how
                we will use such information as set forth in this Privacy Policy.
            </p>
            <p>
                That said, we only ask for personal information when we truly need it to provide a
                service to you. We collect it by fair and lawful means, with your knowledge and
                consent. We also let you know why we are collecting it and how it will be used.
            </p>
            <p>
                We only retain collected information for as long as necessary to provide you with
                your requested service. We will protect the personal data we process and store
                within commercially acceptable means to prevent loss and theft, as well as
                unauthorised access, disclosure, copying, use or modification.
            </p>

            <UI.Spacer />
            <UI.Heading5 as="h2">Your personal data we collect and why we collect it</UI.Heading5>

            <p>
                There are several places on the Main Websites and other websites we own and operate
                where the visitor’s personally identifiable information may be provided to the
                Foundation through subscription forms, application forms, survey forms and by other
                means.
            </p>
            <p>
                When you fill out online forms through our websites, you may be asked for contact
                information (name, address, telephone number, and email address) and demographic
                information (age, gender, education level, etc.). You are free not to provide your
                personal information, with the understanding that we may be unable to provide you
                with some of the desired services.
            </p>
            <p>
                As with many other Internet sites, our web servers record the Internet Protocol (IP)
                address of each visitor. This IP address tells us from which domain you are
                visiting, but not your email address. We use this information to help diagnose
                problems with our server, to administer the website more effectively and to gather
                broad demographic information.
            </p>
            <p>
                When providing personally identifiable information, you are indicating to us your
                agreement to the terms and provisions of this Privacy Policy.
            </p>
            <p>
                In addition, we may post photos from our events and / or on other occasions at our
                Main Websites, other websites we own and operate, social media, business decks and
                other public websites and / or documents, such as annual report. The photos may show
                faces of anyone who attended the events or participates in the Foundation
                initiatives. If you see a photo of yourself and would like to have it removed,
                please contact us by email at{' '}
                <a href="mailto:hi@tffdigitallabs.org" target="noopener">
                    hi@tffdigitallabs.org
                </a>
                .
            </p>

            <UI.Spacer />
            <UI.Heading5 as="h2">How personal data is processed</UI.Heading5>

            <p>
                We use contact information to send visitors updates and news about the Foundation
                and its initiatives through an opt-in request process. The visitor’s contact
                information may also be processed at other times when necessary. Stored email
                addresses may be processed for internal correspondence with our user base. If users
                tell us that they do not wish to have this information processed as a basis for
                further contact, we will respect those wishes.
            </p>
            <p>
                We do keep track of the domains from which people visit us. We analyze this data for
                trends, insights and statistics, and then we discard it. The Foundation may on
                occasion share segments of its mailing list with other non-profit organizations for
                mutually beneficial programs, including for the purpose of providing services in
                relation to the Foundation initiatives. We do not sell our mailing lists to
                for-profit entities.
            </p>

            <UI.Spacer />
            <UI.Heading5 as="h2">Personal data transfers</UI.Heading5>

            <p>
                Please note that since we are a non-profit organization that operates globally, your
                information may be transferred outside of Europe, including to the United States or
                other jurisdictions.
            </p>

            <UI.Spacer />
            <UI.Heading5 as="h2">Who we share your data with</UI.Heading5>

            <p>
                We do not disclose collected personal data to third parties unless such third
                parties are authorized by a contractual relationship, legal basis or legal
                requirements to receive such personal data.
            </p>
            <p>
                The authorized third parties may include, but are not limited to partners, other
                non-profits, professional advisors, website hosting partners and other parties who
                assist us in operating the Main Websites, as well as other websites owned and
                operated by the Foundation, so long as those parties agree to keep this information
                confidential, private and secure.
            </p>
            <p>
                We may also share personal data when we believe that such disclosure is appropriate
                to comply with the law, to enforce the websites’ policies, or protect rights,
                property, or safety of our websites, the users, as well other persons.
            </p>

            <UI.Spacer />
            <UI.Heading5 as="h2">Cookies</UI.Heading5>

            <p>
                As is common practice with almost all professional websites, our websites use
                cookies, which are tiny files that are downloaded to your computer, to improve your
                experience. Here we would like to describe what information these cookies gather,
                how we use it and why we sometimes need to store them. We will also share how you
                can prevent these cookies from being stored however this may downgrade or 'break'
                certain elements of the websites’ functionality. For more information about
                cookies, and how to disable cookies, visit{' '}
                <a href="https://www.allaboutcookies.org/" target="noopener">
                    allaboutcookies.org
                </a>.
            </p>
            <p>
                We use cookies for a variety of reasons. Unfortunately, in most cases there are no
                industry standard options for disabling cookies without completely disabling the
                functionality and features they add to our websites. It is recommended that you
                leave on all cookies if you are not sure whether you need them or not in case they
                are used to provide a service that you use.
            </p>
            <p>
                You can prevent the setting of cookies by adjusting the settings on your browser
                (see your browser Help for how to do this). Be aware that disabling cookies may
                affect the functionality of this and many other websites that you visit. Disabling
                cookies may result in disabling certain functionality and features of our websites.
                Therefore it is recommended that you do not disable cookies.
            </p>
            <p>
                We use cookies when you are logged in to help you to stay logged in and not having
                to log in every single time you visit new sections of our websites or other
                websites. These cookies are typically removed or cleared when you log out to ensure
                that you can only access restricted features and areas when logged in. Our Main
                Websites offers newsletter or email subscription services and cookies may be used to
                remember if you are already registered and whether to show certain notifications
                which might only be available to subscribed/unsubscribed users.
            </p>
            <p>
                In some special cases we also use cookies provided by trusted third parties. Our
                websites use Google Analytics, which is one of the most widespread and trusted
                analytics solutions currently available for helping us to understand how you use our
                websites, as well as find ways we can improve your experience.
            </p>
            <p>
                These cookies may track things such as how much time you spend on our websites and
                the pages that you visit so we can continue to produce engaging content. For more
                information on Google Analytics cookies, see the official Google Analytics page.
            </p>
            <p>
                Our websites also use social media buttons and/or plugins that allow you to connect
                with your social network in various ways. For these to work, social media services,
                including LinkedIn, Facebook, Instagram, Twitter, Vimeo, will set cookies through
                our websites which may be used to enhance your profile on their websites or
                contribute to the data they hold for various purposes outlined in their respective
                privacy policies.
            </p>

            <UI.Spacer />
            <UI.Heading5 as="h2">
                Third-party use of hyperlinks information and other services
            </UI.Heading5>

            <p>
                Our websites may link to external websites that are not operated by us. Unless
                otherwise stated, please be aware that we have no control over the content and
                privacy practices of such websites, and cannot accept responsibility or liability
                for their respective content and privacy policies.
            </p>
            <p>
                The Foundation may on occasion share segments of its mailing list with other
                non-profit organizations for mutually beneficial programs, including the Foundation
                initiatives. We do not sell our mailing list to for-profit entities.
            </p>

            <UI.Spacer />
            <UI.Heading5 as="h2">How long we store your data</UI.Heading5>

            <p>
                We store your personal data only for as long as is necessary and for the purposes
                described in this Privacy Policy. We will retain and use such information to the
                extent necessary to comply with our legal obligations (for example, if we are
                required to store your data to comply with applicable laws or to properly fulfill
                our obligations under a contract), to resolve disputes, and to legally enforce our
                agreements and/or policies.
            </p>

            <UI.Spacer />
            <UI.Heading5 as="h2">
                How we protect your personally identifiable information
            </UI.Heading5>

            <p>
                The Foundation takes precautions in seeking to ensure that our systems are secure
                and that they meet industry standards, including, but limited to the application of
                firewalls. When appropriate, we rely on user authentication systems, encryption
                technologies and access control mechanisms.
            </p>
            <p>
                To the extent websites owned and operated by the Foundation contain links to other
                websites, the Foundation is in no position to accept responsibility for the content
                or privacy practices of these sites.
            </p>
            <p>
                We make sure to keep a record of all personal data, collected and processed in
                accordance with this Privacy Policy. We revise the records annually to make sure
                they are up-to-date. We have set up an internal policy and process, which allows us
                to react responsibly and promptly in case of data breaches.
            </p>

            <UI.Spacer />
            <UI.Heading5 as="h2">Opt-in and opt-out</UI.Heading5>

            <p>
                Our Main Websites provides visitors with the opportunity to request future
                communications through email. By opting-in to a newsletter or an email subscription,
                visitors explicitly agree to receive emails, updates and news, including those about
                the Foundation’s services and initiatives.
            </p>
            <p>
                To give you an opportunity to any time opt out from receiving the Foundation’s
                emails, we always include the unsubscribe link at the bottom of each of the
                Foundation’s emails. You can also contact us at{' '}
                <a href="mailto:info@thoughtforfood.org" target="noopener">
                    info@thoughtforfood.org
                </a>{' '}
                and ask to be placed on the ‘do not email’ list.
            </p>

            <UI.Spacer />
            <UI.Heading5 as="h2">Rights of the Data Subjects under GDPR</UI.Heading5>

            <p>
                If you qualify as the “data subject” under the terms of the European General Data
                Protection Regulation (GDPR) , you have the right to:
            </p>

            <UI.UnorderedList>
                <li>
                    request information on personal data processed by us about you as provided by
                    Art. 15 GDPR.
                </li>
                <li>
                    in accordance with Art. 16 GDPR, to immediately demand the correction of
                    incorrect data or completion of incomplete personal data stored with us;
                </li>
                <li>
                    pursuant to Art. 17 GDPR, to request deletion of your personal data stored by
                    us, unless the processing of the data is required for the exercise of the right
                    to freedom of expression and information, for the fulfillment of a legal
                    obligation, for reasons of public interest or for the assertion, exercise or
                    defense of legal claims;
                </li>
                <li>
                    in accordance with Art. 18 GDPR, to request the restriction of the processing of
                    your personal data as far as the accuracy of the data is disputed by you or the
                    processing is unlawful;
                </li>
                <li>
                    in accordance with Art. 20 GDPR, to receive your personal data provided to us in
                    a structured, standard and machine-readable format or to request transmission to
                    another controller;
                </li>
                <li>
                    in accordance with Art. 7 (3) GDPR, to revoke at any time your consent
                    previously granted to us. As a result, we will be no longer able to continue the
                    data processing based on this consent for the future;
                </li>
                <li>
                    in accordance with Art. 77 GDPR, users have the right to lodge a complaint with
                    a supervisory authority. As a rule, you can contact the supervisory authority of
                    your usual place of residence or work or our company headquarters for this
                    purpose.
                </li>
                <li>
                    also, if your personal data is processed based on a legitimate interests in
                    accordance with Art. 6 (1) (f) GDPR, you have the right to file an objection
                    against the processing of your personal data in accordance with Art. 21 GDPR,
                    provided that there are reasons based on your particular situation or the
                    objection is directed against direct marketing. In the latter case, you have a
                    general right of objection, which shall be implemented by us without you
                    specifying any particular situation.
                </li>
            </UI.UnorderedList>

            <p>
                If you would like to exercise these rights, please contact us via email at{' '}
                <a href="mailto:hi@tffdigitallabs.org" target="noopener">
                    hi@tffdigitallabs.org
                </a>
                . Please include any information that would help us identify you in our database,
                such as name and email address.
            </p>

            <UI.Spacer />
            <UI.Heading5 as="h2">Security</UI.Heading5>

            <p>
                We put great effort and will continue to attempt to establish internal procedures to
                ensure that your personal information is both accurate and protected from
                unauthorized access. Nevertheless, you understand and agree that “perfect” security
                does not exist anywhere, including on the Internet. Information you send to our
                websites, including email messages, will not be encrypted unless stated otherwise.
                Our websites are protected by a firewall and monitored for security.
            </p>
            <p>
                Your continued use of the Main Websites as well as other websites owned and operated
                by the Foundation will be regarded as acceptance of our practices around privacy and
                personal information.
            </p>

            <UI.Spacer />
            <UI.Heading5 as="h2">Contact</UI.Heading5>

            <p>
                If you wish to receive more information about this Privacy Policy and the cookies we
                use on our websites, please contact us via email at{' '}
                <a href="mailto:hi@tffdigitallabs.org" target="noopener">
                    hi@tffdigitallabs.org
                </a>
                . This policy is effective as of October 7, 2019.
            </p>
        </Container>
    </Layout>
);

export default withAuthSync(PrivacyPolicyPage, false);
