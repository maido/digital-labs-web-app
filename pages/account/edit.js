// @flow
import React, {useEffect} from 'react';
import {connect} from 'react-redux';
import {updateForm} from '../../store/actions';
import {withAuthSync} from '../../globals/auth';
import Layout from '../../components/Layout';
import AccountEditPageLayout from '../../components/AccountEditPageLayout';
import SEO from '../../components/SEO';

type Props = {dispatch: Function};

const AccountEditPage = ({dispatch}: Props) => {
    dispatch(updateForm({fields: {}, name: 'account-edit', state: 'default'}));

    return (
        <Layout theme="tertiary">
            <SEO title="Edit your account" />
            <AccountEditPageLayout />
        </Layout>
    );
};

export default withAuthSync(connect()(AccountEditPage));
