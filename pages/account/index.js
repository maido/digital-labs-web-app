// @flow
import React, {useEffect} from 'react';
import Router from 'next/router';
import {connect} from 'react-redux';
import {withAuthSync} from '../../globals/auth';
import Layout from '../../components/Layout';
import UserDetailPageLayout from '../../components/UserDetailPageLayout';
import SEO from '../../components/SEO';

type Props = {
    dispatch: Function,
    user: User
};

const AccountIndexPage = ({network, user}) => (
    <Layout>
        <SEO title="Your account" />
        <UserDetailPageLayout network={network} user={user} />
    </Layout>
);

AccountIndexPage.getInitialProps = async ({req, res}) => {
    /**
     * There's no use for this page right now..
     */
    if (req) {
        res.writeHead(302, {Location: '/account/edit'});
        res.end();
    } else {
        Router.push('/account/edit');
    }
};

const mapStateToProps = state => ({
    network: state.network,
    user: state.user
});

export default withAuthSync(connect(mapStateToProps)(AccountIndexPage));
