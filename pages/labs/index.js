// @flow
import React, {useEffect} from 'react';
import axios from 'axios';
import get from 'lodash/get';
import flatMap from 'lodash/flatMap';
import {connect} from 'react-redux';
import {
    updateNetwork,
    updateSyllabus,
    updateSyllabusContent,
    updateSyllabusFetches
} from '../../store/actions';
import __ from '../../globals/strings';
import {withAuthSync} from '../../globals/auth';
import Layout from '../../components/Layout';
import LearningContentListPageLayout from '../../components/LearningContentListPageLayout';
import SEO from '../../components/SEO';

type Props = {
    allLabs: SyllabusContent,
    dispatch: Function,
    network: NetworkStatus,
    syllabus: Syllabus
};

const SyllabusDetailPage = ({allLabs, dispatch, network, syllabus}: Props) => {
    let syllabusLabs = [];

    if (allLabs && allLabs.allIds.length) {
        syllabusLabs = allLabs.allIds.map(id => allLabs.byId[id]);
    }

    useEffect(() => {
        async function fetchData() {
            dispatch(updateNetwork({isLoading: true}));

            try {
                const response = await axios.get(`/api/labs/syllabus`);
                const {syllabus, labs, modules} = response.data;

                if (syllabus) {
                    dispatch(updateSyllabus(syllabus));
                } else {
                    dispatch(
                        updateSyllabus({
                            title: __('learningContent.syllabus.title'),
                            headline: __('learningContent.syllabus.headline')
                        })
                    );
                }

                if (labs.length) {
                    dispatch(updateSyllabusContent('labs', labs));
                    dispatch(updateSyllabusFetches('labs', true));
                }

                if (modules.length) {
                    dispatch(updateSyllabusContent('modules', modules));
                }

                dispatch(updateNetwork({isLoading: false}));
            } catch (e) {
                dispatch(updateNetwork({isLoading: false}));
            }
        }

        if (!syllabus.labs || !syllabus.labs.hasFetched) {
            fetchData();
        }
    }, []);

    return (
        <Layout>
            <SEO title={syllabus ? syllabus.title : ''} />
            <LearningContentListPageLayout
                content={syllabus}
                contentType="syllabus"
                hasFetched={allLabs.hasFetched}
                items={syllabusLabs}
                itemsType="lab"
                network={network}
            />
        </Layout>
    );
};

const mapStateToProps = state => ({
    allLabs: state.syllabus.labs,
    network: state.network,
    syllabus: state.syllabus
});

export default withAuthSync(connect(mapStateToProps)(SyllabusDetailPage));
