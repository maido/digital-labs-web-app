// @flow
import React, {useEffect, useState} from 'react';
import {useRouter} from 'next/router';
import axios from 'axios';
import get from 'lodash/get';
import size from 'lodash/size';
import {connect} from 'react-redux';
import {withAuthSync} from '../../../globals/auth';
import {updateNetwork, updateSyllabus, updateSyllabusContent} from '../../../store/actions';
import ErrorPage from '../../_error';
import Layout from '../../../components/Layout';
import LearningContentListPageLayout from '../../../components/LearningContentListPageLayout';
import SEO from '../../../components/SEO';

type Props = {
    allLabs: SyllabusContent,
    allCourses: SyllabusContent,
    allModules: SyllabusContent,
    allTopics: SyllabusContent,
    dispatch: Function,
    network: NetworkStatus
};

const CoursesDetailPage = ({
    allCourses,
    allLabs,
    allModules,
    allTopics,
    dispatch,
    network
}: Props) => {
    const [hasFetched, setHasFetched] = useState(false);
    const router = useRouter();
    const [errorStatus, setErrorStatus] = useState(null);
    const {courseId} = router.query;

    let course;
    let lab;
    let module;
    let courseTopics = [];
    let breadcrumbNav;

    if (allCourses.byId[courseId]) {
        course = allCourses.byId[courseId];

        if (size(get(course, 'topics')) && size(get(allTopics, 'allIds'))) {
            courseTopics = course.topics.map(id => allTopics.byId[id]).filter(t => t);
        }

        if (course.lab && allLabs.byId[course.lab]) {
            lab = allLabs.byId[course.lab];
        }

        if (course.module && allModules.byId[course.module]) {
            module = allModules.byId[course.module];
        }
    }

    if (lab && course && module) {
        breadcrumbNav = [
            {label: 'Labs', url: '/labs'},
            {label: lab.title, url: {href: '/labs/[labId]', as: `/labs/${lab.id}`}},
            {
                label: module.title,
                url: {
                    href: '/labs/modules/[moduleId]',
                    as: `/labs/modules/${module.id}`
                }
            },
            {
                label: course.title,
                url: {
                    href: '/labs/courses/[courseId]',
                    as: `/labs/courses/${course.id}`
                }
            }
        ];
    }

    useEffect(() => {
        async function fetchData() {
            dispatch(updateNetwork({isLoading: true}));

            try {
                const response = await axios.get(`/api/labs/course?id=${courseId}`);
                const {data} = response;
                const {lab, course, module, topics} = data;

                if (lab) {
                    dispatch(updateSyllabusContent('labs', [lab]));
                }

                if (course) {
                    dispatch(updateSyllabusContent('courses', [course]));
                }

                if (module) {
                    dispatch(updateSyllabusContent('modules', [module]));
                }

                if (topics) {
                    dispatch(updateSyllabusContent('topics', topics));
                }

                dispatch(updateNetwork({isLoading: false}));
                setHasFetched(true);
            } catch (e) {
                setErrorStatus(e.response.status);
                dispatch(updateNetwork({isLoading: false}));
            }
        }

        if (!course || courseTopics.length === 0) {
            fetchData();
        } else {
            setHasFetched(true);
        }
    }, []);

    if (errorStatus) {
        return (
            <ErrorPage
                statusCode={errorStatus}
                title="Course not found"
                text="Sorry, we couldn't find the course you were looking for."
            />
        );
    }

    return (
        <Layout>
            <SEO title={course ? course.title : ''} />
            <LearningContentListPageLayout
                breadcrumbNav={breadcrumbNav}
                network={network}
                content={course}
                contentType="course"
                hasFetched={hasFetched}
                items={courseTopics}
                itemsType="topic"
            />
        </Layout>
    );
};

const mapStateToProps = state => ({
    allLabs: state.syllabus.labs,
    allCourses: state.syllabus.courses,
    allModules: state.syllabus.modules,
    allTopics: state.syllabus.topics,
    network: state.network
});

export default withAuthSync(connect(mapStateToProps)(CoursesDetailPage));
