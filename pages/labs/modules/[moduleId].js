// @flow
import React, {useEffect, useState} from 'react';
import {useRouter} from 'next/router';
import axios from 'axios';
import get from 'lodash/get';
import size from 'lodash/size';
import {connect} from 'react-redux';
import {withAuthSync} from '../../../globals/auth';
import {updateNetwork, updateSyllabusContent} from '../../../store/actions';
import ErrorPage from '../../_error';
import Layout from '../../../components/Layout';
import LearningContentListPageLayout from '../../../components/LearningContentListPageLayout';
import SEO from '../../../components/SEO';

type Props = {
    allLabs: SyllabusContent,
    allCourses: SyllabusContent,
    allModules: SyllabusContent,
    dispatch: Function,
    network: NetworkStatus
};

const ModuleDetailPage = ({allCourses, allLabs, allModules, dispatch, network}: Props) => {
    const [hasFetched, setHasFetched] = useState(false);
    const [errorStatus, setErrorStatus] = useState(null);
    const router = useRouter();
    const {moduleId} = router.query;

    let breadcrumbNav = [];
    let lab;
    let module;
    let moduleCourses = [];
    let pageIntro;

    if (allModules.byId[moduleId]) {
        module = allModules.byId[moduleId];

        if (size(get(module, 'courses')) && size(get(allCourses, 'allIds'))) {
            moduleCourses = module.courses.map(id => allCourses.byId[id]).filter(c => c);
        }

        if (module.lab && allLabs.byId[module.lab]) {
            lab = allLabs.byId[module.lab];
        }
    }

    if (lab && module) {
        breadcrumbNav = [
            {label: 'Labs', url: '/labs'},
            {label: lab.title, url: {href: '/labs/[labId]', as: `/labs/${lab.id}`}},
            {
                label: module.title,
                url: {
                    href: '/labs/modules/[moduleId]',
                    as: `/labs/modules/${module.id}`
                }
            }
        ];
    }

    useEffect(() => {
        async function fetchData() {
            dispatch(updateNetwork({isLoading: true}));

            try {
                const response = await axios.get(`/api/labs/module?id=${moduleId}`);
                const {data} = response;
                const {lab, courses, module} = data;

                if (lab) {
                    dispatch(updateSyllabusContent('labs', [lab]));
                }

                if (courses) {
                    dispatch(updateSyllabusContent('courses', courses));
                }

                if (module) {
                    dispatch(updateSyllabusContent('modules', [module]));
                }

                dispatch(updateNetwork({isLoading: false}));
                setHasFetched(true);
            } catch (e) {
                setErrorStatus(e.response.status);
                dispatch(updateNetwork({isLoading: false}));
            }
        }

        if (!module || moduleCourses.length === 0) {
            fetchData();
        } else {
            setHasFetched(true);
        }
    }, []);

    if (errorStatus) {
        return (
            <ErrorPage
                statusCode={errorStatus}
                title="Module not found"
                text="Sorry, we couldn't find the module you were looking for."
            />
        );
    }

    return (
        <Layout>
            <SEO title={module ? module.title : ''} />
            <LearningContentListPageLayout
                breadcrumbNav={breadcrumbNav}
                network={network}
                content={module}
                contentType="module"
                hasFetched={hasFetched}
                items={moduleCourses}
                itemsType="course"
            />
        </Layout>
    );
};

const mapStateToProps = state => ({
    allLabs: state.syllabus.labs,
    allCourses: state.syllabus.courses,
    allModules: state.syllabus.modules,
    network: state.network
});

export default withAuthSync(connect(mapStateToProps)(ModuleDetailPage));
