// @flow
import React, {useEffect, useState} from 'react';
import {useRouter} from 'next/router';
import axios from 'axios';
import get from 'lodash/get';
import size from 'lodash/size';
import {connect} from 'react-redux';
import {withAuthSync} from '../../globals/auth';
import {
    updateNetwork,
    updateSyllabus,
    updateSyllabusContent,
    updateSyllabusFetches
} from '../../store/actions';
import Layout from '../../components/Layout';
import LearningContentListPageLayout from '../../components/LearningContentListPageLayout';
import SEO from '../../components/SEO';

type Props = {
    dispatch: Function,
    allLabs: SyllabusContent,
    allModules: SyllabusContent,
    network: NetworkStatus
};

const LabsDetailPage = ({dispatch, allLabs, allModules, network}: Props) => {
    const router = useRouter();
    const {labId} = router.query;
    const [errorStatus, setErrorStatus] = useState(null);
    const lab = allLabs.byId[labId];

    let breadcrumbNav;
    let labModules = [];

    if (lab) {
        breadcrumbNav = [
            {label: 'Labs', url: '/labs'},
            {
                label: lab.title,
                url: {href: '/labs/[labId]', as: `/labs/${lab.id}`}
            }
        ];

        if (size(get(lab, 'modules')) && size(get(allModules, 'allIds'))) {
            labModules = lab.modules.map(id => allModules.byId[id]);
        }
    }

    useEffect(() => {
        async function fetchData() {
            dispatch(updateNetwork({isLoading: true}));

            try {
                const response = await axios.get(`/api/labs/syllabus`);
                const {syllabus, labs, modules} = response.data;

                if (syllabus) {
                    dispatch(updateSyllabus(syllabus));
                }

                if (labs.length) {
                    dispatch(updateSyllabusContent('labs', labs));
                }

                if (modules.length) {
                    dispatch(updateSyllabusContent('modules', modules));
                    dispatch(updateSyllabusFetches('modules', true));
                }

                dispatch(updateNetwork({isLoading: false}));
            } catch (e) {
                setErrorStatus(e.response.status);
                dispatch(updateNetwork({isLoading: false}));
            }
        }

        if (!lab || !labModules.hasFetched) {
            fetchData();
        }
    }, []);

    if (errorStatus) {
        return (
            <ErrorPage
                statusCode={errorStatus}
                title="Lab not found"
                text="Sorry, we couldn't find the lab you were looking for."
            />
        );
    }

    return (
        <Layout>
            <SEO title={lab ? lab.title : ''} />
            <LearningContentListPageLayout
                breadcrumbNav={breadcrumbNav}
                network={network}
                content={lab}
                contentType="lab"
                hasFetched={allModules.hasFetched}
                items={labModules}
                itemsType="module"
            />
        </Layout>
    );
};

const mapStateToProps = state => ({
    allLabs: state.syllabus.labs,
    allModules: state.syllabus.modules,
    network: state.network
});

export default withAuthSync(connect(mapStateToProps)(LabsDetailPage));
