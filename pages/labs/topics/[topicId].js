// @flow
import React, {useEffect, useState} from 'react';
import {useRouter} from 'next/router';
import axios from 'axios';
import get from 'lodash/get';
import omit from 'lodash/omit';
import size from 'lodash/size';
import {connect} from 'react-redux';
import {withAuthSync} from '../../../globals/auth';
import bugsnagClient from '../../../globals/bugsnag';
import {pageThemes} from '../../../globals/variables';
import {updateNetwork, updateSyllabus, updateSyllabusContent} from '../../../store/actions';
import ErrorPage from '../../_error';
import Layout from '../../../components/Layout';
import TopicsDetailPageLayout from '../../../components/TopicsDetailPageLayout';
import SEO from '../../../components/SEO';

type Props = {
    allTopics: SyllabusContent,
    allCourses: SyllabusContent,
    dispatch: Function,
    allLabss: SyllabusContent,
    allModules: SyllabusContent,
    allTopics: SyllabusContent,
    network: NetworkStatus
};

const TopicsDetailPage = ({allTopics, allCourses, dispatch, allLabs, allModules, network}) => {
    const [hasFetched, setHasFetched] = useState(false);
    const [errorStatus, setErrorStatus] = useState(null);
    const router = useRouter();
    const {topicId} = router.query;

    let topic;
    let relatedTopics;
    let breadcrumbNav;

    if (allTopics.byId[topicId]) {
        topic = allTopics.byId[topicId];

        const {lab, module, course} = topic;

        if (lab && module && course) {
            breadcrumbNav = [
                {label: 'Labs', url: '/labs'},
                {label: lab.title, url: {href: '/labs/[labId]', as: `/labs/${lab.id}`}},
                {
                    label: module.title,
                    url: {
                        href: '/labs/modules/[moduleId]',
                        as: `/labs/modules/${module.id}`
                    }
                },
                {
                    label: course.title,
                    url: {
                        href: '/labs/courses/[courseId]',
                        as: `/labs/courses/${course.id}`
                    }
                },
                {
                    label: topic.title,
                    url: {
                        href: '/labs/topics/[topicId]',
                        as: `/labs/topics/${topic.id}`
                    }
                }
            ];
        }
    }

    useEffect(() => {
        async function fetchData() {
            dispatch(updateNetwork({isLoading: true}));

            try {
                const response = await axios.get(`/api/labs/topic?id=${topicId}`);
                const {data} = response;
                const {lab, course, module} = data;

                dispatch(updateSyllabusContent('labs', [lab]));
                dispatch(updateSyllabusContent('courses', [course]));
                dispatch(updateSyllabusContent('modules', [module]));
                dispatch(updateSyllabusContent('topics', [data]));
                dispatch(updateNetwork({isLoading: false}));
                setHasFetched(true);
            } catch (e) {
                setErrorStatus(e.response.status);
                dispatch(updateNetwork({isLoading: false}));
            }
        }

        if (!hasFetched || !topic) {
            fetchData();
        } else {
            setHasFetched(true);
        }
    }, [topicId]);

    const layoutProps = {};

    if (topic && pageThemes.includes(topic.theme)) {
        layoutProps.theme = topic.theme;
    }

    if (errorStatus) {
        return (
            <ErrorPage
                statusCode={errorStatus}
                title="Topic not found"
                text="Sorry, we couldn't find the topic you were looking for."
            />
        );
    }

    /**
     * We add a key to the layout component below so we can force a re-render if clicking on a
     * related topic. Otherwise, clicking on a related topic will force the main content to change
     * but not do an update on related topics.
     *
     * We also don't return the topic content from the getIntitialProps, otherwise when changes to
     * the topic are dispatched, we won't get the update. Instead, we take the topic id and then use
     * that to render the correct topic, and then subsequently any changes to the topic (such as
     * being marked as complete).
     */
    return (
        <Layout {...layoutProps} type="topic">
            <SEO title={topic ? topic.title : ''} />
            <TopicsDetailPageLayout
                breadcrumbNav={breadcrumbNav}
                content={topic}
                contentType="topic"
                hasFetched={hasFetched}
                key={topicId}
                module={topic ? topic.module : {}}
                network={network}
            />
        </Layout>
    );
};

const mapStateToProps = state => ({
    allLabs: state.syllabus.labs,
    allCourses: state.syllabus.courses,
    allModules: state.syllabus.modules,
    allTopics: state.syllabus.topics,
    network: state.network
});

export default withAuthSync(connect(mapStateToProps)(TopicsDetailPage));
