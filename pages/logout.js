// @flow
import React, {useEffect} from 'react';
import {useRouter} from 'next/router';
import Cookie from 'js-cookie';
import {connect} from 'react-redux';
import {updateUser} from '../store/actions';
import {withAuthSync} from '../globals/auth';
import FullScreenMessage from '../components/FullScreenMessage';
import Layout from '../components/Layout';
import SEO from '../components/SEO';
import Spinner from '../components/Spinner';

type Props = {dispatch: Function};

const LogoutPage = ({dispatch}: Props) => {
    const router = useRouter();

    useEffect(() => {
        dispatch(updateUser({isAuthenticated: false}));
        Cookie.remove('token');
        router.replace('/login');
    }, []);

    return (
        <Layout hasSiteHeader={false} hasSiteFooter={false}>
            <SEO title="Logging out..." />
            <FullScreenMessage state="visible">
                <Spinner />
            </FullScreenMessage>
        </Layout>
    );
};

export default withAuthSync(connect()(LogoutPage));
