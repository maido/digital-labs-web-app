// @flow
import React from 'react';
import nextCookie from 'next-cookies';
import Router from 'next/router';
import Layout from '../components/Layout';
import LoginPageLayout from '../components/LoginPageLayout';
import SEO from '../components/SEO';

type Props = {dispatch: Function};

const LoginPage = ({dispatch}: Props) => (
    <>
        <SEO title="Login" />
        <Layout theme="secondary" hasSiteHeader={false} hasSiteFooter={false}>
            <LoginPageLayout />
        </Layout>
    </>
);

LoginPageLayout.getInitialProps = ctx => {
    const {token} = nextCookie(ctx);

    /**
     * If the user is signed-in, redirect backto the dashboard
     */
    if (token) {
        if (ctx.req) {
            ctx.res.writeHead(302, {Location: '/dashboard'});
            ctx.res.end();
        } else {
            Router.replace('/dashboard');
        }
    }
};

export default LoginPage;
