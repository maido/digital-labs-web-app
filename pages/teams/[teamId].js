// @flow
import React, {useEffect, useState} from 'react';
import {useRouter} from 'next/router';
import axios from 'axios';
import get from 'lodash/get';
import size from 'lodash/size';
import {connect} from 'react-redux';
import {withAuthSync} from '../../globals/auth';
import {updateNetwork, updateTeams} from '../../store/actions';
import Layout from '../../components/Layout';
import TeamIndexPageLayout from '../../components/TeamIndexPageLayout';
import SEO from '../../components/SEO';

type Props = {
    dispatch: Function,
    teams: Teams,
    network: NetworkStatus,
    user: User
};

const TeamDetailPage = ({dispatch, teams, network, user}: Props) => {
    const [hasFetched, setHasFetched] = useState(false);
    const [submissions, setSubmissions] = useState([]);
    const router = useRouter();
    const {teamId} = router.query;
    const team = teams.byId[teamId];

    let breadcrumbNav;

    if (team) {
        breadcrumbNav = [
            {label: 'Teams', url: '/teams'},
            {
                label: team.name,
                url: {href: '/teams/[teamId]', as: `/teams/${team.id}`}
            }
        ];
    }

    useEffect(() => {
        async function fetchData() {
            dispatch(updateNetwork({isLoading: true}));

            try {
                const response = await axios.get(`/api/teams/${teamId}`);
                const {data} = response;

                dispatch(updateTeams([data]));
                dispatch(updateNetwork({isLoading: false}));
            } catch (e) {
                dispatch(updateNetwork({isLoading: false}));
            }

            try {
                const response = await axios.get(`/api/teams/submissions/${teamId}?limit=100`);
                const {data} = response;

                setSubmissions(data);
                setHasFetched(true);
            } catch (e) {
                setHasFetched(true);
                console.log(e);
            }
        }

        if (!team || !hasFetched) {
            fetchData();
        }
    }, [teamId]);

    return (
        <Layout>
            <SEO title={team ? team.name : ''} />
            {team ? (
                <TeamIndexPageLayout
                    breadcrumbNav={breadcrumbNav}
                    hasFetched={hasFetched}
                    network={network}
                    key={teamId}
                    submissions={submissions}
                    team={team}
                    user={user}
                />
            ) : null}
        </Layout>
    );
};

const mapStateToProps = state => ({
    teams: state.teams,
    network: state.network,
    user: state.user
});

export default withAuthSync(connect(mapStateToProps)(TeamDetailPage));
