// @flow
import React, {useEffect, useState} from 'react';
import {useRouter} from 'next/router';
import axios from 'axios';
import get from 'lodash/get';
import size from 'lodash/size';
import {connect} from 'react-redux';
import {updateNetwork, updatePagination, updateTeams} from '../../store/actions';
import {withAuthSync} from '../../globals/auth';
import Layout from '../../components/Layout';
import TeamsListPageLayout from '../../components/TeamsListPageLayout';
import SEO from '../../components/SEO';

type Props = {
    dispatch: Function,
    network: NetworkStatus,
    pagination: Pagination,
    teams: Teams
};

const TeamsListingPage = ({dispatch, network, pagination, teams}: Props) => {
    const [totalTeamsAndCountries, setTotalTeamsAndCountries] = useState('');
    const router = useRouter();
    const page = parseInt(get(router.query, 'page', 1));

    const handlePageChange = newPage => {
        const newPath = `/teams?page=${newPage}`;

        /**
         * A shallow replace allows us to 'update' the page without running getInitialProps.
         */
        router.push(newPath, newPath, {shallow: true});
    };

    useEffect(() => {
        async function fetchData() {
            dispatch(updateNetwork({isLoading: true}));
            dispatch(updatePagination({activePage: page}));

            try {
                const response = await axios.get(
                    `/api/teams?limit=${pagination.itemsCountPerPage}&page=${page}`
                );
                const {data} = response;
                const teamIds = data.data.map(d => d.id);

                dispatch(updateTeams(data.data));
                dispatch(
                    updatePagination({
                        activePage: page,
                        currentItems: teamIds.length,
                        fetchedPages: {[page]: teamIds},
                        skip: page > 1 ? page * pagination.itemsCountPerPage : 0,
                        totalItemsCount: data.meta.total
                    })
                );
                dispatch(updateNetwork({isLoading: false}));
                setTotalTeamsAndCountries(
                    `${data.meta.total} teams across ${data.meta.country_count} countries`
                );
            } catch (e) {
                dispatch(updateNetwork({isLoading: false}));
            }
        }

        if (!pagination.fetchedPages[page]) {
            fetchData();
        } else {
            dispatch(
                updatePagination({
                    activePage: page,
                    currentItems: pagination.fetchedPages[page].length,
                    skip: page > 1 ? page * pagination.itemsCountPerPage : 0
                })
            );
        }

        window.scrollTo({behavior: 'smooth', top: 0});
    }, [page, size(get(pagination.fetchedPages, page))]);

    return (
        <Layout>
            <SEO title="Teams" />
            <TeamsListPageLayout
                totalTeamsAndCountries={totalTeamsAndCountries}
                network={network}
                handlePageChange={handlePageChange}
                pagination={pagination}
                teams={teams}
            />
        </Layout>
    );
};

const mapStateToProps = state => ({
    pagination: state.pagination,
    network: state.network,
    teams: state.teams
});

export default withAuthSync(connect(mapStateToProps)(TeamsListingPage));
