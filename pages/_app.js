// @flow
import React from 'react';
import App from 'next/app';
import {Provider} from 'react-redux';
import Error from './_error';
import SEO from '../components/SEO';
import bugsnagClient from '../globals/bugsnag';
import withStore from '../store/with-store';

type Props = {
    Component: *,
    pageProps: Object,
    reduxStore: Object
};

const ErrorBoundary = bugsnagClient.getPlugin('react');

const AppContainer = ({Component, pageProps, reduxStore}: Props) => (
    <ErrorBoundary FallbackComponent={Error}>
        <Provider store={reduxStore}>
            <SEO
                title="TFF Digital Labs"
                description="Join the TFF Challenge, a global “collaborative competition” to develop and launch breakthrough solutions that sustainably feed the world."
            />
            <Component {...pageProps} />
        </Provider>
    </ErrorBoundary>
);

AppContainer.getInitialProps = async ({Component, ctx}) => {
    let pageProps = {};

    if (Component.getInitialProps) {
        pageProps = await Component.getInitialProps(ctx);
    }

    return {pageProps};
};

export default withStore(AppContainer);
