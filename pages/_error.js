// @flow
import React from 'react';
import {useRouter} from 'next/router';
import {connect} from 'react-redux';
import __ from '../globals/strings';
import {withAuthSync} from '../globals/auth';
import bugsnagClient from '../globals/bugsnag';
import CTAButton from '../components/CTAButton';
import Layout from '../components/Layout';
import FullScreenMessage from '../components/FullScreenMessage';
import SEO from '../components/SEO';
import * as UI from '../components/UI/styles';

type Props = {
    err: Object,
    statusCode: string,
    text?: string,
    title?: string,
    user: User
};

const Error = ({err, statusCode, text, title, user}: Props) => {
    const router = useRouter();

    if (!text) {
        text = statusCode === 404 ? __('errors.notFound') : __('errors.other');
    }

    if (!title) {
        title = 'Uh oh!';
    }

    return (
        <Layout>
            <SEO title={title} />
            <FullScreenMessage state="visible" title={title} theme="error" text={text}>
                <UI.Label>Error {statusCode}</UI.Label>

                <UI.Spacer />

                {user.isAuthenticated ? (
                    <CTAButton href="/dashboard">Return to your dashboard</CTAButton>
                ) : (
                    <CTAButton href="/">Return home</CTAButton>
                )}
            </FullScreenMessage>
        </Layout>
    );
};

Error.getInitialProps = ({err, res}) => {
    const statusCode = res ? res.statusCode : err ? err.statusCode : null;

    if (err && bugsnagClient) {
        bugsnagClient.notify(err);
    }

    return {statusCode};
};

const mapStateToProps = state => ({
    user: state.user
});

export default withAuthSync(connect(mapStateToProps)(Error), false);
