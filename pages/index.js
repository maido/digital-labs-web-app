// @flow
import React from 'react';
import {connect} from 'react-redux';
import {withAuthSync} from '../globals/auth';
import HomePageLayout from '../components/HomePageLayout';
import Layout from '../components/Layout';
import SEO from '../components/SEO';

const IndexPage = ({user}) => {
    const content = {
        intro: {
            title: 'Shaping the Future of Our Food Systems',
            text:
                'The TFF Digital Labs embodies a new approach to global learning, startup acceleration, and collaboration that improves the speed and impact of beneficial innovation across the entire food and agriculture value chain.'
        },
        winnerPhotos: {
            images: ['/home-1.jpg', '/home-2.jpg', '/home-3.jpg'],
            card: {
                image: '/home-badge.jpg',
                text: '',
                title: 'Next Gen Innovation'
            }
        },
        version2Teaser: {
            title: 'Coming Spring 2021',
            text: `<p><strong>Our TFF Digital Labs is getting an upgrade! We will be offering new programs, partners, features, and functionalities—plus we’ll be announcing the launch of the TFF Challenge 2021! Stay tuned and get excited!</strong></p>
            <p>In the meantime, please go ahead and get involved with our activities and community. We have live conversations with inspiring speakers from around the world, and TFF Topical Weeks where we take on a trending topic to get past the hype and high-level messages that dominate headlines and webinars.
            Together, we will create new insights and research, be inspired, and share lessons, ideas, and opportunities with fellow burgeoning innovators and entrepreneurs in all parts of the world.</p>
            <p>Welcome to TFF!</p>`,
            video: ''
        },
        howItWorks: {
            title: 'About the TFF Digital Labs',
            text: `<p><strong>When we built the TFF Digital Labs, we wanted to make the knowledge from within
            our global network freely available to anyone from anywhere at any time. It is
            our aim to reinvent how and where solutions to the most pressing challenges
            facing our food systems are developed and taken forward.</strong></p>

            <p>The TFF Digital Labs is a mobile-optimised and highly-scalable digital platform addresses the specific needs and expectations of the next generation of agrifood innovators everywhere on the planet.</p>

            <p>Focused on real impact, it helps empower passionate young people with the skills, mindsets, and connections they require to build and lead successful social-impact companies in the 21st century. The TFF Digital Labs features <strong>smart content</strong> that is well-placed to define and inspire the future of food and agriculture.</p>`,
            video: ''
        },
        judging: {
            title: 'Judging criteria',
            text:
                "<p>Here's what we're looking for.</p><h3>Innovative solutions</h3><p>TFF commits to being on the cusp of new idea generation. Leverage breakthrough technologies and business models to create a project that is fresh and exciting, in a way that has never been seen before.</p><h3>Implementation & scalability</h3><p>Food security solutions require short, medium, and long-term goals that shouldn’t end when our competition is over. The project should be implementable, with scalability potential that puts sustainability first. </p><h3>Uniqueness</h3><p>The project should stand out among the wide-array of proposals we see, providing game-changing solutions and embodying a “wow” factor in terms of presentation, so that it gets noticed in a noisy world of innovation and startups.</p><h3>Team spirit</h3><p>Being an innovator and an entrepreneur is one of the most exciting and rewarding things one can do. However, it is also a long and demanding journey. The team needs to demonstrate that it is able to withstand these challenges and deliver a solution that will improve lives.</p>",
            card: {
                title: 'Have what it takes?',
                text: '',
                image: '/home-card-2.png',
                cta: {
                    url: '/signup',
                    label: 'Join us',
                    tracking: {
                        category: 'Sign up',
                        action: 'click',
                        label: 'Home – Judging card'
                    }
                }
            }
        },
        finalists: {
            title: "Meet last year's finalists",
            text: '',
            images: [
                {
                    text: 'Laticin',
                    image: '/finalist-1.jpg'
                },
                {
                    text: 'Likabs Food',
                    image: '/finalist-2.jpg'
                }
            ]
        },
        partners: {
            title: 'Our partners',
            text:
                'We collaborate with leaders in innovation to provide exclusive perks and resources for our participants.',
            logos: [
                {
                    title: 'Arigbusiness Academy',
                    image: '/partners/agribusiness-academy.png'
                },
                {
                    title: 'Blinkist',
                    image: '/partners/blinkist.png'
                },
                {
                    title: 'Sentinel Hub',
                    image: '/partners/sentinel-hub.png'
                },
                {
                    title: 'Startup Guts',
                    image: '/partners/startup-guts.png'
                },
                {
                    title: 'The Beam',
                    image: '/partners/the-beam.png'
                },
                {
                    title: 'Wikifactory',
                    image: '/partners/wikifactory.png'
                }
            ]
        },
        textBanner: {
            title: 'Up to the challenge?',
            text: 'Join our global community of next-gen changemakers',
            cta: {
                url: '/signup',
                label: 'Sign up for free',
                tracking: {
                    category: 'Sign up',
                    action: 'click',
                    label: 'Home – Footer'
                }
            }
        }
    };

    return (
        <Layout>
            <SEO
                title="TFF Digital Labs"
                description="The TFF Digital Labs embodies a new approach to global learning, startup acceleration, and collaboration. Sign up for free today!"
            />
            <HomePageLayout {...content} user={user} />
        </Layout>
    );
};

const mapStateToProps = state => ({
    user: state.user
});

export default withAuthSync(connect(mapStateToProps)(IndexPage), false);
