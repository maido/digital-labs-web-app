// @flow
import React, {useEffect, useState} from 'react';
import axios from 'axios';
import {connect} from 'react-redux';
import take from 'lodash/take';
import {
    updateNetwork,
    updateNotifications,
    updateSyllabusContent,
    updateSyllabusFetches
} from '../store/actions';
import {withAuthSync} from '../globals/auth';
import DashboardPageLayout from '../components/DashboardPageLayout';
import Layout from '../components/Layout';
import SEO from '../components/SEO';

type Props = {
    allLabs: SyllabusContent,
    allTopics: SyllabusContent,
    dispatch: Function,
    network: NetworkStatus,
    user: User
};

const DashboardPage = ({allLabs, allTopics, dispatch, network, user}: Props) => {
    const [nextTopicId, setNextTopicId] = useState(null);
    const [featuredLabs, setFeaturedLabs] = useState([]);

    useEffect(() => {
        async function fetchData() {
            if (typeof nextTopicId !== 'number') {
                dispatch(updateNetwork({isLoading: true}));

                try {
                    const response = await axios.get(
                        `/api/labs/all-topics?state=incomplete&limit=1`
                    );
                    const {data} = response;

                    if (data && data.length > 0) {
                        setNextTopicId(data[0].id);
                        dispatch(updateSyllabusContent('topics', data));
                    }

                    dispatch(updateNetwork({isLoading: false}));
                } catch (e) {
                    dispatch(updateNetwork({isLoading: false}));
                }
            }

            if (!allLabs || !allLabs.hasFetched) {
                try {
                    const response = await axios.get(`/api/labs/syllabus`);
                    const {labs, modules} = response.data;

                    if (labs) {
                        dispatch(updateSyllabusContent('labs', labs));
                        dispatch(updateSyllabusFetches('labs', true));
                    }

                    if (modules) {
                        dispatch(updateSyllabusContent('modules', modules));
                    }
                } catch (e) {
                    console.log(e);
                }
            }

            try {
                const response = await axios.get(`/api/notifications`);
                const {data} = response;

                dispatch(updateNotifications(data));
            } catch (e) {
                console.log(e);
            }
        }

        fetchData();
        setFeaturedLabs(take(allLabs.allIds, 4).map(id => allLabs.byId[id]));
    }, [allLabs.allIds.length, allLabs.hasFetched]);

    return (
        <Layout>
            <SEO title="Your dashboard" />
            <DashboardPageLayout
                network={network}
                nextTopic={nextTopicId ? allTopics.byId[nextTopicId] : null}
                nextTopicIsLoading={network.isLoading}
                featuredLabs={featuredLabs}
                user={user}
            />
        </Layout>
    );
};

const mapStateToProps = state => ({
    allLabs: state.syllabus.labs,
    allTopics: state.syllabus.topics,
    network: state.network,
    user: state.user
});

export default withAuthSync(connect(mapStateToProps)(DashboardPage));
