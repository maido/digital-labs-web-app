// @flow
import React, {useEffect} from 'react';
import {useRouter} from 'next/router';
import axios from 'axios';
import get from 'lodash/get';
import size from 'lodash/size';
import {connect} from 'react-redux';
import {updateNetwork, updatePagination, updateMentors} from '../../store/actions';
import {withAuthSync} from '../../globals/auth';
import Layout from '../../components/Layout';
import MentorsListPageLayout from '../../components/MentorsListPageLayout';
import SEO from '../../components/SEO';

type Props = {
    dispatch: Function,
    network: NetworkStatus,
    pagination: Pagination,
    mentors: Mentors
};

const MentorsListingPage = ({dispatch, network, mentors}: Props) => {
    const router = useRouter();

    useEffect(() => {
        async function fetchData() {
            dispatch(updateNetwork({isLoading: true}));

            try {
                const response = await axios.get(`/api/mentors`);
                const {data} = response;

                dispatch(updateMentors(data.data));
                dispatch(updateNetwork({isLoading: false}));
            } catch (e) {
                dispatch(updateNetwork({isLoading: false}));
            }
        }

        if (!mentors.allIds.length || mentors.allIds.length === 1 || !mentors.hasFetched) {
            fetchData();
        }
    }, []);

    return (
        <Layout>
            <SEO title="Mentors" />
            <MentorsListPageLayout network={network} mentors={mentors} />
        </Layout>
    );
};

const mapStateToProps = state => ({
    network: state.network,
    mentors: state.mentors
});

export default withAuthSync(connect(mapStateToProps)(MentorsListingPage));
