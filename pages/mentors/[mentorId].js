// @flow
import React, {useEffect, useState} from 'react';
import {useRouter} from 'next/router';
import axios from 'axios';
import {connect} from 'react-redux';
import {withAuthSync} from '../../globals/auth';
import {updateNetwork, updateMentors} from '../../store/actions';
import Layout from '../../components/Layout';
import MentorDetailPageLayout from '../../components/MentorDetailPageLayout';
import SEO from '../../components/SEO';

type Props = {
    dispatch: Function,
    mentors: Mentors,
    network: NetworkStatus
};

const MentorDetailPage = ({dispatch, network, mentors}: Props) => {
    const [mentor, setMentor] = useState(null);
    const router = useRouter();
    const {mentorId} = router.query;

    let breadcrumbNav;

    if (mentor) {
        breadcrumbNav = [
            {label: 'Mentors', url: '/mentors'},
            {
                label: `${mentor.first_name} ${mentor.last_name}`,
                url: {href: '/mentors/[mentorId]', as: `/mentors/${mentor.id}`}
            }
        ];
    }

    useEffect(() => {
        async function fetchData() {
            dispatch(updateNetwork({isLoading: true}));

            try {
                const response = await axios.get(`/api/mentors/${mentorId}`);
                const {data} = response;

                setMentor(data);
                dispatch(updateMentors([data]));
                dispatch(updateNetwork({isLoading: false}));
            } catch (e) {
                dispatch(updateNetwork({isLoading: false}));
            }
        }
        if (mentors && mentors.allIds.includes(mentorId)) {
            setMentor(mentors.byId[mentorId]);
        } else {
            fetchData();
        }
    }, []);

    return (
        <Layout>
            <SEO title={mentor ? `${mentor.first_name} ${mentor.last_name}` : ''} />
            {mentor ? (
                <MentorDetailPageLayout
                    breadcrumbNav={breadcrumbNav}
                    network={network}
                    mentor={mentor}
                />
            ) : null}
        </Layout>
    );
};

const mapStateToProps = state => ({
    mentors: state.mentors,
    network: state.network
});

export default withAuthSync(connect(mapStateToProps)(MentorDetailPage));
