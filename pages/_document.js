// @flow
import React from 'react';
import Document, {Html, Head, Main, NextScript} from 'next/document';
import {Global} from '@emotion/core';
import Helmet from 'react-helmet';
import {styles} from '../globals/styles';

class MyDocument extends Document {
    static async getInitialProps(ctx: Object) {
        const initialProps = await Document.getInitialProps(ctx);

        return {...initialProps, helmet: Helmet.renderStatic()};
    }

    helmetHtmlAttrComponents() {
        return this.props.helmet.htmlAttributes.toComponent();
    }

    helmetBodyAttrComponents() {
        return this.props.helmet.bodyAttributes.toComponent();
    }

    helmetHeadComponents() {
        // $FlowFixMe
        return Object.keys(this.props.helmet)
            .filter(el => el !== 'htmlAttributes' && el !== 'bodyAttributes')
            .map(el => this.props.helmet[el].toComponent());
    }

    render() {
        const cookieBotKey = process.env.NEXT_STATIC_COOKIEBOT_KEY;

        return (
            <Html {...this.helmetHtmlAttrComponents}>
                <Head>
                    <Global styles={styles} />
                    {this.helmetHeadComponents}
                    <meta
                        name="description"
                        content="The TFF Digital Labs embodies a new approach to global learning, startup acceleration, and collaboration. Sign up for free today!"
                    />
                    <meta property="og:title" content="TFF Digital Labs" />
                    <meta
                        property="og:description"
                        content="The TFF Digital Labs embodies a new approach to global learning, startup acceleration, and collaboration. Sign up for free today!"
                    />
                    <meta
                        property="og:image"
                        content="https://tffdigitallabs.org/academy-and-finalists.jpg"
                    />
                </Head>
                <body {...this.helmetBodyAttrComponents}>
                    <Main />
                    <div id="portals" />
                    <NextScript />
                </body>
            </Html>
        );
    }
}

export default MyDocument;
