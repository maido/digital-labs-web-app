// @flow
import React from 'react';
import {connect} from 'react-redux';
import Router from 'next/router';
import atob from 'atob';
import {updateForm} from '../../store/actions';
import Layout from '../../components/Layout';
import ResetPasswordPageLayout from '../../components/ResetPasswordPageLayout';
import SEO from '../../components/SEO';

type Props = {
    dispatch: Function,
    email: string,
    token: string
};

const ResetPasswordPage = ({dispatch, email, token}: Props) => {
    dispatch(
        updateForm({
            fields: {password: '', password_confirmation: '', email, token},
            name: 'reset-password',
            state: 'default'
        })
    );

    return (
        <>
            <SEO title="Reset your password" />
            <Layout theme="secondary" hasSiteHeader={false} hasSiteFooter={false}>
                <ResetPasswordPageLayout />
            </Layout>
        </>
    );
};

ResetPasswordPage.getInitialProps = ({query, req, res}) => {
    let token;

    if (query.token) {
        token = JSON.parse(atob(query.token));
    }

    /**
     * If the user is signed-in, redirect backto the dashboard
     */
    if (!token) {
        if (req) {
            res.writeHead(302, {Location: '/login'});
            res.end();
        } else {
            Router.replace('/login');
        }
    } else {
        return {
            email: token.email,
            token: token.token
        };
    }
};

export default connect()(ResetPasswordPage);
