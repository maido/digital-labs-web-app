// @flow
import React from 'react';
import Head from 'next/head';
import {withAuthSync} from '../globals/auth';
import Container from '../components/Container';
import Layout from '../components/Layout';
import SEO from '../components/SEO';
import Spinner from '../components/Spinner';
import * as UI from '../components/UI/styles';

const CookiePolicyPage = () => {
    const cookieBotKey = process.env.NEXT_STATIC_COOKIEBOT_KEY;

    return (
        <Layout theme="tertiary">
            <SEO title="Cookie Policy" />
            <Container size="medium" paddingVertical="l">
                <UI.Heading2 as="h1" css={UI.centerText}>
                    Cookie Policy
                </UI.Heading2>

                <UI.Spacer />

                <div>
                    {cookieBotKey && (
                        <script
                            id="CookieDeclaration"
                            src={`https://consent.cookiebot.com/${cookieBotKey}/cd.js`}
                            type="text/javascript"
                            key="cookiebot-review"
                            async
                        ></script>
                    )}
                </div>
            </Container>
        </Layout>
    );
};

export default withAuthSync(CookiePolicyPage, false);
