// @flow
import map from 'lodash/map';
import flatMap from 'lodash/flatMap';
import axios from 'axios';
import {handleFetchError} from '../../../globals/functions';

export default async (req, res) => {
    try {
        const response = await axios.get(
            `${process.env.NEXT_SERVER_API_URL}api/courses/${req.query.id}`,
            {
                headers: {Authorization: `Bearer ${req.cookies.token}`}
            }
        );
        const {data} = response.data;

        /**
         * Return module without topics content, just topic IDs. We can then match them out to separated
         * topic content after. This removes duplicated, nested data in the store.
         */
        const module = data.module;
        const lab = data.lab;
        const course = {
            ...data,
            lab: lab.id,
            module: module.id,
            topics: map(data.topics, t => t.id),
            completed_topics: map(data.completed_topics, t => t.id)
        };
        const topics = data.topics;

        res.status(200).json({course, lab, module, topics});
    } catch (error) {
        handleFetchError(error, res);
    }
};
