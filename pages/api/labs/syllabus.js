// @flow
import filter from 'lodash/filter';
import map from 'lodash/map';
import flatMap from 'lodash/flatMap';
import axios from 'axios';
import {handleFetchError} from '../../../globals/functions';

export default async (req, res) => {
    try {
        const response = await axios.get(`${process.env.NEXT_SERVER_API_URL}api/labs`, {
            headers: {Authorization: `Bearer ${req.cookies.token}`}
        });
        const {data} = response.data;

        /**
         * Return labs without module content, just module IDs. We can then match them out to separated
         * module content after. This removes duplicated, nested data in the store.
         */
        const labs = data.map(lab => ({
            ...lab,
            modules: map(lab.modules, m => m.id),
            completed_modules: map(lab.completed_modules, m => m.id)
        }));
        const modules = flatMap(filter(data, l => l.modules.length).map(l => l.modules)).map(
            module => ({
                ...module,
                course: [],
                completed_courses: []
            })
        );

        res.status(200).json({labs, modules});
    } catch (error) {
        handleFetchError(error, res);
    }
};
