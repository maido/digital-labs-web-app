// @flow
import map from 'lodash/map';
import flatMap from 'lodash/flatMap';
import axios from 'axios';
import {handleFetchError} from '../../../globals/functions';

export default async (req, res) => {
    let content = {};

    try {
        const response = await axios.get(
            `${process.env.NEXT_SERVER_API_URL}api/modules/${req.query.id}`,
            {
                headers: {Authorization: `Bearer ${req.cookies.token}`}
            }
        );
        const {data} = response.data;

        /**
         * Return module without course content, just course IDs. We can then match them out to separated
         * course content after. This removes duplicated, nested data in the store.
         */

        const lab = data.lab;
        const module = {
            ...data,
            lab: lab.id,
            courses: map(data.courses, c => c.id),
            completed_courses: map(data.completed_courses, c => c.id)
        };
        const courses = map(data.courses, course => ({
            ...course,
            topics: course.topics.map(t => t.id),
            completed_topics: course.completed_topics.map(t => t.id)
        }));
        const topics = map(data.courses, course => course.topics.map(t => t));

        res.status(200).json({lab, module, courses, topics});
    } catch (error) {
        handleFetchError(error, res);
    }
};
