// @flow
import axios from 'axios';
import {handleFetchError} from '../../../globals/functions';

export default async (req, res) => {
    try {
        const response = await axios.post(
            `${process.env.NEXT_SERVER_API_URL}api/topics/${req.query.id}/actions/read`,
            {},
            {
                headers: {Authorization: `Bearer ${req.cookies.token}`}
            }
        );

        /**
         * Successful requests return empty. Add a custom message here to prevent an error from the empty return;
         */
        res.status(200).json({data: {success: true}});
    } catch (error) {
        handleFetchError(error, res);
    }
};
