// @flow
import get from 'lodash/get';
import axios from 'axios';
import omit from 'lodash/omit';
import {handleFetchError} from '../../../globals/functions';

export default async (req, res) => {
    try {
        const page = get(req, 'query.page', 1);
        const limit = get(req, 'query.limit', 200);

        const response = await axios.get(
            `${process.env.NEXT_SERVER_API_URL}api/mentors?page=${page}&limit=${limit}`,
            {
                headers: {Authorization: `Bearer ${req.cookies.token}`}
            }
        );
        const {data, links, meta} = response.data;

        res.status(200).json({data, links, meta});
    } catch (error) {
        handleFetchError(error, res);
    }
};
