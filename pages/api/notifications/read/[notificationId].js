// @flow
import axios from 'axios';
import {handleFetchError} from '../../../../globals/functions';

export default async (req, res) => {
    try {
        const response = await axios.delete(
            `${process.env.NEXT_SERVER_API_URL}api/notifications${
                req.query.notificationId ? `/${req.query.notificationId} ` : ''
            }`,
            {
                headers: {Authorization: `Bearer ${req.cookies.token}`}
            }
        );
        const {data} = response.data;

        res.status(200).json(data);
    } catch (error) {
        handleFetchError(error, res);
    }
};
