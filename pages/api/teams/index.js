// @flow
import get from 'lodash/get';
import axios from 'axios';
import omit from 'lodash/omit';
import {handleFetchError} from '../../../globals/functions';

export default async (req, res) => {
    try {
        const name = get(req, 'query.name', '');
        const country = get(req, 'query.country', '');
        const page = get(req, 'query.page', 1);
        const limit = get(req, 'query.limit', 50);

        const response = await axios.get(
            // TODO: Remove `encodeURIComponent when this issue is resolved: https://github.com/axios/axios/issues/1111
            //  Note: We're encoding the team name to ensure symbols are encoded correctly.
            `${process.env.NEXT_SERVER_API_URL}api/teams?filter[name]=${encodeURIComponent(name)}&filter[country]=${country}&page=${page}&limit=${limit}`,
            {
                headers: {Authorization: `Bearer ${req.cookies.token}`}
            }
        );
        const {data, links, meta} = response.data;
        const formattedData = data.map(item => omit(item, ['pitch', 'created_at', 'updated_at']));

        res.status(200).json({data: formattedData, links, meta});
    } catch (error) {
        handleFetchError(error, res);
    }
};
