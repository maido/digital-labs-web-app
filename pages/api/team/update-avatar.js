// @flow
import axios from 'axios';
import {handleFetchError} from '../../../globals/functions';

export default async (req, res) => {
    try {
        const response = await axios.post(
            `${process.env.NEXT_SERVER_API_URL}api/teams/${req.query.id}/avatars`,
            {avatar: req.file},
            {
                headers: {
                    Authorization: `Bearer ${req.cookies.token}`,
                    'Content-Type': 'multipart/form-data'
                }
            }
        );
        const {data} = response.data;

        res.status(200).json(data);
    } catch (error) {
        handleFetchError(error, res);
    }
};
