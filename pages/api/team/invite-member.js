// @flow
import axios from 'axios';
import {handleFetchError} from '../../../globals/functions';

export default async (req, res) => {
    try {
        const response = await axios.post(
            `${process.env.NEXT_SERVER_API_URL}api/teams/${req.query.id}/invitations`,
            req.body,
            {
                headers: {Authorization: `Bearer ${req.cookies.token}`}
            }
        );

        res.status(200).json({success: true});
    } catch (error) {
        console.log(error);
        handleFetchError(error, res);
    }
};
