// @flow
import axios from 'axios';
import {handleFetchError, getPitchActions} from '../../../globals/functions';

export default async (req, res) => {
    let {token} = req.cookies;

    if (!token) {
        token = req.query.token;
    }

    try {
        const response = await axios.get(`${process.env.NEXT_SERVER_API_URL}api/users/me`, {
            headers: {Authorization: `Bearer ${token}`}
        });
        const {data} = response;
        const pitchActions = await getPitchActions(
            process.env.NEXT_SERVER_PITCH_SUBMISSION_DEADLINE
        );

        /**
         * TODO: Figure out why these arrays are strings...
         */
        const formattedData = data.data;

        ['areas_of_interest', 'areas_of_learning'].map(field => {
            if (typeof formattedData[field] === 'string') {
                formattedData[field] = JSON.parse(formattedData[field]);
            }
        });

        if (pitchActions) {
            formattedData.pitch = pitchActions;
        }

        res.status(200).json(formattedData);
    } catch (error) {
        handleFetchError(error, res);
    }
};
