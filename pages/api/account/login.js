// @flow
import axios from 'axios';
import {handleFetchError} from '../../../globals/functions';

export default async (req, res) => {
    try {
        const response = await axios.post(`${process.env.NEXT_SERVER_API_URL}oauth/token`, {
            grant_type: 'password',
            client_id: process.env.NEXT_SERVER_API_CLIENT_ID,
            client_secret: process.env.NEXT_SERVER_API_CLIENT_SECRET,
            username: req.body.email,
            password: req.body.password,
            scope: ''
        });
        const {data} = response;

        res.status(200).json(data);
    } catch (error) {
        handleFetchError(error, res);
    }
};
