// @flow
import get from 'lodash/get';
import axios from 'axios';
import {handleFetchError} from '../../../globals/functions';

export default async (req, res) => {
    try {
        let type = get(req, 'query.type', '');
        const limit = get(req, 'query.limit', 200);
        const page = get(req, 'query.page', 1);

        if (type === 'All') {
            type = '';
        }

        const response = await axios.get(
            `${process.env.NEXT_SERVER_API_URL}api/resources?filter[type]=${type}&page=${page}&limit=${limit}`,
            {
                headers: {Authorization: `Bearer ${req.cookies.token}`}
            }
        );

        res.status(200).json(response.data);
    } catch (error) {
        handleFetchError(error, res);
    }
};
