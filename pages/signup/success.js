// @flow
import React, {useEffect} from 'react';
import {useRouter} from 'next/router';
import {connect} from 'react-redux';
import axios from 'axios';
import Cookies from 'js-cookie';
import nextCookies from 'next-cookies';
import __ from '../../globals/strings';
import bugsnagClient from '../../globals/bugsnag';
import {updateUser} from '../../store/actions';
import FullScreenMessage from '../../components/FullScreenMessage';
import Layout from '../../components/Layout';
import Spinner from '../../components/Spinner';
import SEO from '../../components/SEO';

type Props = {
    dispatch: Function,
    invite?: AccountInviteCookie,
    user: User
};

const SignupSuccessPage = ({dispatch, invite, user}: Props) => {
    const router = useRouter();
    const {access_token} = router.query;

    useEffect(() => {
        async function setToken() {
            if (access_token) {
                Cookies.set('token', access_token, {
                    expires: 1,
                    path: '/'
                    // httpOnly: process.env.NODE_ENV === 'production'
                });

                try {
                    const response = await axios.get('/api/account/me');

                    dispatch(updateUser(response.data));
                    setTimeout(() => router.push('/signup/onboarding'), 2000);
                } catch (e) {
                    if (bugsnagClient) {
                        bugsnagClient.notify(e);
                    }
                }
            } else {
                window.location.href = '/';
            }
        }

        setToken();
    }, []);

    return (
        <Layout theme="secondary" hasSiteHeader={false} hasSiteFooter={false}>
            <SEO title="Account created" />
            <FullScreenMessage
                state="visible"
                badge="generic-1"
                title={__('account.signup.success.title')}
                text={
                    invite
                        ? __('account.signup.success.textInvitation')
                        : __('account.signup.success.text')
                }
            />
        </Layout>
    );
};

SignupSuccessPage.getInitialProps = ({req}) => {
    const cookies = nextCookies({req});

    if (cookies.invite) {
        return {invite: JSON.parse(cookies.invite)};
    }
};

const mapStateToProps = state => ({user: state.user});

export default connect(mapStateToProps)(SignupSuccessPage);
