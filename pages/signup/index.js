// @flow
import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import Router from 'next/router';
import nextCookies from 'next-cookies';
import axios from 'axios';
import map from 'lodash/map';
import zipObject from 'lodash/zipObject';
import Cookies from 'js-cookie';
import accountCreationSchema from '../../schemas/create-account';
import {updateForm} from '../../store/actions';
import {getSchemaDefaultFieldValues} from '../../globals/functions';
import Layout from '../../components/Layout';
import SignupPageLayout from '../../components/SignupPageLayout';
import SEO from '../../components/SEO';

type Props = {
    dispatch: Function,
    form: Object,
    token?: string
};

const SignupPage = ({dispatch, form, token}: Props) => {
    const [invite, setInvite] = useState(null);
    const isEmpty = Object.keys(form.fields).length === 0;

    if (isEmpty || form.name !== 'signup') {
        const fields = getSchemaDefaultFieldValues({
            ...accountCreationSchema.step1.fields,
            ...accountCreationSchema.step2.fields
        });

        dispatch(
            updateForm({
                fields: {...fields, invite_token: token},
                name: 'signup',
                state: 'default',
                step: 1
            })
        );
    }

    useEffect(() => {
        async function fetchData() {
            try {
                const response = await axios.get(`/api/invitations?token=${token}`);
                const {data} = response;

                setInvite(data);
                Cookies.set('invite', JSON.stringify({team: data.team.id, token}));
            } catch (e) {
                alert("Sorry, we can't find your invite");
            }
        }

        if (token) {
            fetchData();
        }
    }, []);

    return (
        <Layout hasSiteFooter={false} hasSiteHeader={false}>
            <SEO title="Signup to the TFF Challenge" />
            <SignupPageLayout invite={invite} key={invite ? 'has-invite' : 'no-invite'} />
        </Layout>
    );
};

SignupPage.getInitialProps = ({query, reduxStore, req, res}) => {
    const state = reduxStore.getState();

    /**
     * If the user is signed-in then redict back to the dashboard
     */
    if (state.user.isAuthenticated) {
        if (req) {
            res.writeHead(302, {Location: '/dashboard'});
            res.end();
        } else {
            Router.replace('/dashboard');
        }
    } else {
        let token;

        if (query.token) {
            token = query.token;
        } else {
            const cookies = nextCookies({req});

            if (cookies.invite) {
                token = cookies.invite;
            }
        }

        return {token};
    }
};

const mapStateToProps = state => ({form: state.form});

export default connect(mapStateToProps)(SignupPage);
