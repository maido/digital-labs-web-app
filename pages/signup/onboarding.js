// @flow
import React from 'react';
import Router from 'next/router';
import get from 'lodash/get';
import nextCookies from 'next-cookies';
import {withAuthSync} from '../../globals/auth';
import Layout from '../../components/Layout';
import OnboardingPageLayout from '../../components/OnboardingPageLayout';
import SEO from '../../components/SEO';

type Props = {
    invite?: AccountInviteCookie
};

const OnboardingPage = ({invite}: Props) => (
    <Layout>
        <SEO title="Onboarding" />
        <OnboardingPageLayout invite={invite} />
    </Layout>
);

OnboardingPage.getInitialProps = ({reduxStore, req}) => {
    const cookies = nextCookies({req});
    const state = reduxStore.getState();

    if (cookies.invite) {
        const invite = JSON.parse(cookies.invite);

        if (invite.team === get(state, 'user.team.id')) {
            return {invite};
        }
    }
};

export default withAuthSync(OnboardingPage);
