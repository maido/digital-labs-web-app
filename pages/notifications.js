// @flow
import React, {useEffect} from 'react';
import axios from 'axios';
import {connect} from 'react-redux';
import {updateNetwork, updateNotifications} from '../store/actions';
import {withAuthSync} from '../globals/auth';
import NotificationsPageLayout from '../components/NotificationsPageLayout';
import Layout from '../components/Layout';
import SEO from '../components/SEO';

type Props = {
    dispatch: Function,
    network: NetworkStatus,
    notifications: Notifications
};

const NotificationshboardPage = ({dispatch, network, notifications}: Props) => {
    useEffect(() => {
        async function fetchData() {
            dispatch(updateNetwork({isLoading: true}));

            try {
                const response = await axios.get(`/api/notifications`);
                const {data} = response;

                dispatch(updateNotifications(data));
                dispatch(updateNetwork({isLoading: false}));
            } catch (e) {
                console.log(e);
                dispatch(updateNetwork({isLoading: false}));
            }
        }

        fetchData();
    }, []);

    return (
        <Layout>
            <SEO title="Notifications" />
            <NotificationsPageLayout network={network} notifications={notifications} />
        </Layout>
    );
};

const mapStateToProps = state => ({
    network: state.network,
    notifications: state.notifications
});

export default withAuthSync(connect(mapStateToProps)(NotificationshboardPage));
