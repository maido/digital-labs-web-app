declare module 'next' {
    declare module.exports: any;
}

declare module 'next/router' {
    declare module.exports: any;
}

declare module 'next/app' {
    declare module.exports: any;
}

declare module 'next/dynamic' {
    declare module.exports: any;
}

declare module 'next/document' {
    declare module.exports: any;
}

declare module 'next/link' {
    declare module.exports: any;
}
