declare module 'gatsby-link' {
    declare type Props = {
        children: any,
        to: string
    };

    declare class GatsbyLink extends React$Component<Props> {}
    
    declare export default typeof GatsbyLink;
}

type GatsbyImage = {
    aspectRatio?: number,
    base64?: string,
    height?: number,
    sizes?: string,
    src?: string,
    srcWebp?: string,
    srcSet?: string,
    srcSetWebp?: string,
    tracedSVG?: string,
    width?: number
};


