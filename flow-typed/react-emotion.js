declare module 'react-emotion' {
    declare class Emotion {
        static injectGlobal(): Function;
    }
}
