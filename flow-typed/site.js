// @flow

// Process

declare class process {
    static env: {
        NEXT_SERVER_API_URL: string,
        NEXT_SERVER_API_CLIENT_ID: string,
        NEXT_SERVER_API_CLIENT_SECRET: string,
        NEXT_SERVER_BUGSNAG_CLIENT_ID?: string,
        NEXT_SERVER_BUGSNAG_REACT_CLIENT_ID?: string,
        NEXT_SERVER_UPLOADS_API_URL?: string,
        NEXT_SERVER_PITCH_SUBMISSION_DEADLINE?: string,
        NEXT_STATIC_COOKIEBOT_KEY?: string,
        NEXT_STATIC_GOOGLE_ANALYTICS_ID?: string,
        PORT: number,
        NODE_ENV: string
    };
    static exit(number: number): void;
}

// Data

type Avatar = {
    id?: number,
    file_name?: string,
    original_name?: string,
    url?: string,
    thumb_url?: string,
    hero_url?: string,
    mime_type?: string,
    size?: number,
    formatted_size?: string
};

type BlocksItem = {
    content: string,
    position: number,
    type: 'text' | 'image' | 'quote' | 'video',
    hero?: string,
    body?: string,
    citation?: string
};

type Topic = {
    id: number,
    title: string,
    intro: string,
    difficulty: string,
    theme: string,
    position: number,
    completed_at: string,
    resources?: Array<Resource>,
    blocks?: Array<BlocksItem>,
    assignments?: Array<number | Assignment>,
    completed_assignments?: Array<number | Assignment>,
    course: Course | number,
    lab: number | Lab,
    module: number | Module
};

type Course = {
    id: number,
    hero?: string,
    position: number,
    title: string,
    headline?: string,
    theme?: string,
    topics?: Array<number | Topic>,
    completed_topics?: Array<number | Topic>
    topics_count?: number,
    completed_topics_count?: number,
};

type Assignment = {};

type Resource = {
    id: number,
    title: string,
    type: string,
    url: string
};

type Resources = {
    byId: Resource,
    allIds: Array<number>,
    type: string,
    hasFetched: boolean
};

type Submission = {
    created_at: string,
    file: Avatar,
    id: number,
    reflections?: string,
    topic: Topic
};

type Submissions = {};

type Teams = {
    byId: Team,
    allIds: Array<number>,
    filter: {
        name: string,
        country: string
    },
    hasFetched: boolean
};

type Module = {
    id: number,
    hero?: string,
    position: number,
    title: string,
    headline?: string,
    courses?: Array<number | Course>,
    completed_courses?: Array<number | Course>,
    courses_count?: number,
    topics_count?: number,
    completed_topics_count?: number,
};

type Lab = {
    id: number,
    hero?: string,
    position: number,
    title: string,
    headline?: string,
    modules?: Array<number | Module>,
    completed_modules?: Array<number | Module>,
    modules_count?: number,
    topics_count?: number,
    completed_topics_count?: number,
};

type Form = {
    fields: Object,
    name: string,
    state: 'default' | 'pending' | 'success',
    step?: number
};

type NetworkStatus = {
    hasError: boolean,
    isLoading: boolean,
    isSuccessful: boolean
};

type Notifications = {
    allIds: {
        read: Array<number>,
        unread: Array<number>
    },
    byId: Array<Notification>,
    hasFetched: boolean
};

type Notification = {
    id: number,
    title: string,
    content: string,
    model_type: string,
    model_id: number,
    read_at?: string,
    created_at: string,
    updated_at: string
};

type Pitch = {
    id: number,
    reflections?: string,
    categories?: Array<string>,
    video?: string,
    documents?: Array<string>,
    submitted_at?: string
};

type SyllabusContent = {
    byId: Object,
    allIds: Array<string>,
    hasFetched: boolean
};

type Syllabus = {
    title: string,
    slug?: string,
    description: string,
    image: string,
    labs: SyllabusContent,
    modules: SyllabusContent,
    courses: SyllabusContent,
    topics: SyllabusContent
};

type Team = {
    id?: number,
    name: string,
    mission?: string,
    avatar?: Avatar,
    pitch?: Pitch,
    members?: Array<{
        id?: number,
        first_name: string,
        last_name: string,
        avatar: Avatar,
        country?: string
    }>,
    created_at?: string,
    updated_at?: string
};

type User = {
    isAuthenticated?: boolean,
    team: Team,
    id: number,
    first_name: string,
    last_name: string,
    birthdate: string,
    email: string,
    country_origin: string,
    country_residence: string,
    location?: string,
    university?: string,
    field_of_expertise?: string,
    occupation?: string,
    linkedin?: string,
    avatar?: Avatar,
    degree_date?: string,
    graduation_date?: string,
    areas_of_interest?: Array<string>,
    areas_of_learning?: Array<string>,
    heard_via?: string,
    pitch?: {
        daysRemaining: number,
        canSubmit: boolean
    }
};

type Pagination = {
    activePage: number,
    currentItems: number,
    fetchedPages: Object,
    itemsCountPerPage: number,
    onChange?: Function,
    pageRangeDisplayed: number,
    skip: number,
    totalItemsCount: number,
    type?: string
};

type AccountInvite = {
    email: string,
    invited_by: User,
    team: Team
};

type AccountInviteCookie = {
    team: number,
    token: string
};

// UI

type EventTracking = {
    action: string,
    category: string,
    label?: string
};

type CTA = {
    to?:
        | string
        | {
              href: string,
              as: string
          },
    label: string,
    onClick?: Function,
    track?: EventTracking
};

type TeamSummaryUICard = {
    avatar?: string | null,
    editUrl?: string,
    mission?: string,
    footnote?: string,
    members?: Array<{
        id?: number,
        first_name: string,
        last_name: string,
        avatar: Avatar,
        country?: string
    }>,
    handleClick?: Function,
    title?: string,
    user?: User,
    teamId?: number
};

type LearningContentUICard = {
    completed?: boolean,
    difficulty?: 'Beginner' | 'Intermediate' | 'Advanced',
    headline?: string,
    image?: string,
    index?: string,
    isNew?: boolean,
    level?: string,
    items?: {
        all: number,
        completed: number,
        type: string
    },
    size?: string,
    status?: string,
    title: string,
    url?: {
        href: string,
        as: string
    }
};
