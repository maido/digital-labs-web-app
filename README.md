# Thought For Food - Digital Labs - Web App

Consequat minim pariatur incididunt duis ex culpa velit exercitation incididunt fugiat dolore sint nostrud dolor nulla mollit veniam.

## ⬇️ Installing / Getting started

To get started, cd into your working/dev folder.

```shell
git clone git@bitbucket.org:maido/digital-labs-web-app.git
cd digital-labs-web-app
```

## 🛠 Stack

### JavaScript

### CSS

## 🖥 Developing

### Linting

The project uses [Flow](https://flow.org/) and [Prettier](https://github.com/prettier/prettier). These are tools that can be used from the command line and/or as part of a build system, but work even better when use with your favourite editor (i.e. Sublime) as they format and validate your code as you type/save.

These instructions are for Sublime, but there are alternatives available for other editors or IDEs too.

-   [http://www.sublimelinter.com/en/stable/](http://www.sublimelinter.com/en/stable/)
-   [https://github.com/SublimeLinter/SublimeLinter-flow](https://github.com/SublimeLinter/SublimeLinter-flow)
-   [https://github.com/jonlabelle/SublimeJsPrettier](https://github.com/jonlabelle/SublimeJsPrettier)

### Building

### Testing

### Deploying / Publishing

<a name="notable_features"></a>

## 🌟 Notable Features

### Syllabus contents

Syllabus is the main grouping of all learning content. The group consists of:

- **Syllabus**: All labs
- **Lab**: A single track of learning, containing one or more courses
- **Course**: A focused track of learning, containing one or more modules
- **Module**: A specific piece of learning, containing one of more topics
- **Topic**: An actionalbe piece of learning

##### Syllabus

- **Route** `/labs`
- **Page** `/pages/labs/index.js`
- **Layout** `/components/LabsListingPageLayout/index.js`
- **API** `/pages/api/labs/syllabus.js`

##### Lab

- **Route** `/labs/[labId]`
- **Page** `/pages/labs/[labId].js`
- **Layout** `/components/LabsDetailPageLayout/index.js`
- **API** `/pages/api/labs/syllabus.js`

##### Course

- **Route** `/labs/courses/[courseId]`
- **Page** `/pages/labs/courses/[courseId].js`
- **Layout** `/components/CoursesDetailPageLayout/index.js`
- **API** `/pages/api/labs/course.js`

##### Module

- **Route** `/labs/modules/[moduleId]`
- **Page** `/pages/labs/modules/[moduleId].js`
- **Layout** `/components/ModulesDetailPageLayout/index.js`
- **API** `/pages/api/labs/module.js`

##### Topic

- **Route** `/labs/topics/[topicId]`
- **Page** `/pages/labs/topics/[topicId].js`
- **Layout** `/components/TopicsDetailPageLayout/index.js`
- **API** `/pages/api/labs/topic.js`

## 👋 Contributing

If you are adding a new feature or fix to the project please follow these steps:

-   Checkout master and pull all latest changes
-   Create a new branch (`git checkout -b (fix|feature)/[branch name]`). For example: `git checkout -b feature/product-listing`.
-   Do your typing and write your code
-   When ready to go, create a Pull Request to Staging detailing the changes made with an updated Changelog and Readme if required
-   Once code reviewed, deploy changes to the Staging store and test here
-   Once approved, create a Pull Request to Master
-   Once code reviewed, deploy changes to Production store
-   Smoke test changes on Production
-   Done!

## 💬 Discussion

Any questions can be raised in the [#tff](https://maidoteam.slack.com/messages/CGQFG7VV5) Slack channel.
