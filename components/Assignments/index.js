// @flow
import React, {useCallback, useRef, useState} from 'react';
import find from 'lodash/find';
import {connect} from 'react-redux';
import __ from '../../globals/strings';
import {getFileTypeFromMimeType} from '../../globals/functions';
import {updateSyllabusContent} from '../../store/actions';
import Accordion from '../Accordion';
import AssignmentUpload from '../AssignmentUpload';
import CardLayout from '../CardLayout';
import FullScreenMessage from '../FullScreenMessage';
import TextCard from '../TextCard';
import * as UI from '../UI/styles';
import * as S from './styles';

type Props = {
    assignments: Array<Object>,
    completed_assignments: Array<Object>,
    courseId: number,
    dispatch: Function,
    title: string,
    topicId: number
};

const Assignments = ({
    assignments,
    completed_assignments,
    courseId,
    dispatch,
    title,
    topicId
}: Props) => {
    const [modalVisibility, setModalVisibility] = useState('hidden');
    const allCompleted = assignments.length === completed_assignments.length;

    const handleSubmitSuccess = assignment => {
        /**
         * Once the assignment has a submission, update the store to show it's new state
         */
        dispatch(
            updateSyllabusContent('topics', [
                {
                    id: topicId,
                    completed_assignments: [...completed_assignments, ...[assignment]]
                }
            ])
        );
        setModalVisibility('visible');
    };

    return (
        <>
            {modalVisibility !== 'hidden' && (
                <FullScreenMessage
                    badge="generic-3"
                    cta={{
                        label: 'Back to all tasks',
                        onClick: () => {
                            /**
                             * Force a refresh to reset cached syllabus data
                             */
                            window.location = `/labs/courses/${courseId}`;
                        }
                    }}
                    key={topicId}
                    state={modalVisibility}
                    title={__(
                        `learningContent.assignment.success.${
                            allCompleted ? 'titleAllCompleted' : 'title'
                        }`
                    )}
                    text={__(
                        `learningContent.assignment.success.${
                            allCompleted ? 'textAllCompleted' : 'text'
                        }`
                    )}
                />
            )}

            <S.Header>
                <span css={[UI.textColor('greyDark'), UI.textSize(16)]}>{title}</span>

                <UI.Heading4 style={{margin: '6px 0 2px'}}>
                    This topic has {assignments.length}{' '}
                    {assignments.length > 1 ? 'assignments' : 'assignment'}
                </UI.Heading4>

                <strong
                    css={[
                        UI.textColor(allCompleted ? 'aquaMarine' : 'mediumPink'),
                        UI.textSize(14)
                    ]}
                >
                    {completed_assignments.length}/{assignments.length} Completed
                </strong>
            </S.Header>
            <S.ListContainer>
                {assignments.map((assignment, index) => {
                    const hasWorksheets = assignment.worksheets && assignment.worksheets.length;
                    const isComplete = find(completed_assignments, {id: assignment.id});

                    return (
                        <S.ListItem key={`assignment-${assignment.id}`}>
                            <Accordion
                                isDisabled={isComplete}
                                header={
                                    <>
                                        <UI.Heading5 css={UI.margin('bottom', 'xs')}>
                                            Assignment {index + 1}
                                        </UI.Heading5>
                                        <span
                                            style={{lineHeight: 1.4}}
                                            dangerouslySetInnerHTML={{
                                                __html: assignment.description
                                            }}
                                        />
                                    </>
                                }
                            >
                                {hasWorksheets > 0 && (
                                    <>
                                        <CardLayout
                                            items={assignment.worksheets.map(
                                                (worksheet, index) => ({
                                                    key: worksheet.id,
                                                    component: (
                                                        <TextCard
                                                            url={worksheet.url}
                                                            title={`Worksheet ${index + 1}`}
                                                            footnote={`${getFileTypeFromMimeType(
                                                                worksheet.mime_type
                                                            )}—${worksheet.formatted_size}`}
                                                            icon="download"
                                                        />
                                                    )
                                                })
                                            )}
                                        />
                                        <UI.Spacer />
                                    </>
                                )}

                                {!isComplete && (
                                    <>
                                        <strong>Complete your assignment</strong>
                                        <UI.Spacer size="s" />

                                        <AssignmentUpload
                                            assignment={assignment}
                                            handleSuccess={() => {
                                                handleSubmitSuccess(assignment);
                                            }}
                                        />
                                    </>
                                )}
                            </Accordion>

                            {isComplete && (
                                <UI.Flex
                                    css={[
                                        UI.textColor('aquaMarine'),
                                        UI.textSize(14),
                                        UI.margin('top', 'xs')
                                    ]}
                                >
                                    <S.CompleteIcon
                                        height="24"
                                        width="24"
                                        xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 24 24"
                                    >
                                        <path
                                            clipRule="evenodd"
                                            d="M21.652 3.211c-.293-.295-.77-.295-1.061 0L9.41 14.34c-.293.297-.771.297-1.062 0L3.449 9.351c-.145-.148-.335-.221-.526-.222-.193-.001-.389.072-.536.222L.222 11.297c-.144.148-.222.333-.222.526 0 .194.078.397.223.544l4.94 5.184c.292.296.771.776 1.062 1.07l2.124 2.141c.292.293.769.293 1.062 0l14.366-14.34c.293-.294.293-.777 0-1.071l-2.125-2.14z"
                                            fillRule="evenodd"
                                        />
                                    </S.CompleteIcon>
                                    <strong>Complete</strong>
                                </UI.Flex>
                            )}
                        </S.ListItem>
                    );
                })}
            </S.ListContainer>
        </>
    );
};

export default connect()(Assignments);
