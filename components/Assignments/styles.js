// @flow
import {css} from '@emotion/core';
import styled from '@emotion/styled';
import {rem} from 'polished';
import {responsiveRem} from '../../globals/functions';
import {breakpoints, colors, spacing, transitions} from '../../globals/variables';

export const Header = styled.header`
    padding: ${responsiveRem(spacing.m)};

    @media (min-width: ${rem(breakpoints.tablet)}) {
        padding: ${responsiveRem(spacing.m)} ${responsiveRem(spacing.l)};
    }
`;

export const ListContainer = styled.ul`
    list-style: none;
    margin: 0;
    padding: 0;
`;

export const ListItem = styled.li`
    background-color: ${colors.greyLightest};
    border-top: 1px solid ${colors.grey};
    padding: ${rem(spacing.s)} ${responsiveRem(spacing.m)};

    @media (min-width: ${rem(breakpoints.tablet)}) {
        padding: ${responsiveRem(spacing.s)} ${responsiveRem(spacing.l)};
    }
`;

export const WorksheetsIcon = styled.svg`
    height: auto;
    opacity: 0.6;
    margin-left: ${rem(spacing.s)};
    width: ${rem(14)};

    path {
        fill: currentColor;
    }
`;

export const CompleteIcon = styled.svg`
    height: auto;
    margin-right: ${rem(spacing.xs)};
    width: ${rem(spacing.s)};

    path {
        fill: currentColor;
    }
`;
