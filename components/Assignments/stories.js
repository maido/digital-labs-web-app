// @flow
import React from 'react';
import {addDecorator, storiesOf} from '@storybook/react';
import Assignment from './';
import Modal from '../Modal';
import Theme from '../Theme';

const stories = storiesOf('Assignment', module);

stories.addDecorator(story => (
    <Theme theme="tertiary">
        <Modal state="visible" containerPadding={false}>
            {story()}
        </Modal>
    </Theme>
));

stories.add('default', () => {
    return (
        <Assignment
            title="Amet tempor sint officia laborum voluptate"
            assignments={[
                {
                    id: 1,
                    description: 'Labore in do ut pariatur proident sit id.',
                    worksheets: [
                        {
                            id: 1,
                            mime_type: 'image/jpg',
                            formatted_size: '1mb',
                            url: 'https://www.google.com'
                        }
                    ]
                }
            ]}
            completed_assignments={[]}
        />
    );
});

stories.add('with multiple assignments', () => {
    return (
        <Assignment
            title="Amet tempor sint officia laborum voluptate"
            assignments={[
                {
                    id: 1,
                    description: 'Labore in do ut pariatur proident sit id.',
                    worksheets: [
                        {
                            id: 1,
                            mime_type: 'image/jpg',
                            formatted_size: '1mb',
                            url: 'https://www.google.com'
                        }
                    ]
                },
                {
                    id: 2,
                    description:
                        'Aute sint occaecat eiusmod id velit qui. Exercitation sint velit irure quis sunt fugiat incididunt sit duis ad exercitation.',
                    worksheets: [
                        {
                            id: 1,
                            mime_type: 'application/pdf',
                            formatted_size: '2mb',
                            url: 'https://www.google.com'
                        },
                        {
                            id: 1,
                            mime_type: 'application/pdf',
                            formatted_size: '2mb',
                            url: 'https://www.google.com'
                        }
                    ]
                }
            ]}
            completed_assignments={[]}
        />
    );
});

stories.add('with multiple worksheets', () => {
    return (
        <Assignment
            title="Amet tempor sint officia laborum voluptate"
            assignments={[
                {
                    id: 1,
                    description: 'Labore in do ut pariatur proident sit id.',
                    worksheets: [
                        {
                            id: 1,
                            mime_type: 'image/jpg',
                            formatted_size: '1mb',
                            url: 'https://www.google.com'
                        },
                        {
                            id: 1,
                            mime_type: 'application/pdf',
                            formatted_size: '2mb',
                            url: 'https://www.google.com'
                        }
                    ]
                }
            ]}
            completed_assignments={[]}
        />
    );
});

stories.add('with no worksheets', () => {
    return (
        <Assignment
            title="Amet tempor sint officia laborum voluptate"
            assignments={[
                {
                    id: 1,
                    description: 'Labore in do ut pariatur proident sit id.',
                    worksheets: []
                },
                {
                    id: 2,
                    description:
                        'Aute sint occaecat eiusmod id velit qui. Exercitation sint velit irure quis sunt fugiat incididunt sit duis ad exercitation.',
                    worksheets: []
                }
            ]}
            completed_assignments={[]}
        />
    );
});

stories.add('with a completed assignment', () => {
    return (
        <Assignment
            title="Amet tempor sint officia laborum voluptate"
            assignments={[
                {
                    id: 1,
                    description: 'Labore in do ut pariatur proident sit id.',
                    worksheets: []
                },
                {
                    id: 2,
                    description:
                        'Aute sint occaecat eiusmod id velit qui. Exercitation sint velit irure quis sunt fugiat incididunt sit duis ad exercitation.'
                }
            ]}
            completed_assignments={[{id: 1}]}
        />
    );
});

stories.add('with all assignments completed', () => {
    return (
        <Assignment
            title="Amet tempor sint officia laborum voluptate"
            assignments={[
                {
                    id: 1,
                    description: 'Labore in do ut pariatur proident sit id.',
                    worksheets: []
                },
                {
                    id: 2,
                    description:
                        'Aute sint occaecat eiusmod id velit qui. Exercitation sint velit irure quis sunt fugiat incididunt sit duis ad exercitation.',
                    worksheets: []
                }
            ]}
            completed_assignments={[{id: 1}, {id: 2}]}
        />
    );
});
