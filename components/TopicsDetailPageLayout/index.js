// @flow
import React, {useRef, useState} from 'react';
import Link from 'next/link';
import dynamic from 'next/dynamic';
import axios from 'axios';
import get from 'lodash/get';
import {logEvent} from '../../globals/analytics';
import __ from '../../globals/strings';
import {getContentStatus} from '../../globals/functions';
import AnimateWhenVisible from '../AnimateWhenVisible';
import BreadcrumbNav from '../BreadcrumbNav';
import CardLayout from '../CardLayout';
import LearningContentCard from '../LearningContentCard';
import Container from '../Container';
import ContentBlocks from '../ContentBlocks';
import CTAButton from '../CTAButton';
import CTATextBanner from '../CTATextBanner';
import PageIntro from '../PageIntro';
import BreadcrumbNavPlaceholder from '../Placeholder/BreadcrumbNav';
import PageIntroPlaceholder from '../Placeholder/PageIntro';
import ScrollIndicator from '../ScrollIndicator';
import SlackBanner from '../SlackBanner';
import Spinner from '../Spinner';
import Theme from '../Theme';
import * as UI from '../UI/styles';
import * as S from './styles';

const Assignments = dynamic(() => import('../Assignments'), {ssr: false});
const Modal = dynamic(() => import('../Modal'), {ssr: false});
const MarkAsComplete = dynamic(() => import('../MarkAsComplete'), {ssr: false});
const RelatedTopics = dynamic(() => import('../RelatedTopics'), {ssr: false});

type Props = {
    breadcrumbNav: Array<Object>,
    content: Object,
    hasFetched: boolean,
    module: Module,
    network: NetworkStatus,
    relatedTopics: Array<Topic>
};

const TopicsDetailPageLayout = ({
    breadcrumbNav,
    content,
    hasFetched,
    module,
    network,
    relatedTopics
}: Props) => {
    const [modalVisibility, setModalVisibility] = useState('hidden');
    const $contentRef = useRef(null);

    const {showContent, showFallback, showPlaceholders} = getContentStatus(
        network.isLoading,
        hasFetched,
        content,
        true
    );
    const assignments = get(content, 'assignments', []);
    const hasAssignments = assignments.length > 0;
    const completedAssignments = assignments.filter(a => a.completed_at);
    const allCompleted = assignments.length === completedAssignments.length;
    const hasCompletedAnAssignment =
        hasAssignments &&
        completedAssignments.length > 0 &&
        completedAssignments.length !== content.assignments.length;

    const handleScrollToContent = () => {
        if ($contentRef.current && typeof window !== 'undefined') {
            window.scrollTo({
                behavior: 'smooth',
                top: $contentRef.current.getBoundingClientRect().top
            });
        }
    };

    const handleOpenAssignments = (from = '') => {
        setModalVisibility('visible');
        logEvent({action: 'Click', category: 'Topic assignments', label: from});
    };

    const handleCloseAssignments = () => {
        setModalVisibility('leaving');
        setTimeout(() => setModalVisibility('hidden'), 600);
    };

    return (
        <>
            {modalVisibility !== 'hidden' && (
                <Modal
                    containerPadding={false}
                    state={modalVisibility}
                    handleClose={handleCloseAssignments}
                >
                    <Assignments
                        assignments={content.assignments}
                        completed_assignments={content.completed_assignments}
                        courseId={content.course.id}
                        title={content.title}
                        topicId={content.id}
                    />
                </Modal>
            )}

            <Container paddingVertical={0}>
                {showPlaceholders && <BreadcrumbNavPlaceholder />}
                {showContent && <BreadcrumbNav links={breadcrumbNav} />}
            </Container>

            <Container paddingVertical={0} css={S.hero} size="medium">
                {showPlaceholders && (
                    <>
                        <PageIntroPlaceholder hasSubtitle={true} />
                        <span id="scrollTo">
                            <Spinner />
                        </span>
                    </>
                )}
                {showContent && (
                    <>
                        <AnimateWhenVisible watch={false}>
                            <Link
                                href="/labs/courses/[courseId]"
                                as={`/labs/courses/${content.course.id}`}
                                passHref
                            >
                                <UI.Subheading as="a" css={UI.fadeIn} title="Back to course">
                                    {content.course.title}
                                </UI.Subheading>
                            </Link>

                            <UI.Spacer size="xs" />

                            <UI.Heading1 css={S.titleText}>{content.title}</UI.Heading1>
                        </AnimateWhenVisible>
                        <AnimateWhenVisible delay={300} watch={false}>
                            <S.IntroText dangerouslySetInnerHTML={{__html: content.intro}} />
                        </AnimateWhenVisible>

                        {hasAssignments && (
                            <>
                                {content.intro && <UI.Spacer />}
                                <CTAButton
                                    theme="white"
                                    blockAtMobile={false}
                                    onClick={() => handleOpenAssignments('Intro')}
                                >
                                    <span css={UI.textColor('black')}>
                                        {hasCompletedAnAssignment ? 'Continue' : 'View'} topic
                                        assignments
                                    </span>
                                </CTAButton>
                            </>
                        )}

                        <UI.ShowAt breakpoint="tablet">
                            <button id="scrollTo" type="button" onClick={handleScrollToContent}>
                                <ScrollIndicator />
                            </button>
                        </UI.ShowAt>
                    </>
                )}
            </Container>

            <Theme theme="white" overlap={content ? content.theme : 'secondary'} overlapSize={0}>
                <div ref={$contentRef}>
                    <Container css={S.contentBlocksContainer}>
                        {showContent && (
                            <ContentBlocks blocks={content.blocks} wrappedWithContainers={true} />
                        )}
                    </Container>
                </div>
            </Theme>

            {showContent && (
                <>
                    <Theme theme={content.theme}>
                        <Container paddingVertical="l">
                            <AnimateWhenVisible delay={100}>
                                {!hasAssignments && (
                                    <MarkAsComplete
                                        completedAt={content.completed_at}
                                        courseId={content.course.id}
                                        topicId={content.id}
                                        theme={content.theme}
                                    />
                                )}
                                {hasAssignments && (
                                    <CTATextBanner
                                        theme={content.theme}
                                        title={__('learningContent.assignment.title')}
                                        text={__(
                                            `learningContent.assignment.${
                                                allCompleted ? 'textAllCompleted' : 'text'
                                            }`
                                        )}
                                    >
                                        <CTAButton
                                            theme="white"
                                            onClick={() => handleOpenAssignments('Footer')}
                                        >
                                            <span css={UI.textColor('black')}>
                                                {__(
                                                    `learningContent.assignment.${
                                                        hasCompletedAnAssignment
                                                            ? 'continue'
                                                            : 'view'
                                                    }`
                                                )}
                                            </span>
                                        </CTAButton>
                                    </CTATextBanner>
                                )}
                            </AnimateWhenVisible>
                        </Container>
                    </Theme>

                    <RelatedTopics
                        courseId={content.course.id}
                        moduleTitle={module.title}
                        topicId={content.id}
                    />
                </>
            )}
            <SlackBanner trackingLabel={`topic - ${content ? content.title : ''}`} />
        </>
    );
};

export default TopicsDetailPageLayout;
