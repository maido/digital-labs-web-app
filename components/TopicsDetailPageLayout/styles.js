// @flow
import {css} from '@emotion/core';
import styled from '@emotion/styled';
import {between, rem} from 'polished';
import {responsiveRem} from '../../globals/functions';
import {breakpoints, fontFamilies, fontSizes, spacing} from '../../globals/variables';

export const hero = css`
    align-items: center;
    display: flex;
    flex-direction: column;
    justify-content: center;
    padding: ${rem(120)} 0 ${rem(140)};
    text-align: center;

    #scrollTo {
        bottom: 40px;
        left: 50%;
        position: absolute;
        transform: translateX(-50%);
    }

    @media (min-width: ${rem(breakpoints.mobile)}) and (max-width: ${rem(breakpoints.tablet)}) {
        max-width: ${rem(700)};
    }

    @media (min-width: ${rem(breakpoints.tablet)}) {
        min-height: calc(100vh - 70px);
    }

    @media (min-width: ${rem(breakpoints.desktop)}) {
        min-height: 80vh;
        padding-top: 0;
    }
`;

export const titleText = css`
    @media (min-width: ${rem(breakpoints.desktop)}) {
        font-size: ${between(`${fontSizes.h1 - 20}px`, `${fontSizes.h1 - 10}px`)};
    }
`;

export const IntroText = styled.p`
    line-height: 1.5;

    @media (min-width: ${rem(breakpoints.mobile)}) {
        font-size: ${rem(fontSizes.lead)};
    }
`;

export const contentBlocksContainer = css`
    padding: ${rem(spacing.m)} 0 !important;

    @media (min-width: ${rem(breakpoints.mobile)}) {
        padding: ${responsiveRem(spacing.m)} 0 !important;
    }

    @media (min-width: ${rem(breakpoints.desktop)}) {
        padding: ${responsiveRem(spacing.l)} 0 !important;
    }
`;
