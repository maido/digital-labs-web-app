// @flow
import styled from '@emotion/styled';
import {rem} from 'polished';
import {breakpoints, colors, fontFamilies, fontSizes, spacing} from '../../globals/variables';

export const Container = styled.div`
    background-image: url('/slack-bg.png');
    background-repeat: no-repeat;
    background-position: center center;
    background-size: contain;
    border-bottom: 2px solid ${colors.tertiary};
    padding-bottom: ${rem(spacing.l)};
    padding-top: ${rem(spacing.m)};
    text-align: center;
`;

export const Title = styled.span`
    display: block;
    font-family: ${fontFamilies.heading};
    font-style: normal;
    font-size: ${rem(28)};
    line-height: 1.2;
    margin-bottom: ${rem(spacing.xs)};
`;

export const SlackLogo = styled.svg`
    height: auto;
    width: ${rem(70)};
`;
