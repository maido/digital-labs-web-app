// @flow
import React from 'react';
import Link from 'next/link';
import {connect} from 'react-redux';
import ReactCountryFlag from 'react-country-flag';
import findKey from 'lodash/findKey';
import isEqual from 'lodash/isEqual';
import partial from 'lodash/partial';
import {countryList} from '../../globals/content';
import AnimateWhenVisible from '../AnimateWhenVisible';
import * as UI from '../UI/styles';
import * as S from './styles';

type Props = {
    authenticatedUser: User,
    user: User
};

export const LinkedInLink = ({url = ''}) => (
    <UI.FlexMediaWrapper>
        <S.Icon viewBox="0 0 64 64" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
            <path
                d="M2 6v52c0 2.2 1.8 4 4 4h52c2.2 0 4-1.8 4-4V6c0-2.2-1.8-4-4-4H6C3.8 2 2 3.8 2 6zm17.1 46H12V24.4h7.1V52zm-3.5-33.1c-2 0-3.6-1.5-3.6-3.4s1.6-3.4 3.6-3.4 3.6 1.5 3.6 3.4c-.1 1.9-1.7 3.4-3.6 3.4zM52 52h-7.1V38.2c0-2.9-.1-4.8-.4-5.7-.3-.9-.8-1.5-1.4-2-.7-.5-1.5-.7-2.4-.7-1.2 0-2.3.3-3.2 1-1 .7-1.6 1.6-2 2.7-.4 1.1-.5 3.2-.5 6.2V52h-8.6V24.4h7.1v4.1c2.4-3.1 5.5-4.7 9.2-4.7 1.6 0 3.1.3 4.5.9 1.3.6 2.4 1.3 3.1 2.2.7.9 1.2 1.9 1.4 3.1.3 1.1.4 2.8.4 4.9V52z"
                fill="#0077b5"
            />
        </S.Icon>

        <UI.FlexMediaContent css={S.teamContainer}>
            <UI.TextLink href={url} target="noopener" css={UI.textColor('darkGreyBlue')}>
                LinkedIn
            </UI.TextLink>
        </UI.FlexMediaContent>
    </UI.FlexMediaWrapper>
);

export const SlackLink = ({url = ''}) => (
    <UI.FlexMediaWrapper>
        <S.Icon xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" viewBox="0 0 512 512">
            <path
                d="M122.643 316.682c0 26.596-21.727 48.323-48.321 48.323-26.593 0-48.319-21.728-48.319-48.323 0-26.592 21.727-48.318 48.319-48.318h48.321v48.318zM146.996 316.682c0-26.592 21.728-48.318 48.321-48.318 26.593 0 48.32 21.727 48.32 48.318V437.68c0 26.592-21.728 48.319-48.32 48.319-26.594 0-48.321-21.728-48.321-48.319V316.682z"
                fill="#e01e5a"
            />
            <path
                d="M195.317 122.643c-26.594 0-48.321-21.728-48.321-48.321 0-26.593 21.728-48.32 48.321-48.32 26.593 0 48.32 21.728 48.32 48.32v48.321h-48.32zM195.317 146.997c26.593 0 48.32 21.727 48.32 48.321 0 26.593-21.728 48.318-48.32 48.318H74.321c-26.593 0-48.319-21.726-48.319-48.318 0-26.595 21.727-48.321 48.319-48.321h120.996z"
                fill="#36c5f0"
            />
            <path
                d="M389.359 195.318c0-26.595 21.725-48.321 48.32-48.321 26.593 0 48.318 21.727 48.318 48.321 0 26.593-21.726 48.318-48.318 48.318h-48.32v-48.318zM365.004 195.318c0 26.593-21.728 48.318-48.321 48.318-26.593 0-48.32-21.726-48.32-48.318V74.321c0-26.593 21.728-48.32 48.32-48.32 26.594 0 48.321 21.728 48.321 48.32v120.997z"
                fill="#2eb67d"
            />
            <path
                d="M316.683 389.358c26.594 0 48.321 21.727 48.321 48.321 0 26.592-21.728 48.319-48.321 48.319-26.593 0-48.32-21.728-48.32-48.319v-48.321h48.32zM316.683 365.005c-26.593 0-48.32-21.728-48.32-48.323 0-26.592 21.728-48.318 48.32-48.318H437.68c26.593 0 48.318 21.727 48.318 48.318 0 26.596-21.726 48.323-48.318 48.323H316.683z"
                fill="#ecb22e"
            />
        </S.Icon>
        <UI.FlexMediaContent css={S.teamContainer}>{url} on Slack</UI.FlexMediaContent>
    </UI.FlexMediaWrapper>
);

const UserDetailCard = ({authenticatedUser, user}: Props) => {
    const countryCode = findKey(countryList, partial(isEqual, user.country_residence));

    return (
        <S.Container>
            <UI.FlexMediaWrapper>
                <UI.Avatar image={user.avatar ? user.avatar.url : ''} size="large" />
                <UI.FlexMediaContent css={S.contentContainer}>
                    <S.Title css={UI.fadeInUp}>
                        {user.first_name} {user.last_name}
                    </S.Title>

                    {user.id === authenticatedUser.id && (
                        <Link href="/account/edit" passHref>
                            <UI.TextLink>Edit profile</UI.TextLink>
                        </Link>
                    )}

                    <UI.Spacer size="s" />

                    <UI.FlexMediaWrapper>
                        <UI.Avatar
                            image={user.team.avatar ? user.team.avatar.url : ''}
                            size="medium"
                        />
                        <UI.FlexMediaContent css={S.teamContainer}>
                            <UI.Label css={S.label}>Member of</UI.Label>
                            <br />
                            <Link href="/teams/[teamId]" as={`/teams/${user.team.id}`}>
                                <a>
                                    <strong css={UI.textColor('darkGreyBlue')}>
                                        {user.team.name}
                                    </strong>
                                </a>
                            </Link>
                        </UI.FlexMediaContent>
                    </UI.FlexMediaWrapper>
                </UI.FlexMediaContent>
            </UI.FlexMediaWrapper>

            <UI.Spacer />

            <UI.FlexMediaWrapper>
                <ReactCountryFlag code={countryCode} styleProps={{width: 20}} svg />
                <UI.FlexMediaContent css={S.teamContainer}>
                    {user.country_residence}
                </UI.FlexMediaContent>
            </UI.FlexMediaWrapper>

            {(user.linkedin || user.slack) && (
                <>
                    {user.linkedin && <LinkedInLink url={user.linkedin} />}
                    {user.slack && (
                        <>
                            <UI.Spacer size="xs" />
                            <SlackLink url={user.slack} />
                        </>
                    )}
                </>
            )}
        </S.Container>
    );
};

const mapStateToProps = state => ({
    authenticatedUser: state.user
});

export default connect(mapStateToProps)(UserDetailCard);
