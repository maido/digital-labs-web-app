// @flow
import {css} from '@emotion/core';
import styled from '@emotion/styled';
import {rem} from 'polished';
import {responsiveRem} from '../../globals/functions';
import {
    breakpoints,
    colors,
    fontFamilies,
    fontSizes,
    spacing,
    themes
} from '../../globals/variables';
import * as TeamSummaryStyles from '../TeamSummaryCard/styles';

/**
 * TODO: Make this card style reusable from a single component instead of duplicating work from the team card.
 */

export const Container = styled(TeamSummaryStyles.Container)`
    cursor: default;

    &:hover,
    &:focus {
        box-shadow: none;
        transform: none;
    }

    a {
        color: ${colors.purplishBlue};
    }
`;

export const contentContainer = css`
    margin-left: ${rem(spacing.m)};
`;

export const Title = styled.span`
    display: block;
    font-size: ${responsiveRem(fontSizes.h4)};
    font-family: ${fontFamilies.heading};
    line-height: 1.3;

    @media (max-width: ${rem(breakpoints.mobile)}) {
        font-size: ${rem(fontSizes.h3)};
    }
`;

export const teamContainer = css`
    line-height: 1.4;

    a {
        color: ${colors.white};
        vertical-align: text-top;
    }

    @media (min-width: ${rem(breakpoints.mobile)}) {
        margin-left: ${rem(spacing.s)};

        a {
            color: ${colors.darkGreyBlue};
        }
    }
`;

export const label = css`
    @media (min-width: ${rem(breakpoints.mobile)}) {
        color: ${colors.greyDark};
    }
`;

export const Icon = styled.svg`
    height: auto;
    width: ${rem(20)};
`;
