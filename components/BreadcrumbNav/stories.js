// @flow
import React from 'react';
import {addDecorator, storiesOf} from '@storybook/react';
import {withKnobs, object, select, text} from '@storybook/addon-knobs';
import BreadcrumbNav from './';
import Theme from '../Theme';

const themeOptions = {
    Primary: 'primary',
    Secondary: 'secondary',
    Tertiary: 'tertiary',
    White: 'white'
};
const stories = storiesOf('BreadcrumbNav', module);

stories.addDecorator(withKnobs);

stories.addDecorator(story => (
    <Theme theme={select('Theme', themeOptions, 'secondary')}>
        <div style={{padding: 20}}>{story()}</div>
    </Theme>
));

stories.add('default', () => {
    return <BreadcrumbNav links={[{label: 'Foo', url: '/'}, {label: 'Bar', url: '/'}]} />;
});

stories.add('with more links', () => {
    return (
        <BreadcrumbNav
            links={[{label: 'Foo', url: '/'}, {label: 'Bar', url: '/'}, {label: 'Baz', url: '/'}]}
        />
    );
});
