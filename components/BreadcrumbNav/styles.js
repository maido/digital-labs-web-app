// @flow
import {css} from '@emotion/core';
import styled from '@emotion/styled';
import {rem} from 'polished';
import {responsiveRem} from '../../globals/functions';
import {breakpoints, colors, fontFamilies, spacing} from '../../globals/variables';

export const Container = styled.nav`
    display: flex;
    flex-wrap: wrap;
    margin: ${responsiveRem(spacing.s)} 0 0;
    max-width: 80%;
    position: relative;
    font-family: ${fontFamilies.bold};

    @media (min-width: ${rem(breakpoints.mobile)}) {
        margin: 0 0 ${responsiveRem(spacing.s)};
    }

    @media (max-width: ${rem(breakpoints.mobile)}) {
        /**
         * On mobile, let's show the second last item in the breadcrumb. This is a workaround while we don't
         * have a dedicated mobile solution, otherwise there is no links showing for a nested page.
         *
         * If we're on page 2 of a 2 page nest, there is only 1 link and that's the first page.
         * Let's just hide it. And hide the separator from the link also!
         */
        ${props =>
            props.linksSize === 2 &&
            `
            a::after,
            span {
                display: none;
            }
        `}

        /**
         * On deeper nests, hide all but the last link, and the span that shows the current page.
         */
        ${props =>
            props.linksSize > 2 &&
            `
            span,
            a:not(:last-of-type),
            a::after {
                display: none;
            }
        `}
    }
`;

export const link = css`
    position: relative;

    &::after {
        content: '/';
        margin-left: ${rem(spacing.xs)};
    }

    & + * {
        margin-left: ${rem(spacing.xs)} !important;
    }
`;

export const ActiveLink = styled.span`
    display: inline-block;
    opacity: 0.5;
`;
