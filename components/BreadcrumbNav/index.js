// @flow
import * as React from 'react';
import Link from 'next/link';
import * as UI from '../UI/styles';
import * as S from './styles';

type Props = {
    links: Array<{
        label: string,
        url: string | Object
    }>
};

const BreadcrumbNav = ({links}: Props) => (
    <S.Container linksSize={links.length}>
        {links.map((link, index) => {
            if (index === links.length - 1) {
                return <S.ActiveLink key={link.label}>{link.label}</S.ActiveLink>;
            } else {
                if (typeof link.url === 'string') {
                    return (
                        <Link key={link.label} href={link.url} passHref>
                            <UI.TextLink css={S.link} title={`Go back to ${link.label}`}>
                                {link.label}
                            </UI.TextLink>
                        </Link>
                    );
                } else {
                    return (
                        <Link key={link.label} href={link.url.href} as={link.url.as} passHref>
                            <UI.TextLink css={S.link} title={`Go back to ${link.label}`}>
                                {link.label}
                            </UI.TextLink>
                        </Link>
                    );
                }
            }
        })}
    </S.Container>
);

export default BreadcrumbNav;
