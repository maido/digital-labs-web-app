// @flow
import {css} from '@emotion/core';
import styled from '@emotion/styled';
import {rem} from 'polished';
import {responsiveRem} from '../../globals/functions';
import {
    breakpoints,
    colors,
    fontFamilies,
    fontSizes,
    shadows,
    spacing,
    themes
} from '../../globals/variables';

export const Container = styled.div`
    background-color: ${themes.white.background};
    box-shadow: ${shadows.default};
    border-radius: ${rem(5)};
    color: ${themes.white.text};
    overflow: hidden;

    @media (min-width: ${rem(breakpoints.mobile)}) {
        display: flex;
        flex-direction: row-reverse;
    }
`;

export const ContentContainer = styled.div`
    padding: ${responsiveRem(spacing.m)};

    @media (min-width: ${rem(breakpoints.mobile)}) {
        flex: 0 0 60%;
        padding: ${responsiveRem(spacing.m * 1.5)};
    }

    @media (min-width: ${rem(breakpoints.tablet)}) {
        flex: 0 0 55%;
    }

    @media (min-width: ${rem(breakpoints.desktop)}) {
        flex: 0 0 50%;
    }

    @media (min-width: ${rem(breakpoints.mobile)}) {
        display: flex;
        flex-direction: column;
        justify-content: center;
        min-height: 350px;
    }
`;

export const Category = styled.span`
    color: ${colors.greyDark};
    display: block;
    margin-bottom: ${rem(spacing.xs)};
`;

export const ButtonContainer = styled.div`
    @media (max-width: ${rem(breakpoints.mobileSmall)}) {
        a {
            display: block;
        }
    }
    @media (min-width: ${rem(breakpoints.mobileSmall)}) {
        display: flex;

        a + a {
            margin-left: ${rem(spacing.s)};
        }
    }
`;

export const secondaryCTA = css`
    color: ${colors.purplishBlue} !important;

    @media (max-width: ${rem(breakpoints.mobileSmall)}) {
        margin-top: ${rem(spacing.s)};
        text-align: center !important;
    }
`;

export const Image = styled.div`
    ${props => props.image && `background-image: url('${props.image}');`}
    background-position: center center;
    background-repeat: no-repeat;
    background-size: cover;
    width: 100%;

    @media (max-width: ${rem(breakpoints.mobile)}) {
        height: ${responsiveRem(160)};
    }
`;
