// @flow
import * as React from 'react';
import {logEvent} from '../../globals/analytics';
import CTAButton from '../CTAButton';
import DifficultyIndicator from '../DifficultyIndicator';
import HeroContentCardPlaceholder from '../Placeholder/HeroContentCard';
import * as S from './styles';
import * as UI from '../UI/styles';

type Props = {
    category?: string,
    intro?: string,
    image?: Object,
    isLoading?: boolean,
    primaryCTA?: CTA,
    secondaryCTA?: CTA,
    title?: string
};

const HeroContentCard = ({
    category,
    intro,
    image,
    isLoading,
    primaryCTA,
    secondaryCTA,
    title
}: Props) => {
    if (isLoading) {
        return (
            <S.Container>
                <HeroContentCardPlaceholder />
            </S.Container>
        );
    } else {
        return (
            <S.Container>
                <S.Image image={image && image.url ? image.url : '/topic-placeholder.png'} />
                <S.ContentContainer css={UI.fadeIn}>
                    {category && <S.Category>{category}</S.Category>}
                    {title && <UI.Heading4 as="h3">{title}</UI.Heading4>}
                    {intro && <p dangerouslySetInnerHTML={{__html: intro}} />}
                    {(primaryCTA || secondaryCTA) && (
                        <S.ButtonContainer>
                            {primaryCTA && (
                                <CTAButton
                                    href={primaryCTA.url}
                                    wide={true}
                                    track={{
                                        action: 'Click',
                                        category: 'Next Topic Card',
                                        label: 'Dashboard'
                                    }}
                                >
                                    {primaryCTA.label}
                                </CTAButton>
                            )}
                            {secondaryCTA && (
                                <CTAButton
                                    href={secondaryCTA.url}
                                    basic={true}
                                    css={S.secondaryCTA}
                                >
                                    {secondaryCTA.label}

                                    <svg
                                        width="19"
                                        height="8"
                                        xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 19 8"
                                        className="arrow-icon"
                                    >
                                        <path d="M19 4l-4-4v3H0v2h15v3z" fillRule="evenodd" />
                                    </svg>
                                </CTAButton>
                            )}
                        </S.ButtonContainer>
                    )}
                </S.ContentContainer>
            </S.Container>
        );
    }
};

export default HeroContentCard;
