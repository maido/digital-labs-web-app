// @flow
import React from 'react';
import {addDecorator, storiesOf} from '@storybook/react';
import {withKnobs, boolean, object, select, text} from '@storybook/addon-knobs';
import HeroContentCard from './';
import Theme from '../Theme';

const stories = storiesOf('HeroContentCard', module);

stories.addDecorator(withKnobs);

stories.add('default', () => {
    return (
        <HeroContentCard
            category="Spira: Biohacking 101"
            intro="A round up all of the facts that Elliot Roth, Founder of Spira collected about how to get started teaching yourself the basic tenets of synthetic biology, setting up a laboratory space, starting a biotech business, common events, and resources you need to get started."
            image="https://placehold.it/600x600"
            primaryCTA={{
                label: 'Start topic',
                url: '/topics/x'
            }}
            secondaryCTA={{
                label: 'View all topics',
                url: '/topics/all'
            }}
            title="Lab Session with Elliot Roth"
        />
    );
});

stories.add('loading state', () => {
    return <HeroContentCard category="" intro="" image="" title="" isLoading={true} />;
});

stories.add('with only primary CTA', () => {
    return (
        <HeroContentCard
            category="Spira: Biohacking 101"
            intro="A round up all of the facts that Elliot Roth, Founder of Spira collected about how to get started teaching yourself the basic tenets of synthetic biology, setting up a laboratory space, starting a biotech business, common events, and resources you need to get started."
            image="https://placehold.it/600x600"
            primaryCTA={{
                label: 'Start topic',
                url: '/topics/x'
            }}
            title="Lab Session with Elliot Roth"
        />
    );
});

stories.add('with no image', () => {
    return (
        <HeroContentCard
            category="Spira: Biohacking 101"
            intro="A round up all of the facts that Elliot Roth, Founder of Spira collected about how to get started teaching yourself the basic tenets of synthetic biology, setting up a laboratory space, starting a biotech business, common events, and resources you need to get started."
            primaryCTA={{
                label: 'Start topic',
                url: '/topics/x'
            }}
            title="Lab Session with Elliot Roth"
        />
    );
});

stories.add('playground', () => {
    return (
        <HeroContentCard
            category={text('Category', 'Spira: Biohacking 101')}
            intro={text(
                'intro',
                'A round up all of the facts that Elliot Roth, Founder of Spira collected about how to get started teaching yourself the basic tenets of synthetic biology, setting up a laboratory space, starting a biotech business, common events, and resources you need to get started.'
            )}
            image={text('Image', 'https://placehold.it/600x600')}
            primaryCTA={object('Primary CTA', {
                label: 'Start topic',
                url: '/topics/x'
            })}
            secondaryCTA={object('Secondary CTA', {
                label: 'View all topics',
                url: '/topics/all'
            })}
            title={text('Title', 'Lab Session with Elliot Roth')}
            isLoading={boolean('Is loading', false)}
        />
    );
});
