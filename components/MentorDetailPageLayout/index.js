// @flow
import React from 'react';
import get from 'lodash/get';
import AnimateWhenVisible from '../AnimateWhenVisible';
import BreadcrumbNav from '../BreadcrumbNav';
import Container from '../Container';
import MentorDetailCard from '../MentorDetailCard';
import Theme from '../Theme';
import * as UI from '../UI/styles';

type Props = {
    breadcrumbNav?: Array<Object>,
    network: NetworkStatus,
    mentor: Mentor
};

const MentorDetailPageLayout = ({breadcrumbNav, network, mentor}: Props) => (
    <>
        <Container paddingVertical={false}>
            {breadcrumbNav && <BreadcrumbNav links={breadcrumbNav} />}
        </Container>

        <Theme theme="tertiary" overlap="secondary" overlapSize={150} grow={true}>
            <UI.Spacer />
            <Container paddingVertical="l">
                <UI.LayoutContainer size="l">
                    <UI.LayoutItem sizeAtMobile={5 / 12} css={UI.stickyAt('mobile')}>
                        <AnimateWhenVisible delay={-1}>
                            <MentorDetailCard
                                avatar={mentor.avatar ? mentor.avatar.url : null}
                                linkedin={mentor.linkedin}
                                links={mentor.links}
                                name={`${mentor.first_name} ${mentor.last_name}`}
                                slack={mentor.slack}
                            />
                        </AnimateWhenVisible>
                        <UI.Spacer />
                    </UI.LayoutItem>

                    <UI.LayoutItem sizeAtMobile={7 / 12}>
                        <AnimateWhenVisible delay={-1}>
                            <UI.Box radius={20}>
                                <UI.Heading3>Information</UI.Heading3>

                                <UI.LayoutContainer type="matrix">
                                    <UI.LayoutItem sizeAtMobile={6 / 12}>
                                        <strong>Company</strong>
                                        <br />
                                        {mentor.company}
                                    </UI.LayoutItem>
                                    <UI.LayoutItem sizeAtMobile={6 / 12}>
                                        <strong>Job Title</strong>
                                        <br />
                                        {mentor.job_title}
                                    </UI.LayoutItem>
                                    <UI.LayoutItem sizeAtMobile={6 / 12}>
                                        <strong>Field of expertise</strong>
                                        <br />
                                        {get(mentor, 'field_of_expertise', '-')}
                                    </UI.LayoutItem>
                                </UI.LayoutContainer>
                            </UI.Box>
                        </AnimateWhenVisible>
                    </UI.LayoutItem>
                </UI.LayoutContainer>
            </Container>
        </Theme>
    </>
);

export default MentorDetailPageLayout;
