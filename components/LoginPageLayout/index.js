// @flow
import React, {useEffect, useRef, useState} from 'react';
import {useSpring, animated} from 'react-spring/web.cjs';
import Link from 'next/link';
import dynamic from 'next/dynamic';
import {useRouter} from 'next/router';
import axios from 'axios';
import Cookie from 'js-cookie';
import get from 'lodash/get';
import {connect} from 'react-redux';
import {Formik, Form} from 'formik';
import {logEvent} from '../../globals/analytics';
import __ from '../../globals/strings';
import {updateForm, updateUser} from '../../store/actions';
import loginSchema from '../../schemas/login';
import AnimateWhenVisible from '../AnimateWhenVisible';
import Container from '../Container';
import CTAButton from '../CTAButton';
import InputText from '../InputText';
import Logo from '../Logo';
import * as UI from '../UI/styles';
import * as S from './styles';

const FullScreenMessage = dynamic(() => import('../FullScreenMessage'), {ssr: false});

type Props = {
    dispatch: Function,
    form: Form
};

const getLoginErrorMessage = error => {
    const data = get(error, 'response.data');
    let errors = {};

    if (data) {
        if (data.username) {
            errors.email = data.username;
        } else if (data.error) {
            errors.password = data.error;
        }
    } else if (error.error) {
        errors.password = error.error;
    } else {
        errors.password = `${__('account.login.errors.login')} (${error.response.status})`;
    }

    return errors;
};

const LoginPageLayout = ({dispatch, form}: Props) => {
    const router = useRouter();
    const {redirect} = router.query;
    const $passwordContainer = useRef(null);
    const [formType, setFormType] = useState('login');
    const [resetSuccessVisibility, setResetSuccessVisibility] = useState('hidden');
    const logoAnimation = useSpring({
        config: {tension: 200, mass: 5, friction: 30},
        from: {transform: `scale(1.1)`, opacity: 0},
        to: {transform: `scale(1)`, opacity: 1}
    });
    const [passwordFieldAnimation, setPasswordFieldAnimation] = useSpring(() => ({
        from: {height: 'auto', opacity: 1}
    }));

    const authenticate = async ({access_token}) => {
        if (access_token) {
            Cookie.set('token', access_token, {
                expires: 31,
                path: '/'
                // httpOnly: process.env.NODE_ENV === 'production'
            });

            try {
                const response = await axios.get('/api/account/me');

                dispatch(updateUser(response.data));
                dispatch(updateForm({fields: {}, state: 'success', step: 2}));

                if (get(response, 'data.team.name')) {
                    router.replace({pathname: redirect ? redirect : '/dashboard'});
                } else {
                    router.replace({pathname: '/team/create'});
                }
            } catch (e) {
                console.log(e);
            }
        } else {
            return false;
        }
    };

    const getElementHeight = ref => (ref.current ? ref.current.getBoundingClientRect().height : 0);

    const toggleForgotPassword = (type, setFieldValue) => {
        setFieldValue('type', type);
        setFormType(type);

        setPasswordFieldAnimation({
            to: async next => {
                const height = getElementHeight($passwordContainer);

                if (type === 'forgotPassword') {
                    await next({height, opacity: 1, overflow: 'hidden'});
                    await next({height: 0, opacity: 0, overflow: 'hidden'});
                    await next({display: 'none'});
                } else {
                    await next({display: 'block'});
                    await next({height: 0, opacity: 0, overflow: 'hidden'});
                    await next({height: 100, opacity: 1, overflow: 'visible'});
                    await next({height: 'auto'});
                }
            }
        });
    };

    const passwordResetFormSubmit = async (values, handlers) => {
        dispatch(updateForm({state: 'pending', fields: values}));

        try {
            const response = await axios.post(`/api/account/forgot-password`, {
                email: values.email
            });

            handlers.setSubmitting(false);
            setResetSuccessVisibility('visible');
        } catch (error) {
            dispatch(updateForm({state: 'default'}));

            const errors = get(error, 'response.data')
                ? get(error, 'response.data')
                : {
                      email: `${__('account.errors.passwordReset')} (${get(
                          error,
                          'response.status'
                      )})`
                  };
            handlers.setErrors(errors);
            handlers.setSubmitting(false);
        }
    };

    const loginFormSubmit = async (values, handlers) => {
        dispatch(updateForm({state: 'pending', fields: values}));

        try {
            const response = await axios.post(`/api/account/login`, values);

            authenticate(response.data);
        } catch (error) {
            dispatch(updateForm({state: 'default'}));

            const errors = getLoginErrorMessage(error);

            handlers.setErrors(errors);
            handlers.setSubmitting(false);
        }
    };

    const handleFormSubmit = async (values, handlers) => {
        if (formType === 'login') {
            loginFormSubmit(values, handlers);
        } else {
            passwordResetFormSubmit(values, handlers);
        }
    };

    const handleResetRefresh = () => {
        window.location = window.location.href;
    };

    useEffect(() => {
        dispatch(
            updateForm({
                fields: {email: '', password: '', type: 'login'},
                name: 'login',
                state: 'default'
            })
        );
    }, []);

    return (
        <Container css={S.container}>
            {resetSuccessVisibility !== 'hidden' && (
                <FullScreenMessage
                    state="visible"
                    title={__('account.resetPassword.title')}
                    text={__('account.resetPassword.text')}
                    cta={{
                        label: __('account.resetPassword.ctaLabel'),
                        url: '/login',
                        onClick: handleResetRefresh
                    }}
                />
            )}

            <UI.ResponsiveSpacer />

            <animated.div style={logoAnimation}>
                <Link href="/">
                    <a>
                        <Logo />
                    </a>
                </Link>
            </animated.div>

            <S.MainContent>
                <AnimateWhenVisible delay={300}>
                    <UI.ResponsiveSpacer size="l" />
                    <UI.Heading3 as="h1" css={S.pageHeading}>
                        {__('account.login.title')}
                    </UI.Heading3>
                </AnimateWhenVisible>

                <Formik
                    initialValues={form.fields}
                    onSubmit={handleFormSubmit}
                    validationSchema={loginSchema}
                    validateOnChange={false}
                    render={formProps => (
                        <>
                            <S.FormContainer>
                                {' '}
                                <Form autoComplete="off">
                                    <InputText
                                        name="email"
                                        label="Your email"
                                        placeholder="jane@gmail.com"
                                    />

                                    <animated.div
                                        style={passwordFieldAnimation}
                                        css={S.passwordContainer}
                                        ref={$passwordContainer}
                                    >
                                        <UI.Spacer />
                                        <button
                                            type="button"
                                            onClick={() => {
                                                toggleForgotPassword(
                                                    'forgotPassword',
                                                    formProps.setFieldValue
                                                );
                                                logEvent({
                                                    action: 'click',
                                                    category: 'Forgot Password toggle',
                                                    label: 'Login - Form'
                                                });
                                            }}
                                            tabIndex="-1"
                                            theme="greyDark"
                                            css={[
                                                S.forgottenPasswordLink,
                                                formProps.submitCount >= 3 ? UI.shake : null
                                            ]}
                                        >
                                            {__('account.login.passwordResetLabel')}
                                        </button>

                                        <InputText
                                            name="password"
                                            label="Your password"
                                            type="password"
                                            disabled={form.status === 'success'}
                                        />
                                    </animated.div>

                                    <UI.Spacer size="s" />

                                    <CTAButton
                                        disabled={formProps.isSubmitting || formProps.isValidating}
                                        type="submit"
                                        state={
                                            formProps.isSubmitting || formProps.isValidating
                                                ? 'pending'
                                                : form.state
                                        }
                                        block={true}
                                    >
                                        {__(
                                            formType === 'login'
                                                ? 'account.login.ctaLabel'
                                                : 'account.login.passwordResetCtaLabel'
                                        )}
                                    </CTAButton>
                                </Form>
                            </S.FormContainer>
                            <S.SignupContainer>
                                {formType === 'login' ? (
                                    <Link href="/signup" passHref>
                                        <UI.TextLink
                                            onClick={() => {
                                                logEvent({
                                                    action: 'click',
                                                    category: 'Sign up',
                                                    label: 'Login - Form'
                                                });
                                            }}
                                            dangerouslySetInnerHTML={{
                                                __html: __('account.login.signupLabel')
                                            }}
                                        />
                                    </Link>
                                ) : (
                                    <UI.TextLink
                                        onClick={() =>
                                            toggleForgotPassword('login', formProps.setFieldValue)
                                        }
                                    >
                                        Back to login
                                    </UI.TextLink>
                                )}
                            </S.SignupContainer>
                        </>
                    )}
                />
            </S.MainContent>
        </Container>
    );
};

const mapStateToProps = state => ({form: state.form});

export default connect(mapStateToProps)(LoginPageLayout);
