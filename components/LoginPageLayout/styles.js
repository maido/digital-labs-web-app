// @flow
import {css, keyframes} from '@emotion/core';
import styled from '@emotion/styled';
import {shade, rem} from 'polished';
import {
    breakpoints,
    colors,
    fontFamilies,
    shadows,
    spacing,
    themes,
    transitions
} from '../../globals/variables';

export const container = css`
    display: flex;
    flex-direction: column;
    margin-left: auto;
    margin-right: auto;
    min-height: 100vh;
    text-align: center;
    width: 100%;

    @media (min-width: ${rem(breakpoints.mobile)}) {
        justify-content: center;
        min-height: 100vh;
    }

    @media (max-width: ${rem(breakpoints.mobile)}) {
        justify-content: space-between;
        padding: 0;
    }
`;

export const pageHeading = css`
    @media (max-width: ${rem(breakpoints.mobile)}) {
        margin: 0 auto ${rem(spacing.m)};
        max-width: ${rem(300)};
    }
`;

export const FormContainer = styled.div`
    background-color: ${colors.paleGrey};
    box-shadow: ${shadows.hover};
    border-radius: ${rem(20)};
    color: ${themes.tertiary.text};
    padding: ${rem(spacing.m)};
    position: relative;
    text-align: left;
    z-index: 2;

    @media (min-width: ${rem(breakpoints.mobile)}) {
        margin-left: auto;
        margin-right: auto;
        max-width: ${rem(450)};
    }

    @media (max-width: ${rem(breakpoints.mobile)}) {
        border-bottom-left-radius: 0;
        border-bottom-right-radius: 0;
    }
`;

const entranceAnimation = keyframes`
    to {
        opacity: 1;
        transform: scale(1);
    }
`;
const mobileEntranceAnimation = keyframes`
    to {
        opacity: 1;
        transform: translateY(0);
    }
`;

export const MainContent = styled.div`
    @media (min-width: ${rem(breakpoints.mobile)}) {
        animation: ${entranceAnimation} 1s;
        animation-delay: 0.25s;
        animation-fill-mode: forwards;
        opacity: 0;
        transform: scale(0.95);
    }

    @media (max-width: ${rem(breakpoints.mobile)}) {
        animation: ${mobileEntranceAnimation} 1s;
        animation-delay: 0.5s;
        animation-fill-mode: forwards;
        opacity: 0;
        transform: translateY(100%);
    }
`;

export const passwordContainer = css`
    position: relative;
`;

export const forgottenPasswordLink = css`
    color: ${colors.greyDark};
    font-family: ${fontFamilies.bold};
    font-size: ${rem(12)};
    position: absolute;
    right: 0;
    top: ${rem(spacing.m + 3)};
    z-index: 5;

    &::after {
        bottom: -1px;
    }

    &:hover,
    &:focus {
        color: ${shade(0.3, colors.greyDark)};
    }

    @media (max-width: ${rem(breakpoints.mobile)}) {
        top: ${rem(spacing.m + 6)};
    }
`;

const signupEntranceAnimation = keyframes`
    to {
        transform: translateY(0);
    }
`;

export const SignupContainer = styled.div`
    animation: ${signupEntranceAnimation} 1s;
    animation-delay: 1s;
    animation-fill-mode: forwards;
    background-color: ${colors.darkGreyBlue};
    border-radius: ${rem(20)};
    font-size: ${rem(14)};
    padding: ${rem(spacing.s)};
    margin-top: -${50 - spacing.s}px;
    margin-left: auto;
    margin-right: auto;
    max-width: ${rem(450)};
    padding-top: 50px;
    position: relative;
    transition: ${transitions.default};
    transform: translateY(-100%);
    z-index: 1;

    a,
    a:hover,
    a:focus {
        color: ${colors.white};
        font-family: ${fontFamilies.default};
    }

    @media (max-width: ${rem(breakpoints.mobile)}) {
        animation: none;
        animation-delay: 1.5s;
        background-color: ${colors.paleGrey};
        border-radius: 0;
        max-width: none;
        transform: translateY(0);

        a,
        a:hover,
        a:focus {
            color: ${colors.darkBlueGrey};
        }
    }

    ${props =>
        props.isVisible === false
            ? `@media (min-width: ${rem(breakpoints.mobile)}) {
                    opacity: 0;
                }`
            : ''}
`;
