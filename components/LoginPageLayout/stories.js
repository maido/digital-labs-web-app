// @flow
import React from 'react';
import {addDecorator, storiesOf} from '@storybook/react';
import {withKnobs, object, select, text} from '@storybook/addon-knobs';
import {Provider} from 'react-redux';
import {initializeStore} from '../../store';
import LoginPageLayout from './';
import Theme from '../Theme';

const stories = storiesOf('LoginPageLayout', module);

stories.addDecorator(withKnobs);

stories.addDecorator(story => (
    <Provider
        store={initializeStore({
            form: {fields: {}, state: 'default'},
            user: {isAuthenticated: true, id: 1}
        })}
    >
        <Theme theme="secondary">
            <div style={{padding: 30}}>{story()}</div>
        </Theme>
    </Provider>
));

stories.add('default', () => {
    return <LoginPageLayout />;
});
