// @flow
import styled from '@emotion/styled';
import {rem} from 'polished';
import {responsiveRem} from '../../globals/functions';
import {breakpoints, colors, spacing} from '../../globals/variables';

export const Container = styled.section`
    align-items: flex-start;
    display: flex;
    position: relative;
    width: 100%;

    @media (min-width: ${rem(breakpoints.mobile)}) {
        margin-left: 50%;
        transform: translateX(-50%);
        width: 115%;
    }
`;

export const Image = styled.div`
    ${props => props.image && `background-image: url('${props.image}');`}
    background-position: center center;
    background-repeat: no-repeat;
    background-size: cover;
    height: 0;
    padding-bottom: 65%;
    width: 100%;
    vertical-align: top;
    width: 100%;
`;

export const ImageContainer = styled.div`
    background-color: ${colors.greyLight};
    min-height: 150px;
    overflow: hidden;
    width: 100vw;

    @media (max-width: ${rem(breakpoints.mobile)}) {
        &:nth-of-type(1),
        &:nth-of-type(3) {
            display: none;
        }
    }

    @media (min-width: ${rem(breakpoints.mobile)}) {
        & + & {
            margin-left: ${responsiveRem(spacing.xs)};
        }

        &:nth-of-type(1) ${Image} {
            padding-bottom: 45%;
        }
        &:nth-of-type(3) ${Image} {
            padding-bottom: 55%;
        }
    }
`;
