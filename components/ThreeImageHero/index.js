// @flow
import * as React from 'react';
import AnimateWhenVisible from '../AnimateWhenVisible';
import * as S from './styles';

type Props = {
    children?: React.Node,
    images: Array<string>
};

const ThreeImageHero = ({children, images}: Props) => (
    <S.Container>
        {images.map((image, index) => (
            <S.ImageContainer key={image}>
                <AnimateWhenVisible
                    delay={200 * index}
                    watch={false}
                    config={{
                        config: {mass: 3, tension: 100, friction: 40},
                        from: {transform: `scale(1.05)`, opacity: 0},
                        to: {transform: `scale(1)`, opacity: 1}
                    }}
                >
                    <S.Image image={image.replace('.jpg', '_1000.jpg')} i={index} />
                </AnimateWhenVisible>
            </S.ImageContainer>
        ))}
        {children}
    </S.Container>
);

export default ThreeImageHero;
