// @flow
import React, {useEffect, useRef, useState} from 'react';
import dynamic from 'next/dynamic';
import map from 'lodash/map';
import AnimateWhenVisible from '../AnimateWhenVisible';
import Container from '../Container';
import CTAButton from '../CTAButton';
import Link from '../Link';
import ResponsiveVideo from '../ResponsiveVideo';
import TableOfContents from '../TableOfContents';
import Spinner from '../Spinner';
import * as UI from '../UI/styles';
import * as S from './styles';

const TypeFormBlock = dynamic(() => import('./TypeFormBlock'), {ssr: false});

type Props = {
    blocks: Array<Object>,
    wrappedWithContainers?: boolean
};

const BlockContainer = ({children, hasContainer, type}) => {
    if (hasContainer) {
        return (
            <Container
                css={S.container}
                paddingVertical={0}
                size={type !== 'video' ? 'small' : 'medium'}
            >
                <AnimateWhenVisible delay={150}>{children}</AnimateWhenVisible>
            </Container>
        );
    } else {
        return <AnimateWhenVisible delay={150}>{children}</AnimateWhenVisible>;
    }
};

const GifBlock = ({thumb, image}) => {
    const [isVisible, setIsVisible] = useState(false);

    useEffect(() => {
        setIsVisible(true);
    }, []);

    return <S.Image src={isVisible ? image : thumb} alt="" />;
};

const IframeBlock = ({content}) => {
    const [isVisible, setIsVisible] = useState(false);

    useEffect(() => {
        setIsVisible(true);
    }, []);

    return <S.HTMLContainer dangerouslySetInnerHTML={{__html: content}} />;
};

const RteBlock = ({content}) => {
    const $container = useRef(null);

    useEffect(() => {
        if ($container.current) {
            const $links = $container.current.querySelectorAll('a');

            if ($links.length) {
                [...$links].map($link => {
                    $link.setAttribute('target', 'noopener');
                });
            }
        }
    }, []);

    return <S.RteContent ref={$container} dangerouslySetInnerHTML={{__html: content}} />;
};

export const ContentBlock = ({block}) => {
    switch (block.type) {
        case 'text':
            return <RteBlock content={block.content} />;
        case 'table-of-contents': {
            return <TableOfContents items={block.items} />;
        }
        case 'heading':
            return (
                <S.Heading id={block.slug} css={[UI.margin('bottom', 'm'), {fontWeight: 300}]}>
                    {block.heading}
                </S.Heading>
            );
        case 'image':
            return (
                <picture>
                    <source
                        media="(min-width: 46.3125rem)"
                        srcSet={`${block.conversions.default}, ${block.conversions.default_2x} 2x`}
                    />
                    <S.Image
                        src={block.conversions.default_medium}
                        srcSet={` ${block.conversions.default_medium_2x} 2x`}
                        alt=""
                    />
                </picture>
            );
        case 'gif':
            return <GifBlock thumb={block.thumb_url} image={block.url} />;
        case 'embed':
            return <IframeBlock content={block.content} />;
        case 'call-to-action':
            return (
                <CTAButton href={block.url} wide={true}>
                    {block.title}
                </CTAButton>
            );
        case 'video':
            console.log(block);
            return (
                <>
                    <UI.ResponsiveSpacer />
                    <AnimateWhenVisible>
                        {(!block.is_vimeo && block.url) ||
                        (block.vimeo_oembed && block.vimeo_oembed.html) ? (
                            <ResponsiveVideo
                                embed={block.is_vimeo ? block.vimeo_oembed.html : ''}
                                type={block.is_vimeo ? 'vimeo' : 'youtube'}
                                thumbnail={block.is_vimeo ? block.vimeo_oembed.thumbnail_url : ''}
                                url={block.is_vimeo ? '' : block.url}
                            />
                        ) : (
                            <Link css={UI.textLink} href={block.url}>
                                {block.url}
                            </Link>
                        )}
                    </AnimateWhenVisible>
                    <UI.ResponsiveSpacer />
                </>
            );
        case 'description-list':
            return (
                <S.List>
                    {map(block.items, (content, title: string) => (
                        <S.ListItem key={`${block.position}-${title}`}>
                            <strong>{title}</strong>
                            <br />
                            {content}
                        </S.ListItem>
                    ))}
                </S.List>
            );
        case 'quote':
            return (
                <S.Quote>
                    <S.QuoteBody>{block.body}</S.QuoteBody>
                    <S.QuoteFooter>
                        <strong>{block.citation}</strong>
                    </S.QuoteFooter>
                </S.Quote>
            );

        case 'type-form':
            return (
                <S.TypeFormContainer>
                    <TypeFormBlock url={block.url} />
                </S.TypeFormContainer>
            );
        default:
            return <span />;
    }
};

const ContentBlocks = ({blocks = [], wrappedWithContainers = false}: Props) => (
    <>
        {blocks.map(block => {
            if (block.type === 'type-form') {
                return <ContentBlock block={block} />;
            } else {
                return (
                    <BlockContainer
                        type={block.type}
                        hasContainer={wrappedWithContainers}
                        key={`${block.type}-${block.position}`}
                    >
                        <ContentBlock block={block} />
                    </BlockContainer>
                );
            }
        })}
    </>
);

export default ContentBlocks;
