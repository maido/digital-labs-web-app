// @flow
import React, {useEffect, useRef} from 'react';
import * as typeformEmbed from '@typeform/embed';

type Props = {
    url: string
};

const TypeForm = ({url}: Props) => {
    const $typeform = useRef(null);

    useEffect(() => {
        if ($typeform.current) {
            typeformEmbed.makePopup(url, {
                hideScrollbars: true
            });
        }
    }, []);

    return <div className="ReactTypeformEmbed" ref={$typeform} style={{width: '100%'}} />;
};

export default TypeForm;
