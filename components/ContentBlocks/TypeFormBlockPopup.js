// @flow
import React from 'react';
import CTAButton from '../CTAButton';
import * as typeformEmbed from '@typeform/embed';

type Props = {
    handleComplete: Function,
    label: string,
    url: string
};

const TypeForm = ({handleComplete = () => {}, label, url}: Props) => {
    const openForm = () => {
        typeformEmbed.makePopup(url, {open: 'load', onSubmit: handleComplete});
    };

    return (
        <CTAButton block={true} onClick={openForm}>
            {label}
        </CTAButton>
    );
};

export default TypeForm;
