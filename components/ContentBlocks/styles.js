// @flow
import {css} from '@emotion/core';
import styled from '@emotion/styled';
import {rem} from 'polished';
import {responsiveRem} from '../../globals/functions';
import {
    breakpoints,
    colors,
    fontFamilies,
    fontSizes,
    spacing,
    transitions,
    themes
} from '../../globals/variables';

export const container = css`
    & + & {
        margin-top: ${rem(spacing.m)};
    }
`;

export const Heading = styled.h2`
    font-family: ${fontFamilies.bold};
    font-size: ${rem(fontSizes.h2 * 0.75)};
    letter-spacing: 0;
    font-weight: normal;
    line-height: 1.2;
    margin-bottom: ${rem(spacing.m)};
    margin-top: 0;

    @media (min-width: ${props => rem(breakpoints.mobile)}) {
        font-size: ${rem(fontSizes.h2)};
    }
`;

export const RteContent = styled.div`
    & + * {
        margin-top: ${rem(spacing.m)} !important;
    }

    > div + div {
        margin-top: ${rem(spacing.m)};
    }

    pre {
        font-size: small;
        font-family: inherit;
        white-space: normal;
    }

    a {
        color: ${colors.purplishBlue} !important;
        font-family: ${fontFamilies.bold} !important;
        font-style: normal !important;
        text-decoration: none !important;
        position: relative;
        transition: ${transitions.default};

        &::after {
            background-color: currentColor;
            bottom: -5px;
            content: '';
            height: 2px;
            right: 0;
            opacity: 0;
            position: absolute;
            transition: ${transitions.default};
            transition-duration: 0.4s;
            width: 0;
        }

        &:hover::after,
        &:focus::after,
        &[aria-current='true']::after {
            left: 0;
            opacity: 1;
            right: auto;
            transition-duration: 0.2s;
            width: 100%;
        }
    }

    h1 {
        font-size: ${rem(fontSizes.h4 * 0.85)};
        line-height: 1.5;
    }

    @media (min-width: ${rem(breakpoints.mobile)}) {
        h1 {
            font-size: ${rem(fontSizes.h4)};
        }
    }
`;

export const Image = styled.img`
    border-radius: ${rem(fontSizes.default)};
    margin: ${responsiveRem(spacing.xs)} 0;
    margin-left: ${rem(spacing.m * -1)};
    max-width: none;
    width: calc(100% + ${rem(spacing.m * 2)});

    @media (min-width: ${rem(breakpoints.tablet)}) {
        margin-left: ${rem(spacing.l * -1)};
        width: calc(100% + ${rem(spacing.l * 2)});
    }
`;

export const List = styled.ul`
    list-style: none;
    margin: 0;
    padding: 0;
`;

export const ListItem = styled.li`
    border-left: 2px solid ${colors.mediumPink};
    margin: 0;
    padding-left: ${rem(spacing.m)};

    & + & {
        margin-top: ${responsiveRem(spacing.xs)};
    }
`;

export const Quote = styled.blockquote`
    border-left: 2px solid;
    margin: ${responsiveRem(spacing.m)} 0;
    padding: 0;
    padding-left: ${rem(spacing.m)};
`;

export const QuoteBody = styled.p`
    font-family: ${fontFamilies.bold};
    font-size: larger;
`;

export const QuoteFooter = styled.footer`
    margin-top: ${rem(spacing.m)};
`;

export const HTMLContainer = styled.div`
    width: 100%;

    iframe,
    img,
    video {
        border-radius: ${rem(fontSizes.default)};
        width: 100%;
    }
`;

export const PreloadBox = styled.div`
    align-items: center;
    background-color: ${themes.tertiary.background};
    color: ${themes.tertiary.text};
    display: flex;
    height: ${responsiveRem(250)};
    justify-content: center;
    padding: ${responsiveRem(spacing.m)};
`;

export const TypeFormContainer = styled.div`
    margin: ${responsiveRem(spacing.m)} 0;
    // max-height: ${responsiveRem(800)};
    min-height: ${responsiveRem(400)};
    position: relative;
    width: 100%;
`;
