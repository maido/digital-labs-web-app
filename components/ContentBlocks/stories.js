// @flow
import React from 'react';
import {storiesOf} from '@storybook/react';
import ContentBlocks from './';

const stories = storiesOf('ContentBlocks', module);

stories.add('rich text', () => {
    return (
        <div style={{padding: 20}}>
            <ContentBlocks
                blocks={[
                    {
                        type: 'text',
                        content:
                            '<p>Adipisicing cillum nulla <u>onsectetur enim</u> aliquip deserunt. Lorem excepteur consequat incididunt mollit labore cupidatat laboris occaecat ullamco. Sit tempor ut nulla incididunt esse non. Mollit enim aliquip id consequat <em>reprehenderit anim</em> id veniam incididunt nisi ex. Ad velit amet proident consequat eiusmod. Do labore aliquip officia sit. Laborum aliquip ea quis aliquip in elit sint voluptate dolor reprehenderit excepteur elit.</p><p>Ea do irure <strong>duis anim proident</strong> ipsum amet nisi. Qui duis et ad non amet laborum ad proident velit. Ullamco cupidatat commodo minim minim exercitation proident.</p>'
                    }
                ]}
            />
        </div>
    );
});

stories.add('image', () => {
    return (
        <div style={{padding: 20}}>
            <ContentBlocks
                blocks={[
                    {
                        type: 'image',
                        hero: 'https://placehold.it/1200x500'
                    }
                ]}
            />
        </div>
    );
});

stories.add('youtube video', () => {
    return (
        <div style={{padding: 20}}>
            <ContentBlocks
                blocks={[
                    {
                        type: 'video',
                        is_vimeo: false,
                        url: 'https://www.youtube.com/watch?v=AZm1_jtY1SQ'
                    }
                ]}
            />
        </div>
    );
});

stories.add('vimeo video', () => {
    return (
        <div style={{padding: 20}}>
            <ContentBlocks
                blocks={[
                    {
                        type: 'video',
                        is_vimeo: true,
                        vimeo_oembed: {
                            html:
                                '<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/122375452?title=0&byline=0&portrait=0" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>'
                        }
                    }
                ]}
            />
        </div>
    );
});

stories.add('quote', () => {
    return (
        <div style={{padding: 20}}>
            <ContentBlocks
                blocks={[
                    {
                        type: 'quote',
                        citation: 'Fugiat qui pariatur laborum',
                        body: 'Anim excepteur velit laboris excepteur minim consequat ea dolor.'
                    }
                ]}
            />
        </div>
    );
});

stories.add('description list', () => {
    return (
        <div style={{padding: 20}}>
            <ContentBlocks
                blocks={[
                    {
                        type: 'description-list',
                        items: [
                            {
                                position: 1,
                                title: 'Ad Lorem ad irure dolor aliqua sit anim',
                                content:
                                    'Occaecat exercitation Lorem excepteur incididunt nisi sint eiusmod.'
                            }
                        ]
                    }
                ]}
            />
        </div>
    );
});
