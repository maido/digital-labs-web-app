// @flow
import * as React from 'react';
import {useTransition, animated} from 'react-spring/web.cjs';
import * as S from './styles';

type Props = {
    animateOnMount?: boolean,
    children: React.Node,
    styles?: Object,
    header?: React.Node,
    isActive?: boolean,
    isDisabled?: boolean
};

const getElementHeight = ref => (ref.current ? ref.current.getBoundingClientRect().height : 0);

const AccordionContent = ({animateOnMount, children, isActive}: Props) => {
    const isActiveOnMount = React.useRef(isActive && !animateOnMount);
    const $containerRef = React.useRef(null);
    const $innerRef = React.useRef(null);

    const visibleStyle = {height: 'auto', opacity: 1, overflow: 'visible'};
    const hiddenStyle = {opacity: 0, height: 0, overflow: 'hidden'};

    const transitions = useTransition(isActive, null, {
        enter: () => async (next, cancel) => {
            const height = getElementHeight($innerRef);

            cancel();

            await next({height, opacity: 1, overflow: 'hidden'});
            await next(visibleStyle);
        },
        leave: () => async (next, cancel) => {
            const height = getElementHeight($containerRef);

            cancel();

            await next({height, overflow: 'hidden'});
            await next(hiddenStyle);

            isActiveOnMount.current = false;
        },
        from: isActiveOnMount.current ? visibleStyle : hiddenStyle,
        unique: true
    });

    return transitions.map(({item: show, props: springProps, key}) => {
        if (show) {
            return (
                <animated.div ref={$containerRef} key={key} style={springProps}>
                    <div ref={$innerRef} style={{paddingTop: 24}}>
                        {children}
                    </div>
                </animated.div>
            );
        }

        return null;
    });
};

const Accordion = ({
    animateOnMount,
    children,
    styles,
    header,
    isActive = false,
    isDisabled = false
}: Props) => {
    const [isContentActive, setIsContentActive] = React.useState(isActive);

    const handleClick = () => {
        if (!isDisabled) {
            setIsContentActive(!isContentActive);
        }
    };

    return (
        <React.Fragment>
            <S.Container
                onClick={handleClick}
                css={styles}
                isDisabled={isDisabled}
                data-testid="accordion"
            >
                <S.Header>{header}</S.Header>
                {!isDisabled && (
                    <S.ToggleButton>
                        <S.ToggleIcon
                            isActive={isContentActive}
                            fill="none"
                            height="24"
                            viewBox="0 0 24 24"
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            strokeWidth="2"
                            width="24"
                            xmlns="http://www.w3.org/2000/svg"
                        >
                            <path d="M6 9l6 6 6-6" />
                        </S.ToggleIcon>
                    </S.ToggleButton>
                )}
            </S.Container>
            <AccordionContent animateOnMount={animateOnMount} isActive={isContentActive}>
                {children}
            </AccordionContent>
        </React.Fragment>
    );
};

export default Accordion;
