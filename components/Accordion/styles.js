// @flow
import styled from '@emotion/styled';
import {rem} from 'polished';
import {colors, spacing, transitions} from '../../globals/variables';

export const Container = styled.div`
    align-items: cener;
    display: flex;

    ${props => (props.isDisabled ? 'cursor:default; pointer-events:none;' : 'cursor: pointer;')}
`;

export const Header = styled.div`
    flex-grow: 1;
    margin-right: ${rem(spacing.m)};
`;

export const ToggleIcon = styled.svg`
    height: auto;
    transition: ${transitions.default};
    width: ${rem(spacing.m)};

    ${props => props.isActive && `transform:rotate(180deg)`};

    path {
        stroke: ${colors.purplishBlue};
    }
`;

export const ToggleButton = styled.button`
    align-items: center;
    display: flex;
    height: ${rem(40)};
    justify-content: center;
    width: ${rem(40)};
`;
