// @flow
import React from 'react';
import {storiesOf} from '@storybook/react';
import Accordion from './';

const stories = storiesOf('Accordion', module);

stories.add('default', () => {
    return (
        <Accordion header="Eu do elit commodo nisi ipsum id commodo proident aliqua.">
            Sit incididunt ex quis ipsum do sit laboris tempor veniam incididunt. In culpa ullamco
            proident duis ullamco consequat laboris enim cupidatat. Incididunt ullamco aliquip
            labore est amet Lorem nostrud. Anim et dolore tempor enim esse mollit eiusmod consequat
            dolor laborum minim. Duis cillum ullamco ipsum consequat quis cillum irure sunt nisi eu
            nisi ea. Esse incididunt veniam qui proident amet culpa fugiat ea veniam pariatur enim.
            Culpa incididunt proident officia amet cillum aliqua est.
        </Accordion>
    );
});

stories.add('with styled header', () => {
    return (
        <Accordion
            header={
                <div style={{backgroundColor: '#efefef', padding: 10}}>
                    Eu do elit commodo nisi ipsum id commodo proident aliqua.
                </div>
            }
        >
            Sit incididunt ex quis ipsum do sit laboris tempor veniam incididunt. In culpa ullamco
            proident duis ullamco consequat laboris enim cupidatat. Incididunt ullamco aliquip
            labore est amet Lorem nostrud. Anim et dolore tempor enim esse mollit eiusmod consequat
            dolor laborum minim. Duis cillum ullamco ipsum consequat quis cillum irure sunt nisi eu
            nisi ea. Esse incididunt veniam qui proident amet culpa fugiat ea veniam pariatur enim.
            Culpa incididunt proident officia amet cillum aliqua est.
        </Accordion>
    );
});

stories.add('with styled inner content', () => {
    return (
        <Accordion
            header={
                <div style={{backgroundColor: '#efefef', padding: 10}}>
                    Eu do elit commodo nisi ipsum id commodo proident aliqua.
                </div>
            }
        >
            <div style={{backgroundColor: '#999', padding: 10}}>
                Sit incididunt ex quis ipsum do sit laboris tempor veniam incididunt. In culpa
                ullamco proident duis ullamco consequat laboris enim cupidatat. Incididunt ullamco
                aliquip labore est amet Lorem nostrud. Anim et dolore tempor enim esse mollit
                eiusmod consequat dolor laborum minim. Duis cillum ullamco ipsum consequat quis
                cillum irure sunt nisi eu nisi ea. Esse incididunt veniam qui proident amet culpa
                fugiat ea veniam pariatur enim. Culpa incididunt proident officia amet cillum aliqua
                est.
            </div>
        </Accordion>
    );
});

stories.add('with disabled toggle', () => {
    return (
        <Accordion
            isDisabled={true}
            header={
                <div style={{backgroundColor: '#efefef', padding: 10}}>
                    Eu do elit commodo nisi ipsum id commodo proident aliqua.
                </div>
            }
        >
            <div style={{backgroundColor: '#999', padding: 10}}>
                Sit incididunt ex quis ipsum do sit laboris tempor veniam incididunt. In culpa
                ullamco proident duis ullamco consequat laboris enim cupidatat. Incididunt ullamco
                aliquip labore est amet Lorem nostrud. Anim et dolore tempor enim esse mollit
                eiusmod consequat dolor laborum minim. Duis cillum ullamco ipsum consequat quis
                cillum irure sunt nisi eu nisi ea. Esse incididunt veniam qui proident amet culpa
                fugiat ea veniam pariatur enim. Culpa incididunt proident officia amet cillum aliqua
                est.
            </div>
        </Accordion>
    );
});

stories.add('with content visible by default', () => {
    return (
        <Accordion
            isActive={true}
            header={
                <div style={{backgroundColor: '#efefef', padding: 10}}>
                    Eu do elit commodo nisi ipsum id commodo proident aliqua.
                </div>
            }
        >
            <div style={{backgroundColor: '#999', padding: 10}}>
                Sit incididunt ex quis ipsum do sit laboris tempor veniam incididunt. In culpa
                ullamco proident duis ullamco consequat laboris enim cupidatat. Incididunt ullamco
                aliquip labore est amet Lorem nostrud. Anim et dolore tempor enim esse mollit
                eiusmod consequat dolor laborum minim. Duis cillum ullamco ipsum consequat quis
                cillum irure sunt nisi eu nisi ea. Esse incididunt veniam qui proident amet culpa
                fugiat ea veniam pariatur enim. Culpa incididunt proident officia amet cillum aliqua
                est.
            </div>
        </Accordion>
    );
});
