import React from 'react';
import {addDecorator, storiesOf} from '@storybook/react';
import {withKnobs, object, select, text} from '@storybook/addon-knobs';
import BannerNotification from './';
import Theme from '../Theme';

const themeOptions = {
    Primary: 'primary',
    Secondary: 'secondary',
    Tertiary: 'tertiary',
    White: 'white'
};
const stories = storiesOf('BannerNotification', module);

stories.addDecorator(story => (
    <Theme theme={select('Theme', themeOptions, 'secondary')}>
        <div style={{padding: 30}}>{story()}</div>
    </Theme>
));

stories.add('default', () => {
    return (
        <BannerNotification
            text="To submit a pitch you need a team of at least two people"
            cta={{label: "Yes, I'm sure", url: ''}}
        />
    );
});

stories.add('with formatted text', () => {
    return (
        <BannerNotification
            text="To submit a pitch you need a <em>team of</em> at <strong>least two people</strong>"
            cta={{label: "Yes, I'm sure", url: ''}}
        />
    );
});

stories.add('with custom label', () => {
    return (
        <BannerNotification
            text="To submit a pitch you need a team of at least two people"
            label="Your pitch"
            cta={{label: 'Submit pitch', url: ''}}
        />
    );
});
