// @flow
import * as React from 'react';
import CTAButton from '../CTAButton';
import * as S from './styles';

type Props = {
    cta?: {
        label: string,
        url: string
    },
    text?: string
};

const BannerNotification = ({cta, text}: Props) => (
    <S.Container>
        {text && <S.Text dangerouslySetInnerHTML={{__html: text}} />}
        {cta && (
            <CTAButton href={cta.url} theme="white" wide={true}>
                {cta.label}
            </CTAButton>
        )}
    </S.Container>
);

export default BannerNotification;
