// @flow
import styled from '@emotion/styled';
import {rem} from 'polished';
import {breakpoints, fontFamilies, fontSizes, spacing} from '../../globals/variables';

export const Container = styled.div`
    font-size: ${rem(fontSizes.lead)};
    margin: 0 0 ${rem(spacing.l)};
    text-align: center;

    @media (min-width: ${rem(breakpoints.mobile)}) {
        align-items: center;
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
        justify-content: center;
        margin: 0 0 ${rem(spacing.m * 1 * -1)};
        padding-bottom: 0;
        text-align: left;
    }
`;

export const Text = styled.p`
    line-height: 1.2;

    @media (min-width: ${rem(breakpoints.mobile)}) {
        & + * {
            margin-left: ${rem(spacing.m)};
        }
    }

    @media (min-width: ${rem(breakpoints.mobile)}) {
        // flex-grow: 1;

        & + * {
            margin-top: 0;
        }
    }
`;
