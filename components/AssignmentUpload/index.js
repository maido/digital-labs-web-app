// @flow
import React, {useCallback, useRef, useState} from 'react';
import axios from 'axios';
import {useSpring, animated} from 'react-spring/web.cjs';
import {useDropzone} from 'react-dropzone';
import Cookie from 'js-cookie';
import {Formik, Field, Form} from 'formik';
import assignmentSchema from '../../schemas/assignment';
import bugsnagClient from '../../globals/bugsnag';
import CTAButton from '../CTAButton';
import FileUploadField from '../FileUploadField';
import InputTextarea from '../InputTextarea';
import Spinner from '../Spinner';
import * as FormStyles from '../Form/styles';
import * as UI from '../UI/styles';

type Props = {
    assignment: Object,
    handleSuccess: Function
};

const AssignmentUpload = ({assignment, handleSuccess}: Props) => {
    const [formState, setFormState] = useState('default');
    const [stateLabel, setStateLabel] = useState(null);
    const formData = new FormData();

    const handleSubmit = async (values, handlers) => {
        if (formState !== 'default') {
            return;
        }

        setFormState('pending');

        formData.append('file', values.file);
        formData.append('isFileRequired', values.isFileRequired);
        formData.append('reflections', values.reflections);
        formData.append('token', Cookie.get('token'));
        formData.append('apiUrl', `api/assignments/${assignment.id}/submissions`);

        try {
            const response = await axios.post(
                `${process.env.NEXT_STATIC_UPLOADS_API_URL}api/assignment/`,
                formData,
                {
                    onUploadProgress: ({loaded, total}) => {
                        const percentCompleted = Math.round((loaded * 100) / total);

                        if (percentCompleted === 100) {
                            setStateLabel('Processing');
                        } else {
                            setStateLabel(`Uploading (${percentCompleted}%)`);
                        }
                    }
                }
            );
            const {data} = response;

            handlers.setSubmitting(false);
            setFormState('success');

            if (handleSuccess) {
                setTimeout(() => handleSuccess(data), 1000);
            }
        } catch (error) {
            console.log({error});

            if (error.status === 413) {
                handlers.setErrors({file: 'The file is too large, try a smaller file.'});
            } else {
                if (bugsnagClient) {
                    bugsnagClient.notify(error);
                }

                handlers.setErrors({file: 'Error uploading. Please try again.'});
            }

            handlers.setSubmitting(false);
            setFormState('default');
        }
    };

    return (
        <Formik
            initialValues={{
                file: undefined,
                isFileRequired: assignment.is_upload_required === 1,
                reflections: ''
            }}
            onSubmit={handleSubmit}
            validationSchema={assignmentSchema}
            validateOnChange={true}
            validateOnBlur={true}
            render={formProps => (
                <Form>
                    <UI.LayoutContainer size="s">
                        <UI.LayoutItem>
                            <InputTextarea
                                name="reflections"
                                rows={2}
                                placeholder="Add your response/reflection..."
                            />
                            <UI.Spacer size="s" />
                        </UI.LayoutItem>
                        <UI.LayoutItem sizeAtMobile={6 / 12}>
                            <FileUploadField
                                setFieldValue={formProps.setFieldValue}
                                isRequired={assignment.is_upload_required === 1}
                                formState={formState}
                            />
                            <UI.Spacer size="s" />
                        </UI.LayoutItem>
                        <UI.LayoutItem sizeAtMobile={6 / 12}>
                            <CTAButton
                                block={true}
                                state={formState}
                                disabled={formProps.isSubmitting || formProps.isValidating}
                                type="submit">
                                Submit assignment
                            </CTAButton>
                            {stateLabel && (
                                <span css={[UI.centerText, UI.textColor('purplishBlue')]}>
                                    {stateLabel}
                                </span>
                            )}
                        </UI.LayoutItem>
                    </UI.LayoutContainer>
                </Form>
            )}
        />
    );
};

export default AssignmentUpload;
