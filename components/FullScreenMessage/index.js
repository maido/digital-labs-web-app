// @flow
import * as React from 'react';
import {createPortal} from 'react-dom';
import {useSpring, animated} from 'react-spring/web.cjs';
import Badge from '../Badge';
import CTAButton from '../CTAButton';
import * as UI from '../UI/styles';
import * as S from './styles';

type Props = {
    badge?: string,
    children?: React.Node,
    cta?: CTA | null,
    state: 'hidden' | 'leaving' | 'visible',
    text?: string,
    theme?: string,
    title?: string
};

const FullScreenMessage = ({
    badge,
    children,
    cta,
    state = 'hidden',
    text,
    theme = 'secondary',
    title
}: Props) => {
    const [$portal, set$Portal] = React.useState(null);
    const [containerAnimation, setContainerAnimation] = useSpring(() => ({
        config: {tension: 60, mass: 1, friction: 10},
        from: {opacity: 0, visibility: 'hidden'}
    }));
    const [textAnimation, setTextAnimation] = useSpring(() => ({
        config: {tension: 60, mass: 1, friction: 10},
        from: {transform: `translateY(20px)`, opacity: 1}
    }));
    const [buttonAnimation, setButtonAnimation] = useSpring(() => ({
        config: {tension: 60, mass: 1, friction: 10},
        from: {transform: `translateY(20px)`, opacity: 0}
    }));

    React.useEffect(() => {
        if (!$portal) {
            const portal = document.querySelector('#portals');

            set$Portal(portal);
        }

        if (state === 'visible') {
            setContainerAnimation({visibility: 'visible', opacity: 1});
            setTextAnimation({transform: `translateY(0)`, opacity: 1});
            setButtonAnimation({transform: `translateY(0)`, opacity: 1});
        } else if (state === 'leaving') {
            setButtonAnimation({delay: 100, transform: `translateY(20px)`, opacity: 0});
            setTextAnimation({delay: 100, transform: `translateY(20px)`, opacity: 0});
            setContainerAnimation({visibility: 'hidden', opacity: 0});
        }
    }, [state]);

    const component = (
        <animated.div
            style={containerAnimation}
            css={S.container(theme)}
            data-testid="fullscreenmessage"
        >
            <S.Wrapper>
                <S.ContentContainer>
                    {badge && (
                        <>
                            <Badge badge={badge} />
                            <UI.Spacer />
                        </>
                    )}

                    <animated.div style={textAnimation}>
                        {title && (
                            <UI.Heading2 as="h1" style={{marginBottom: 6}}>
                                {title}
                            </UI.Heading2>
                        )}
                        {text && <S.IntroText>{text}</S.IntroText>}
                    </animated.div>

                    {cta && (
                        <S.CTAContainer>
                            <animated.div style={textAnimation}>
                                <animated.div style={buttonAnimation}>
                                    <CTAButton
                                        href={cta.url}
                                        onClick={() => (cta.onClick ? cta.onClick() : null)}
                                    >
                                        {cta.label}
                                    </CTAButton>
                                </animated.div>
                            </animated.div>
                        </S.CTAContainer>
                    )}
                </S.ContentContainer>

                {children}
            </S.Wrapper>
        </animated.div>
    );

    if (!$portal) {
        return component;
    }

    return createPortal(component, $portal);
};

export default FullScreenMessage;
