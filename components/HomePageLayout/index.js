// @flow
import React from 'react';
import Link from 'next/link';
import AnimateWhenVisible from '../AnimateWhenVisible';
import Container from '../Container';
import CTAButton from '../CTAButton';
import CTATextBanner from '../CTATextBanner';
import ImageCard from '../ImageCard';
import Layout from '../Layout';
import PageIntro from '../PageIntro';
import BadgeCard from '../BadgeCard';
import ResponsiveVideo from '../ResponsiveVideo';
import LogoList from '../LogoList';
import Theme from '../Theme';
import ThreeImageHero from '../ThreeImageHero';
import * as UI from '../UI/styles';
import * as S from './styles';

type Props = {
    dispatch: Function,
    finalists: Object,
    howItWorks: Object,
    intro: Object,
    judging: Object,
    partners: Object,
    textBanner: Object,
    winnerPhotos: Object,
    version2Teaser: Object,
    user: Object
};

const HomePageLayout = ({
    finalists,
    howItWorks,
    intro,
    judging,
    partners,
    textBanner,
    winnerPhotos,
    version2Teaser,
    user = {}
}: Props) => (
    <>
        <PageIntro
            subtitle={intro.subtitle}
            title={intro.title}
            text={intro.text}
            primaryCTA={
                user.isAuthenticated
                    ? {
                          url: '/dashboard',
                          label: 'Your dashboard'
                      }
                    : {
                          url: '/signup',
                          label: 'Sign up for free',
                          tracking: {
                              category: 'Sign up',
                              action: 'click',
                              label: 'Home – Header'
                          }
                      }
            }
            secondaryCTA={user.isAuthenticated ? null : {url: '#learn-more', label: 'Learn more'}}
        />

        <UI.ResponsiveSpacer />
        <Theme theme="white" overlap="secondary" overlapSize={180} grow={true}>
            <ThreeImageHero images={winnerPhotos.images}>
                <div css={S.threeImagesHeroCard}>
                    <AnimateWhenVisible>
                        <BadgeCard {...winnerPhotos.card} />
                    </AnimateWhenVisible>
                </div>
            </ThreeImageHero>

            <UI.ResponsiveSpacer size="xs" sizeAtMobile="xl" />

            <Container size="large" paddingVertical="l">
                <UI.LayoutContainer>
                    <UI.LayoutItem sizeAtMobile={4 / 12}>
                        <UI.Heading2>{version2Teaser.title}</UI.Heading2>
                    </UI.LayoutItem>
                    <UI.LayoutItem sizeAtMobile={8 / 12}>
                        <span dangerouslySetInnerHTML={{__html: version2Teaser.text}} />
                    </UI.LayoutItem>
                </UI.LayoutContainer>
            </Container>
        </Theme>

        <Theme theme="white">
            <Container size="large" paddingVertical="l">
                <UI.LayoutContainer>
                    <UI.LayoutItem sizeAtMobile={4 / 12}>
                        <UI.Heading2>{howItWorks.title}</UI.Heading2>
                    </UI.LayoutItem>
                    <UI.LayoutItem sizeAtMobile={8 / 12}>
                        <span dangerouslySetInnerHTML={{__html: howItWorks.text}} />
                    </UI.LayoutItem>
                </UI.LayoutContainer>
            </Container>
        </Theme>

        <Theme
            theme="tertiary"
            overlap="white"
            overlapSize={350}
            overlapSizeAtMobile={400}
            grow={true}
        >
            <Container id="learn-more" size="large">
                <UI.LayoutContainer>
                    <UI.LayoutItem sizeAtMobile={4 / 12}></UI.LayoutItem>
                    <UI.LayoutItem sizeAtMobile={8 / 12}>
                        <UI.Heading3 as="h2">Live Sessions</UI.Heading3>
                        <p>
                            The weekly <strong>live sessions</strong> are a chance to start real
                            discussions with a diverse and multidisciplinary group of experts,
                            creatives, and visionaries, and explore the most relevant topics in the
                            industry.
                        </p>
                        <p>
                            Take a glimpse at one of our past live sessions with Google’s Lead for
                            Circular Economy, Mike Werner:
                        </p>
                    </UI.LayoutItem>
                </UI.LayoutContainer>
                <UI.ResponsiveSpacer />

                <ResponsiveVideo type="youtube" url="https://www.youtube.com/watch?v=HO0p-RDzR-k" />
                <UI.ResponsiveSpacer />

                <UI.LayoutContainer>
                    <UI.LayoutItem sizeAtMobile={4 / 12}></UI.LayoutItem>
                    <UI.LayoutItem sizeAtMobile={8 / 12}>
                        <p>
                            Collaboration is key to any entrepreneur’s success. The TFF Digital Labs
                            incentivises peer-to-peer and expert mentoring by matching entrepreneurs
                            directly with peers, industry partners, growers, customers and
                            investors.
                        </p>

                        <p>
                            <strong>
                                Are you a next-gen changemaker looking to create an impact?{' '}
                                <Link href="/signup" passHref>
                                    <a css={UI.textColor('purplishBlue')}>Join us!</a>
                                </Link>
                            </strong>
                        </p>
                    </UI.LayoutItem>
                </UI.LayoutContainer>

                <UI.Spacer />
            </Container>
        </Theme>

        <Theme theme="white">
            <Container size="large" paddingVertical="xl">
                <div css={UI.centerTextAtMobile} style={{margin: '0 auto', maxWidth: 650}}>
                    <UI.Heading2>{partners.title}</UI.Heading2>
                    <p css={UI.leadText}>{partners.text}</p>
                </div>

                <UI.ResponsiveSpacer size="l" />

                <LogoList logos={partners.logos} />
            </Container>
        </Theme>

        <Theme theme="tertiary">
            <Container size="large" paddingVertical="xl">
                <UI.Heading2 css={UI.centerTextAtMobile}>2019/20 TFF Challenge</UI.Heading2>
                <UI.Spacer />

                <UI.LayoutContainer stack={true}>
                    <UI.LayoutItem sizeAtMobile={6 / 12}>
                        <p>
                            For the first time ever, the{' '}
                            <a
                                href="http://thoughtforfood.org/challenge"
                                target="_blank"
                                rel="noopener"
                            >
                                TFF Challenge
                            </a>{' '}
                            – food and agriculture’s leading collaborative startup competition – was
                            underpinned by the Digital Labs. Within three months, Thought For Food
                            have attracted 5,200 participants from 175 countries who have completed
                            15,000 topics, engaged 145 expert mentors, and hosted over 30 live
                            sessions.
                        </p>
                    </UI.LayoutItem>
                    <UI.LayoutItem sizeAtMobile={6 / 12}>
                        <p>
                            The TFF Challenge puts the commitment of the next generations towards
                            purpose-driven, impact-focused innovation and entrepreneurship into real
                            action. Built for and by the next generation, the TFF Digital Labs helps
                            develop scalable solutions that address some of the world’s biggest
                            challenges.
                        </p>
                    </UI.LayoutItem>
                </UI.LayoutContainer>
                <UI.Spacer />

                <UI.LayoutContainer stack={true}>
                    <UI.LayoutItem sizeAtMobile={6 / 12}>
                        <img
                            src="/1920-themes.jpg"
                            alt="2019/2020 Challenge Themes"
                            style={{borderRadius: 6, width: '100%'}}
                        />
                    </UI.LayoutItem>
                    <UI.LayoutItem sizeAtMobile={6 / 12}>
                        <img
                            src="/1920-finalists.jpg"
                            alt="2019/2020 Challenge Finalists"
                            style={{borderRadius: 6, width: '100%'}}
                        />
                    </UI.LayoutItem>
                </UI.LayoutContainer>
            </Container>
        </Theme>

        {!user.isAuthenticated ? (
            <Container>
                <CTATextBanner {...textBanner} />
            </Container>
        ) : (
            <Theme theme="white">
                <UI.Spacer size="l" />
            </Theme>
        )}
    </>
);

export default HomePageLayout;
