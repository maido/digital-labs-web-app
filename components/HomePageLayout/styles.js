// @flow
import {css} from '@emotion/core';
import {rem} from 'polished';
import {breakpoints, spacing} from '../../globals/variables';

export const threeImagesHeroCard = css`
    bottom: ${rem(spacing.xl * -1)};
    left: 58%;
    position: absolute;
    will-change: transform;
    width: ${rem(200)};
    z-index: 2;

    @media (max-width: ${rem(breakpoints.mobile)}) {
        display: none;
    }

    @media (max-width: ${rem(breakpoints.tablet)}) {
        bottom: ${rem(-100)};
        transform: scale(0.75);
    }
`;

export const judgingHeroCard = css`
    @media (max-width: ${rem(breakpoints.mobile)}) {
        margin-left: auto;
        margin-right: auto;
        max-width: 250px;
    }
`;
