// @flow
import React from 'react';
import {Field} from 'formik';

type Props = {
    name: string,
    value: string
};

const InputHidden = ({name, value = ''}: Props) => (
    <Field name={name}>
        {({field, form, ...props}) => (
            <input {...field} {...props} id={name} name={field.name} type="hidden" value={value} />
        )}
    </Field>
);

export default InputHidden;
