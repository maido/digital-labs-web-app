// @flow
import React from 'react';
import {storiesOf} from '@storybook/react';
import SocialIcon from './';

const stories = storiesOf('SocialIcon', module);

stories.add('twitter', () => {
    return <SocialIcon site="twitter" />;
});

stories.add('facebook', () => {
    return <SocialIcon site="facebook" />;
});

stories.add('instagram', () => {
    return <SocialIcon site="instagram" />;
});

stories.add('linkedIn', () => {
    return <SocialIcon site="linkedIn" />;
});

stories.add('vimeo', () => {
    return <SocialIcon site="vimeo" />;
});
