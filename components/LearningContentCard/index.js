// @flow
import * as React from 'react';
import {useSpring, animated} from 'react-spring/web.cjs';
import Link from 'next/link';
import DifficultyIndicator from '../DifficultyIndicator';
import {getLearningProgress} from '../../globals/functions';
import * as UI from '../UI/styles';
import * as S from './styles';

const LearningContentCard = ({
    completed,
    headline,
    image,
    isNew = false,
    items,
    difficulty,
    size,
    title,
    url
}: LearningContentUICard) => {
    let progress = getLearningProgress(items);

    if (completed) {
        progress = 100;
    }

    const progressAnimation = useSpring({
        delay: 750,
        from: {progress: 0},
        to: {progress}
    });

    return (
        <Link {...url} passHref>
            <S.Container isNew={isNew} hasImage={image} data-testid="learningcontentcard">
                {image && <S.Image image={image} />}
                <S.Content>
                    <S.ArrowIcon
                        width="8"
                        height="12"
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 8 12"
                    >
                        <path
                            d="M.3 10.59L4.93563 6 .3 1.41 1.72713 0 7.8 6l-6.07287 6z"
                            fillRule="evenodd"
                        />
                    </S.ArrowIcon>

                    {title && <S.Title size={size}>{title}</S.Title>}

                    {((items && items.all > 0) || difficulty || isNew) && (
                        <S.InfoContainer>
                            {isNew === true && (
                                <S.NewBadge css={UI.fadeIn}>
                                    <S.NewBadgeIcon
                                        width="20"
                                        height="19"
                                        xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 20 19"
                                    >
                                        <path
                                            d="M10 15.27L16.18 19l-1.64-7.03L20 7.24l-7.19-.61L10 0 7.19 6.63 0 7.24l5.46 4.73L3.82 19z"
                                            fillRule="evenodd"
                                        />
                                    </S.NewBadgeIcon>
                                    New
                                </S.NewBadge>
                            )}
                            {difficulty && <DifficultyIndicator difficulty={difficulty} />}
                            {items && (
                                <S.Modules>
                                    {items.all} {items.type ? items.type.toLowerCase() : 'item'}
                                    {items.all > 1 ? 's' : ''}
                                </S.Modules>
                            )}

                            <S.Progress progress={progress}>
                                {progress > 0 && (
                                    <S.ProgressIcon
                                        width="18"
                                        height="14"
                                        xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 18 14"
                                    >
                                        <path
                                            d="M6 11.17L1.83 7 .41 8.41 6 14 18 2 16.59.59z"
                                            fillRule="evenodd"
                                        />
                                    </S.ProgressIcon>
                                )}
                                <animated.span>
                                    {progressAnimation &&
                                        progressAnimation.progress.interpolate(n => n.toFixed(0))}
                                </animated.span>
                                %
                            </S.Progress>
                        </S.InfoContainer>
                    )}

                    {headline && <S.Description>{headline}</S.Description>}
                </S.Content>
            </S.Container>
        </Link>
    );
};

export default LearningContentCard;
