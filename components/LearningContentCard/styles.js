// @flow
import styled from '@emotion/styled';
import {rem} from 'polished';
import {responsiveRem} from '../../globals/functions';
import {
    breakpoints,
    colors,
    fontFamilies,
    fontSizes,
    shadows,
    spacing,
    themes,
    transitions
} from '../../globals/variables';

export const Container = styled.a`
    align-items: flex-start;
    background-color: ${themes.white.background};
    box-shadow: ${shadows.default};
    border-radius: ${rem(5)};
    color: ${themes.white.text};
    display: flex;
    flex-direction: row !important;
    flex-wrap: wrap;
    overflow: hidden;
    min-height: 100%;
    text-align: left;
    transition: ${transitions.default};
    -webkit-backface-visibility: hidden;

    &:hover,
    &:focus {
        box-shadow: ${shadows.hover};
        transform: translateY(-2px);
    }

    ${props => props.isNew === true && `border: 1px solid ${colors.mediumPink};`}
`;

export const Image = styled.div`
background-color: ${colors.greyLight};
    background-image: url('${props => `${props.image}`}');
    background-position: center center;
    background-repeat: no-repeat;
    background-size: cover;
    height: ${responsiveRem(180)} !important;
    flex: 0 0 100%;
    width: 100%;

    @media (min-width: ${rem(breakpoints.desktop)}) {
        height: ${responsiveRem(260)} !important;
    }
`;

export const Content = styled.div`
    flex-grow: 1;
    padding: ${rem(spacing.s)};
    padding-right: ${rem(spacing.m)};
    position: relative;
    width: 100%;

    @media (min-width: ${rem(breakpoints.mobile)}) {
        padding: ${rem(spacing.m)};
        padding-right: ${rem(spacing.l)};
    }
`;

export const Title = styled.span`
    color: ${colors.darkBlueGrey};
    display: block;
    font-size: ${rem(fontSizes.h4)};
    font-family: ${fontFamilies.heading};
    line-height: 1.3;
    margin-bottom: ${rem(spacing.xs)};
    transition: ${transitions.default};

    ${Container}:hover &,
    ${Container}:focus & {
        color: ${colors.purplishBlue};
    }

    ${props =>
        props.size === 'small' &&
        `
    font-size: ${rem(fontSizes.h5)};
    margin-bottom: 0;`}
`;

export const InfoContainer = styled.div`
    align-items: center;
    color: ${colors.greyDark};
    display: flex;
    font-size: ${rem(14)};
    margin-top: ${rem(spacing.xs)};

    > * + * {
        padding-left: ${rem(spacing.m)};
        position: relative;

        &::before {
            background-color: ${colors.blueGrey};
            content: '';
            height: 120%;
            margin-left: ${rem(spacing.s * -1)};
            position: absolute;
            top: 50%;
            transform: translateY(-50%);
            width: 1px;
        }
    }
`;

export const Modules = styled.span`
    display: inline-block;
    color: ${colors.greyDark};
    line-height: 1.3;
`;

export const Progress = styled.span`
    display: inline-block;
    color: ${props => (props.progress === 100 ? colors.weirdGreen : colors.mediumPink)};
    line-height: 1.3;
    position: relative;
`;

export const ProgressIcon = styled.svg`
    height: auto;
    margin-right: ${rem(spacing.xs)};
    transition: ${transitions.default};
    width: ${rem(14)};

    path {
        fill: currentColor;
    }
`;

export const Description = styled.p`
    color: ${colors.greyDark};
    line-height: 1.4;
    margin-top: ${rem(spacing.s)};
`;

export const ArrowIcon = styled.svg`
    position: absolute;
    right: ${rem(spacing.m)};
    top: 50%;
    transform: translateY(-50%);
    transition: ${transitions.default};

    path {
        fill: ${colors.purplishBlue};
    }

    ${Container}:hover &,
    ${Container}:focus & {
        transform: scale(1.15) translateY(-50%);
    }
`;

export const NewBadge = styled.div`
    animation-delay: 0.4s !important;
    animation-duration: 0.8s !important;
    align-items: center;
    background-color: ${colors.mediumPink};
    border-radius: ${rem(2)};
    color: ${colors.offWhite};
    display: inline-block;
    font-family: ${fontFamilies.bold};
    font-size: ${rem(13)};
    height: auto;
    padding: ${rem(2)} ${rem(spacing.s)};
    vertical-align: middle;
`;

export const NewBadgeIcon = styled.svg`
    margin-right: ${rem(4)};
    margin-top: ${rem(-4)};
    vertical-align: middle;
    width: ${rem(14)};

    path {
        fill: currentColor;
    }
`;
