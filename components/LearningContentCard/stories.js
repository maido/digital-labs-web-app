// @flow
import React from 'react';
import {addDecorator, storiesOf} from '@storybook/react';
import {withKnobs, number, boolean, object, select, text} from '@storybook/addon-knobs';
import LearningContentCard from './';
import Theme from '../Theme';

const stories = storiesOf('LearningContentCard', module);

stories.addDecorator(withKnobs);

stories.add('basic', () => {
    return (
        <LearningContentCard title="Knowledge lab" items={{type: 'module', all: 4, completed: 1}} />
    );
});

stories.add('with no progress', () => {
    return (
        <LearningContentCard title="Knowledge lab" items={{type: 'module', all: 4, completed: 0}} />
    );
});

stories.add('with completed progress', () => {
    return (
        <LearningContentCard title="Knowledge lab" items={{type: 'module', all: 4, completed: 4}} />
    );
});

stories.add('with a longer title', () => {
    return (
        <LearningContentCard
            title="Knowledge lab and another lab"
            items={{type: 'module', all: 4, completed: 1}}
        />
    );
});

stories.add('with `new` badge', () => {
    return (
        <LearningContentCard
            title="Knowledge lab"
            isNew={true}
            items={{type: 'module', all: 4, completed: 1}}
        />
    );
});

stories.add('with an image', () => {
    return (
        <LearningContentCard
            title="Knowledge lab"
            image="https://placehold.it/700x300"
            items={{type: 'module', all: 4, completed: 1}}
        />
    );
});

stories.add('with a description', () => {
    return (
        <LearningContentCard
            title="Knowledge lab"
            image="https://placehold.it/700x300"
            items={{type: 'module', all: 4, completed: 1}}
            headline="Ex velit dolore ipsum ipsum elit nulla quis labore voluptate consectetur proident fugiat."
        />
    );
});

stories.add('with a different module type', () => {
    return (
        <LearningContentCard
            title="Knowledge lab"
            image="https://placehold.it/700x300"
            items={{type: 'module', all: 4, completed: 1, type: 'topic'}}
            headline="Ex velit dolore ipsum ipsum elit nulla quis labore voluptate consectetur proident fugiat."
        />
    );
});

stories.add('with a smaller title', () => {
    return (
        <LearningContentCard
            title="Knowledge lab"
            image="https://placehold.it/700x300"
            items={{type: 'module', all: 4, completed: 1}}
            small="true"
            headline="Ex velit dolore ipsum ipsum elit nulla quis labore voluptate consectetur proident fugiat."
        />
    );
});

stories.add('with a difficulty indicator', () => {
    return (
        <LearningContentCard
            title="Knowledge lab"
            image="https://placehold.it/700x300"
            items={{type: 'module', all: 4, completed: 1, type: 'topic'}}
            difficulty="Intermediate"
            headline="Ex velit dolore ipsum ipsum elit nulla quis labore voluptate consectetur proident fugiat."
        />
    );
});

stories.add('playground', () => {
    return (
        <LearningContentCard
            image={text('Image', 'https://placehold.it/700x300')}
            title={text('Title', 'Knowledge lab and another lab')}
            isNew={boolean('Is new?', false)}
            difficulty={select(
                'Difficulty',
                {
                    None: null,
                    Beginner: 'Beginner',
                    Intermediate: 'Intermediate',
                    Advanced: 'Advanced'
                },
                'None'
            )}
            items={object('Items', {all: 4, completed: 2, type: 'module'})}
            size={select('Size', {Small: 'small', Default: 'default'}, 'default')}
            headline={text(
                'Headline',
                'Ex velit dolore ipsum ipsum elit nulla quis labore voluptate consectetur proident fugiat.'
            )}
        />
    );
});
