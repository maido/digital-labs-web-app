// @flow
import React from 'react';
import {addDecorator, storiesOf} from '@storybook/react';
import DifficultyIndicator from './';
import Theme from '../Theme';

const stories = storiesOf('DifficultyIndicator', module);

stories.addDecorator(story => (
    <Theme theme="secondary">
        <div style={{padding: 20}}>{story()}</div>
    </Theme>
));

stories.add('beginner', () => {
    return <DifficultyIndicator difficulty="Beginner" />;
});

stories.add('intermediate', () => {
    return <DifficultyIndicator difficulty="Intermediate" />;
});

stories.add('advanced', () => {
    return <DifficultyIndicator difficulty="Advanced" />;
});
