// @flow
import styled from '@emotion/styled';
import {rem} from 'polished';
import {colors, spacing} from '../../globals/variables';

export const Container = styled.div`
    align-items: center;
    display: flex;
`;

export const Icon = styled.svg`
    height: ${rem(14)};
    width: auto;

    path {
        fill: currentColor;
        opacity: 0.25;
    }
    path:nth-of-type(2) {
        opacity: 1;
    }

    ${props =>
        props.difficulty &&
        props.difficulty.toLowerCase() === 'intermediate' &&
        `path:nth-of-type(3) {
        opacity: 1;
    }`}
    ${props =>
        props.difficulty &&
        props.difficulty.toLowerCase() === 'advanced' &&
        `path{
        opacity: 1;
    }`}
`;

export const Label = styled.span`
    margin-left: ${rem(spacing.xs)};
`;
