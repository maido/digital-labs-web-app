// @flow
import * as React from 'react';
import * as S from './styles';

type Props = {
    difficulty: 'Beginner' | 'Intermediate' | 'Advanced'
};

const DifficultyIndicator = ({difficulty}: Props) => (
    <S.Container>
        <S.Icon
            width="16"
            height="16"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 16 16"
            difficulty={difficulty}
        >
            <g fillRule="evenodd">
                <path d="M12 16h4V0h-4z" />
                <path d="M0 16h4V8H0z" />
                <path d="M6 5v11h4V5z" />
            </g>
        </S.Icon>

        <S.Label>{difficulty}</S.Label>
    </S.Container>
);

export default DifficultyIndicator;
