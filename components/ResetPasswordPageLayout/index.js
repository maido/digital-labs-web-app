// @flow
import React, {useEffect, useRef, useState} from 'react';
import {useSpring, animated} from 'react-spring/web.cjs';
import Link from 'next/link';
import dynamic from 'next/dynamic';
import {useRouter} from 'next/router';
import axios from 'axios';
import get from 'lodash/get';
import Cookie from 'js-cookie';
import {connect} from 'react-redux';
import {Formik, Form} from 'formik';
import __ from '../../globals/strings';
import {updateForm, updateUser} from '../../store/actions';
import passwordResetSchema from '../../schemas/reset-password';
import AnimateWhenVisible from '../AnimateWhenVisible';
import Container from '../Container';
import CTAButton from '../CTAButton';
import InputText from '../InputText';
import Logo from '../Logo';
import * as UI from '../UI/styles';
import * as S from '../LoginPageLayout/styles';

const FullScreenMessage = dynamic(() => import('../FullScreenMessage'), {ssr: false});

type Props = {
    dispatch: Function,
    form: Form,
    token: string
};

const ResetPasswordPageLayout = ({dispatch, form, token}: Props) => {
    const router = useRouter();
    const [resetSuccessVisibility, setResetSuccessVisibility] = useState('hidden');
    const logoAnimation = useSpring({
        config: {tension: 200, mass: 5, friction: 30},
        from: {transform: `scale(0.4)`, opacity: 0},
        to: {transform: `scale(1)`, opacity: 1}
    });

    const handleFormSubmit = async (values, handlers) => {
        dispatch(updateForm({state: 'pending', fields: values}));

        try {
            const response = await axios.post(`/api/account/reset-password`, values);

            handlers.setSubmitting(false);
            setResetSuccessVisibility('visible');
        } catch (error) {
            dispatch(updateForm({state: 'default'}));

            const errors = get(error, 'response.data')
                ? get(error, 'response.data')
                : {
                      password_confirmation: `${__('account.resetPassword.errors.reset')} (${
                          error.response.status
                      })`
                  };
            handlers.setErrors(errors);
            handlers.setSubmitting(false);
        }
    };

    return (
        <Container css={S.container}>
            {resetSuccessVisibility !== 'hidden' && (
                <FullScreenMessage
                    state="visible"
                    title={__('account.resetPassword.success.title')}
                    text={__('account.resetPassword.success.text')}
                    cta={{label: __('account.resetPassword.success.ctaLabel'), url: '/login'}}
                />
            )}

            <UI.ResponsiveSpacer />

            <animated.div style={logoAnimation}>
                <Link href="/">
                    <a>
                        <Logo />
                    </a>
                </Link>
            </animated.div>

            <S.MainContent>
                <AnimateWhenVisible delay={300}>
                    <UI.ResponsiveSpacer size="l" />
                    <UI.Heading3 as="h1" css={S.pageHeading}>
                        {__('account.resetPassword.title')}
                    </UI.Heading3>
                </AnimateWhenVisible>

                <S.FormContainer>
                    <Formik
                        initialValues={form.fields}
                        onSubmit={handleFormSubmit}
                        validationSchema={passwordResetSchema}
                        validateOnChange={false}
                        render={formProps => (
                            <Form autoComplete="off">
                                <InputText name="password" label="Your password" type="password" />
                                <UI.Spacer />
                                <InputText
                                    name="password_confirmation"
                                    label="Confirm password"
                                    type="password"
                                />

                                <UI.Spacer />

                                <CTAButton
                                    disabled={formProps.isSubmitting || formProps.isValidating}
                                    type="submit"
                                    state={
                                        formProps.isSubmitting || formProps.isValidating
                                            ? 'pending'
                                            : form.state
                                    }
                                    block={true}
                                >
                                    Submit
                                </CTAButton>
                            </Form>
                        )}
                    />
                </S.FormContainer>
            </S.MainContent>
        </Container>
    );
};

const mapStateToProps = state => ({form: state.form});

export default connect(mapStateToProps)(ResetPasswordPageLayout);
