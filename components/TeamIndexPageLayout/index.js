// @flow
import React, {useState} from 'react';
import dynamic from 'next/dynamic';
import dayjs from 'dayjs';
import get from 'lodash/get';
import __ from '../../globals/strings';
import {getContentStatus} from '../../globals/functions';
import AnimateWhenVisible from '../AnimateWhenVisible';
import BreadcrumbNav from '../BreadcrumbNav';
import Container from '../Container';
import CTAButton from '../CTAButton';
import PageIntro from '../PageIntro';
import PitchModal from '../PitchModal';
import TextCardPlaceholder from '../Placeholder/TextCard';
import TimelineList from '../TimelineList';
import TeamDetailCard from '../TeamDetailCard';
import TeamSummaryCardPlaceholder from '../Placeholder/TeamSummaryCard';
import Theme from '../Theme';
import * as UI from '../UI/styles';

const Modal = dynamic(() => import('../Modal'), {ssr: false});

type Props = {
    breadcrumbNav?: Array<Object>,
    hasFetched: boolean,
    network: NetworkStatus,
    submissions: Array<Object>,
    team: Team,
    user: User
};

const getSortedSubmissions = (submissions = []) => {
    const sortedSubmissions = {};
    let totalSubmissions = 0;

    submissions.map(submission => {
        const date = dayjs(submission.created_at).format('MMMM D');
        const fileType = submission.file ? submission.file.mime_type.split('/') : '';

        if (!sortedSubmissions[date]) {
            sortedSubmissions[date] = [];
        }

        sortedSubmissions[date].push({
            id: submission.id,
            footnote: `Topic: ${submission.topic.title}`,
            title: submission.assignment.description,
            text: submission.reflections ? `"${submission.reflections}"` : '',
            url: submission.file ? submission.file.url : '',
            urlLabel: fileType ? `View ${fileType[1].toUpperCase()}` : ''
        });

        totalSubmissions = totalSubmissions + 1;
    });

    return {totalSubmissions, sortedSubmissions};
};

const TeamIndexPageLayout = ({
    breadcrumbNav,
    hasFetched,
    network,
    submissions,
    team,
    user
}: Props) => {
    const [modalVisibility, setModalVisibility] = useState('hidden');
    const {totalSubmissions, sortedSubmissions} = getSortedSubmissions(submissions);
    const {showContent, showFallback, showPlaceholders} = getContentStatus(
        network.isLoading,
        hasFetched,
        team && team.id,
        true
    );

    const handleViewPitchClick = () => setModalVisibility('visible');

    const handleClosePitch = () => {
        setModalVisibility('leaving');
        setTimeout(() => setModalVisibility('hidden'), 600);
    };

    return (
        <>
            {modalVisibility !== 'hidden' && (
                <Modal state={modalVisibility} handleClose={handleClosePitch}>
                    <PitchModal name={team.name} pitch={team.pitch} />
                </Modal>
            )}

            <Container paddingVertical={false}>
                {breadcrumbNav && <BreadcrumbNav links={breadcrumbNav} />}
            </Container>

            <Theme theme="tertiary" overlap="secondary" overlapSize={150} grow={true}>
                <UI.ShowAt breakpoint="mobile">
                    <UI.Spacer />
                </UI.ShowAt>
                <Container paddingVertical="l">
                    <UI.LayoutContainer size="l">
                        <UI.LayoutItem sizeAtTabletSmall={5 / 12} css={UI.stickyAt('mobile')}>
                            {showPlaceholders && <TeamSummaryCardPlaceholder />}
                            {showContent && <TeamDetailCard team={team} type="detail" />}
                            <UI.Spacer />
                        </UI.LayoutItem>
                        <UI.LayoutItem sizeAtTabletSmall={7 / 12}>
                            <AnimateWhenVisible delay={-1}>
                                <UI.Box theme="tertiary" paddingAtMobile={false}>
                                    <UI.HideAt breakpoint="mobile">
                                        <UI.Spacer />
                                    </UI.HideAt>
                                    <UI.FlexMediaWrapper>
                                        <UI.Heading3 css={UI.flexGrow1}>
                                            Team submissions
                                        </UI.Heading3>
                                        {showContent && get(team, 'pitch.submitted_at') && (
                                            <UI.FlexMediaContent>
                                                <CTAButton onClick={handleViewPitchClick}>
                                                    View pitch
                                                </CTAButton>
                                            </UI.FlexMediaContent>
                                        )}
                                    </UI.FlexMediaWrapper>

                                    {showPlaceholders && (
                                        <>
                                            <UI.Spacer />
                                            <TextCardPlaceholder />
                                            <UI.Spacer />
                                            <TextCardPlaceholder />
                                            <UI.Spacer />
                                            <TextCardPlaceholder />
                                        </>
                                    )}
                                    {showContent && (
                                        <TimelineList
                                            items={sortedSubmissions}
                                            totalItems={totalSubmissions}
                                            type="submission"
                                        >
                                            <UI.Spacer />
                                            {team.id === user.team.id ? (
                                                <>
                                                    <p>{__('teams.submissions.emptyUser')}</p>
                                                    <CTAButton href="/labs" wide={true}>
                                                        Go to labs
                                                    </CTAButton>
                                                </>
                                            ) : (
                                                <>
                                                    <p>{__('teams.submissions.empty')}</p>
                                                </>
                                            )}
                                        </TimelineList>
                                    )}
                                    {showFallback && (
                                        <AnimateWhenVisible delay={200} watch={false}>
                                            <UI.Spacer />
                                            <UI.Box
                                                radius={20}
                                                data-testid="nocontent"
                                                css={UI.centerText}
                                                style={{padding: 30}}
                                            >
                                                <UI.Heading5 as="h2" css={UI.margin('bottom', 's')}>
                                                    No submissions yet...
                                                </UI.Heading5>
                                                <p>{__('teams.submissions.empty')}</p>
                                            </UI.Box>
                                        </AnimateWhenVisible>
                                    )}
                                </UI.Box>
                            </AnimateWhenVisible>
                        </UI.LayoutItem>
                    </UI.LayoutContainer>
                </Container>
            </Theme>
        </>
    );
};

export default TeamIndexPageLayout;
