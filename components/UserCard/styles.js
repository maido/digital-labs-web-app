// @flow
import {css} from '@emotion/core';
import styled from '@emotion/styled';
import {rem} from 'polished';
import {responsiveRem} from '../../globals/functions';
import {
    breakpoints,
    colors,
    fontFamilies,
    fontSizes,
    shadows,
    spacing,
    themes,
    transitions
} from '../../globals/variables';

type ContainerProps = {
    padded?: boolean
};

export const container = (props: ContainerProps) => css`
    color: ${colors.darkBlueGrey};

    ${props.padded &&
        `
    border: 1px solid ${colors.grey};
    border-radius: ${rem(3)};
    padding: ${rem(spacing.s)};
`}
`;

export const content = css`
    line-height: 1.3;
`;

export const Name = styled.strong`
    font-family: ${fontFamilies.bold};
    font-weight: normal;
    transition: ${transitions.default};

    a:hover &,
    a:focus & {
        color: ${colors.purplishBlue};
    }
`;

export const Info = styled.span`
    display: block;
    font-size: ${rem(14)};
    margin-top: ${rem(-2)};
`;
