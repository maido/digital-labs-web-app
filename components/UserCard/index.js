// @flow
import * as React from 'react';
import {connect} from 'react-redux';
import ReactCountryFlag from 'react-country-flag';
import {countryList} from '../../globals/content';
import findKey from 'lodash/findKey';
import isEqual from 'lodash/isEqual';
import partial from 'lodash/partial';
import * as S from './styles';
import * as UI from '../UI/styles';

type Props = {
    loggedInUser: Object,
    padded?: boolean,
    user: Object
};

const UserCard = ({loggedInUser, padded = false, user}: Props) => {
    const countryCode = findKey(countryList, partial(isEqual, user.country_residence));
    let isLoggedInUser = false;

    if (loggedInUser && loggedInUser.isAuthenticated && loggedInUser.id === user.id) {
        isLoggedInUser = true;
    }

    return (
        <UI.FlexMediaWrapper css={S.container({padded})}>
            <UI.Avatar
                size="medium"
                image={user.avatar ? user.avatar.url : null}
                title={`${user.first_name} ${user.last_name}`}
            />
            <UI.FlexMediaContent margin="s">
                <S.Name>
                    {user.first_name} {user.last_name}
                </S.Name>

                <S.Info>
                    <ReactCountryFlag code={countryCode} styleProps={{width: 20}} svg />
                    &nbsp;{isLoggedInUser ? "(That's you 👋)" : user.country_residence}
                </S.Info>
            </UI.FlexMediaContent>
        </UI.FlexMediaWrapper>
    );
};

const mapStateToProps = state => ({
    loggedInUser: state.user
});

export default connect(mapStateToProps)(UserCard);
