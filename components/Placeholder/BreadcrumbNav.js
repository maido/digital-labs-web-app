// @flow
import React from 'react';
import * as S from './styles';

type Props = {
    width?: string
};

const BreadcrumbNavPlaceholder = ({width = '30%'}: Props) => (
    <S.BreadcrumbNav data-testid="placeholderbreadcrumb" style={{opacity: 0.25, width}} />
);

export default BreadcrumbNavPlaceholder;
