// @flow
import React from 'react';
import * as UI from '../UI/styles';
import * as S from './styles';

type Props = {
    hasHeadline?: boolean,
    size?: string
};

const TextCardPlaceholder = ({hasHeadline = true, size = 'default'}: Props) => (
    <div
        style={{borderRadius: 5, overflow: 'hidden', minHeight: '100%'}}
        data-testid="placeholdertextcard"
    >
        <S.ContainerCard>
            <S.Title style={{height: 30, width: '80%'}} />
            <UI.Spacer size={size === 'default' ? 'm' : 'xs'} />

            {hasHeadline && (
                <>
                    <S.Paragraph style={{width: '90%'}} />
                    <UI.Spacer size="xs" />
                    <S.Paragraph style={{width: '90%'}} />
                </>
            )}
        </S.ContainerCard>
    </div>
);

export default TextCardPlaceholder;
