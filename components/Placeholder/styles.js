// @flow
import {css, keyframes} from '@emotion/core';
import styled from '@emotion/styled';
import {rem} from 'polished';
import {breakpoints, colors, shadows, spacing} from '../../globals/variables';

const loadingAnimation = keyframes`
    0% {
        transform: translate3d(-50%, 0, 0);
    }
    100% {
        transform: translate3d(50%, 0, 0);
    }
`;

const placeholderStyle = css`
    background-color: ${colors.paleGrey};
    border-radius: 6px;
    overflow: hidden;
    position: relative;

    &::before {
        animation: ${loadingAnimation} 0.8s linear infinite;
        background: linear-gradient(
                to right,
                rgba(0, 0, 0, 0) 36%,
                rgba(0, 0, 0, 0.05) 50%,
                rgba(0, 0, 0, 0) 64%
            )
            50% 50%;
        content: '';
        height: 100%;
        left: 0;
        position: absolute;
        width: 300%;
    }
`;

export const ContainerCenter = styled.div`
    text-align: center;

    * {
        margin-left: auto;
        margin-right: auto;
    }
`;

export const ContainerCard = styled.div`
    background-color: ${colors.white};
    box-shadow: ${shadows.default};
    padding: ${rem(spacing.s)};

    @media (min-width: ${rem(breakpoints.mobile)}) {
        padding: ${rem(spacing.m)};
    }
`;

export const Subtitle = styled.div`
    ${placeholderStyle};
    height: ${rem(20)};
    line-height: 1.7;
    width: ${rem(200)};
`;

export const Title = styled.div`
    ${placeholderStyle};
    height: ${rem(46)};
    width: ${rem(550)};
`;

export const Paragraph = styled.div`
    ${placeholderStyle};
    height: ${rem(22)};
    width: 100%;

    & + & {
        margin-top: ${rem(spacing.xs)};
    }
`;

export const BreadcrumbNav = styled.div`
    ${placeholderStyle};
    height: ${rem(27)};
    width: ${rem(100)};
`;

export const Button = styled.div`
    ${placeholderStyle};
    background-color: ${colors.primary};
    border-radius: ${rem(50)};
    height: ${rem(48)};
    width: ${rem(160)};
`;

export const Avatar = styled.div`
    ${placeholderStyle};
    border-radius: 100%;
    height: ${rem(90)};
    overflow: hidden;
    width: ${rem(90)};
`;

export const FlexContent = styled.div`
    margin-left: ${rem(spacing.m)};
    flex-grow: 1;
`;
