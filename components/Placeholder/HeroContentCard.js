// @flow
import React from 'react';
import {Container, ContentContainer, Image} from '../HeroContentCard/styles';
import * as UI from '../UI/styles';
import * as S from './styles';

const HeroContentCardPlaceholder = () => (
    <>
        <Image style={{backgroundColor: '#efeff4'}} />
        <ContentContainer data-testid="placeholderherocard">
            <S.Subtitle style={{height: 16, width: '30%'}} />
            <UI.Spacer size="s" />
            <S.Title style={{height: 36, width: '60%'}} />
            <UI.Spacer />
            <S.Paragraph style={{width: '100%'}} />
            <S.Paragraph style={{width: '70%'}} />
            <S.Paragraph style={{width: '80%'}} />
            <UI.Spacer />
            <S.Button />
        </ContentContainer>
    </>
);

export default HeroContentCardPlaceholder;
