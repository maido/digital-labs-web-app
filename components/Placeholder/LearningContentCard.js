// @flow
import React from 'react';
import * as UI from '../UI/styles';
import * as S from './styles';

type Props = {
    hasHeadline?: boolean,
    hasImage?: boolean,
    size?: string
};

const LearningContentCardPlaceholder = ({
    hasHeadline = true,
    hasImage = true,
    size = 'default'
}: Props) => (
    <div
        style={{borderRadius: 5, overflow: 'hidden', minHeight: '100%'}}
        data-testid="placeholderlearningcontent"
    >
        {hasImage && <S.Title style={{borderRadius: 0, height: 180, width: '100%'}} />}

        <S.ContainerCard>
            <S.Title style={{height: 30, width: '80%'}} />
            <UI.Spacer size={size === 'default' ? 'm' : 'xs'} />
            <S.Subtitle style={{height: 20, width: '30%'}} />

            {hasHeadline && (
                <>
                    <UI.Spacer size="s" />
                    <S.Paragraph style={{width: '90%'}} />
                    <UI.Spacer size="xs" />
                    <S.Paragraph style={{width: '90%'}} />
                </>
            )}
        </S.ContainerCard>
    </div>
);

export default LearningContentCardPlaceholder;
