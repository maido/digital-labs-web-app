// @flow
import React from 'react';
import * as UI from '../UI/styles';
import * as S from './styles';

const TeamSummaryCard = () => (
    <div
        style={{borderRadius: 5, overflow: 'hidden', minHeight: '100%'}}
        data-testid="placeholderteamsummarycard"
    >
        <S.ContainerCard style={{display: 'flex'}}>
            <S.Avatar />
            <S.FlexContent>
                <S.Title style={{height: 30, width: '80%'}} />
                <UI.Spacer size="s" />

                <S.Paragraph style={{height: 40, width: '40%'}} />
            </S.FlexContent>
        </S.ContainerCard>
    </div>
);

export default TeamSummaryCard;
