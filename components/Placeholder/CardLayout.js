// @flow
import * as React from 'react';
import CardLayout from '../CardLayout';

type Props = {
    columns: number,
    component: React.Node,
    size: number
};

const CardLayoutPlaceholder = ({columns, component, size}: Props) => (
    <CardLayout
        columns={columns}
        items={[...Array(size).keys()].map(index => ({
            key: index,
            component
        }))}
        animate={false}
        watchForAnimations={false}
    />
);

export default CardLayoutPlaceholder;
