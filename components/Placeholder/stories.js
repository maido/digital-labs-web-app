// @flow
import React from 'react';
import {storiesOf} from '@storybook/react';
import BreadcrumbNavPlaceholder from './BreadcrumbNav';
import LearningContentCardPlaceholder from './LearningContentCard';
import PageIntroPlaceholder from './PageIntro';
import Theme from '../Theme';

const stories = storiesOf('Placeholders', module);

stories.add('breadcrumb nav', () => {
    return (
        <Theme theme="secondary" style={{padding: 20}}>
            <BreadcrumbNavPlaceholder />
        </Theme>
    );
});

stories.add('page intro', () => {
    return (
        <Theme theme="secondary" style={{padding: 20}}>
            <PageIntroPlaceholder />
        </Theme>
    );
});

stories.add('lab card', () => {
    return (
        <Theme theme="secondary" style={{padding: 20}}>
            <LearningContentCardPlaceholder />
        </Theme>
    );
});
