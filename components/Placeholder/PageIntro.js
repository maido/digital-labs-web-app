// @flow
import React from 'react';
import {Container} from '../PageIntro/styles';
import * as UI from '../UI/styles';
import * as S from './styles';

type Props = {
    hasHeadline?: boolean,
    hasSubtitle?: boolean
};

const PageIntroPlaceholder = ({hasHeadline = true, hasSubtitle = true}: Props) => (
    <Container style={{opacity: 0.25, width: '100%'}} data-testid="placeholderpageintro">
        <S.ContainerCenter>
            {hasSubtitle && (
                <>
                    <S.Subtitle style={{width: '30%'}} />
                    <UI.Spacer size="s" />
                </>
            )}

            <S.Title style={{width: '80%'}} />

            {hasHeadline && (
                <>
                    <UI.Spacer size="s" />
                    <S.Paragraph style={{width: '90%'}} />
                    <S.Paragraph style={{width: '80%'}} />
                </>
            )}
        </S.ContainerCenter>
    </Container>
);

export default PageIntroPlaceholder;
