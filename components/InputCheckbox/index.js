// @flow
import * as React from 'react';
import {ErrorMessage, Field} from 'formik';
import * as FormStyles from '../Form/styles';
import * as S from './styles';

type Props = {
    children?: React.Node,
    name: string,
    label: string,
    value?: string
};
type FieldProps = {
    name: string,
    label: string
};

export const InputCheckboxField = ({children, name, label, ...props}: FieldProps) => (
    <S.Container>
        <S.Input id={name} type="checkbox" name={name} {...props} />
        <S.Label htmlFor={name}>{children ? children : label}</S.Label>
    </S.Container>
);

const InputCheckbox = ({children, name, label, value, ...props}: Props) => (
    <Field name={name}>
        {({field, form}) => {
            if (field.value === true) {
                field.checked = 'checked';
            }

            return (
                <>
                    <InputCheckboxField name={name} label={label} {...field} {...props}>
                        {children}
                    </InputCheckboxField>
                    <ErrorMessage
                        component="span"
                        name={name}
                        css={FormStyles.error}
                        data-testid={`error-${name}`}
                    />
                </>
            );
        }}
    </Field>
);

export default InputCheckbox;
