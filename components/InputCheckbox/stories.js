// @flow
import React from 'react';
import {storiesOf} from '@storybook/react';
import {Formik, Form} from 'formik';
import accountCreationSchema from '../../schemas/create-account';
import InputButtonGroup from '../InputButtonGroup';
import InputCheckbox from './';
import {occupations} from '../../globals/content';

const stories = storiesOf('InputCheckbox', module);

stories.add('default', () => {
    return (
        <Formik
            initialValues={{}}
            onSubmit={() => {}}
            render={() => (
                <Form>
                    <InputCheckbox name="terms" label="I agree to the T&Cs" />
                </Form>
            )}
        />
    );
});

stories.add('has error', () => {
    return (
        <Formik
            initialValues={{terms: null}}
            isInitialValid={true}
            validationSchema={accountCreationSchema.step2}
            onSubmit={() => {}}
            render={props => (
                <Form>
                    <InputCheckbox name="terms" label="I agree to the T&Cs" />
                </Form>
            )}
        />
    );
});

stories.add('has field disabled', () => {
    return (
        <Formik
            initialValues={{}}
            onSubmit={() => {}}
            render={() => (
                <Form>
                    <InputCheckbox name="terms" label="I agree to the T&Cs" disabled={true} />
                </Form>
            )}
        />
    );
});
