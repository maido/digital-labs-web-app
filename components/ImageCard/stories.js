// @flow
import React from 'react';
import {addDecorator, storiesOf} from '@storybook/react';
import ImageCard from './';
import Theme from '../Theme';

const stories = storiesOf('ImageCard', module);

stories.addDecorator(story => (
    <Theme theme="secondary">
        <div style={{padding: 30, maxWidth: 600}}>{story()}</div>
    </Theme>
));

stories.add('default', () => {
    return (
        <ImageCard
            image="https://placehold.it/600x350"
            text="Laborum velit consequat eiusmod enim."
        />
    );
});

stories.add('with lots of text', () => {
    return (
        <ImageCard
            image="https://placehold.it/600x350"
            text="Mollit incididunt est Lorem commodo incididunt. Elit quis sit anim velit laborum cupidatat et reprehenderit officia id fugiat consequat tempor."
        />
    );
});
