// @flow
import styled from '@emotion/styled';
import {rem} from 'polished';
import {responsiveRem} from '../../globals/functions';
import {colors, fontFamilies, spacing} from '../../globals/variables';

export const Container = styled.div`
    ${props => props.image && `background-image: url('${props.image}');`}
    background-position: center center;
    background-size: cover;
    height: 0;
    padding-bottom: 65%;
    position: relative;
    width: 100%;
`;

export const Caption = styled.span`
    background-color: ${colors.white};
    border-radius: ${rem(2)};
    bottom: ${responsiveRem(spacing.s)};
    color: ${colors.darkBlueGrey};
    font-family: ${fontFamilies.bold};
    left: ${responsiveRem(spacing.s)};
    line-height: 1.4;
    max-width: calc(100% - ${rem(spacing.m * 2)});
    padding: ${rem(spacing.xs * 1.5)} ${rem(spacing.s * 1.5)};
    position: absolute;
`;
