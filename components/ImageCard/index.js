// @flow
import * as React from 'react';
import * as S from './styles';

type Props = {
    image?: string,
    text?: string
};

const ImageCard = ({image, text}: Props) => (
    <S.Container image={image}>{text && <S.Caption>{text}</S.Caption>}</S.Container>
);

export default ImageCard;
