// @flow
import React from 'react';
import * as S from './styles';

const ScrollIndicator = () => (
    <S.SVG
        width="52"
        height="52"
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 52 52"
        title="Scroll to content"
        ariaLabel="Scroll to content"
    >
        <g transform="translate(1 1)" fill="none" fillRule="evenodd">
            <circle cx="25" cy="25" r="25" />
            <path d="M13 37V13h24v24z" />
            <path d="M29.09 21.59l-4.59 4.58-4.59-4.58L18.5 23l6 6 6-6z" />
        </g>
    </S.SVG>
);

export default ScrollIndicator;
