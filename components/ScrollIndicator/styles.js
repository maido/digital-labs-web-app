// @flow
import {keyframes} from '@emotion/core';
import styled from '@emotion/styled';
import {colors, transitions} from '../../globals/variables';

const bounceAnimation = keyframes`
    0% { transform: translateY(0); }
    50% { transform: translateY(-20px); }
    100% { transform: translateY(0); }
`;

export const SVG = styled.svg`
    animation: ${bounceAnimation} 2s infinite ease;

    circle {
        opacity: 0.6;
        stroke: ${props => props.theme.text};
        transition: ${transitions.default};
    }
    path:last-of-type {
        fill: ${props => props.theme.text};
        transition: ${transitions.default};
    }

    &:hover circle,
    &:focus circle {
        opacity: 1;
    }
`;
