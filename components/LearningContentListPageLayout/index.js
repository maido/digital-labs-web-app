// @flow
import React, {useState} from 'react';
import {useRouter} from 'next/router';
import get from 'lodash/get';
import size from 'lodash/size';
import AnimateWhenVisible from '../AnimateWhenVisible';
import {
    getCardPropsForLearningContent,
    getContentStatus,
    getSyllabusSubtitle
} from '../../globals/functions';
import {logEvent} from '../../globals/analytics';
import __ from '../../globals/strings';
import BreadcrumbNav from '../BreadcrumbNav';
import ButtonList from '../ButtonList';
import CardLayout from '../CardLayout';
import CardLayoutPlaceholder from '../Placeholder/CardLayout';
import Container from '../Container';
import CTAButton from '../CTAButton';
import LearningContentCard from '../LearningContentCard';
import BreadcrumbNavPlaceholder from '../Placeholder/BreadcrumbNav';
import PageIntroPlaceholder from '../Placeholder/PageIntro';
import ProgressTabLinks from '../ProgressTabLinks';
import LearningContentCardPlaceholder from '../Placeholder/LearningContentCard';
import PageIntro from '../PageIntro';
import SlackBanner from '../SlackBanner';
import TextCard from '../TextCard';
import Theme from '../Theme';
import * as UI from '../UI/styles';

type Props = {
    breadcrumbNav?: Array<Object>,
    content: Object,
    contentType: 'course' | 'lab' | 'module' | 'syllabus',
    hasFetched: boolean,
    items: Array<Lab | Course | Module | Topic>,
    itemsType: 'lab' | 'course' | 'module' | 'topic',
    network: NetworkStatus,
    pageIntro?: Object
};

const LearningContentListPageLayout = ({
    breadcrumbNav,
    content,
    contentType,
    hasFetched,
    items,
    itemsType,
    network,
    pageIntro = {}
}: Props) => {
    const router = useRouter();
    const {view} = router.query;
    const [contentView, setContentView] = useState(view && itemsSize > 0 ? view : 'learn');

    const itemSize = size(items);
    const hasPageCache = useState(itemSize > 0);
    const columns = ['course', 'module'].includes(contentType) ? 1 : 2;

    const {showContent, showFallback, showPlaceholders} = getContentStatus(
        network.isLoading,
        hasFetched,
        itemSize > 0,
        true,
        true
    );

    /**
     * We treat the page intro different for Labs, as while we might not have child content
     * we will always have parent content (otherwise it's a 404).
     *
     * We also try to pull into state we already know to show the breadcrumb nav and page intro.
     * This means we can show that content while the labs content for cards loads (preventing a full page
     * of placeholders).
     */
    let pageIntroContent = pageIntro;

    if (contentType === 'syllabus') {
        pageIntroContent = {
            title: __('learningContent.syllabus.title'),
            subtitle: __('learningContent.syllabus.subtitle')
        };
    } else {
        if (content && (content.title || content.headline)) {
            pageIntroContent = {
                title: content.title,
                headline: content.headline
            };
        }

        if (itemsType && itemSize) {
            pageIntroContent.subtitle = getSyllabusSubtitle(itemsType, itemSize);
        }
    }

    return (
        <>
            <Container paddingVertical={0}>
                {showPlaceholders && !pageIntroContent && (
                    <>
                        <BreadcrumbNavPlaceholder />
                        <PageIntroPlaceholder />
                    </>
                )}
                {pageIntroContent && (
                    <>
                        {breadcrumbNav && <BreadcrumbNav links={breadcrumbNav} />}
                        <PageIntro
                            animate={hasPageCache === false}
                            title={pageIntroContent.title}
                            text={pageIntroContent.headline}
                            subtitle={pageIntroContent.subtitle}
                        />
                    </>
                )}
            </Container>

            <Theme
                theme="tertiary"
                overlap="secondary"
                overlapSize={contentType === 'course' ? 80 : 120}
                grow={true}
            >
                <Container
                    size={
                        ['course', 'module'].includes(contentType) && contentView !== 'resources'
                            ? 'medium'
                            : 'default'
                    }
                    paddingVertical="l"
                >
                    {size(get(content, 'resources')) > 0 && (
                        <div css={UI.centerText}>
                            <ButtonList blockAtMobile={false} center={true}>
                                {[
                                    <CTAButton
                                        theme={
                                            contentView === 'learn'
                                                ? 'activeButton'
                                                : 'inactiveButton'
                                        }
                                        onClick={() => {
                                            if (contentView !== 'learn') {
                                                setContentView('learn');
                                                logEvent({
                                                    action: 'Click',
                                                    category: 'Learning Content Tab',
                                                    label: 'Learn'
                                                });
                                            }
                                        }}
                                    >
                                        Learn
                                    </CTAButton>,
                                    <CTAButton
                                        theme={
                                            contentView === 'resources'
                                                ? 'activeButton'
                                                : 'inactiveButton'
                                        }
                                        onClick={() => {
                                            if (contentView !== 'resources') {
                                                setContentView('resources');
                                                logEvent({
                                                    action: 'Click',
                                                    category: 'Learning Content Tab',
                                                    label: 'Resources'
                                                });
                                            }
                                        }}
                                    >
                                        Resources
                                    </CTAButton>
                                ]}
                            </ButtonList>
                            <UI.ResponsiveSpacer />
                        </div>
                    )}

                    <div style={{display: contentView === 'learn' ? 'block' : 'none'}}>
                        {showPlaceholders && (
                            <CardLayoutPlaceholder
                                columns={columns}
                                size={6}
                                component={
                                    <LearningContentCardPlaceholder
                                        hasHeadline={contentType !== 'module'}
                                        hasImage={contentType === 'lab' ? true : false}
                                    />
                                }
                            />
                        )}
                        {showContent && (
                            <CardLayout
                                animate={false}
                                columns={columns}
                                items={items.map(item => {
                                    const cardContent = getCardPropsForLearningContent(
                                        item,
                                        contentType
                                    );

                                    return {
                                        key: item.id,
                                        component: <LearningContentCard {...cardContent} />
                                    };
                                })}
                            />
                        )}
                        {showFallback && (
                            <AnimateWhenVisible delay={200} watch={false}>
                                <UI.Box
                                    radius={20}
                                    data-testid="nocontent"
                                    style={{
                                        marginLeft: 'auto',
                                        marginRight: 'auto',
                                        maxWidth: 600,
                                        textAlign: 'center'
                                    }}
                                >
                                    <UI.Heading5 as="h2" css={UI.centerText}>
                                        {__('learningContent.emptyTitle')}
                                    </UI.Heading5>
                                    <p>{__('learningContent.empty')}</p>
                                </UI.Box>
                            </AnimateWhenVisible>
                        )}
                    </div>
                    <div style={{display: contentView === 'resources' ? 'block' : 'none'}}>
                        {content && content.resources && (
                            <CardLayout
                                animate={false}
                                items={content.resources.map(resource => ({
                                    key: resource.id,
                                    component: (
                                        <TextCard
                                            icon={resource.type.toLowerCase()}
                                            title={resource.title}
                                            text={resource.type}
                                            url={resource.url}
                                        />
                                    )
                                }))}
                            />
                        )}
                    </div>
                </Container>
                <UI.Spacer />
            </Theme>
            <SlackBanner
                trackingLabel={`${contentType} - ${pageIntroContent ? pageIntroContent.title : ''}`}
            />
        </>
    );
};

export default LearningContentListPageLayout;
