// @flow
import styled from '@emotion/styled';
import {rem} from 'polished';
import {breakpoints, spacing} from '../../globals/variables';

export const Container = styled.ul`
    align-items: center;
    display: flex;
    flex-wrap: wrap;
    list-style: none;
    justify-content: center;
    margin: 0;
    margin-bottom: ${rem(spacing.m * -1)};
    padding: 0 ${rem(spacing.m)};
`;

export const LogoWrapper = styled.li`
    display: inline-block;
    margin-bottom: ${rem(spacing.m)};
    max-width: 100%;
    padding-left: ${rem(spacing.s)};
    padding-right: ${rem(spacing.s)};
    text-align: center;
    width: 100%;

    @media (min-width: ${rem(breakpoints.mobileSmall)}) {
        max-width: ${rem(220)};
    }
`;

export const Logo = styled.img`
    // width: 100%;
`;
