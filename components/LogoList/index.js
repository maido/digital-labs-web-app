// @flow
import React from 'react';
import * as UI from '../UI/styles';
import * as S from './styles';

type Props = {
    logos: Array<Object>
};

const LogoList = ({logos = []}: Props) => (
    <S.Container>
        {logos.map(logo => (
            <S.LogoWrapper key={logo.title}>
                <S.Logo
                    src={logo.image}
                    srcSet={`${logo.image.replace('.png', '_2x.png')} 2x`}
                    alt=""
                />
            </S.LogoWrapper>
        ))}
    </S.Container>
);

export default LogoList;
