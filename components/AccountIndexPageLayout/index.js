// @flow
import React from 'react';
import AnimateWhenVisible from '../AnimateWhenVisible';
import Container from '../Container';
import TeamDetailCard from '../TeamDetailCard';
import * as UI from '../UI/styles';

type Props = {
    user: Object
};

const AccountIndexPageLayout = ({user}: Props) => (
    <Container>
        <UI.LayoutContainer>
            <UI.LayoutItem sizeAtMobile={5 / 12}>
                <TeamDetailCard
                    avatar={user.avatar ? user.avatar.url : ''}
                    editUrl="/account/edit"
                    title={`${user.first_name} ${user.last_name}`}
                    type="detail"
                />
            </UI.LayoutItem>
            <UI.LayoutItem sizeAtMobile={1 / 12} />
            <UI.LayoutItem sizeAtMobile={6 / 12}></UI.LayoutItem>
        </UI.LayoutContainer>
    </Container>
);

export default AccountIndexPageLayout;
