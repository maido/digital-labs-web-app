// @flow
import React from 'react';
import Link from 'next/link';
import {getMentorTitle} from '../../globals/functions';
import AnimateWhenVisible from '../AnimateWhenVisible';
import {LinkedInLink, SlackLink} from '../UserDetailCard';
import * as UI from '../UI/styles';
import * as S from './styles';

type Props = {
    avatar?: Object,
    linkedin?: string,
    links?: Array<{
        label: string,
        url: string
    }>,
    name: string,
    slack?: string
};

const MentorDetailCard = ({avatar, linkedin, links = [], name, slack}: Props) => (
    <S.Container>
        <UI.FlexMediaWrapper>
            <UI.Avatar image={avatar} />
            <UI.FlexMediaContent css={S.contentContainer}>
                {name && <S.Title css={UI.fadeInUp}>{name}</S.Title>}
            </UI.FlexMediaContent>
        </UI.FlexMediaWrapper>

        <UI.Spacer />

        <UI.LayoutContainer>
            <UI.LayoutItem sizeAtTablet={6 / 12}>
                {linkedin && <LinkedInLink url={linkedin} />}
                {linkedin && slack && <UI.Spacer size="xs" />}
                {slack && <SlackLink url={slack} />}
                {links.length > 0 && <UI.Spacer />}
            </UI.LayoutItem>
            {links.length > 0 && (
                <UI.LayoutItem sizeAtTablet={6 / 12}>
                    {links.map(link => (
                        <>
                            <UI.TextLink href={link.url} target="_blank" rel="noopener">
                                {link.label}
                                <S.ExternalIcon
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="24"
                                    height="24"
                                    viewBox="0 0 24 24">
                                    <path d="M9 5v2h6.59L4 18.59 5.41 20 17 8.41V15h2V5z" />
                                </S.ExternalIcon>
                            </UI.TextLink>
                            <br />
                        </>
                    ))}
                </UI.LayoutItem>
            )}
        </UI.LayoutContainer>
    </S.Container>
);

export default MentorDetailCard;
