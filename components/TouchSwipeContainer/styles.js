// @flow
import styled from '@emotion/styled';
import {colors} from '../../globals/variables';

export const Container = styled.div`
    position: relative;
    text-align: center;
    white-space: nowrap;

    // &::after,
    // &::before {
    //     background-image: linear-gradient(to right, transparent, ${colors.secondary});
    //     content: '';
    //     height: 60px;
    //     position: fixed;
    //     right: 0;
    //     margin-top: -60px;
    //     width: 30px;
    //     z-index: 2;
    // }
    // &::before {
    //     background-image: linear-gradient(to left, transparent, ${colors.secondary});
    //     left: 0;
    //     margin-top: 0;
    //     right: auto;
    // }
`;
