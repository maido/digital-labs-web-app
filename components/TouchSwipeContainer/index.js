// @flow
import * as React from 'react';
import * as UI from '../UI/styles';
import * as S from './styles';

type Props = {
    children: React.Node
};

const TouchSwipeContainer = ({children}) => (
    <S.Container>
        <UI.ScrollSwipe>
            <UI.ScrollSwipeInner>{children}</UI.ScrollSwipeInner>
        </UI.ScrollSwipe>
    </S.Container>
);

export default TouchSwipeContainer;
