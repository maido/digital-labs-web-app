// @flow
import React, {useEffect, useState} from 'react';
import axios from 'axios';
import get from 'lodash/get';
import {connect} from 'react-redux';
import __ from '../../globals/strings';
import AnimateWhenVisible from '../AnimateWhenVisible';
import CardLayout from '../CardLayout';
import LearningContentCard from '../LearningContentCard';
import Container from '../Container';
import Theme from '../Theme';
import * as UI from '../UI/styles';

type Props = {
    courseId: number,
    dispatch: Function,
    moduleTitle: string,
    topicId: number
};

const RelatedTopics = ({courseId, dispatch, moduleTitle, topicId}: Props) => {
    const [topics, setTopics] = useState([]);

    const handleRelatedTopicClick = id => {
        window.location.href = `/labs/topics/${id}`;
    };

    useEffect(() => {
        async function fetchData() {
            try {
                const response = await axios.get(`/api/labs/course?id=${courseId}`);
                const {data} = response;
                const {topics} = data;

                setTopics(topics);
            } catch (e) {
                console.log(e);
            }
        }

        fetchData();
    }, []);

    const filteredTopics = topics
        .filter(topic => topic.id !== topicId && !topic.completed_at)
        .filter((topic, index) => index < 4);

    if (filteredTopics.length > 0) {
        return (
            <Theme theme="tertiary">
                <Container paddingVertical="l">
                    <AnimateWhenVisible delay={200}>
                        <div css={UI.centerText}>
                            <UI.Heading3 as="h3">
                                {__('learningContent.relatedTopics.title')}
                            </UI.Heading3>
                        </div>

                        <CardLayout
                            animate={false}
                            items={filteredTopics.map(topic => ({
                                key: topic.title,
                                component: (
                                    <div
                                        onClick={event => {
                                            event.preventDefault();
                                            handleRelatedTopicClick(topic.id);
                                        }}
                                    >
                                        <LearningContentCard
                                            title={topic.title}
                                            headline={get(topic, 'headline')}
                                            difficulty={get(topic, 'difficulty')}
                                            completed={get(topic, 'completed_at')}
                                            size="small"
                                            url={{
                                                href: '/labs/topics/[topicId]',
                                                as: `/labs/topics/${topic.id}`
                                            }}
                                        />
                                    </div>
                                )
                            }))}
                        />
                    </AnimateWhenVisible>
                </Container>
            </Theme>
        );
    } else {
        return null;
    }
};

export default RelatedTopics;
