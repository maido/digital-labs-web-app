// @flow
import React from 'react';
import axios from 'axios';
import sortBy from 'lodash/sortBy';
import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import {
    getContentStatus,
    getUnreadNotifications,
    getUrlForNotification
} from '../../globals/functions';
import __ from '../../globals/strings';
import AnimateWhenVisible from '../AnimateWhenVisible';
import CardLayout from '../CardLayout';
import CardLayoutPlaceholder from '../Placeholder/CardLayout';
import Container from '../Container';
import Spinner from '../Spinner';
import TextCard from '../TextCard';
import TextCardPlaceholder from '../Placeholder/TextCard';
import Theme from '../Theme';
import * as UI from '../UI/styles';

type Props = {
    network: NetworkStatus,
    notifications: Notifications
};
type NotificationListProps = {
    allNotifications: Notifications,
    type: 'read' | 'unread'
};

dayjs.extend(relativeTime);

const NotificationList = ({allNotifications, type = 'unread'}: NotificationListProps) => {
    let notificationIds = allNotifications.allIds[type];

    if (type === 'unread' && allNotifications.allIds.unread.length > 0) {
        notificationIds = getUnreadNotifications(allNotifications);
    }

    const {showContent, showFallback} = getContentStatus(
        false,
        true,
        notificationIds.length > 0,
        true
    );

    const markAsRead = async id => {
        try {
            await axios.delete(`/api/notifications/read/${id}`);
        } catch (error) {
            console.log(error);
        }
    };

    if (showContent) {
        const sortedNotifications = sortBy(
            notificationIds.map(id => allNotifications.byId[id]),
            'created_at'
        )
            .reverse()
            .map((notification, index) => {
                const timestamp = notification.created_at
                    ? dayjs().to(new Date(notification.created_at))
                    : '';
                const url = getUrlForNotification(notification);
                const component = (
                    <AnimateWhenVisible
                        delay={100 * index}
                        key={notification.id}
                        onVisible={() => {
                            if (type === 'unread') {
                                markAsRead(notification.id);
                            }
                        }}
                    >
                        <TextCard
                            size="small"
                            title={notification.title}
                            text={notification.content}
                            footnote={timestamp ? `– ${timestamp}` : ''}
                            url={url}
                        />
                    </AnimateWhenVisible>
                );

                return {key: notification.id, component};
            });

        return <CardLayout animate={false} columns={1} items={sortedNotifications} />;
    } else {
        if (showFallback) {
            return (
                <div>{__(['notifications', type === 'read' ? 'emptyRead' : 'emptyUnread'])}</div>
            );
        } else {
            return null;
        }
    }
};

const NotificationsPageLayout = ({network, notifications}: Props) => {
    const {showContent, showFallback, showPlaceholders} = getContentStatus(
        network.isLoading,
        notifications.hasFetched,
        false,
        true
    );

    return (
        <>
            <Container
                paddingVertical="l"
                size="small"
                css={UI.centerText}
                data-testid="unreadnotifications"
            >
                <UI.Heading2 as="h1">{__('notifications.titleUnread')}</UI.Heading2>

                {showPlaceholders && <Spinner />}
                {!showPlaceholders && (
                    <NotificationList allNotifications={notifications} type="unread" />
                )}
            </Container>

            <Theme theme="tertiary" overlap="secondary" overlapSize={50} grow={true}>
                <UI.Spacer />
                <Container
                    size="small"
                    paddingVertical="l"
                    css={UI.centerText}
                    data-testid="readnotifications"
                >
                    <AnimateWhenVisible delay={-1}>
                        <UI.Heading3 as="h2">{__('notifications.titleRead')}</UI.Heading3>
                    </AnimateWhenVisible>

                    {showPlaceholders && (
                        <CardLayoutPlaceholder
                            columns={1}
                            size={4}
                            component={<TextCardPlaceholder />}
                        />
                    )}
                    {(showContent || showFallback) && (
                        <NotificationList allNotifications={notifications} type="read" />
                    )}
                </Container>
            </Theme>
        </>
    );
};

export default NotificationsPageLayout;
