// @flow
import React from 'react';
import {storiesOf} from '@storybook/react';
import YouTubeVideo from './';

const stories = storiesOf('YouTubeVideo', module);

stories.add('default', () => {
    return <YouTubeVideo url="https://www.youtube.com/watch?v=iIGKlicb8n0" />;
});

stories.add('with player enabled', () => {
    return <YouTubeVideo url="https://www.youtube.com/watch?v=iIGKlicb8n0" autoplay={true} />;
});
