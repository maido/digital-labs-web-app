// @flow
import React, {useState} from 'react';
import {getYouTubeIdFromUrl} from '../../globals/functions';
import * as S from './styles';

type Props = {
    autoplay?: boolean,
    embed?: string,
    thumbnail: string,
    type: 'vimeo' | 'youtube',
    url: string
};

const ResponsiveVideo = ({autoplay = false, embed = '', thumbnail, type, url}: Props) => {
    const [isEmbedded, setIsEmbedded] = useState(autoplay);

    if (type === 'youtube') {
        const videoId = getYouTubeIdFromUrl(url);

        embed = `<iframe
            width="100%"
            height="auto"
            src="https://www.youtube-nocookie.com/embed/${videoId}?autoplay=1&rel=0"
            frameborder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowfullscreen="true"
        />`;
        thumbnail = `https://img.youtube.com/vi/${videoId}/maxresdefault.jpg`;
    }

    return (
        <S.VideoContainer>
            {!isEmbedded && (
                <S.Thumbnail onClick={() => setIsEmbedded(true)} image={thumbnail}>
                    <S.PlayIcon
                        height="20"
                        width="20"
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 20 20"
                    >
                        <path
                            d="M10 0C4.5 0 0 4.5 0 10s4.5 10 10 10 10-4.5 10-10S15.5 0 10 0zM8 14.5v-9l6 4.5-6 4.5z"
                            fill="#000"
                            fillRule="evenodd"
                        />
                    </S.PlayIcon>
                </S.Thumbnail>
            )}
            {isEmbedded && <S.IframeContainer dangerouslySetInnerHTML={{__html: embed}} />}
        </S.VideoContainer>
    );
};

export default ResponsiveVideo;
