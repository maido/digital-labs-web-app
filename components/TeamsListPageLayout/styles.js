// @flow
import {css} from '@emotion/core';
import styled from '@emotion/styled';
import {rem} from 'polished';
import {breakpoints, spacing} from '../../globals/variables';

export const searchContainer = css`
    @media (min-width: ${rem(breakpoints.mobile)}) {
        display: flex;
        flex-direction: row;
    }
`;

export const SearchItem = styled.div`
    flex-grow: 1;
    width: 100%;

    @media (max-width: ${rem(breakpoints.mobile)}) {
        & + & {
            margin-top: ${rem(spacing.s)};
        }
    }

    @media (min-width: ${rem(breakpoints.mobile)}) {
        & + & {
            margin-left: ${rem(spacing.s)};
        }

        &:last-of-type {
            width: auto;
        }
    }
`;

export const searchButton = css`
    padding-left: ${rem(spacing.m)};
    padding-right: ${rem(spacing.m)};
`;

export const StatLabel = styled.div`
    align-items: center;
    display: flex;
    justify-content: center;
    line-height: 1.4;
    margin-top: ${rem(spacing.m)};
`;

export const Icon = styled.svg`
    height: ${rem(22)};
    margin-right: ${rem(spacing.xs)};
    width: ${rem(22)};

    path {
        fill: currentColor;
    }
`;
