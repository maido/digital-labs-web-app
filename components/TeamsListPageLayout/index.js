// @flow
import React, {useEffect, useState} from 'react';
import {useRouter} from 'next/router';
import axios from 'axios';
import get from 'lodash/get';
import size from 'lodash/size';
import {connect} from 'react-redux';
import {Formik, Form} from 'formik';
import {countryList} from '../../globals/content';
import {getContentStatus} from '../../globals/functions';
import {logEvent} from '../../globals/analytics';
import __ from '../../globals/strings';
import {updateTeamsFilter} from '../../store/actions';
import AnimateWhenVisible from '../AnimateWhenVisible';
import CardLayout from '../CardLayout';
import CardLayoutPlaceholder from '../Placeholder/CardLayout';
import Container from '../Container';
import CTAButton from '../CTAButton';
import InputText from '../InputText';
import Pagination from '../Pagination';
import SelectField from '../SelectField';
import BreadcrumbNavPlaceholder from '../Placeholder/BreadcrumbNav';
import TeamSummaryPlaceholder from '../Placeholder/TeamSummaryCard';
import TeamSummaryCard from '../TeamSummaryCard';
import Theme from '../Theme';
import * as UI from '../UI/styles';
import * as S from './styles';

type Props = {
    dispatch: Function,
    teams: Teams,
    totalTeamsAndCountries: string,
    handlePageChange: Function,
    network: NetworkStatus,
    pagination: Pagination
};

const TeamsListPageLayout = ({
    dispatch,
    teams,
    totalTeamsAndCountries,
    handlePageChange,
    network,
    pagination
}: Props) => {
    const router = useRouter();
    const [filteredTeams, setFilteredTeams] = useState([]);

    const content = get(pagination.fetchedPages, pagination.activePage, []);
    const {showContent, showFallback, showPlaceholders} = getContentStatus(
        network.isLoading,
        content.length > 0,
        content.length > 0,
        true,
        true
    );
    const hasFilter = teams.filter.country !== '' || teams.filter.name !== '';
    const mappedTeams = content.map(id => teams.byId[id]);

    const handleSubmit = async values => {
        const {name, country} = values;

        if (name !== '' || country !== '') {
            try {
                // TODO: Remove `encodeURIComponent when this issue is resolved: https://github.com/axios/axios/issues/1111
                //  Note: We're encoding the team name to ensure symbols are encoded correctly.
                const response = await axios.get(`/api/teams?name=${encodeURIComponent(name)}&country=${country}`);
                const {data} = response;

                setFilteredTeams(data.data);
            } catch (e) {
                console.log({e});
            }
        } else {
            setFilteredTeams(mappedTeams);
        }

        logEvent({
            action: 'Search',
            category: 'Teams',
            label: `${name}${country ? `, ${country}` : ''}`
        });

        dispatch(updateTeamsFilter({name, country}));
    };

    const handleReset = () => {
        dispatch(updateTeamsFilter({name: '', country: ''}));
        setFilteredTeams(mappedTeams);
    };

    const handleCardClick = id => {
        router.push('/teams/[teamId]', `/teams/${id}`);
    };

    useEffect(() => {
        if (!hasFilter) {
            setFilteredTeams(mappedTeams);
        } else {
            handleSubmit(teams.filter);
        }
    }, [
        hasFilter,
        pagination.activePage,
        size(get(pagination.fetchedPages, pagination.activePage)),
        get(content, '0.id')
    ]);

    return (
        <>
            <Container size="small" style={{zIndex: 100}}>
                <AnimateWhenVisible watch={false}>
                    <UI.Heading2 css={UI.centerText} as="h1">
                        Teams
                    </UI.Heading2>

                    <Formik
                        initialValues={teams.filter}
                        onSubmit={handleSubmit}
                        onReset={handleReset}
                        render={formProps => (
                            <Form autoComplete="off" css={S.searchContainer}>
                                <S.SearchItem>
                                    <InputText name="name" placeholder="Team name" minLength="3" />
                                </S.SearchItem>
                                <S.SearchItem>
                                    <SelectField
                                        options={[
                                            ...[{label: 'All', value: ''}],
                                            ...Object.values(countryList).map(c => ({
                                                label: c,
                                                value: c
                                            }))
                                        ]}
                                        name="country"
                                        placeholder="Country"
                                    />
                                </S.SearchItem>
                                <S.SearchItem>
                                    <CTAButton
                                        style={{
                                            opacity:
                                                formProps.values.country === '' &&
                                                formProps.values.name === ''
                                                    ? 0.5
                                                    : 1
                                        }}
                                        type="submit"
                                        css={S.searchButton}
                                    >
                                        Search
                                    </CTAButton>
                                </S.SearchItem>
                            </Form>
                        )}
                    />
                </AnimateWhenVisible>

                <S.StatLabel>
                    {totalTeamsAndCountries || hasFilter ? (
                        <>
                            {!hasFilter && (
                                <S.Icon
                                    height="1024"
                                    width="1024"
                                    xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 1024 1024"
                                >
                                    <path
                                        style={{fill: 'currentColor'}}
                                        d="M512 128c-212.077 0-384 171.923-384 384s171.923 384 384 384c25.953 0 51.303-2.582 75.812-7.49-9.879-4.725-10.957-40.174-1.188-60.385 10.875-22.5 45-79.5 11.25-98.625s-24.375-27.75-45-49.875-12.19-25.451-13.5-31.125c-4.5-19.5 19.875-48.75 21-51.75s1.125-14.25.75-17.625S545.75 566.75 542 566.375s-5.625 6-10.875 6.375-28.125-13.875-33-17.625-7.125-12.75-13.875-19.5-7.5-1.5-18-5.625-44.25-16.5-70.125-27-28.125-25.219-28.5-35.625-15.75-25.5-22.961-36.375c-7.209-10.875-8.539-25.875-11.164-22.5s13.5 42.75 10.875 43.875-8.25-10.875-15.75-20.625 7.875-4.5-16.125-51.75 7.5-71.344 9-96 20.25 9 10.5-6.75.75-48.75-6.75-60.75S275 230 275 230c1.125-11.625 37.5-31.5 63.75-49.875s42.281-4.125 63.375 2.625 22.5 4.5 15.375-2.25 3-10.125 19.5-7.5 21 22.5 46.125 20.625 2.625 4.875 6 11.25-3.75 5.625-20.25 16.875.375 11.25 29.625 32.625 20.25-14.25 17.25-30S537.125 221 537.125 221c18 12 14.674.66 27.799 4.785S613.625 260 613.625 260c-44.625 24.375-16.5 27-9 32.625s-15.375 16.5-15.375 16.5c-9.375-9.375-10.875.375-16.875 3.75s-.375 12-.375 12c-31.031 4.875-24 37.5-23.625 45.375s-19.875 19.875-25.125 31.125S536.75 437 527 438.5s-19.5-36.75-72-22.5c-15.828 4.297-51 22.5-32.25 59.625s49.875-10.5 60.375-5.25-3 28.875-.75 29.25 29.625 1.031 31.125 33 41.625 29.25 50.25 30 37.5-23.625 41.625-24.75S626 522.875 662 543.5s54.375 17.625 66.75 26.25 3.75 25.875 15.375 31.5 58.125-1.875 69.75 17.25-48 115.125-66.75 125.625S719.75 778.625 701 794s-45 34.406-69.75 49.125c-21.908 13.027-25.85 36.365-35.609 43.732C767.496 848.68 896 695.35 896 512c0-212.077-171.923-384-384-384zm90 360.375c-5.25 1.5-16.125 11.25-42.75-4.5s-45-12.75-47.25-15.375c0 0-2.25-6.375 9.375-7.5 23.871-2.311 54 22.125 60.75 22.5s10.125-6.75 22.125-2.883c12 3.863 3 6.258-2.25 7.758zM476.375 166.25c-2.615-1.902 2.166-4.092 5.016-7.875 1.645-2.186.425-5.815 2.484-7.875 5.625-5.625 33.375-13.5 27.949 1.875-5.424 15.375-31.324 16.875-35.449 13.875zM543.5 215c-9.375-.375-31.443-2.707-27.375-6.75 15.844-15.75-6-20.25-19.5-21.375S477.5 178.25 484.25 177.5s33.75.375 38.25 4.125 28.875 13.5 30.375 20.625 0 13.125-9.375 12.75zm81.375-2.625c-7.5 6-45.24-21.529-52.5-27.75-31.5-27-48.375-18-54.99-22.5-6.617-4.5-4.26-10.5 5.865-19.5s38.625 3 55.125 4.875 35.625 14.625 36 29.781c.375 15.155 18 29.094 10.5 35.094z"
                                    />
                                </S.Icon>
                            )}
                            <span>
                                {!hasFilter && totalTeamsAndCountries}
                                {hasFilter && <em>{filteredTeams.length} teams found</em>}
                            </span>
                        </>
                    ) : (
                        <BreadcrumbNavPlaceholder style={{width: 180}} />
                    )}
                </S.StatLabel>
            </Container>

            <Theme theme="tertiary" overlap="secondary" overlapSize={120} grow={true}>
                <Container paddingVertical="l">
                    {showPlaceholders && (
                        <CardLayoutPlaceholder
                            columns={2}
                            size={12}
                            component={
                                <TeamSummaryPlaceholder hasImage={false} hasHeadline={false} />
                            }
                        />
                    )}
                    {showContent && (
                        <>
                            <CardLayout
                                animate={false}
                                items={filteredTeams.map(team => ({
                                    key: team.id,
                                    component: (
                                        <TeamSummaryCard
                                            handleClick={() => handleCardClick(team.id)}
                                            avatar={team.avatar}
                                            title={team.name}
                                            members={team.members}
                                        />
                                    )
                                }))}
                            />

                            {!hasFilter && (
                                <>
                                    <UI.ResponsiveSpacer />
                                    <Pagination
                                        activePage={pagination.activePage}
                                        currentItems={pagination.currentItems}
                                        itemsCountPerPage={pagination.itemsCountPerPage}
                                        onChange={handlePageChange}
                                        pageRangeDisplayed={pagination.pageRangeDisplayed}
                                        skip={pagination.skip}
                                        totalItemsCount={pagination.totalItemsCount}
                                        type="teams"
                                    />
                                </>
                            )}
                        </>
                    )}
                    {showFallback && (
                        <AnimateWhenVisible delay={200} watch={false}>
                            <UI.Box
                                css={UI.centerText}
                                style={{
                                    marginLeft: 'auto',
                                    marginRight: 'auto',
                                    padding: 30,
                                    maxWidth: 600
                                }}
                            >
                                <UI.Heading5 as="h2" style={{marginBottom: 10}}>
                                    {__('resources.emptyTitle')}
                                </UI.Heading5>
                                <p>{__('resources.empty')}</p>
                            </UI.Box>
                        </AnimateWhenVisible>
                    )}
                </Container>
                <UI.Spacer />
            </Theme>
        </>
    );
};

export default connect()(TeamsListPageLayout);
