// @flow
import React from 'react';
import ReactPagination from 'react-js-pagination';
import * as S from './styles';
import * as UI from '../UI/styles';

type Props = {
    activePage: number,
    currentItems: number,
    itemsCountPerPage: number,
    onChange?: Function,
    pageUrl?: string,
    pageRangeDisplayed: number,
    skip?: number,
    totalItemsCount: number,
    type?: string
};

const Pagination = ({
    activePage = 1,
    currentItems = 1,
    itemsCountPerPage,
    onChange,
    pageRangeDisplayed,
    pageUrl,
    skip = 0,
    totalItemsCount,
    type = 'posts'
}: Props) => (
    <S.Container>
        <UI.FadeInUp css={UI.centerText}>
            <UI.Label css={UI.textColor('greyDark')}>
                Showing {skip + 1}-{skip + currentItems} of {totalItemsCount} {type}
            </UI.Label>
        </UI.FadeInUp>

        {totalItemsCount > itemsCountPerPage && (
            <ReactPagination
                pageRangeDisplayed={pageRangeDisplayed}
                activePage={activePage}
                itemsCountPerPage={itemsCountPerPage}
                totalItemsCount={totalItemsCount}
                onChange={onChange}
                getPageUrl={pageUrl}
                itemClassFirst="simple"
                itemClassLast="simple"
                itemClassNext="simple"
                itemClassPrev="simple"
            />
        )}
    </S.Container>
);

export default Pagination;
