// @flow
import React, {useEffect, useRef, useState} from 'react';
import {connect} from 'react-redux';
import Link from 'next/link';
import {useRouter} from 'next/router';
import {useSpring, animated} from 'react-spring/web.cjs';
import axios from 'axios';
import dayjs from 'dayjs';
import {Formik, Form} from 'formik';
import get from 'lodash/get';
import map from 'lodash/map';
import {updateForm} from '../../store/actions';
import {scrollToTop} from '../../globals/functions';
import {areasOfInterest, countryList, heardVia, occupations} from '../../globals/content';
import __ from '../../globals/strings';
import accountCreationSchema from '../../schemas/create-account';
import AnimateWhenVisible from '../AnimateWhenVisible';
import CTAButton from '../CTAButton';
import FullScreenMessage from '../FullScreenMessage';
import InputButtonGroup from '../InputButtonGroup';
import InputCheckbox, {InputCheckboxField} from '../InputCheckbox';
import InputRadio from '../InputRadio';
import InputText from '../InputText';
import {LogoSymbol} from '../Logo';
import SelectField from '../SelectField';
import Spinner from '../Spinner';
import * as FormStyles from '../Form/styles';
import * as UI from '../UI/styles';
import * as S from './styles';

type Props = {
    dispatch: Function,
    form: Object,
    invite?: AccountInvite,
    user: Object
};

const getCheckboxLabel = (fields, field, max) => {
    if (fields[field].length < max) {
        return `Pick ${max - fields[field].length}`;
    }
};

const SignupPageLayout = ({dispatch, form, invite, user = {}}: Props) => {
    const router = useRouter();
    const $containerRef = useRef(null);
    const [description, setDescription] = useState(__('account.signup.step1Text'));

    const [formAnimation, setFormAnimation] = useSpring(() => ({
        config: {tension: 60, mass: 1, friction: 10},
        delay: 800,
        from: {transform: `translateY(20px)`, opacity: 0},
        to: {transform: `translateY(0)`, opacity: 1}
    }));
    const [form2Animation, setForm2Animation] = useSpring(() => ({
        config: {tension: 60, mass: 1, friction: 10},
        delay: 800,
        from: {transform: `translateY(20px)`, opacity: 0}
    }));

    const handleStep1FormSubmit = async (values, handlers) => {
        dispatch(updateForm({state: 'pending'}));

        const fields = {
            first_name: values.first_name,
            last_name: values.last_name,
            email: values.email,
            birthdate: dayjs(values.birthdate).format('YYYY-MM-DD'),
            password: values.password,
            password_confirmation: values.password_confirmation
        };

        try {
            await axios.post(`/api/account/signup`, fields);

            /**
             * This will never return as validation errors will be given because step 2 fields
             * are missing.
             *
             * We use this post here to validate a user's email before they continue to step 2.
             * If the email field does not error (likely to be because it's already in use), them
             * we are good to continue.
             */
            return;
        } catch (error) {
            const formErrors = error.response.data;

            /**
             * For now we only need to check errors for step1
             */
            const step1FieldKeys = Object.keys(accountCreationSchema.step1.fields);
            const step1Errors = map(formErrors, (value, key) => {
                return step1FieldKeys.includes(key) ? key : null;
            }).filter(value => value);

            if (Object.keys(step1Errors).length > 0) {
                dispatch(updateForm({state: 'default'}));
                handlers.setErrors(formErrors);
                handlers.setSubmitting(false);
            } else {
                setFormAnimation({transform: `scale(0.95)`, opacity: 0});

                setTimeout(() => {
                    dispatch(
                        updateForm({
                            state: 'default',
                            step: 2,
                            fields: {...values, birthdate: fields.birthdate}
                        })
                    );
                    setForm2Animation({delay: 200, transform: `translateY(0)`, opacity: 1});
                    setDescription(__('account.signup.step2Text'));
                }, 200);
            }
        }
    };

    const handleStep2FormSubmit = async (values, handlers) => {
        dispatch(updateForm({state: 'pending', fields: values}));

        if (values.degree_date) {
            values.degree_date = dayjs(values.degree_date).format('YYYY-MM-DD');
        }
        if (values.graduation_date) {
            values.graduation_date = dayjs(values.graduation_date).format('YYYY-MM-DD');
        }

        const fields = {
            ...form.fields,
            ...values
        };

        try {
            const response = await axios.post(`/api/account/signup`, fields);

            dispatch(updateForm({state: 'success', step: 3}));

            /**
             * A successsful signup will return the user's oauth access tokens which
             * we will store at the next stage
             */
            router.push({pathname: '/signup/success', query: response.data});
        } catch (error) {
            dispatch(updateForm({state: 'default'}));

            const errorResponse = get(error, 'response.data.errors');
            const errors = errorResponse || {
                terms: __('account.errors.signup')
            };
            handlers.setErrors(errors);
            handlers.setSubmitting(false);
        }
    };

    useEffect(() => {
        if (form.step > 1) {
            scrollToTop($containerRef);
        }
    }, [form.step]);

    return (
        <>
            <S.Container>
                <S.Column css={UI.centerText}>
                    <AnimateWhenVisible delay={100} watch={false}>
                        <Link href="/">
                            <a css={S.logo}>
                                <LogoSymbol />
                            </a>
                        </Link>
                    </AnimateWhenVisible>

                    <UI.ResponsiveSpacer />

                    <AnimateWhenVisible delay={300} watch={false}>
                        <UI.Heading1 as="h1" css={UI.margin('bottom', 's')}>
                            {__('account.signup.title')}
                        </UI.Heading1>
                    </AnimateWhenVisible>
                    <AnimateWhenVisible delay={400} watch={false}>
                        <p
                            style={{
                                fontSize: 18,
                                marginLeft: 'auto',
                                marginRight: 'auto',
                                maxWidth: 550
                            }}>
                            {description}
                        </p>
                    </AnimateWhenVisible>

                    <UI.Spacer size="s" />

                    <AnimateWhenVisible delay={600} watch={false}>
                        <Link href="/terms-and-conditions" prefetch={false}>
                            <UI.TextLink theme="primary">Terms &amp; Conditions</UI.TextLink>
                        </Link>
                        <Link href="/privacy-policy" prefetch={false}>
                            <UI.TextLink theme="primary" style={{marginLeft: 12}}>
                                Privacy Policy
                            </UI.TextLink>
                        </Link>
                    </AnimateWhenVisible>
                </S.Column>
                <S.Column theme="white" scroll={form.step > 1}>
                    <div ref={$containerRef}>
                        {form.step === 1 && (
                            <animated.div style={formAnimation}>
                                <UI.Heading3 as="h2" css={UI.centerText}>
                                    {__('account.signup.step1Title')}
                                </UI.Heading3>

                                {invite && (
                                    <div
                                        css={UI.fadeInUp}
                                        style={{margin: '-15px auto 0', maxWidth: 550}}>
                                        <UI.FlexMediaWrapper style={{justifyContent: 'center'}}>
                                            <UI.Avatar
                                                image={get(invite, 'team.avatar.thumb_url')}
                                                size="small"
                                            />
                                            <UI.FlexMediaContent
                                                style={{lineHeight: 1.3, marginLeft: 10}}>
                                                Sign up to join {invite.invited_by.first_name}{' '}
                                                {invite.team.members.length > 1
                                                    ? 'and the rest of the team from'
                                                    : 'and'}{' '}
                                                <strong> {invite.team.name}</strong>
                                            </UI.FlexMediaContent>
                                        </UI.FlexMediaWrapper>
                                        <UI.ResponsiveSpacer />
                                    </div>
                                )}

                                <Formik
                                    initialValues={{
                                        ...form.fields,
                                        email: invite && invite.email ? invite.email : ''
                                    }}
                                    onSubmit={handleStep1FormSubmit}
                                    validationSchema={accountCreationSchema.step1}
                                    render={formProps => (
                                        <Form>
                                            <UI.LayoutContainer type="matrix">
                                                <UI.LayoutItem sizeAtMobile={6 / 12}>
                                                    <InputText
                                                        name="first_name"
                                                        label="First name"
                                                        placeholder="John"
                                                    />
                                                </UI.LayoutItem>
                                                <UI.LayoutItem sizeAtMobile={6 / 12}>
                                                    <InputText
                                                        name="last_name"
                                                        label="Last name"
                                                        placeholder="Doe"
                                                    />
                                                </UI.LayoutItem>
                                                <UI.LayoutItem sizeAtMobile={6 / 12}>
                                                    <InputText
                                                        name="email"
                                                        label="Email"
                                                        placeholder="johndoe@gmail.com"
                                                        type="email"
                                                        disabled={
                                                            invite && invite.email
                                                                ? 'disabled'
                                                                : null
                                                        }
                                                    />
                                                </UI.LayoutItem>
                                                <UI.LayoutItem sizeAtMobile={6 / 12}>
                                                    <InputText
                                                        name="birthdate"
                                                        label="Date of Birth"
                                                        placeholder="mm/dd/yyyy"
                                                        type="date"
                                                    />
                                                </UI.LayoutItem>
                                                <UI.LayoutItem sizeAtMobile={6 / 12}>
                                                    <InputText
                                                        name="password"
                                                        label="Password"
                                                        type="password"
                                                    />
                                                </UI.LayoutItem>
                                                <UI.LayoutItem sizeAtMobile={6 / 12}>
                                                    <InputText
                                                        name="password_confirmation"
                                                        label="Confirm password"
                                                        type="password"
                                                    />
                                                </UI.LayoutItem>
                                            </UI.LayoutContainer>

                                            <UI.Spacer />

                                            <footer css={UI.centerText}>
                                                <input
                                                    type="hidden"
                                                    name="token"
                                                    data-testid="invitetoken"
                                                    value={get(form, 'fields.invite_token')}
                                                />
                                                <CTAButton
                                                    disabled={form.state !== 'default'}
                                                    state={form.state}
                                                    type="submit"
                                                    wide={true}>
                                                    Continue
                                                </CTAButton>
                                            </footer>
                                        </Form>
                                    )}
                                />
                            </animated.div>
                        )}
                        {form.step >= 2 && (
                            <animated.div style={form2Animation}>
                                <UI.Heading3 as="h1" css={UI.centerText}>
                                    {__('account.signup.step2Title')}
                                </UI.Heading3>

                                <UI.Spacer size="xs" />

                                <Formik
                                    initialValues={form.fields}
                                    onSubmit={handleStep2FormSubmit}
                                    validationSchema={accountCreationSchema.step2}
                                    render={formProps => (
                                        <Form data-testid="formstep2">
                                            <UI.LayoutContainer type="matrix">
                                                <UI.LayoutItem sizeAtMobile={6 / 12}>
                                                    <SelectField
                                                        label="Country of origin"
                                                        name="country_origin"
                                                        options={Object.values(countryList).map(
                                                            c => ({
                                                                label: c,
                                                                value: c
                                                            })
                                                        )}
                                                        placeholder="Select a country..."
                                                    />
                                                </UI.LayoutItem>
                                                <UI.LayoutItem sizeAtMobile={6 / 12}>
                                                    <SelectField
                                                        label="Country of residence"
                                                        name="country_residence"
                                                        options={Object.values(countryList).map(
                                                            c => ({
                                                                label: c,
                                                                value: c
                                                            })
                                                        )}
                                                        placeholder="Select a country..."
                                                    />
                                                    {formProps.values.country_origin !== '' &&
                                                        formProps.values.country_residence ===
                                                            '' && (
                                                            <UI.FadeInUp>
                                                                <UI.Spacer size="s" />
                                                                <InputCheckboxField
                                                                    name="terms"
                                                                    label={__(
                                                                        'account.signup.sameCountry'
                                                                    )}
                                                                    onClick={() => {
                                                                        formProps.setFieldValue(
                                                                            'country_residence',
                                                                            formProps.values
                                                                                .country_origin
                                                                        );
                                                                    }}
                                                                />
                                                            </UI.FadeInUp>
                                                        )}
                                                </UI.LayoutItem>
                                                <UI.LayoutItem>
                                                    <UI.Divider size="xs" />
                                                </UI.LayoutItem>
                                                <UI.LayoutItem>
                                                    <InputButtonGroup
                                                        label="Occupation"
                                                        grow={true}
                                                        name="occupation">
                                                        {occupations.map((option, index) => (
                                                            <InputRadio
                                                                key={option}
                                                                name="occupation"
                                                                value={option}
                                                                tabIndex={index + 1}
                                                            />
                                                        ))}
                                                    </InputButtonGroup>
                                                </UI.LayoutItem>
                                                {formProps.values &&
                                                    formProps.values.occupation === 'Student' && (
                                                        <AnimateWhenVisible delay={0}>
                                                            <UI.LayoutItem sizeAtMobile={6 / 12}>
                                                                <InputText
                                                                    name="degree_date"
                                                                    label={__(
                                                                        'account.signup.degreeDate'
                                                                    )}
                                                                    placeholder="mm/dd/yyyy"
                                                                    type="date"
                                                                />
                                                            </UI.LayoutItem>
                                                            <UI.LayoutItem sizeAtMobile={6 / 12}>
                                                                <InputText
                                                                    name="graduation_date"
                                                                    label={__(
                                                                        'account.signup.graduationDate'
                                                                    )}
                                                                    placeholder="mm/dd/yyyy"
                                                                    type="date"
                                                                />
                                                            </UI.LayoutItem>
                                                        </AnimateWhenVisible>
                                                    )}
                                                <UI.LayoutItem>
                                                    <UI.Divider size="xs" />
                                                </UI.LayoutItem>
                                                <UI.LayoutItem>
                                                    <SelectField
                                                        label="What are your areas of interest?"
                                                        name="areas_of_interest"
                                                        options={areasOfInterest.map(option => ({
                                                            label: option.label,
                                                            options: option.options.map(o => ({
                                                                value: o,
                                                                label: o
                                                            }))
                                                        }))}
                                                        placeholder="Select an option..."
                                                        isMulti={true}
                                                        maxOptions={3}
                                                        noOptionsMessage="3 of 3 selected"
                                                        tip="Select up to 3"
                                                    />
                                                </UI.LayoutItem>
                                                <UI.LayoutItem>
                                                    <SelectField
                                                        label="What are you looking to learn more about?"
                                                        name="areas_of_learning"
                                                        options={areasOfInterest.map(option => ({
                                                            label: option.label,
                                                            options: option.options.map(o => ({
                                                                value: o,
                                                                label: o
                                                            }))
                                                        }))}
                                                        placeholder="Select an option..."
                                                        isMulti={true}
                                                        maxOptions={3}
                                                        noOptionsMessage="3 of 3 selected"
                                                        tip="Select up to 3"
                                                    />
                                                </UI.LayoutItem>
                                                <UI.LayoutItem>
                                                    <UI.Divider size="xs" />
                                                </UI.LayoutItem>
                                                <UI.LayoutItem>
                                                    <SelectField
                                                        label={__('account.signup.heardVia')}
                                                        name="heard_via"
                                                        options={heardVia.map(i => ({
                                                            label: i,
                                                            value: i
                                                        }))}
                                                        placeholder="Select an option..."
                                                    />
                                                </UI.LayoutItem>
                                                <UI.LayoutItem>
                                                    <UI.Divider size="xs" />
                                                </UI.LayoutItem>
                                                <UI.LayoutItem>
                                                    <span css={FormStyles.label}>
                                                        Terms &amp; Conditions
                                                    </span>
                                                    <InputCheckbox name="terms">
                                                        I have read and accept the{' '}
                                                        <Link href="/terms-and-conditions">
                                                            <a target="noopener">
                                                                <strong
                                                                    css={UI.textColor(
                                                                        'purplishBlue'
                                                                    )}
                                                                    style={{marginLeft: 3}}>
                                                                    Terms &amp; Conditions
                                                                </strong>
                                                            </a>
                                                        </Link>
                                                    </InputCheckbox>
                                                </UI.LayoutItem>
                                            </UI.LayoutContainer>

                                            <UI.Spacer />

                                            <footer css={UI.centerText}>
                                                <CTAButton
                                                    disabled={
                                                        form.state !== 'default' || form.stage === 3
                                                    }
                                                    state={form.state}
                                                    wide={true}
                                                    type="submit">
                                                    Create account
                                                </CTAButton>
                                            </footer>

                                            <UI.ResponsiveSpacer />
                                        </Form>
                                    )}
                                />
                            </animated.div>
                        )}
                    </div>
                </S.Column>
            </S.Container>
        </>
    );
};

const mapStateToProps = state => ({form: state.form, user: state.form});

export default connect(mapStateToProps)(SignupPageLayout);
