// @flow
import {css} from '@emotion/core';
import styled from '@emotion/styled';
import {rem} from 'polished';
import {useSpring, animated} from 'react-spring/web.cjs';
import {themeStyles} from '../Theme/styles';
import {breakpoints, colors, spacing} from '../../globals/variables';
import {responsiveRem} from '../../globals/functions';

export const Container = styled.section`
    display: flex;
    flex-direction: column;
    height: 100vh;
    max-height: 100vh;

    @media (min-width: ${rem(breakpoints.tabletSmall)}) {
        align-items: center;
        flex-direction: row;
        overflow: hidden;
    }
`;

export const badge = css`
    height: ${rem(100)};
    width: ${rem(100)};
`;

export const IntroText = styled.p`
    margin-left: auto;
    margin-right: auto;
    max-width: ${rem(500)};
`;

export const Column = styled.div`
    padding: ${responsiveRem(spacing.l)} ${responsiveRem(spacing.m)};

    @media (min-width: ${rem(breakpoints.tabletSmall)}) {
        align-items: center;
        display: flex;
        flex-direction: column;
        height: 100vh;
        overflow: auto;
        width: 50%;

        &:nth-of-type(2) {
            overflow: auto;
        }

        ${props => !props.scroll && `justify-content: center;`}
    }

    @media (min-width: ${rem(breakpoints.desktop)}) {
        padding: ${responsiveRem(spacing.l)};
    }

    @media (max-width: ${rem(breakpoints.tabletSmall)}) {
        &:nth-of-type(2) {
            border-top-left-radius: ${rem(20)};
            border-top-right-radius: ${rem(20)};
            flex-grow: 1;
        }
    }

    ${props => themeStyles(props.theme)}};
`;

export const logo = css`
    cursor: pointer;

    svg {
        height: auto;
        width: ${rem(60)};

        * {
            fill: ${colors.primary};
        }
    }
`;
