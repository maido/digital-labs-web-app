// @flow
import {css} from '@emotion/core';
import styled from '@emotion/styled';
import {rem} from 'polished';
import {responsiveRem} from '../../globals/functions';
import {
    breakpoints,
    colors,
    fontFamilies,
    fontSizes,
    spacing,
    themes
} from '../../globals/variables';
import * as TeamSummaryStyles from '../TeamSummaryCard/styles';

export const Container = styled(TeamSummaryStyles.Container)`
    cursor: default;

    &:hover,
    &:focus {
        box-shadow: none;
        transform: none;
    }

    a {
        color: ${colors.purplishBlue};
    }
`;

export const contentContainer = css`
    margin-left: ${rem(spacing.m)};

    @media (max-width: ${rem(breakpoints.mobile)}) {
        a {
            color: ${colors.primary};
        }
    }
`;

export const Title = styled.span`
    display: block;
    font-size: ${responsiveRem(fontSizes.h4)};
    font-family: ${fontFamilies.heading};
    line-height: 1.1;

    @media (max-width: ${rem(breakpoints.mobile)}) {
        font-size: ${rem(fontSizes.h3)};
    }
`;

export const MissingMissionLink = styled.a`
    background-color: ${colors.greyLight};
    border: 1px dashed ${colors.grey};
    color: ${colors.darkBlueGrey};
    display: block;
    font-family: ${fontFamilies.bold};
    padding: ${rem(spacing.s)};
    text-align: center;

    &:hover,
    &:focus {
        background-color: ${colors.paleGrey};
    }
`;

export const MembersListContainer = styled.div`
    margin: ${rem(spacing.m)} 0 0;

    @media (max-width: ${rem(breakpoints.mobile)}) {
        background-color: ${themes.white.background};
        border-radius: ${rem(5)};
        color: ${themes.white.text};
        padding: ${rem(spacing.s)};
    }
`;

export const ExternalIcon = styled.svg`
    height: auto;
    margin-left: ${rem(spacing.xs)};
    vertical-align: top;
    width: ${rem(18)};

    path {
        fill: ${colors.greyDark};
    }
`;
