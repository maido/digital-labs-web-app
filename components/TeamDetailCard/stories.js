// @flow
import React from 'react';
import {addDecorator, storiesOf} from '@storybook/react';
import {withKnobs, object, select, text} from '@storybook/addon-knobs';
import {Provider} from 'react-redux';
import {initializeStore} from '../../store';
import TeamDetailCard from './';
import Theme from '../Theme';

const stories = storiesOf('TeamDetailCard', module);

stories.addDecorator(withKnobs);

stories.addDecorator(story => (
    <Provider store={initializeStore({user: {isAuthenticated: true, id: 1}})}>
        <Theme theme="secondary">
            <div style={{padding: 30}}>{story()}</div>
        </Theme>
    </Provider>
));

stories.add('default', () => {
    return <TeamDetailCard title="Golden Retriever" />;
});

stories.add('with mission', () => {
    return (
        <TeamDetailCard
            title="Golden Retriever"
            mission="Dolore esse ex anim incididunt labore sint ipsum exercitation sit laborum."
        />
    );
});

stories.add('with members', () => {
    return (
        <TeamDetailCard
            title="Golden Retriever"
            mission="Dolore esse ex anim incididunt labore sint ipsum exercitation sit laborum."
            members={[
                {
                    first_name: 'Foo',
                    last_name: 'Bar',
                    last_name: 'Bar',
                    country: 'United Kingdom',
                    avatar: {url: 'https://placehold.it/200x200'}
                },
                {
                    first_name: 'Foo',
                    last_name: 'Bar',
                    last_name: 'Bar',
                    country: 'United Kingdom',
                    avatar: {url: 'https://placehold.it/200x200'}
                }
            ]}
        />
    );
});

stories.add('with logged-in user in team', () => {
    return (
        <TeamDetailCard
            title="Golden Retriever"
            mission="Dolore esse ex anim incididunt labore sint ipsum exercitation sit laborum."
            members={[
                {
                    first_name: 'Foo',
                    last_name: 'Bar',
                    id: 1,
                    country: 'United Kingdom',
                    avatar: {url: 'https://placehold.it/200x200'}
                },
                {
                    first_name: 'Foo',
                    last_name: 'Bar',
                    country: 'United Kingdom',
                    avatar: {url: 'https://placehold.it/200x200'}
                }
            ]}
        />
    );
});
