// @flow
import React from 'react';
import Link from 'next/link';
import {connect} from 'react-redux';
import AnimateWhenVisible from '../AnimateWhenVisible';
import UserCard from '../UserCard';
import * as UI from '../UI/styles';
import * as S from './styles';

const TeamDetailCard = ({editUrl = '/team/edit', team, type, user}: TeamSummaryUICard) => {
    const links = [];

    if (team.website_url) {
        links.push({url: team.website_url, label: 'Website'});
    }
    if (team.twitter_url) {
        links.push({url: team.twitter_url, label: 'Twitter'});
    }
    if (team.facebook_url) {
        links.push({url: team.facebook_url, label: 'Facebook'});
    }
    if (team.instagram_url) {
        links.push({url: team.instagram_url, label: 'Instagram'});
    }
    if (team.linkedin_url) {
        links.push({url: team.linkedin_url, label: 'LinkedIn'});
    }

    return (
        <S.Container>
            <UI.FlexMediaWrapper>
                <UI.Avatar image={team.avatar ? team.avatar.url : null} />
                <UI.FlexMediaContent css={S.contentContainer}>
                    <S.Title css={UI.fadeInUp}>{team.name}</S.Title>

                    {team && user && team.id === user.team.id && (
                        <Link href={editUrl} passHref>
                            <UI.TextLink>Edit profile</UI.TextLink>
                        </Link>
                    )}
                </UI.FlexMediaContent>
            </UI.FlexMediaWrapper>

            {team.mission && (
                <>
                    <UI.Spacer size="s" />
                    <p>{team.mission}</p>
                </>
            )}

            {links.length > 0 && (
                <>
                    <br />
                    <UI.LayoutContainer>
                        {links.map(link => (
                            <UI.LayoutItem sizeAtTablet={6 / 12}>
                                <UI.TextLink href={link.url} target="_blank" rel="noopener">
                                    {link.label}
                                    <S.ExternalIcon
                                        xmlns="http://www.w3.org/2000/svg"
                                        width="24"
                                        height="24"
                                        viewBox="0 0 24 24"
                                    >
                                        <path d="M9 5v2h6.59L4 18.59 5.41 20 17 8.41V15h2V5z" />
                                    </S.ExternalIcon>
                                </UI.TextLink>
                            </UI.LayoutItem>
                        ))}
                    </UI.LayoutContainer>
                </>
            )}

            {team.members && team.members.length > 0 && (
                <>
                    <UI.ShowAt breakpoint="mobile">
                        <UI.Divider />
                    </UI.ShowAt>
                    <S.MembersListContainer>
                        {team.members.map((member, index) => (
                            <AnimateWhenVisible
                                watch={false}
                                delay={index * 150}
                                key={`${member.first_name} ${member.last_name}`}
                            >
                                <Link href="/users/[userId]" as={`/users/${member.id}`}>
                                    <a>
                                        <UserCard user={member} />
                                    </a>
                                </Link>
                                {index < team.members.length - 1 && <UI.Spacer size="s" />}
                            </AnimateWhenVisible>
                        ))}
                    </S.MembersListContainer>
                </>
            )}
        </S.Container>
    );
};

const mapStateToProps = state => ({
    user: state.user
});

export default connect(mapStateToProps)(TeamDetailCard);
