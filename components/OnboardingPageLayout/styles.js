// @flow
import {css} from '@emotion/core';
import {rem} from 'polished';
import {breakpoints, spacing} from '../../globals/variables';

export const threeImagesHeroCard = css`
    bottom: ${rem(spacing.m * -1)};
    left: 60%;
    position: absolute;
    z-index: 2;
    width: ${rem(200)};
`;
