// @flow
import React, {useRef} from 'react';
import __ from '../../globals/strings';
import Cookies from 'js-cookie';
import AnimateWhenVisible from '../AnimateWhenVisible';
import Container from '../Container';
import CTAButton from '../CTAButton';
import PageIntro from '../PageIntro';
import ScrollIndicator from '../ScrollIndicator';
import Theme from '../Theme';
import ThreeImageHero from '../ThreeImageHero';
import * as UI from '../UI/styles';
import * as S from './styles';

type Props = {
    invite?: AccountInviteCookie
};

const OnboardingPageLayout = ({invite}: Props) => {
    const $contentRef = useRef(null);

    const handleScrollToContent = () => {
        if ($contentRef.current && typeof window !== 'undefined') {
            window.scrollTo({
                behavior: 'smooth',
                top: $contentRef.current.getBoundingClientRect().top
            });
        }
    };

    const handleInvitationCTAClick = () => {
        Cookies.remove('invite');
    };

    return (
        <>
            <PageIntro title={__('account.onboarding.title')}>
                <button id="scrollTo" type="button" onClick={handleScrollToContent}>
                    <ScrollIndicator />
                </button>
            </PageIntro>

            <UI.ResponsiveSpacer />

            <Theme theme="white" overlap="secondary" overlapSize={180} grow={true}>
                <ThreeImageHero images={['/home-1.jpg', '/home-2.jpg', '/home-3.jpg']} />

                <UI.ResponsiveSpacer />

                <div ref={$contentRef}>
                    <Container size="large" paddingVertical="l">
                        <UI.LayoutContainer>
                            <UI.LayoutItem sizeAtMobile={4 / 12}>
                                <UI.Heading2>{__('account.onboarding.subtitle')}</UI.Heading2>
                            </UI.LayoutItem>
                            <UI.LayoutItem sizeAtMobile={8 / 12}>
                                <span
                                    css={UI.leadText}
                                    dangerouslySetInnerHTML={{
                                        __html: __('account.onboarding.text')
                                    }}
                                />

                                <UI.ResponsiveSpacer />

                                <AnimateWhenVisible>
                                    {invite ? (
                                        <CTAButton
                                            href="/dashboard"
                                            wide={true}
                                            onClick={handleInvitationCTAClick}
                                        >
                                            {__('account.onboarding.ctaLabelInvite')}
                                        </CTAButton>
                                    ) : (
                                        <CTAButton href="/team/create" wide={true}>
                                            {__('account.onboarding.ctaLabel')}
                                        </CTAButton>
                                    )}
                                </AnimateWhenVisible>
                            </UI.LayoutItem>
                        </UI.LayoutContainer>
                    </Container>
                </div>
            </Theme>
        </>
    );
};

export default OnboardingPageLayout;
