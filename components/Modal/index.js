// @flow
import * as React from 'react';
import {useSpring, animated} from 'react-spring/web.cjs';
import CTAButton from '../CTAButton';
import * as UI from '../UI/styles';
import * as S from './styles';

type Props = {
    children: React.Node,
    containerPadding?: boolean,
    handleClose?: Function,
    state: 'hidden' | 'leaving' | 'visible'
};

const Modal = ({
    children,
    containerPadding = true,
    handleClose = () => {},
    state = 'hidden'
}: Props) => {
    const [overlayAnimation, setOverlayAnimation] = useSpring(() => ({
        config: {tension: 60, mass: 1, friction: 20},
        from: {opacity: 0}
    }));
    const [containerAnimation, setContainerAnimation] = useSpring(() => ({
        config: {tension: 300, mass: 4, friction: 70},
        from: {transform: `scale(0.75)`, opacity: 0}
    }));
    const [contentAnimation, setContentAnimation] = useSpring(() => ({
        config: {tension: 60, mass: 1, friction: 20},
        from: {transform: `translateY(10px)`, opacity: 0}
    }));

    if (state === 'visible') {
        setOverlayAnimation({delay: 0, opacity: 1});
        setContainerAnimation({delay: 200, transform: `scale(1)`, opacity: 1});
        setContentAnimation({delay: 600, transform: `translateY(0)`, opacity: 1});
    } else if (state === 'leaving') {
        setOverlayAnimation({delay: 200, opacity: 0});
        setContainerAnimation({delay: 100, transform: `scale(0.75)`, opacity: 0});
        setContentAnimation({delay: 0, transform: `translateY(10px)`, opacity: 0});
    }

    return (
        <animated.div style={overlayAnimation} css={S.overlay}>
            <animated.div
                style={containerAnimation}
                aria-modal="true"
                css={S.container({containerPadding})}
            >
                <S.CloseButton onClick={handleClose}>
                    <S.CloseIcon
                        height="512"
                        width="512"
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 512 512"
                    >
                        <path d="M437.5 386.6L306.9 256l130.6-130.6c14.1-14.1 14.1-36.8 0-50.9-14.1-14.1-36.8-14.1-50.9 0L256 205.1 125.4 74.5c-14.1-14.1-36.8-14.1-50.9 0-14.1 14.1-14.1 36.8 0 50.9L205.1 256 74.5 386.6c-14.1 14.1-14.1 36.8 0 50.9 14.1 14.1 36.8 14.1 50.9 0L256 306.9l130.6 130.6c14.1 14.1 36.8 14.1 50.9 0 14-14.1 14-36.9 0-50.9z" />
                    </S.CloseIcon>
                </S.CloseButton>
                <animated.div style={contentAnimation}>
                    <S.ScrollingContent>{children}</S.ScrollingContent>
                </animated.div>
            </animated.div>
        </animated.div>
    );
};

export default Modal;
