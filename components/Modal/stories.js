// @flow
import React from 'react';
import {addDecorator, storiesOf} from '@storybook/react';
import {withKnobs, object, select, text} from '@storybook/addon-knobs';
import Modal from './';

const stateOptions = {
    Visible: 'visible',
    Leaving: 'leaving',
    Hidden: 'hidden'
};
const stories = storiesOf('Modal', module);

stories.addDecorator(withKnobs);

stories.add('default', () => {
    return (
        <Modal state="visible">
            <p>
                Officia fugiat voluptate fugiat fugiat et commodo ut labore magna mollit commodo in
                aliquip dolor. Ea in magna minim eu. Aute eiusmod quis exercitation officia nisi ut
                irure ea nostrud sit. In reprehenderit occaecat velit cillum. Sint amet ea
                incididunt esse dolore do ipsum occaecat esse veniam. Non officia dolor enim
                cupidatat consequat consequat labore aliquip cupidatat ad qui amet. Nisi ullamco
                irure incididunt est occaecat nulla ut nostrud.
            </p>
        </Modal>
    );
});

stories.add('with long scrolling content', () => {
    return (
        <Modal state={select('State', stateOptions, 'visible')}>
            <p>
                Officia fugiat voluptate fugiat fugiat et commodo ut labore magna mollit commodo in
                aliquip dolor. Ea in magna minim eu. Aute eiusmod quis exercitation officia nisi ut
                irure ea nostrud sit. In reprehenderit occaecat velit cillum. Sint amet ea
                incididunt esse dolore do ipsum occaecat esse veniam. Non officia dolor enim
                cupidatat consequat consequat labore aliquip cupidatat ad qui amet. Nisi ullamco
                irure incididunt est occaecat nulla ut nostrud.
            </p>
            <p>
                Officia fugiat voluptate fugiat fugiat et commodo ut labore magna mollit commodo in
                aliquip dolor. Ea in magna minim eu. Aute eiusmod quis exercitation officia nisi ut
                irure ea nostrud sit. In reprehenderit occaecat velit cillum. Sint amet ea
                incididunt esse dolore do ipsum occaecat esse veniam. Non officia dolor enim
                cupidatat consequat consequat labore aliquip cupidatat ad qui amet. Nisi ullamco
                irure incididunt est occaecat nulla ut nostrud.
            </p>
            <p>
                Officia fugiat voluptate fugiat fugiat et commodo ut labore magna mollit commodo in
                aliquip dolor. Ea in magna minim eu. Aute eiusmod quis exercitation officia nisi ut
                irure ea nostrud sit. In reprehenderit occaecat velit cillum. Sint amet ea
                incididunt esse dolore do ipsum occaecat esse veniam. Non officia dolor enim
                cupidatat consequat consequat labore aliquip cupidatat ad qui amet. Nisi ullamco
                irure incididunt est occaecat nulla ut nostrud.
            </p>
            <p>
                Officia fugiat voluptate fugiat fugiat et commodo ut labore magna mollit commodo in
                aliquip dolor. Ea in magna minim eu. Aute eiusmod quis exercitation officia nisi ut
                irure ea nostrud sit. In reprehenderit occaecat velit cillum. Sint amet ea
                incididunt esse dolore do ipsum occaecat esse veniam. Non officia dolor enim
                cupidatat consequat consequat labore aliquip cupidatat ad qui amet. Nisi ullamco
                irure incididunt est occaecat nulla ut nostrud.
            </p>
            <p>
                Officia fugiat voluptate fugiat fugiat et commodo ut labore magna mollit commodo in
                aliquip dolor. Ea in magna minim eu. Aute eiusmod quis exercitation officia nisi ut
                irure ea nostrud sit. In reprehenderit occaecat velit cillum. Sint amet ea
                incididunt esse dolore do ipsum occaecat esse veniam. Non officia dolor enim
                cupidatat consequat consequat labore aliquip cupidatat ad qui amet. Nisi ullamco
                irure incididunt est occaecat nulla ut nostrud.
            </p>
            <p>
                Officia fugiat voluptate fugiat fugiat et commodo ut labore magna mollit commodo in
                aliquip dolor. Ea in magna minim eu. Aute eiusmod quis exercitation officia nisi ut
                irure ea nostrud sit. In reprehenderit occaecat velit cillum. Sint amet ea
                incididunt esse dolore do ipsum occaecat esse veniam. Non officia dolor enim
                cupidatat consequat consequat labore aliquip cupidatat ad qui amet. Nisi ullamco
                irure incididunt est occaecat nulla ut nostrud.
            </p>
            <p>
                Officia fugiat voluptate fugiat fugiat et commodo ut labore magna mollit commodo in
                aliquip dolor. Ea in magna minim eu. Aute eiusmod quis exercitation officia nisi ut
                irure ea nostrud sit. In reprehenderit occaecat velit cillum. Sint amet ea
                incididunt esse dolore do ipsum occaecat esse veniam. Non officia dolor enim
                cupidatat consequat consequat labore aliquip cupidatat ad qui amet. Nisi ullamco
                irure incididunt est occaecat nulla ut nostrud.
            </p>
        </Modal>
    );
});
