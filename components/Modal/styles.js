// @flow
import {css} from '@emotion/core';
import styled from '@emotion/styled';
import {rem, rgba} from 'polished';
import {themeStyles} from '../Theme/styles';
import {responsiveRem} from '../../globals/functions';
import {breakpoints, colors, spacing, themes, shadows, transitions} from '../../globals/variables';

type ContainerProps = {
    containerPadding?: boolean
};

export const overlay = css`
    ${themeStyles('tertiary')};
    background-color: ${rgba(themes.modal.background, 0.6)};
    align-items: center;
    bottom: 0;
    display: flex;
    height: 100%;
    left: 0;
    justify-content: center;
    position: fixed;
    padding: ${rem(spacing.m)};
    right: 0;
    top: 0;
    width: 100%;
    z-index: 201;

    @media (min-width: ${rem(breakpoints.mobile)}) {
        padding: ${rem(spacing.l)};
    }
`;

export const container = (props: ContainerProps) => css`
    background-color: ${themes.white.background};
    border-radius: ${rem(10)};
    box-shadow: ${shadows.default};
    max-height: 90vh;
    overflow: auto;
    -webkit-overflow-scrolling: touch;
    padding: ${props.containerPadding ? responsiveRem(spacing.m) : 0};
    position: relative;
    width: 100%;

    @media (min-width: ${rem(breakpoints.mobile)}) {
        max-width: ${rem(800)};
    }
`;

export const ScrollingContent = styled.div`
    height: 100%;
    overflow: auto;
    -webkit-overflow-scrolling: touch;
`;

export const CloseIcon = styled.svg`
    height: ${rem(spacing.s * 1.5)};
    transition: ${transitions.slow};
    width: ${rem(spacing.s * 1.5)};

    path {
        fill: ${colors.aquaMarine};
    }
`;

export const CloseButton = styled.button`
    position: absolute;
    right: ${rem(spacing.s)};
    top: ${rem(spacing.s)};
    z-index: 10;

    &:hover ${CloseIcon}, &:focus ${CloseIcon} {
        transform: rotate(180deg) scale(1.1);
    }
`;
