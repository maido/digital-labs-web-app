// @flow
import {css} from '@emotion/core';
import styled from '@emotion/styled';
import {rem} from 'polished';
import {responsiveRem} from '../../globals/functions';
import {breakpoints, spacing} from '../../globals/variables';

type ContainerProps = {
    paddingHorizontal: number | string,
    paddingVertical: number | string,
    size: string
};

export const CONTAINER_WIDTHS = {
    default: 1350,
    large: 1100,
    medium: 900,
    small: 700
};

export const containerCSS = (props: ContainerProps) => css`
    box-sizing: border-box;
    display: block;
    margin-left: auto;
    margin-right: auto;
    max-width: ${rem(CONTAINER_WIDTHS[props.size])};
    position: relative;
    width: 100%;

    ${
        props.paddingHorizontal === 0
            ? `padding-left: 0;
           padding-right: 0;`
            : `padding-left: ${rem(spacing[props.paddingHorizontal])};
        padding-right: ${rem(spacing[props.paddingHorizontal])};`
    }
    ${
        props.paddingVertical === 0
            ? `padding-bottom: 0;
               padding-top: 0;`
            : `padding-bottom: ${rem(spacing[props.paddingVertical])};
            padding-top: ${rem(spacing[props.paddingVertical])};`
    }

    @media (max-width: ${rem(breakpoints.mobile)}) {
        ${props.paddingHorizontal !== 0 &&
            `padding-left: ${rem(spacing[props.paddingHorizontal] * 0.85)};
            padding-right: ${rem(spacing[props.paddingHorizontal] * 0.85)};`}
    }
`;

export const Container = styled.div`
    ${containerCSS};
`;
