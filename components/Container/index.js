// @flow
import * as React from 'react';
import * as S from './styles';

type Props = {
    as?: string,
    css?: Object,
    children: React.Node,
    paddingHorizontal?: number | string,
    paddingVertical?: number | string,
    size?: string,
    style?: Object
};

const Container = ({
    as = 'div',
    children,
    paddingHorizontal,
    paddingVertical,
    size = 'default',
    ...props
}: Props) => {
    /**
     * We can't use default values for this as Flow doesn't union types/default values. It's a known bug.
     */
    if (typeof paddingHorizontal === 'undefined') {
        paddingHorizontal = 'm';
    }

    if (typeof paddingVertical === 'undefined') {
        paddingVertical = 'm';
    }

    return (
        <S.Container
            as={as}
            paddingHorizontal={paddingHorizontal}
            paddingVertical={paddingVertical}
            size={size}
            {...props}
        >
            {children}
        </S.Container>
    );
};

export default Container;
