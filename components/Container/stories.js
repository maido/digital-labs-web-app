// @flow
import React from 'react';
import {storiesOf} from '@storybook/react';
import Container from './';
import Theme from '../Theme';
import * as UI from '../UI/styles';

const stories = storiesOf('Container', module);

stories.add('default', () => {
    return (
        <Theme theme="secondary">
            <Container>
                <UI.Box theme="white">
                    <p css={UI.textColor('black')}>
                        Fugiat duis ex laborum esse sunt ipsum voluptate ipsum cillum ex non. Mollit
                        qui laboris mollit id nisi. Cillum elit Lorem nulla et nulla consectetur.
                        Adipisicing consequat ex consequat veniam nisi. Tempor aute veniam voluptate
                        dolore ex. Nulla reprehenderit exercitation proident laboris laboris eu sunt
                        proident voluptate fugiat est laboris ullamco.
                    </p>
                </UI.Box>
            </Container>
        </Theme>
    );
});

stories.add('with large container', () => {
    return (
        <Theme theme="secondary">
            <Container size="large">
                <UI.Box theme="white">
                    <p css={UI.textColor('black')}>
                        Fugiat duis ex laborum esse sunt ipsum voluptate ipsum cillum ex non. Mollit
                        qui laboris mollit id nisi. Cillum elit Lorem nulla et nulla consectetur.
                        Adipisicing consequat ex consequat veniam nisi. Tempor aute veniam voluptate
                        dolore ex. Nulla reprehenderit exercitation proident laboris laboris eu sunt
                        proident voluptate fugiat est laboris ullamco.
                    </p>
                </UI.Box>
            </Container>
        </Theme>
    );
});

stories.add('with medium container', () => {
    return (
        <Theme theme="secondary">
            <Container size="medium">
                <UI.Box theme="white">
                    <p css={UI.textColor('black')}>
                        Fugiat duis ex laborum esse sunt ipsum voluptate ipsum cillum ex non. Mollit
                        qui laboris mollit id nisi. Cillum elit Lorem nulla et nulla consectetur.
                        Adipisicing consequat ex consequat veniam nisi. Tempor aute veniam voluptate
                        dolore ex. Nulla reprehenderit exercitation proident laboris laboris eu sunt
                        proident voluptate fugiat est laboris ullamco.
                    </p>
                </UI.Box>
            </Container>
        </Theme>
    );
});

stories.add('with small container', () => {
    return (
        <Theme theme="secondary">
            <Container size="small">
                <UI.Box theme="white">
                    <p css={UI.textColor('black')}>
                        Fugiat duis ex laborum esse sunt ipsum voluptate ipsum cillum ex non. Mollit
                        qui laboris mollit id nisi. Cillum elit Lorem nulla et nulla consectetur.
                        Adipisicing consequat ex consequat veniam nisi. Tempor aute veniam voluptate
                        dolore ex. Nulla reprehenderit exercitation proident laboris laboris eu sunt
                        proident voluptate fugiat est laboris ullamco.
                    </p>
                </UI.Box>
            </Container>
        </Theme>
    );
});

stories.add('with large vertical padding', () => {
    return (
        <Theme theme="secondary">
            <Container paddingVertical="l">
                <UI.Box theme="white">
                    <p css={UI.textColor('black')}>
                        Fugiat duis ex laborum esse sunt ipsum voluptate ipsum cillum ex non. Mollit
                        qui laboris mollit id nisi. Cillum elit Lorem nulla et nulla consectetur.
                        Adipisicing consequat ex consequat veniam nisi. Tempor aute veniam voluptate
                        dolore ex. Nulla reprehenderit exercitation proident laboris laboris eu sunt
                        proident voluptate fugiat est laboris ullamco.
                    </p>
                </UI.Box>
            </Container>
        </Theme>
    );
});

stories.add('with extra large vertical padding', () => {
    return (
        <Theme theme="secondary">
            <Container paddingVertical="xl">
                <UI.Box theme="white">
                    <p css={UI.textColor('black')}>
                        Fugiat duis ex laborum esse sunt ipsum voluptate ipsum cillum ex non. Mollit
                        qui laboris mollit id nisi. Cillum elit Lorem nulla et nulla consectetur.
                        Adipisicing consequat ex consequat veniam nisi. Tempor aute veniam voluptate
                        dolore ex. Nulla reprehenderit exercitation proident laboris laboris eu sunt
                        proident voluptate fugiat est laboris ullamco.
                    </p>
                </UI.Box>
            </Container>
        </Theme>
    );
});
