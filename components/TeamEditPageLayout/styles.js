// @flow
import {css} from '@emotion/core';
import styled from '@emotion/styled';
import {rem, shade} from 'polished';
import {breakpoints, fontFamilies, spacing, themes, transitions} from '../../globals/variables';

export const container = css`
    margin-left: auto;
    margin-right: auto;
    max-width: 700px;
`;

export const membersContainer = css`
    @media (min-width: ${rem(breakpoints.mobile)}) {
        display: flex;
        flex-wrap: wrap;
    }
`;

export const PlusIcon = styled.svg`
    height: ${rem(spacing.m)};
    margin-right: ${rem(spacing.xs)};
    transition: ${transitions.default};
    width: ${rem(spacing.m)};
    will-change: transform;

    path {
        fill: ${themes.tertiary.text};
    }
`;

export const AddMembersButton = styled.button`
    align-items: center;
    background-color: ${themes.tertiary.background};
    border-radius: ${rem(5)};
    color: ${themes.tertiary.text};
    display: flex;
    font-family: ${fontFamilies.bold};
    height: 100%;
    min-height: 80px;
    justify-content: center;
    padding: ${rem(spacing.m)};
    transition: ${transitions.slow};
    width: 100%;

    &:hover,
    &:focus {
        background-color: ${shade(0.05, themes.tertiary.background)};

        ${PlusIcon} {
            transform: rotate(90deg) scale(1.05);
        }
    }
`;
