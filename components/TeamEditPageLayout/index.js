// @flow
import React, {useState} from 'react';
import dynamic from 'next/dynamic';
import Link from 'next/link';
import axios from 'axios';
import get from 'lodash/get';
import {connect} from 'react-redux';
import {Formik, Form} from 'formik';
import {updateForm, updateUser} from '../../store/actions';
import teamEditSchema from '../../schemas/edit-team';
import {areasOfInterest, countryList} from '../../globals/content';
import __ from '../../globals/strings';
import AnimateWhenVisible from '../AnimateWhenVisible';
import AddTeamMembers from '../AddTeamMembers';
import AvatarUpload from '../AvatarUpload';
import BreadcrumbNav from '../BreadcrumbNav';
import CardLayout from '../CardLayout';
import Container from '../Container';
import CTAButton from '../CTAButton';
import {label} from '../Form/styles';
import InputText from '../InputText';
import InputTextarea from '../InputTextarea';
import SelectField from '../SelectField';
import UserCard from '../UserCard';
import * as UI from '../UI/styles';
import * as S from './styles';

const Modal = dynamic(() => import('../Modal'), {ssr: false});

type Props = {
    dispatch: Function,
    form: Form,
    team: Team,
    user: User
};

const TeamEditPageLayout = ({dispatch, form, team, user}: Props) => {
    const [modalVisibility, setModalVisibility] = useState('hidden');

    const handleAvatarUpdate = avatar => {
        dispatch(
            updateUser({
                team: {
                    ...team,
                    avatar
                }
            })
        );
    };

    const handleOpenMembers = () => {
        setModalVisibility('visible');
    };

    const handleCloseMembers = () => {
        setModalVisibility('leaving');
        setTimeout(() => setModalVisibility('hidden'), 600);
    };

    const handleFormSubmit = async (values, handlers) => {
        if (team.id && values.name) {
            try {
                const response = await axios.post(`/api/team/update?id=${team.id}`, values);

                dispatch(updateForm({state: 'success', fields: values}));
                dispatch(updateUser({team: values}));
                handlers.setSubmitting(false);

                setTimeout(() => {
                    dispatch(updateForm({state: 'default'}));
                }, 1500);
            } catch (error) {
                dispatch(updateForm({state: 'default'}));

                const errors = get(error, 'response.data')
                    ? get(error, 'response.data')
                    : {
                          mission: __('team.errors.edit')
                      };
                handlers.setErrors(errors);
                handlers.setSubmitting(false);
            }
        }
    };

    return (
        <>
            {modalVisibility !== 'hidden' && (
                <Modal
                    containerPadding={false}
                    state={modalVisibility}
                    handleClose={handleCloseMembers}
                >
                    <AddTeamMembers team={team} />
                </Modal>
            )}
            <Container paddingVertical="l">
                <BreadcrumbNav
                    links={[
                        {label: 'Teams', url: `/teams/${team.id ? team.id : ''}`},
                        {label: team.name, url: {href: '/teams/[teamId]', as: `/teams/${team.id}`}},
                        {label: 'Edit team', url: '/team/edit'}
                    ]}
                />

                <AvatarUpload
                    apiUrl={team.id ? `api/teams/${team.id}/avatars` : ''}
                    avatar={team.avatar && team.avatar.url ? team.avatar.url : ''}
                    handleSuccess={handleAvatarUpdate}
                    label={__('team.edit.avatarLabel')}
                />

                <UI.Spacer />

                <Formik
                    initialValues={form.fields}
                    onSubmit={handleFormSubmit}
                    validationSchema={teamEditSchema}
                    render={formProps => (
                        <Form encType="multipart/form-data">
                            <AnimateWhenVisible delay={150}>
                                <UI.Box css={S.container}>
                                    <UI.Heading5 as="h2" css={UI.formHeading}>
                                        Team details
                                    </UI.Heading5>
                                    <UI.LayoutContainer type="matrix">
                                        <UI.LayoutItem>
                                            <InputText
                                                name="name"
                                                label="Name"
                                                placeholder="Your team name"
                                                autoComplete="off"
                                            />
                                        </UI.LayoutItem>
                                        <UI.LayoutItem>
                                            <InputTextarea
                                                name="mission"
                                                label="Mission statement"
                                                placeholder="Mission statement"
                                                rows={2}
                                            />
                                        </UI.LayoutItem>
                                        <UI.LayoutItem>
                                            <InputTextarea
                                                name="about"
                                                label="About"
                                                placeholder="About your team"
                                                rows={3}
                                            />
                                        </UI.LayoutItem>
                                        <UI.LayoutItem>
                                            <InputTextarea
                                                name="story"
                                                label="Story"
                                                placeholder="Your team's story"
                                                rows={3}
                                            />
                                        </UI.LayoutItem>
                                        <UI.LayoutItem sizeAtTablet={6 / 12}>
                                            <SelectField
                                                label="Country"
                                                name="country"
                                                options={Object.values(countryList).map(c => ({
                                                    label: c,
                                                    value: c
                                                }))}
                                                placeholder="Select a country..."
                                            />
                                        </UI.LayoutItem>
                                        <UI.LayoutItem sizeAtTablet={6 / 12}>
                                            <SelectField
                                                label="Category"
                                                name="category"
                                                options={areasOfInterest
                                                    .filter(option => {
                                                        return ![
                                                            'Personal Development',
                                                            'Entrepreneurship'
                                                        ].includes(option.label);
                                                    })
                                                    .map(option => ({
                                                        label: option.label,
                                                        options: option.options.map(o => ({
                                                            value: o,
                                                            label: o
                                                        }))
                                                    }))}
                                                placeholder="Select an option..."
                                            />
                                        </UI.LayoutItem>
                                        <UI.LayoutItem sizeAtMobile={6 / 12}>
                                            <InputText
                                                name="website_url"
                                                label="Website URL"
                                                placeholder="https://"
                                            />
                                        </UI.LayoutItem>
                                        <UI.LayoutItem sizeAtMobile={6 / 12}>
                                            <InputText
                                                name="facebook_url"
                                                label="Facebook URL"
                                                placeholder="https://"
                                            />
                                        </UI.LayoutItem>
                                        <UI.LayoutItem sizeAtMobile={6 / 12}>
                                            <InputText
                                                name="twitter_url"
                                                label="Twitter URL"
                                                placeholder="https://"
                                            />
                                        </UI.LayoutItem>
                                        <UI.LayoutItem sizeAtMobile={6 / 12}>
                                            <InputText
                                                name="instagram_url"
                                                label="Instagram URL"
                                                placeholder="https://"
                                            />
                                        </UI.LayoutItem>
                                        <UI.LayoutItem sizeAtMobile={6 / 12}>
                                            <InputText
                                                name="linkedin_url"
                                                label="LinkedIn URL"
                                                placeholder="https://"
                                            />
                                        </UI.LayoutItem>
                                    </UI.LayoutContainer>

                                    {team.members && team.members.length > 0 && (
                                        <>
                                            <UI.ResponsiveSpacer />
                                            <UI.Heading5 as="h2" css={UI.formHeading}>
                                                {__('team.edit.members.title')}
                                            </UI.Heading5>

                                            <UI.LayoutContainer
                                                type="matrix"
                                                size="s"
                                                css={S.membersContainer}
                                                style={{marginTop: 8}}
                                            >
                                                {team.members &&
                                                    team.members.map(member => (
                                                        <UI.LayoutItem
                                                            sizeAtMobile={6 / 12}
                                                            key={`${member.first_name} ${member.last_name}`}
                                                        >
                                                            <AnimateWhenVisible>
                                                                <Link
                                                                    href="/users/[userId]"
                                                                    as={`/users/${member.id}`}
                                                                >
                                                                    <a>
                                                                        <UserCard
                                                                            user={member}
                                                                            padded={true}
                                                                        />
                                                                    </a>
                                                                </Link>
                                                            </AnimateWhenVisible>
                                                        </UI.LayoutItem>
                                                    ))}
                                                <UI.LayoutItem sizeAtMobile={6 / 12}>
                                                    <S.AddMembersButton
                                                        type="button"
                                                        onClick={handleOpenMembers}
                                                    >
                                                        <S.PlusIcon
                                                            xmlns="http://www.w3.org/2000/svg"
                                                            viewBox="0 0 512 512"
                                                        >
                                                            <path d="M256 48C141.1 48 48 141.1 48 256s93.1 208 208 208 208-93.1 208-208S370.9 48 256 48zm0 398.7c-105.1 0-190.7-85.5-190.7-190.7S150.9 65.3 256 65.3 446.7 150.9 446.7 256 361.1 446.7 256 446.7z" />
                                                            <path d="M264.1 128h-16.8v119.9H128v16.8h119.3V384h16.8V264.7H384v-16.8H264.1z" />
                                                        </S.PlusIcon>
                                                        {__('team.edit.members.addLabel')}
                                                    </S.AddMembersButton>
                                                </UI.LayoutItem>
                                            </UI.LayoutContainer>

                                            <UI.Spacer size="s" />
                                            <em>{__('team.edit.members.text')}</em>
                                        </>
                                    )}

                                    <UI.Spacer />

                                    <div css={UI.centerText}>
                                        <UI.Spacer size="s" />
                                        <CTAButton
                                            disabled={
                                                formProps.isSubmitting || formProps.isValidating
                                            }
                                            type="submit"
                                            state={
                                                formProps.isSubmitting || formProps.isValidating
                                                    ? 'pending'
                                                    : form.state
                                            }
                                            wide={true}
                                        >
                                            {__('team.edit.ctaLabel')}
                                        </CTAButton>
                                    </div>
                                </UI.Box>
                            </AnimateWhenVisible>
                        </Form>
                    )}
                />
            </Container>
        </>
    );
};

const mapStateToProps = state => ({form: state.form});

export default connect(mapStateToProps)(TeamEditPageLayout);
