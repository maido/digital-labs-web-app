// @flow
import * as React from 'react';
import VisibilitySensor from 'react-visibility-sensor';
import {useSpring, animated} from 'react-spring/web.cjs';

type Props = {
    children: React.Node,
    css?: Object,
    delay?: number,
    config?: Object,
    offset?: Object,
    onVisible?: Function,
    watch?: boolean
};

const AnimateWhenVisible = ({
    children,
    css = {},
    delay = 0,
    config,
    offset = {top: -200},
    onVisible,
    watch = true
}: Props) => {
    const defaultConfig = {
        config: {mass: 1, tension: 85, friction: 14},
        delay,
        from: {transform: `translateY(20px)`, opacity: 0},
        to: {transform: `translateY(0)`, opacity: 1}
    };
    const springConfig = {
        ...defaultConfig,
        ...config
    };
    let mounted = true;

    const [hasBeenVisible, setHasBeenVisible] = React.useState(false);
    const [styleProps, setStyleProps] = useSpring(() => ({
        config: springConfig.config,
        delay: springConfig.delay,
        from: springConfig.from
    }));

    const handleVisibilityChange = (isVisible: boolean) => {
        if (mounted && hasBeenVisible === false && isVisible === true) {
            setHasBeenVisible(true);

            if (onVisible) {
                onVisible();
            }

            setTimeout(() => setStyleProps(springConfig.to), springConfig.delay);
        }
    };

    React.useEffect(() => {
        mounted = true;

        return () => {
            mounted = false;
        };
    }, []);

    if (delay < 0 || !watch) {
        setTimeout(() => setStyleProps(springConfig.to), springConfig.delay);

        return <animated.div style={styleProps}>{children}</animated.div>;
    } else {
        return (
            <VisibilitySensor
                delayedCall={true}
                onChange={handleVisibilityChange}
                active={hasBeenVisible === false}
                partialVisibility={true}
                offset={offset}
            >
                <animated.div style={styleProps} css={css}>
                    {children}
                </animated.div>
            </VisibilitySensor>
        );
    }
};

export default AnimateWhenVisible;
