// @flow
import React from 'react';
import {storiesOf} from '@storybook/react';
import AnimateWhenVisible from './';

const stories = storiesOf('AnimateWhenVisible', module);

stories.add('default', () => {
    return (
        <AnimateWhenVisible>
            <div style={{height: '120vh'}}>Scroll down...</div>
            Hello
        </AnimateWhenVisible>
    );
});

stories.add('with a delay', () => {
    return (
        <AnimateWhenVisible delay={500}>
            <div style={{height: '120vh'}}>Scroll down...</div>
            Hello
        </AnimateWhenVisible>
    );
});

stories.add('with a custom offset', () => {
    return (
        <AnimateWhenVisible offset={{top: 400}}>
            <div style={{height: '120vh'}}>Scroll down...</div>
            Hello
        </AnimateWhenVisible>
    );
});

stories.add('with a callback', () => {
    return (
        <AnimateWhenVisible onVisible={() => alert('visible!')}>
            <div style={{height: '120vh'}}>Scroll down...</div>
            Hello
        </AnimateWhenVisible>
    );
});
