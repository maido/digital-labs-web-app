// @flow
import {css} from '@emotion/core';
import styled from '@emotion/styled';
import {rem} from 'polished';
import {responsiveRem} from '../../globals/functions';
import {breakpoints, colors, spacing} from '../../globals/variables';
import {List, ListItem} from '../UI/styles';

export const logo = css`
    * {
        fill: ${colors.purplishBlue} !important;
    }

    svg {
        height: auto;
        width: ${rem(80)};
    }

    @media (max-width: ${rem(breakpoints.tablet)}) {
        svg {
            margin-bottom: -30px;
            width: ${rem(70)};
        }
    }
`;

export const columnHeading = css`
    color: ${colors.greyDark};
    display: block;
    margin-bottom: ${rem(spacing.s)};
    text-align: left;
`;

export const Container = styled.div`
    display: flex;
    flex-wrap: wrap;

    a {
        align-self: flex-start !important;
        color: ${colors.secondary} !important;
        line-height: 1;
        margin-bottom: ${rem(spacing.s)};
    }
    a + a {
        margin-left: 0 !important;
    }

    @media (max-width: ${rem(breakpoints.mobile)}) {
        align-items: flex-start;
        justify-content: flex-start;
    }

    @media (min-width: ${rem(breakpoints.tablet)}) {
        // justify-content: space-evenly;
        padding-bottom: ${responsiveRem(spacing.s)};
        padding-top: ${responsiveRem(spacing.s)};
    }
`;

export const Column = styled.div`
    display: flex;
    flex-direction: column;

    @media (max-width: ${rem(breakpoints.mobile)}) {
        justify-content: flex-start;
        margin-bottom: ${rem(spacing.m)};
        width: 50%;

        &:nth-of-type(1),
        &:nth-of-type(5) {
            width: 100%;
        }

        &:nth-of-type(1) svg {
            margin-bottom: ${rem(spacing.s)};
        }
    }

    @media (min-width: ${rem(breakpoints.mobile)}) and (max-width: ${rem(breakpoints.tablet)}) {
        width: calc(25% - ${responsiveRem(spacing.l)});

        &:nth-of-type(5) {
            margin-left: 0;
            text-align: left;
            width: 100%;

            ul {
                align-self: flex-start;
            }
        }
    }

    @media (min-width: ${rem(breakpoints.mobile)}) {
        &:first-of-type {
            margin-left: 0;
        }

        margin-left: ${responsiveRem(spacing.l)};
    }

    @media (min-width: ${rem(breakpoints.desktop)}) {
        margin-left: ${responsiveRem(spacing.xl)};
    }
`;

export const socialIcons = css`
    align-items: center;
    align-self: flex-end;
    display: flex;
    flex-grow: 0;
    flex-wrap: wrap;

    > li {
        margin-left: 0 !important;
        margin-right: ${responsiveRem(spacing.xs)};
        margin-top: 0;
    }
    > li:last-of-type {
        margin: 0;
    }
`;
