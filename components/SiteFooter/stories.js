// @flow
import React from 'react';
import {storiesOf} from '@storybook/react';
import SiteFooter from './';

const stories = storiesOf('SiteFooter', module);

stories.add('default', () => {
    return <SiteFooter />;
});
