// @flow
import React from 'react';
import Link from 'next/link';
import {logEvent} from '../../globals/analytics';
import {canUserSubmitPitch} from '../../globals/functions';
import __ from '../../globals/strings';
import CTAButton from '../CTAButton';
import Logo from '../Logo';
import SocialIcon, {siteUrls as socialSiteUrls} from '../SocialIcon';
import Theme from '../Theme';
import * as UI from '../UI/styles';
import * as S from './styles';

type Props = {
    user: User
};

const SiteFooter = ({user}) => {
    const canSubmitPitch = canUserSubmitPitch(user);

    return (
        <S.Container>
            <S.Column>
                <Link href="/">
                    <a css={S.logo}>
                        <Logo />
                    </a>
                </Link>
            </S.Column>
            <S.Column>
                <UI.Label css={S.columnHeading}>Labs</UI.Label>

                {user.isAuthenticated ? (
                    <>
                        <Link href="/account" passHref>
                            <UI.TextLink>Your Account</UI.TextLink>
                        </Link>
                        <Link href="/dashboard" passHref>
                            <UI.TextLink>Dashboard</UI.TextLink>
                        </Link>
                        <Link href="/labs" passHref>
                            <UI.TextLink>Labs</UI.TextLink>
                        </Link>
                    </>
                ) : (
                    <>
                        <Link href="/signup" passHref>
                            <UI.TextLink
                                onClick={() => {
                                    logEvent({
                                        action: 'click',
                                        category: 'Sign up',
                                        label: 'General - Footer'
                                    });
                                }}
                            >
                                Signup
                            </UI.TextLink>
                        </Link>
                        <Link href="/login" passHref>
                            <UI.TextLink>Login</UI.TextLink>
                        </Link>
                    </>
                )}
            </S.Column>
            <S.Column>
                <UI.Label css={S.columnHeading}>Legal</UI.Label>

                <Link href="/terms-and-conditions" passHref prefetch={false}>
                    <UI.TextLink>Terms &amp; Conditions</UI.TextLink>
                </Link>
                <Link href="/privacy-policy" passHref prefetch={false}>
                    <UI.TextLink>Privacy Policy</UI.TextLink>
                </Link>
                <Link href="/cookie-policy" passHref prefetch={false}>
                    <UI.TextLink style={{display: 'none'}}>Cookie Policy</UI.TextLink>
                </Link>
            </S.Column>
            <S.Column>
                <UI.Label css={S.columnHeading}>Stay in touch</UI.Label>

                <UI.List css={S.socialIcons}>
                    {Object.keys(socialSiteUrls).map(site => (
                        <UI.ListItem size="xs" key={site}>
                            <SocialIcon site={site} />
                        </UI.ListItem>
                    ))}
                </UI.List>

                <CTAButton
                    size="small"
                    theme="tertiary"
                    href={__('footer.links.emailUrl')}
                    track={{
                        action: 'click',
                        category: 'Website',
                        label: 'General - Footer'
                    }}
                >
                    {__('footer.links.emailLabel')}
                </CTAButton>

                <CTAButton
                    size="small"
                    theme="tertiary"
                    href={__('footer.links.websiteUrl')}
                    track={{
                        action: 'click',
                        category: 'Contact',
                        label: 'General - Footer'
                    }}
                >
                    {__('footer.links.websiteLabel')}
                </CTAButton>
            </S.Column>
        </S.Container>
    );
};

export default SiteFooter;
