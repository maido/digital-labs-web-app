// @flow
import {css} from '@emotion/core';
import {rem} from 'polished';
import {breakpoints, spacing} from '../../globals/variables';

type ContainerProps = {
    columns: number,
    items: number
};
type ItemProps = {
    columns: number,
    items: number
};

export const container = (props: ContainerProps) => css`
    @media (min-width: ${rem(breakpoints.mobile)}) {
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
    }
`;

export const item = (props: ItemProps) => css`
    @media (max-width: ${rem(breakpoints.mobile)}) {
        width: 100%;

        & + & {
            margin-top: ${rem(spacing.xs)};
        }
    }

    @media (max-width: ${rem(breakpoints.mobile)}) {
        button {
            width: 100%;
        }
    }

    @media (min-width: ${rem(breakpoints.mobile)}) {
        flex: 0 0 ${100 / props.columns}%;
        min-height: 100%;

        /**
         * Target the animating div that wraps the component, and the annoying Next link wrapper
         * 🤮
         */
        > div,
        > div > a,
        > div a > div,
        > a > div {
            display: flex;
            flex-direction: column;
            height: 100%;
            width: 100%;
        }
    }
`;
