// @flow
import React from 'react';
import {addDecorator, storiesOf} from '@storybook/react';
import CardLayout from './';
import Theme from '../Theme';

const stories = storiesOf('CardLayout', module);

stories.addDecorator(story => (
    <Theme theme="secondary">
        <div style={{padding: 30}}>{story()}</div>
    </Theme>
));

stories.add('default', () => {
    return (
        <CardLayout
            items={[
                {key: '1', component: <div>Card 1</div>},
                {key: '2', component: <div>Card 2</div>},
                {key: '3', component: <div>Card 3</div>},
                {key: '4', component: <div>Card 4</div>},
                {key: '5', component: <div>Card 5</div>},
                {key: '6', component: <div>Card 6</div>},
                {key: '7', component: <div>Card 7</div>},
                {key: '8', component: <div>Card 8</div>},
                {key: '9', component: <div>Card 9</div>}
            ]}
        />
    );
});
