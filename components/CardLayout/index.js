// @flow
import * as React from 'react';
import clamp from 'lodash/clamp';
import AnimateWhenVisible from '../AnimateWhenVisible';
import * as UI from '../UI/styles';
import * as S from './styles';

type Props = {
    animate?: boolean,
    columns?: number,
    items: Array<{
        key: string | number,
        component: React.Node
    }>
};

const CardLayout = ({animate = true, columns = 2, items = []}: Props) => (
    <UI.LayoutContainer type="matrix" css={S.container({columns, items: items.length})}>
        {items.map((item, index) => (
            <UI.LayoutItem
                key={item.key}
                css={S.item({columns, items: items.length})}
                sizeAtMobile={columns === 1 ? 1 / 1 : 6 / 12}
            >
                {animate && (
                    <AnimateWhenVisible delay={clamp(index * 85, 1000)} watch={false}>
                        {item.component}
                    </AnimateWhenVisible>
                )}
                {!animate ? item.component : null}
            </UI.LayoutItem>
        ))}
    </UI.LayoutContainer>
);

export default CardLayout;
