// @flow
import styled from '@emotion/styled';
import {rem} from 'polished';
import {
    breakpoints,
    colors,
    fontFamilies,
    fontSizes,
    shadows,
    spacing,
    themes,
    transitions
} from '../../globals/variables';
import {textLink} from '../UI/styles';

export const Title = styled.span`
    color: ${colors.darkBlueGrey};
    display: block;
    font-size: ${rem(fontSizes.h5)};
    font-family: ${fontFamilies.heading};
    line-height: 1.3;
    margin-bottom: 0;
    transition: ${transitions.default};
`;

export const Container = styled.div`
    align-items: center;
    background-color: ${themes.white.background};
    box-shadow: ${shadows.default};
    border-radius: ${rem(5)};
    color: ${themes.white.text};
    display: flex;
    flex-direction: row !important;
    padding: ${rem(spacing.s)};
    text-align: left;
    transition: ${transitions.default};
    -webkit-backface-visibility: hidden;
    width: 100%;

    @media (min-width: ${rem(breakpoints.mobile)}) {
        padding: ${rem(spacing.m)};
    }

    ${props =>
        props.hasLink &&
        `
        &:hover,
        &:focus {
            box-shadow: ${shadows.hover};
            transform: translateY(-2px);

            ${Title} {
                color: ${colors.purplishBlue};
            }
        }
    `}
`;

export const Content = styled.div`
    flex-grow: 1;
    text-align: left;
    width: 100%;
`;

export const Text = styled.div`
    color: ${colors.darkBlueGrey};
    line-height: 1.4;
    margin-top: ${rem(spacing.xs)};

    a {
        ${textLink}
        color: ${colors.purplishBlue};
    }
    a:hover,
    a:focus {
        color: ${colors.darkBlueGrey};
    }
`;

export const UrlLabel = styled.span`
    color: ${colors.purplishBlue};
    display: inline-block;
    font-family: ${fontFamilies.bold};
    margin-top: ${rem(spacing.xs)};
`;

export const Footnote = styled.p`
    color: ${colors.greyDark};
    line-height: 1.4;
    font-size: ${rem(12)};
    margin-top: ${rem(spacing.xs * 1.5)};
    word-break: break-all;
`;

export const Icon = styled.svg`
    height: auto;
    max-height: ${rem(25)};
    margin-left: ${rem(spacing.m)};
    margin-right: ${rem(spacing.s)};
    width: ${rem(35)};
`;
