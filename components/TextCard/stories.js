// @flow
import React from 'react';
import {addDecorator, storiesOf} from '@storybook/react';
import {withKnobs, number, boolean, object, select, text} from '@storybook/addon-knobs';
import TextCard from './';
import Theme from '../Theme';

const stories = storiesOf('TextCard', module);

stories.addDecorator(withKnobs);

stories.add('basic', () => {
    return <TextCard title="Cupidatat sit nulla eu elit deserunt" />;
});

stories.add('with text', () => {
    return (
        <TextCard
            title="Cupidatat sit nulla eu elit deserunt"
            text="Cupidatat sit nulla eu elit deserunt dolor exercitation anim ea do Lorem est. Sint irure aute magna sit ea dolore dolore laborum ex ex voluptate ea ut cillum. Dolore id aliqua reprehenderit qui nisi dolore ea exercitation elit mollit pariatur anim qui pariatur."
        />
    );
});

stories.add('with a footnote', () => {
    return (
        <TextCard title="Cupidatat sit nulla eu elit deserunt" footnote="Consequat non tempor" />
    );
});

stories.add('with an icon', () => {
    return (
        <TextCard title="Cupidatat sit nulla eu elit deserunt" footnote="JPG–1mb" icon="download" />
    );
});

stories.add('playground', () => {
    return (
        <TextCard
            title={text('Title', 'Cupidatat sit nulla eu elit deserunt ')}
            text={text(
                'Text',
                'Cupidatat sit nulla eu elit deserunt dolor exercitation anim ea do Lorem est. Sint irure aute magna sit ea dolore dolore laborum ex ex voluptate ea ut cillum. Dolore id aliqua reprehenderit qui nisi dolore ea exercitation elit mollit pariatur anim qui pariatur.'
            )}
            footnote={text('Footnote', 'Fugiat ex nostrud eiusmod')}
        />
    );
});
