// @flow
import * as React from 'react';
import {logEvent} from '../../globals/analytics';
import Link from '../Link';
import * as S from './styles';

type Props = {
    footnote?: string,
    icon?: ?'download',
    size?: string,
    text?: string,
    title?: string,
    track?: EventTracking,
    url?: Object | string,
    urlLabel?: string
};

const TextCardContent = ({
    footnote,
    icon,
    size,
    text = '',
    title,
    track,
    url = '',
    urlLabel
}: Props) => {
    const formattedText = text ? text.replace('<a ', '<a target="_blank" rel="noopener" ') : '';

    return (
        <S.Container hasLink={url}>
            <S.Content>
                {title && (
                    <S.Title
                        size={size}
                        data-testid="textcardtitle"
                        dangerouslySetInnerHTML={{__html: title}}
                    />
                )}
                {text && <S.Text dangerouslySetInnerHTML={{__html: formattedText}} />}
                {urlLabel && <S.UrlLabel>{urlLabel}</S.UrlLabel>}
                {footnote && <S.Footnote>{footnote}</S.Footnote>}
            </S.Content>
            {icon === 'book' && (
                <S.Icon xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32">
                    <path fill="none" d="M1 1h32v32H1z" />
                    <path
                        fill="#651FFF"
                        d="M27.002 1v1.999h-2V5h2v28h-24s-2 0-2-2V4.018c0-.006-.001-.012-.001-.018-.035-1.355.807-2.314 1.555-2.646.738-.362 1.362-.35 1.446-.354h23M3.998 5h19.004V2.999h-19c-.002.006-.032-.002-.149.019-.115.019-.274.06-.404.125-.253.171-.414.21-.447.857.015.5.134.609.272.743.144.126.401.212.579.239.095.017.133.018.145.018m1.004 26h20V7h-20v24"
                    />
                    <path fill="#651FFF" d="M7 23v-2h12v2H7zM7 15v-2h16v2H7zM7 19v-2h16v2H7z" />
                </S.Icon>
            )}
            {icon === 'download' && (
                <S.Icon
                    xmlns="http://www.w3.org/2000/svg"
                    width="14"
                    height="17"
                    viewBox="0 0 14 17"
                >
                    <g fill="none" fillRule="evenodd">
                        <path
                            stroke="#000"
                            strokeOpacity=".012"
                            strokeWidth="0"
                            d="M-5-3h24v24H-5z"
                        />
                        <path fill="#651FFF" d="M14 6h-4V0H4v6H0l7 7 7-7zM0 15v2h14v-2H0z" />
                    </g>
                </S.Icon>
            )}
            {icon === 'article' && (
                <S.Icon xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25 25">
                    <g fill="none" fillRule="evenodd">
                        <path d="M0 0h24v24H0z" />
                        <path
                            fill="#651FFF"
                            d="M14 17H4v2h10v-2zm6-8H4v2h16V9zM4 15h16v-2H4v2zM4 5v2h16V5H4z"
                        />
                    </g>
                </S.Icon>
            )}
            {icon === 'audio' && (
                <S.Icon xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25 25">
                    <g fill="none" fillRule="evenodd">
                        <path d="M0 0h24v24H0z" />
                        <path
                            fill="#651FFF"
                            d="M3 9v6h4l5 5V4L7 9H3zm13.5 3A4.5 4.5 0 0014 7.97v8.05c1.48-.73 2.5-2.25 2.5-4.02zM14 3.23v2.06c2.89.86 5 3.54 5 6.71s-2.11 5.85-5 6.71v2.06c4.01-.91 7-4.49 7-8.77 0-4.28-2.99-7.86-7-8.77z"
                        />
                    </g>
                </S.Icon>
            )}
            {icon === 'video' && (
                <S.Icon xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25 25">
                    <g fill="none" fillRule="evenodd">
                        <path d="M0 0h24v24H0z" />
                        <path fill="#651FFF" d="M8 5v14l11-7z" />
                    </g>
                </S.Icon>
            )}
            {icon === 'website' && (
                <S.Icon xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25 25">
                    <g fill="none" fillRule="evenodd">
                        <path d="M0 0h24v24H0z" />
                        <path
                            fill="#651FFF"
                            d="M20 4H4c-1.1 0-1.99.9-1.99 2L2 18c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zm-5 14H4v-4h11v4zm0-5H4V9h11v4zm5 5h-4V9h4v9z"
                        />
                    </g>
                </S.Icon>
            )}
            {icon === 'podcast' && (
                <S.Icon xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25 25">
                    <g fill="none" fillRule="evenodd">
                        <path d="M0 0h24v24H0z" />
                        <path
                            fill="#651FFF"
                            d="M12 15c1.66 0 2.99-1.34 2.99-3L15 6c0-1.66-1.34-3-3-3S9 4.34 9 6v6c0 1.66 1.34 3 3 3zm5.3-3c0 3-2.54 5.1-5.3 5.1S6.7 15 6.7 12H5c0 3.42 2.72 6.23 6 6.72V22h2v-3.28c3.28-.48 6-3.3 6-6.72h-1.7z"
                        />
                    </g>
                </S.Icon>
            )}
        </S.Container>
    );
};

const TextCard = (props: Props) => {
    return props.url ? (
        <Link
            href={props.url}
            data-testid="textcard"
            style={{pointerEvents: props.url ? 'auto' : 'none', width: '100%'}}
            onClick={() => {
                if (props.track) {
                    logEvent(props.track);
                }
            }}
        >
            <TextCardContent {...props} />
        </Link>
    ) : (
        <div data-testid="textcard">
            <TextCardContent {...props} />
        </div>
    );
};

export default TextCard;
