// @flow
import {css} from '@emotion/core';
import styled from '@emotion/styled';
import {rem} from 'polished';
import {breakpoints, colors, spacing} from '../../globals/variables';

export const logoStyles = css`
    #header-logo {
        pointer-events: none;
    }
`;

export const pageContainer = css`
    @media (min-width: ${rem(breakpoints.mobile)}) {
        display: flex;
        flex-direction: column;
        flex-grow: 1;
    }
`;

export const contentContainer = css`
    display: flex;
    flex-direction: column;
    flex-grow: 1;
    height: 100%;
    margin-bottom: ${rem(spacing.m)};
`;

export const formContentContainer = css`
    justify-content: center;
    display: flex;
    flex-direction: column;
    flex-grow: 1;
    margin-bottom: ${rem(spacing.xl)};
`;

export const formContainer = css`
    margin-left: auto;
    margin-right: auto;
    max-width: ${rem(550)};
    text-align: center;
    width: 100%;

    input[type='text'],
    textarea {
        margin-left: auto;
        margin-right: auto;
        text-align: center;
        width: 100%;
    }
`;

export const WinnersContainer = styled.div`
    margin: 0 auto;
    max-width: ${rem(1000)};

    * {
        pointer-events: none;
    }
`;

export const FooterLinks = styled.div`
    margin-top: ${rem(spacing.m)};

    * + * {
        margin-left: ${rem(spacing.s)};
    }
`;

export const SlackReminder = styled.div`
    align-items: center;
    background-color: ${colors.blueGrey};
    border-radius: 5px;
    display: flex;
    font-size: ${rem(15)};
    line-height: 1.6;
    margin-top: ${rem(spacing.m)};
    padding: ${rem(spacing.s * 1.25)};

    @media (min-width: ${rem(breakpoints.mobile)}) {
        margin-top: ${rem(spacing.l)};
    }
`;

export const SlackLogo = styled.svg`
    flex: 0 0 40px;
`;

export const SlackText = styled.p`
    color: ${colors.darkBlueGrey};
    line-height: 1.5;
    margin-left: 20px;
    text-align: left;
`;
