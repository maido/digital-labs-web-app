// @flow
import React, {useEffect, useState} from 'react';
import dynamic from 'next/dynamic';
import Link from 'next/link';
import Router from 'next/router';
import axios from 'axios';
import get from 'lodash/get';
import Cookie from 'js-cookie';
import {connect} from 'react-redux';
import {useSpring, animated} from 'react-spring/web.cjs';
import {Global} from '@emotion/core';
import {Formik, Form} from 'formik';
import __ from '../../globals/strings';
import {updateForm, updateUser} from '../../store/actions';
import teamCreationSchema from '../../schemas/create-team';
import AnimateWhenVisible from '../AnimateWhenVisible';
import CardLayout from '../CardLayout';
import Container from '../Container';
import CTAButton from '../CTAButton';
import FullScreenMessage from '../FullScreenMessage';
import InputText from '../InputText';
import InputTextarea from '../InputTextarea';
import Layout from '../Layout';
import {withAuthSync} from '../../globals/auth';
import ProgressTabLinks from '../ProgressTabLinks';
import TeamSummaryCard from '../TeamSummaryCard';
import * as UI from '../UI/styles';
import * as S from './styles';

const AlertDialog = dynamic(() => import('../AlertDialog'), {ssr: false});

type Props = {
    form: Object,
    dispatch: Function,
    signupForm: Object,
    team: Object,
    teams: Array<TeamSummaryUICard>,
    user: Object
};

const TeamCreatePageLayout = ({form, dispatch, team, teams = []}: Props) => {
    const [alertVisibility, setAlertVisibility] = useState('hidden');

    const steps = [
        {label: 'Step 1', to: '/team/create/'},
        {label: 'Step 2', to: '/team/create?invite'}
    ];
    const [formAnimation, setFormAnimation] = useSpring(() => ({
        config: {tension: 60, mass: 1, friction: 10},
        delay: 200,
        from: {transform: `translateY(20px)`, opacity: 0}
    }));
    const [form2Animation, setForm2Animation] = useSpring(() => ({
        config: {tension: 60, mass: 1, friction: 10},
        delay: 200,
        from: {transform: `translateY(20px)`, opacity: 0}
    }));

    if (form.step === 1) {
        setFormAnimation({delay: 200, transform: `translateY(0)`, opacity: 1});
    } else {
        setForm2Animation({delay: 200, transform: `translateY(0)`, opacity: 1});
    }

    const handleFormReset = () => {
        Cookie.remove('name_draft');
        dispatch(updateForm({state: 'default', fields: {name: ''}, step: 1}));
    };

    const handleSkip = (event: SyntheticMouseEvent<HTMLButtonElement>) => {
        event.preventDefault();
        setAlertVisibility('visible');
    };

    const handleSkipConfirmation = () => Router.replace('/dashboard');

    const handleSkipCancellation = () => {
        setAlertVisibility('leaving');
        setTimeout(() => setAlertVisibility('hidden'), 600);
    };

    const handleStep1FormSubmit = async (values, handlers) => {
        if (values.name) {
            Cookie.set('name_draft', values.name);
            handlers.setSubmitting(false);
            dispatch(updateForm({state: 'pending'}));

            try {
                const response = await axios.post(`/api/team/update?id=${team.id}`, values);

                dispatch(updateForm({state: 'success', fields: {name: values.name}}));
                dispatch(updateUser({team: {name: values.name}}));

                setTimeout(() => {
                    setFormAnimation({transform: `scale(0.95)`, opacity: 0});
                }, 500);
                setTimeout(() => {
                    dispatch(updateForm({state: 'default', step: 2}));
                }, 800);
            } catch (error) {
                const errorResponse = get(error, 'response.data');

                const errors = errorResponse || {
                    name: __('team.errors.create')
                };

                dispatch(updateForm({state: 'default'}));
                handlers.setErrors(errors);

                handlers.setSubmitting(false);
            }
        }
    };

    const handleStep2FormSubmit = async (values, handlers) => {
        if (values.emails) {
            const emails = values.emails.split(',');

            try {
                const response = await axios.post(`/api/team/invite-member?id=${team.id}`, {
                    emails
                });

                if (response.status === 200) {
                    dispatch(
                        updateForm({
                            state: 'success',
                            fields: {...form.fields, emails: values.emails}
                        })
                    );

                    Cookie.remove('name_draft');
                    setTimeout(() => dispatch(updateForm({step: 3})), 1000);
                }
            } catch (error) {
                const errorResponse = get(error, 'response.data');
                const errors = errorResponse || {
                    emails: __('team.errors.invite')
                };

                dispatch(updateForm({state: 'default'}));
                handlers.setErrors(errors);
                handlers.setSubmitting(false);
            }
        }
    };

    return (
        <>
            <Global styles={S.logoStyles} />
            {form.step === 3 && (
                <FullScreenMessage
                    state="visible"
                    title={__('team.created.title')}
                    text={__('team.created.text')}
                    badge="generic-1"
                    cta={{label: __('team.created.ctaLabel'), url: '/dashboard'}}
                />
            )}
            {alertVisibility !== 'hidden' && (
                <AlertDialog
                    title={__('team.invite.skipConfirm.title')}
                    text={__('team.invite.skipConfirm.text')}
                    confirmCTA={{
                        label: __('team.invite.skipConfirm.confirmLabel'),
                        onClick: handleSkipConfirmation
                    }}
                    cancelCTA={{
                        label: __('team.invite.skipConfirm.cancelLabel'),
                        onClick: handleSkipCancellation
                    }}
                    state={alertVisibility}
                />
            )}

            <Container css={S.pageContainer} paddingVertical="l">
                <ProgressTabLinks currentStep={form.step} disableCompleted={true} steps={steps} />

                <UI.Spacer size="l" />

                {form.step === 1 && (
                    <animated.section
                        style={formAnimation}
                        css={[UI.centerText, S.contentContainer]}
                    >
                        <div css={S.formContentContainer}>
                            <UI.Heading2 as="h1">{__('team.create.title')}</UI.Heading2>

                            <p>{__('team.create.text')}</p>

                            <div css={S.formContainer}>
                                <Formik
                                    initialValues={form.fields}
                                    onSubmit={handleStep1FormSubmit}
                                    validationSchema={teamCreationSchema.step1}
                                    render={formProps => (
                                        <Form autoComplete="off">
                                            <InputText
                                                name="name"
                                                placeholder="Your team name"
                                                autoComplete="off"
                                            />
                                            <S.SlackReminder style={{marginTop: 14}}>
                                                {__('team.create.tip')}
                                            </S.SlackReminder>

                                            <UI.Spacer />

                                            <CTAButton
                                                disabled={
                                                    formProps.isSubmitting || formProps.isValidating
                                                }
                                                type="submit"
                                                state={
                                                    formProps.isSubmitting || formProps.isValidating
                                                        ? 'pending'
                                                        : form.state
                                                }
                                                wide={true}
                                            >
                                                {__('team.create.ctaLabel')}
                                            </CTAButton>
                                        </Form>
                                    )}
                                />
                            </div>
                        </div>

                        <S.WinnersContainer>
                            <AnimateWhenVisible>
                                <UI.Heading3 as="h2" css={UI.centerText}>
                                    {__('team.invite.winningTeams.title')}
                                </UI.Heading3>
                            </AnimateWhenVisible>

                            <CardLayout
                                items={teams.map((_team, index) => ({
                                    key: _team.title ? _team.title : '',
                                    component: <TeamSummaryCard {..._team} />
                                }))}
                            />
                        </S.WinnersContainer>
                    </animated.section>
                )}
                {form.step >= 2 && (
                    <animated.section
                        style={form2Animation}
                        css={[S.formContainer, S.contentContainer]}
                    >
                        <div css={S.formContentContainer}>
                            <UI.Subheading>{form.fields.name}</UI.Subheading>
                            <UI.Heading2 as="h1">{__('team.invite.title')}</UI.Heading2>
                            <p style={{margin: '0 auto', maxWidth: 550}}>
                                {__('team.invite.text')}
                            </p>

                            <small css={[UI.padding('bottom', 'xs'), UI.textColor('greyDark')]}>
                                {__('team.invite.tip')}
                            </small>

                            <Formik
                                initialValues={{emails: ''}}
                                onSubmit={handleStep2FormSubmit}
                                validationSchema={teamCreationSchema.step2}
                                validateOnBlur={false}
                                validateOnChange={false}
                                render={formProps => (
                                    <Form autoComplete="off">
                                        <InputTextarea
                                            name="emails"
                                            placeholder="john@doe.com"
                                            autoComplete="off"
                                            autoFocus
                                        />
                                        <UI.Spacer size="s" />
                                        <CTAButton
                                            disabled={
                                                formProps.isSubmitting || formProps.isValidating
                                            }
                                            type="submit"
                                            state={
                                                formProps.isSubmitting || formProps.isValidating
                                                    ? 'pending'
                                                    : form.state
                                            }
                                            wide={true}
                                        >
                                            {__('team.invite.ctaLabel')}
                                        </CTAButton>
                                    </Form>
                                )}
                            />
                            {form.step !== 3 && (
                                <S.FooterLinks>
                                    <CTAButton
                                        basic={true}
                                        blockAtMobile={false}
                                        css={[UI.textColor('greyDark'), UI.dangerLink]}
                                        onClick={handleFormReset}
                                    >
                                        {__('team.invite.restartLabel')}
                                    </CTAButton>
                                    <CTAButton
                                        basic={true}
                                        blockAtMobile={false}
                                        css={UI.textColor('purplishBlue')}
                                        href="/dashboard"
                                        onClick={handleSkip}
                                    >
                                        {__('team.invite.skipLabel')}
                                    </CTAButton>
                                </S.FooterLinks>
                            )}
                        </div>

                        <AnimateWhenVisible delay={1000}>
                            <S.SlackReminder>
                                <S.SlackLogo
                                    width="40"
                                    height="40"
                                    xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 40 40"
                                >
                                    <g fill="none" fillRule="evenodd">
                                        <circle fill="#FFF" cx="20" cy="20" r="20" />
                                        <path
                                            d="M13.10968 23.13548c0 1.3742-1.12258 2.49678-2.49678 2.49678-1.37419 0-2.49677-1.12258-2.49677-2.49678 0-1.37419 1.12258-2.49677 2.49677-2.49677h2.49678v2.49677zm1.25806 0c0-1.37419 1.12258-2.49677 2.49678-2.49677 1.37419 0 2.49677 1.12258 2.49677 2.49677v6.25162c0 1.37419-1.12258 2.49677-2.49677 2.49677-1.3742 0-2.49678-1.12258-2.49678-2.49677v-6.25162z"
                                            fill="#E01F5A"
                                        />
                                        <path
                                            d="M23.13548 26.89032c1.3742 0 2.49678 1.12258 2.49678 2.49678 0 1.37419-1.12258 2.49677-2.49678 2.49677-1.37419 0-2.49677-1.12258-2.49677-2.49677v-2.49678h2.49677zm0-1.25806c-1.37419 0-2.49677-1.12258-2.49677-2.49678 0-1.37419 1.12258-2.49677 2.49677-2.49677h6.25162c1.37419 0 2.49677 1.12258 2.49677 2.49677 0 1.3742-1.12258 2.49678-2.49677 2.49678h-6.25162z"
                                            fill="#ECB22D"
                                        />
                                        <path
                                            d="M26.89032 16.86452c0-1.3742 1.12258-2.49678 2.49678-2.49678 1.37419 0 2.49677 1.12258 2.49677 2.49678 0 1.37419-1.12258 2.49677-2.49677 2.49677h-2.49678v-2.49677zm-1.25806 0c0 1.37419-1.12258 2.49677-2.49678 2.49677-1.37419 0-2.49677-1.12258-2.49677-2.49677V10.6129c0-1.37419 1.12258-2.49677 2.49677-2.49677 1.3742 0 2.49678 1.12258 2.49678 2.49677v6.25162z"
                                            fill="#2FB67C"
                                        />
                                        <path
                                            d="M16.86452 13.10968c-1.3742 0-2.49678-1.12258-2.49678-2.49678 0-1.37419 1.12258-2.49677 2.49678-2.49677 1.37419 0 2.49677 1.12258 2.49677 2.49677v2.49678h-2.49677zm0 1.25806c1.37419 0 2.49677 1.12258 2.49677 2.49678 0 1.37419-1.12258 2.49677-2.49677 2.49677H10.6129c-1.37419 0-2.49677-1.12258-2.49677-2.49677 0-1.3742 1.12258-2.49678 2.49677-2.49678h6.25162z"
                                            fill="#36C5F1"
                                        />
                                    </g>
                                </S.SlackLogo>
                                <S.SlackText
                                    dangerouslySetInnerHTML={{__html: __('team.invite.slack')}}
                                />
                            </S.SlackReminder>
                        </AnimateWhenVisible>
                    </animated.section>
                )}
            </Container>
        </>
    );
};

const mapStateToProps = state => ({form: state.form, user: state.form});

export default withAuthSync(connect(mapStateToProps)(TeamCreatePageLayout));
