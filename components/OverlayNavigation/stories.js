// @flow
import React from 'react';
import {addDecorator, storiesOf} from '@storybook/react';
import OverlayNavigation from './';
import Theme from '../Theme';

const stories = storiesOf('OverlayNavigation', module);

stories.addDecorator(story => (
    <Theme theme="secondary">
        <div style={{minHeight: '100vh', width: '100%', position: 'relative'}}>{story()}</div>
    </Theme>
));

stories.add('default', () => {
    return (
        <OverlayNavigation
            user={{name: 'Foo bar', avatar: {url: 'https://placehold.it/100x100'}}}
            handleClick={() => {}}
            isActive={true}
        />
    );
});
