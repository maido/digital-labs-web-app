import {css} from '@emotion/core';
import styled from '@emotion/styled';
import {rem} from 'polished';
import {
    breakpoints,
    colors,
    fontFamilies,
    shadows,
    spacing,
    themes,
    transitions
} from '../../globals/variables';

export const Container = styled.div`
    background-color: ${props =>
        props.theme ? props.theme.background : themes.secondary.background};
    bottom: 0;
    color: ${props => (props.theme ? props.theme.text : themes.secondary.text)};
    left: 0;
    right: 0;
    opacity: 0;
    overflow: auto;
    -webkit-overflow-scrolling: touch;
    position: fixed;
    transition: ${transitions.slow};
    transition-duration: 2s;
    visibility: hidden;
    top: 0;
    z-index: 200;

    a {
        color: ${props => (props.theme ? props.theme.links : themes.secondary.links)};
    }

    ${props => props.isActive && `opacity:1; visibility: visible; transition-duration:0.2s;`};

    @media (min-width: ${rem(breakpoints.tablet)}) {
        display: none;
    }
`;

export const wrapper = css`
    display: flex;
    flex-direction: column;
    // height: calc(100% - 100px);
    justify-content: flex-start;
    margin-top: ${rem(80)};
    padding: ${rem(spacing.m)};
    padding-top: 0;
    position: relative;
    z-index: 1;
    width: 100%;
`;

export const linksContainer = css`
    background-color: ${themes.tertiary.background};
    border-top-left-radius: ${rem(30)};
    border-top-right-radius: ${rem(30)};
    box-shadow: 0 -6px 18px 0 rgba(0, 0, 0, 0.1);
    color: ${themes.tertiary.text};
    display: flex;
    flex-direction: column;
    flex-grow: 1;
    justify-content: flex-start;
    min-height: calc(100vh - 100px);
    overflow: hidden;
    position: relative;
    z-index: 2;

    @media only screen and (max-height: 850px) and (orientation: landscape) {
        display: flex;
        text-align: center;

        > div,
        a {
            display: inline-block;
        }

        > div + div,
        a + a {
            margin-left: ${rem(spacing.m)};
        }

        a {
            font-size: ${rem(22)} !important;
        }
    }
`;

export const Links = styled.nav`
    background-color: ${themes.white.background};
    flex-grow: 1;
    height: 100%;
    padding: ${rem(spacing.s)} ${rem(spacing.m)};
`;

export const Link = styled.a`
    color: ${colors.purplishBlue} !important;
    display: block;
    font-family: ${fontFamilies.bold};
    padding: ${rem(spacing.s * 0.75)} 0;

    & + & {
        border-top: 1px solid ${themes.white.accent};
    }

    &:last-child {
        color: ${colors.greyDark};
    }
`;

export const MentorContainer = styled.div`
    background-color: ${colors.tertiary};
    padding: ${rem(spacing.m * 0.75)} ${rem(spacing.m)};
`;

export const MentorLabel = styled.strong`
    display: block;
    font-size: ${rem(14)};
    line-height: 1.2;
`;

export const SlackLogo = styled.svg`
    box-shadow: ${shadows.default};
    height: ${rem(20)};
    left: ${rem(55)};
    position: absolute;
    top: ${rem(45)};
    width: ${rem(20)};
    will-change: transform;
`;
