// @flow
import React from 'react';
import Link from 'next/link';
import {useSpring, animated} from 'react-spring/web.cjs';
import get from 'lodash/get';
import CTAButton from '../CTAButton';
import {ExternalIcon} from '../SiteHeader/styles';
import * as UI from '../UI/styles';
import * as S from './styles';
import {INTEGRATIONS} from '../../globals/config';

type Props = {
    handleClick: Function,
    isActive: boolean,
    user: Object
};

const OverlayNavigation = ({handleClick, isActive, user}: Props) => {
    const mentor = get(user, 'team.mentor');

    const [userContainerAnimation, setUserContainerAnimation] = useSpring(() => ({
        config: {tension: 60, mass: 1, friction: 10},
        from: {transform: `translateY(80px)`, opacity: 0}
    }));
    const [linksContainerAnimation, setLinksContainerAnimation] = useSpring(() => ({
        config: {tension: 60, mass: 1, friction: 10},
        from: {transform: `translateY(100px)`, opacity: 0}
    }));

    if (isActive) {
        setUserContainerAnimation({delay: 400, opacity: 1, transform: 'translateY(0)'});
        setLinksContainerAnimation({delay: 100, transform: 'translateY(0)', opacity: 1});
    } else {
        setUserContainerAnimation({delay: 0, opacity: 0, transform: 'translateY(80px)'});
        setLinksContainerAnimation({delay: 0, transform: 'translateY(100px)', opacity: 0});
    }

    return (
        <S.Container isActive={isActive} className={isActive && 'is-active'}>
            <animated.div style={userContainerAnimation} css={S.wrapper}>
                <Link href="/users/[userId]" as={`/users/${user.id}`} passHref>
                    <UI.FlexMediaWrapper as="a">
                        <UI.Avatar
                            image={user.avatar ? user.avatar.thumb_url : null}
                            size="medium"
                        />
                        <UI.FlexMediaContent>
                            <UI.Heading4 as="span">
                                {user.first_name} {user.last_name}
                            </UI.Heading4>
                            <br />
                            {user.team.name}
                        </UI.FlexMediaContent>
                    </UI.FlexMediaWrapper>
                </Link>
            </animated.div>
            <animated.div css={S.linksContainer} style={linksContainerAnimation}>
                {mentor && (
                    <S.MentorContainer>
                        <UI.FlexMediaWrapper>
                            <UI.Avatar
                                image={mentor.avatar ? mentor.avatar.thumb_url : null}
                                size="medium"
                                alt={`${mentor.first_name} ${mentor.last_name}`}
                                title={`${mentor.first_name} ${mentor.last_name}`}
                            />
                            <UI.FlexMediaContent>
                                <S.SlackLogo
                                    width="40"
                                    height="40"
                                    xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 40 40">
                                    <g fill="none" fillRule="evenodd">
                                        <circle fill="#FFF" cx="20" cy="20" r="20" />
                                        <path
                                            d="M13.10968 23.13548c0 1.3742-1.12258 2.49678-2.49678 2.49678-1.37419 0-2.49677-1.12258-2.49677-2.49678 0-1.37419 1.12258-2.49677 2.49677-2.49677h2.49678v2.49677zm1.25806 0c0-1.37419 1.12258-2.49677 2.49678-2.49677 1.37419 0 2.49677 1.12258 2.49677 2.49677v6.25162c0 1.37419-1.12258 2.49677-2.49677 2.49677-1.3742 0-2.49678-1.12258-2.49678-2.49677v-6.25162z"
                                            fill="#E01F5A"
                                        />
                                        <path
                                            d="M23.13548 26.89032c1.3742 0 2.49678 1.12258 2.49678 2.49678 0 1.37419-1.12258 2.49677-2.49678 2.49677-1.37419 0-2.49677-1.12258-2.49677-2.49677v-2.49678h2.49677zm0-1.25806c-1.37419 0-2.49677-1.12258-2.49677-2.49678 0-1.37419 1.12258-2.49677 2.49677-2.49677h6.25162c1.37419 0 2.49677 1.12258 2.49677 2.49677 0 1.3742-1.12258 2.49678-2.49677 2.49678h-6.25162z"
                                            fill="#ECB22D"
                                        />
                                        <path
                                            d="M26.89032 16.86452c0-1.3742 1.12258-2.49678 2.49678-2.49678 1.37419 0 2.49677 1.12258 2.49677 2.49678 0 1.37419-1.12258 2.49677-2.49677 2.49677h-2.49678v-2.49677zm-1.25806 0c0 1.37419-1.12258 2.49677-2.49678 2.49677-1.37419 0-2.49677-1.12258-2.49677-2.49677V10.6129c0-1.37419 1.12258-2.49677 2.49677-2.49677 1.3742 0 2.49678 1.12258 2.49678 2.49677v6.25162z"
                                            fill="#2FB67C"
                                        />
                                        <path
                                            d="M16.86452 13.10968c-1.3742 0-2.49678-1.12258-2.49678-2.49678 0-1.37419 1.12258-2.49677 2.49678-2.49677 1.37419 0 2.49677 1.12258 2.49677 2.49677v2.49678h-2.49677zm0 1.25806c1.37419 0 2.49677 1.12258 2.49677 2.49678 0 1.37419-1.12258 2.49677-2.49677 2.49677H10.6129c-1.37419 0-2.49677-1.12258-2.49677-2.49677 0-1.3742 1.12258-2.49678 2.49677-2.49678h6.25162z"
                                            fill="#36C5F1"
                                        />
                                    </g>
                                </S.SlackLogo>
                                <S.MentorLabel>Your team's mentor</S.MentorLabel>
                                {mentor.slack ? (
                                    <span>
                                        <em>{mentor.slack}</em> on Slack
                                    </span>
                                ) : (
                                    <span>
                                        {mentor.first_name} {mentor.last_name}
                                    </span>
                                )}
                            </UI.FlexMediaContent>
                        </UI.FlexMediaWrapper>
                    </S.MentorContainer>
                )}
                <S.Links>
                    <Link href="/dashboard" passHref>
                        <S.Link onClick={handleClick}>Dashboard</S.Link>
                    </Link>
                    <Link href="/labs" passHref>
                        <S.Link onClick={handleClick}>Labs</S.Link>
                    </Link>
                    <Link href="/resources" passHref>
                        <S.Link onClick={handleClick}>Resources</S.Link>
                    </Link>
                    <Link href="/teams" passHref>
                        <S.Link onClick={handleClick}>Teams</S.Link>
                    </Link>
                    <Link href="/mentors" passHref>
                        <S.Link onClick={handleClick}>Mentors</S.Link>
                    </Link>
                    <S.Link href={INTEGRATIONS.wikifactory.url} onClick={handleClick}>
                        {INTEGRATIONS.wikifactory.name}
                        <ExternalIcon
                            xmlns="http://www.w3.org/2000/svg"
                            width="24"
                            height="24"
                            viewBox="0 0 24 24">
                            <path d="M9 5v2h6.59L4 18.59 5.41 20 17 8.41V15h2V5z" />
                        </ExternalIcon>
                    </S.Link>

                    <UI.Spacer size="s" />

                    <UI.Label css={UI.textColor('greyDark')}>Your account</UI.Label>
                    <Link href="/users/[userId]" as={`/users/${user.id}`} passHref>
                        <S.Link onClick={handleClick}>View profile</S.Link>
                    </Link>
                    <Link href="/account/edit" passHref>
                        <S.Link onClick={handleClick}>Edit profile</S.Link>
                    </Link>
                    <Link href="/teams/[teamId]" as={`/teams/${user.team.id}`} passHref>
                        <S.Link onClick={handleClick}>View team</S.Link>
                    </Link>
                    {user.pitch && user.pitch.canSubmit && (
                        <Link href="/team/pitch" passHref>
                            <S.Link onClick={handleClick}>Submit pitch</S.Link>
                        </Link>
                    )}
                    <Link href="/logout" passHref>
                        <S.Link>Logout</S.Link>
                    </Link>
                </S.Links>
            </animated.div>
        </S.Container>
    );
};

export default OverlayNavigation;
