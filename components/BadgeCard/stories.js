// @flow
import React from 'react';
import {storiesOf} from '@storybook/react';
import BadgeCard from './';

const stories = storiesOf('BadgeCard', module);

stories.add('default', () => {
    return (
        <BadgeCard
            image="https://placehold.it/200x200"
            title="Consectetur ipsum eu"
            text="Enim ex id irure veniam deserunt pariatur nulla ullamco pariatur."
            cta={{url: '/', label: 'Find out more'}}
        />
    );
});

stories.add('with a longer title', () => {
    return (
        <BadgeCard
            title="Et irure aute ullamco exercitation nulla aliquip"
            text="Enim ex id irure veniam deserunt pariatur nulla ullamco pariatur."
            cta={{url: '/', label: 'Find out more'}}
        />
    );
});

stories.add('with no text', () => {
    return (
        <BadgeCard
            title="Et irure aute ullamco exercitation nulla aliquip"
            cta={{url: '/', label: 'Find out more'}}
        />
    );
});

stories.add('with no CTA', () => {
    return (
        <BadgeCard
            title="Et irure aute ullamco exercitation nulla aliquip"
            text="Enim ex id irure veniam deserunt pariatur nulla ullamco pariatur."
        />
    );
});
