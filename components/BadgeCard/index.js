// @flow
import * as React from 'react';
import CTAButton from '../CTAButton';
import * as S from './styles';

type Props = {
    cta?: {
        label: string,
        url: string
    },
    image?: string,
    text?: string,
    title?: string
};

const BadgeCard = ({cta, image, text, title}: Props) => (
    <S.Container>
        {image && <S.Image src={image} />}
        {title && <S.Title>{title}</S.Title>}
        {text && <S.Text>{text}</S.Text>}
        {cta && cta.url && (
            <CTAButton href={cta.url} size="small">
                {cta.label}
            </CTAButton>
        )}
    </S.Container>
);

export default BadgeCard;
