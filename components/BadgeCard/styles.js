// @flow
import {keyframes} from '@emotion/core';
import styled from '@emotion/styled';
import {rem} from 'polished';
import {fontFamilies, fontSizes, shadows, spacing, themes} from '../../globals/variables';

const shineAnimation = keyframes`
    from {
        opacity: 0;
        right: -100%;
    }
    to {
        opacity: 1;
        right: 100%;
    }
`;

export const Container = styled.div`
    background-color: ${themes.white.background};
    box-shadow: ${shadows.hover};
    border-radius: ${rem(5)};
    color: ${themes.white.text};
    cursor: default;
    padding: ${rem(spacing.m)};
    overflow: hidden;
    position: relative;
    text-align: center;

    &::after {
        background: rgba(255, 255, 255, 0.3);
        background: linear-gradient(
            to right,
            rgba(255, 255, 255, 0.2) 0%,
            rgba(255, 255, 255, 0.2) 77%,
            rgba(255, 255, 255, 0.6) 92%,
            rgba(255, 255, 255, 0) 100%
        );
        content: '';
        height: 200%;
        opacity: 0;
        position: absolute;
        right: -200%;
        top: -30%;
        transform: rotate(30deg);
        width: 50%;
        z-index: 2;
    }
    &:hover::after,
    &:focus::after {
        animation: ${shineAnimation} 1s;
    }
`;

export const Image = styled.img`
    margin-bottom: ${rem(spacing.xs)};
    width: 70%;
    z-index: 1;
`;

export const Text = styled.span`
    display: block;
    margin: ${rem(spacing.s * -1)} 0 ${rem(spacing.s)};
`;

export const Title = styled.span`
    display: block;
    font-family: ${fontFamilies.heading};
    font-size: ${rem(28)};
    line-height: 1.2;
    margin-bottom: ${rem(spacing.s)};
    z-index: 1;
`;
