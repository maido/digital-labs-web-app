// @flow
import React, {useState} from 'react';
import {ErrorMessage, Field} from 'formik';
import {logEvent} from '../../globals/analytics';
import * as FormStyles from '../Form/styles';
import * as S from './styles';

type Props = {
    autoComplete?: string,
    description?: string,
    disabled?: string,
    label?: string,
    minLength?: string,
    name: string,
    placeholder?: string,
    type?: string
};

const PasswordToggle = ({isPasswordVisible, handleToggle}) => (
    <S.PasswordToggle
        type="button"
        onClick={handleToggle}
        aria-label={`${isPasswordVisible ? 'Hide' : 'Show'} password`}
        title={`${isPasswordVisible ? 'Hide' : 'Show'} password`}>
        {isPasswordVisible ? (
            <svg height="20" width="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                <path
                    d="M12.81 4.36l-1.77 1.78a4 4 0 0 0-4.9 4.9l-2.76 2.75C2.06 12.79.96 11.49.2 10a11 11 0 0 1 12.6-5.64zm3.8 1.85c1.33 1 2.43 2.3 3.2 3.79a11 11 0 0 1-12.62 5.64l1.77-1.78a4 4 0 0 0 4.9-4.9l2.76-2.75zm-.25-3.99l1.42 1.42L3.64 17.78l-1.42-1.42L16.36 2.22z"
                    fill="#32ebb4"
                />
            </svg>
        ) : (
            <svg height="20" width="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                <path
                    d="M.2 10a11 11 0 0 1 19.6 0A11 11 0 0 1 .2 10zm9.8 4a4 4 0 1 0 0-8 4 4 0 0 0 0 8zm0-2a2 2 0 1 1 0-4 2 2 0 0 1 0 4z"
                    fill="#32ebb4"
                />
            </svg>
        )}
    </S.PasswordToggle>
);

const InputText = ({
    autoComplete,
    description,
    disabled,
    label,
    minLength,
    name,
    placeholder,
    type = 'text'
}: Props) => {
    const [isPasswordVisible, setIsPasswordVisible] = useState(false);
    const [fieldType, setFieldType] = useState(type);

    const togglePasswordVisibility = () => {
        if (isPasswordVisible) {
            setFieldType('password');
            setIsPasswordVisible(false);
        } else {
            setFieldType('text');
            setIsPasswordVisible(true);
            logEvent({action: 'Click', category: 'Password visiblity', label: 'Login – Form'});
        }
    };

    return (
        <>
            <Field label={label} name={name} placeholder={placeholder}>
                {({field, form, ...props}) => {
                    /**
                     * TODO: This is a quick workaround for not having validation on Team Search.
                     */
                    if (minLength) {
                        field.minLength = minLength;
                    }

                    if (disabled) {
                        field.disabled = 'disabled';
                    }

                    return (
                        <S.Container>
                            {label && (
                                <label htmlFor={name} css={FormStyles.label}>
                                    {label}
                                </label>
                            )}
                            {description && (
                                <span
                                    css={FormStyles.description}
                                    dangerouslySetInnerHTML={{__html: description}}
                                />
                            )}
                            <S.InputContainer type={type}>
                                <input
                                    css={[
                                        FormStyles.input,
                                        form.touched[field.name] && form.errors[field.name]
                                            ? FormStyles.inputError
                                            : null
                                    ]}
                                    {...field}
                                    {...props}
                                    id={name}
                                    name={field.name}
                                    type={fieldType}
                                    autoComplete={type === 'password' ? 'off' : autoComplete}
                                    placeholder={placeholder}
                                    value={field.value || ''}
                                />
                                {type === 'password' && (
                                    <PasswordToggle
                                        isPasswordVisible={isPasswordVisible}
                                        handleToggle={togglePasswordVisibility}
                                    />
                                )}
                            </S.InputContainer>
                        </S.Container>
                    );
                }}
            </Field>
            <ErrorMessage
                component="span"
                name={name}
                css={FormStyles.error}
                data-testid={`error-${name}`}
            />
        </>
    );
};

export default InputText;
