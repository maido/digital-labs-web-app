// @flow
import styled from '@emotion/styled';
import {rem} from 'polished';
import {colors, spacing, themes, transitions} from '../../globals/variables';

export const Container = styled.div`
    position: relative;
`;

export const InputContainer = styled.div`
    display: flex;
    flex-wrap: wrap;
    position: relative;

    ${props => props.type === 'password' && `width: calc(100% - ${rem(40)})`}
`;

export const PasswordToggle = styled.button`
    background-color: ${colors.white};
    border: 1px solid ${themes.inactiveFormField.border};
    border-left: 0;
    border-radius: ${rem(2)};
    border-bottom-left-radius: 0;
    border-top-left-radius: 0;
    height: 100%;
    position: absolute;
    right: ${rem(40 * -1)};
    top: 0;
    width: ${rem(44)};
    transition: ${transitions.default};
`;
