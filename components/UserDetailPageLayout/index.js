// @flow
import React from 'react';
import get from 'lodash/get';
import AnimateWhenVisible from '../AnimateWhenVisible';
import BreadcrumbNav from '../BreadcrumbNav';
import Container from '../Container';
import Theme from '../Theme';
import UserDetailCard from '../UserDetailCard';
import * as UI from '../UI/styles';

type Props = {
    breadcrumbNav?: Array<Object>,
    network: NetworkStatus,
    user: User
};

const UserDetailPageLayout = ({breadcrumbNav, network, user}: Props) => (
    <>
        <Container paddingVertical={false}>
            {breadcrumbNav && <BreadcrumbNav links={breadcrumbNav} />}
        </Container>

        <Theme theme="tertiary" overlap="secondary" overlapSize={150} grow={true}>
            <UI.Spacer />
            <Container paddingVertical="l">
                <UI.LayoutContainer size="l">
                    <UI.LayoutItem sizeAtMobile={5 / 12} css={UI.stickyAt('mobile')}>
                        <AnimateWhenVisible delay={-1}>
                            <UserDetailCard user={user} />
                        </AnimateWhenVisible>
                        <UI.Spacer />
                    </UI.LayoutItem>

                    <UI.LayoutItem sizeAtMobile={7 / 12}>
                        <AnimateWhenVisible delay={-1}>
                            <UI.Box radius={20}>
                                <UI.Heading3>Information</UI.Heading3>

                                <UI.LayoutContainer type="matrix">
                                    <UI.LayoutItem sizeAtMobile={6 / 12}>
                                        <strong>Country</strong>
                                        <br />
                                        {user.country_origin}
                                    </UI.LayoutItem>
                                    <UI.LayoutItem sizeAtMobile={6 / 12}>
                                        <strong>Location</strong>
                                        <br />
                                        {user.country_residence}
                                    </UI.LayoutItem>
                                    <UI.LayoutItem sizeAtMobile={6 / 12}>
                                        <strong>Occupation</strong>
                                        <br />
                                        {get(user, 'occupation')}
                                    </UI.LayoutItem>
                                    <UI.LayoutItem sizeAtMobile={6 / 12}>
                                        <strong>Field of expertise</strong>
                                        <br />
                                        {get(user, 'field_of_expertise', '-')}
                                    </UI.LayoutItem>
                                    <UI.LayoutItem sizeAtMobile={6 / 12}>
                                        <strong>University</strong>
                                        <br />
                                        {get(user, 'university', '-')}
                                    </UI.LayoutItem>
                                </UI.LayoutContainer>
                            </UI.Box>
                        </AnimateWhenVisible>
                    </UI.LayoutItem>
                </UI.LayoutContainer>
            </Container>
        </Theme>
    </>
);

export default UserDetailPageLayout;
