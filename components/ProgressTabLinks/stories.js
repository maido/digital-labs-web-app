// @flow
import React from 'react';
import {storiesOf} from '@storybook/react';
import ProgressTabLinks from '.';

const stories = storiesOf('ProgressTabLinks', module);

stories.add('default', () => {
    return (
        <ProgressTabLinks
            steps={[
                {label: 'Step 1', to: '/team/create/1'},
                {label: 'Step 2', to: '/team/create/2'},
                {label: 'Step 3', to: '/team/create/3'}
            ]}
        />
    );
});

stories.add('with completed step', () => {
    return (
        <ProgressTabLinks
            currentStep={2}
            steps={[
                {label: 'Step 1', to: '/team/create/1'},
                {label: 'Step 2', to: '/team/create/2'},
                {label: 'Step 3', to: '/team/create/3'}
            ]}
        />
    );
});

stories.add('with disabled linking for previous steps', () => {
    return (
        <ProgressTabLinks
            currentStep={2}
            disableCompleted={true}
            steps={[
                {label: 'Step 1', to: '/team/create/1'},
                {label: 'Step 2', to: '/team/create/2'},
                {label: 'Step 3', to: '/team/create/3'}
            ]}
        />
    );
});

stories.add('with disabled completed status', () => {
    return (
        <ProgressTabLinks
            currentStep={2}
            showCompleted={false}
            steps={[
                {label: 'Step 1', to: '/team/create/1'},
                {label: 'Step 2', to: '/team/create/2'},
                {label: 'Step 3', to: '/team/create/3'}
            ]}
        />
    );
});
