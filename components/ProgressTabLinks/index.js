// @flow
import * as React from 'react';
import ButtonList from '../ButtonList';
import Container from '../Container';
import CTAButton from '../CTAButton';
import * as S from './styles';

type Props = {
    allActive?: boolean,
    currentStep?: number,
    disableCompleted?: boolean,
    handleClick?: Function,
    showCompleted?: boolean,
    steps: Array<{
        label: string,
        to: string
    }>
};

const ProgressTabLinks = ({
    allActive = false,
    currentStep = 1,
    disableCompleted = false,
    handleClick,
    showCompleted = true,
    steps = []
}: Props) => {
    const [completedSteps, setCompletedSteps] = React.useState(0);

    React.useEffect(() => {
        if (currentStep - 1 > completedSteps) {
            setCompletedSteps(currentStep - 1);
        }
    }, [currentStep]);

    return (
        <ButtonList center={true}>
            {steps.map((step, i) => {
                const index = i + 1;
                let isActive = index === currentStep;
                let isComplete = index < currentStep;
                let isDisabled = index > currentStep;

                if (isComplete && disableCompleted) {
                    isDisabled = true;
                } else if (allActive || completedSteps >= index) {
                    isDisabled = false;
                }

                if (index <= completedSteps) {
                    isActive = true;
                    isComplete = true;
                } else if (index === completedSteps + 1) {
                    isActive = true;
                    isDisabled = false;
                }

                return (
                    <CTAButton
                        key={step.label}
                        href={step.to}
                        theme={isActive ? 'activeButton' : 'inactiveButton'}
                        css={S.button}
                        aria-current={currentStep === index ? 'step' : ''}
                        data-complete={isComplete ? true : false}
                        disabled={isDisabled}
                        blockAtMobile={false}
                        onClick={() => {
                            if (handleClick) {
                                handleClick(index);
                            }
                        }}>
                        {showCompleted && isComplete && (
                            <S.Icon
                                width="18"
                                height="14"
                                viewBox="0 0 18 14"
                                xmlns="http://www.w3.org/2000/svg">
                                <g fill="none" fillRule="evenodd">
                                    <path d="M-3-5h24v24H-3z" />
                                    <path d="M6 11.17L1.83 7 .41 8.41 6 14 18 2 16.59.59z" />
                                </g>
                            </S.Icon>
                        )}
                        {step.label}
                    </CTAButton>
                );
            })}
        </ButtonList>
    );
};

export default ProgressTabLinks;
