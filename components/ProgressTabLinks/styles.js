// @flow
import {css} from '@emotion/core';
import styled from '@emotion/styled';
import {rem} from 'polished';
import {breakpoints, colors, spacing} from '../../globals/variables';

export const button = css`
    @media (max-width: ${rem(breakpoints.mobile)}) {
        font-size: ${rem(15)};
        padding-left: ${rem(spacing.m * 0.85)};
        padding-right: ${rem(spacing.m * 0.85)};
    }
`;

export const Icon = styled.svg`
    height: auto;
    margin-right: ${rem(spacing.s)};
    width: ${rem(14)};

    path:nth-of-type(2) {
        fill: currentColor;
    }
`;
