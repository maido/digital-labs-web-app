// @flow
import React from 'react';
import {addDecorator, storiesOf} from '@storybook/react';
import {withKnobs, object, select, text} from '@storybook/addon-knobs';
import AlertDialog from './';

const stateOptions = {
    Visible: 'visible',
    Leaving: 'leaving',
    Hidden: 'hidden'
};
const stories = storiesOf('AlertDialog', module);

stories.addDecorator(withKnobs);

stories.add('default', () => {
    return (
        <AlertDialog
            title="Are you sure?"
            text="To submit a pitch you need a team of at least two people"
            confirmCTA={{label: "Yes, I'm sure"}}
            cancelCTA={{label: 'No, invite team members now'}}
            state="visible"
        />
    );
});

stories.add('playground', () => {
    return (
        <AlertDialog
            title={text('Title', 'Are you sure?')}
            text={text('Text', 'To submit a pitch you need a team of at least two people')}
            confirmCTA={object('Confirm CTA', {label: "Yes, I'm sure"})}
            cancelCTA={object('Cancel CTA', {label: 'No, invite team members now'})}
            state={select('State', stateOptions, 'visible')}
        />
    );
});
