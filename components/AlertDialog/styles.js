// @flow
import {css} from '@emotion/core';
import {rem, rgba} from 'polished';
import {themeStyles} from '../Theme/styles';
import {responsiveRem} from '../../globals/functions';
import {breakpoints, spacing, themes, shadows} from '../../globals/variables';

export const container = css`
    ${themeStyles('tertiary')};
    background-color: ${rgba(themes.modal.background, 0.5)};
    align-items: center;
    bottom: 0;
    display: flex;
    height: 100%;
    left: 0;
    justify-content: center;
    position: fixed;
    padding: ${rem(spacing.m)};
    right: 0;
    text-align: center;
    top: 0;
    width: 100%;
    z-index: 100;

    @media (min-width: ${rem(breakpoints.mobile)}) {
        padding: ${rem(spacing.l)};
    }
`;

export const content = css`
    background-color: ${themes.white.background};
    border-radius: ${rem(4)};
    box-shadow: ${shadows.default};
    padding: ${responsiveRem(spacing.m)};

    @media (min-width: ${rem(breakpoints.mobile)}) {
        max-width: ${rem(420)};
    }
`;

export const cancelButton = css`
    // font-weight: 500 !important;
`;
