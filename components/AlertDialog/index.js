// @flow
import React, {useEffect} from 'react';
import {useSpring, animated} from 'react-spring/web.cjs';
import CTAButton from '../CTAButton';
import * as UI from '../UI/styles';
import * as S from './styles';

type Props = {
    confirmCTA?: CTA,
    cancelCTA?: CTA,
    state: 'hidden' | 'leaving' | 'visible',
    text?: string,
    title?: string
};

const AlertDialog = ({confirmCTA, cancelCTA, state = 'hidden', text, title}: Props) => {
    const [overlayAnimation, setOverlayAnimation] = useSpring(() => ({
        config: {tension: 60, mass: 1, friction: 20},
        from: {opacity: 0}
    }));
    const [containerAnimation, setContainerAnimation] = useSpring(() => ({
        config: {tension: 100, mass: 1, friction: 10},
        from: {transform: `scale(0.5)`, opacity: 0}
    }));
    const [contentAnimation, setContentAnimation] = useSpring(() => ({
        config: {tension: 60, mass: 1, friction: 10},
        from: {transform: `translateY(10px)`, opacity: 0}
    }));
    const [buttonAnimation, setButtonAnimation] = useSpring(() => ({
        config: {tension: 60, mass: 1, friction: 10},
        from: {transform: `translateY(20px)`, opacity: 0}
    }));

    useEffect(() => {
        if (state === 'visible') {
            setOverlayAnimation({delay: 0, opacity: 1});
            setContainerAnimation({delay: 200, transform: `scale(1)`, opacity: 1});
            setContentAnimation({delay: 400, transform: `translateY(0)`, opacity: 1});
            setButtonAnimation({delay: 700, transform: `translateY(0)`, opacity: 1});
        } else if (state === 'leaving') {
            setOverlayAnimation({delay: 200, opacity: 0});
            setContainerAnimation({
                delay: 100,
                transform: `scale(.75) translateY(10px)`,
                opacity: 0
            });
            setContentAnimation({delay: 0, opacity: 0});
            setButtonAnimation({delay: 0, opacity: 0});
        }
    }, [state]);

    return (
        <animated.div style={overlayAnimation} css={S.container}>
            <animated.div
                style={containerAnimation}
                aria-modal="true"
                css={S.content}
                tabIndex="-1"
                role="alertdialog"
            >
                <animated.div style={contentAnimation}>
                    <UI.Heading3 as="span">{title}</UI.Heading3>
                    <UI.Spacer size="s" />

                    <p>{text}</p>

                    {confirmCTA && (
                        <animated.div style={buttonAnimation}>
                            <CTAButton onClick={confirmCTA.onClick}>{confirmCTA.label}</CTAButton>
                        </animated.div>
                    )}
                    {cancelCTA && (
                        <animated.div style={buttonAnimation}>
                            <UI.Spacer size="s" />
                            <CTAButton
                                onClick={cancelCTA.onClick}
                                basic={true}
                                css={[UI.textColor('purplishBlue'), S.cancelButton]}
                            >
                                {cancelCTA.label}
                            </CTAButton>
                        </animated.div>
                    )}
                </animated.div>
            </animated.div>
        </animated.div>
    );
};

export default AlertDialog;
