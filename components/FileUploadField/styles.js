// @flow
import {css} from '@emotion/core';
import styled from '@emotion/styled';
import {rem, rgba} from 'polished';
import {colors, fontFamilies, spacing, transitions} from '../../globals/variables';

const BADGE_SIZE = 140;

export const documentCTA = props => css`
    align-items: center;
    background-color: transparent;
    border: 1px dashed ${colors.grey};
    border-radius: ${rem(4)};
    color: ${colors.darkBlueGrey};
    display: flex !important;
    flex-direction: row;
    font-size: ${rem(12)};
    height: ${rem(150)};
    justify-content: center;
    line-height: 1.6;
    padding: ${rem(spacing.s)};
    text-transform: uppercase;
    transition: ${transitions.slow};
    width: ${rem(120)} !important;

    &:hover,
    &:focus {
        background-color: ${colors.greyLightest};
        border-color: ${colors.greyDark};
        border-style: solid;
        color: ${colors.darkBlueGrey} !important;
        transform: scale(1.03);
    }

    ${props.hasFile &&
        `
        background-color: ${rgba(colors.darkBlueGrey, 0.05)} !important;
        color: ${colors.white} !important;
    `}
`;

export const FileInfo = styled.span`
    display: block;
    font-family: ${fontFamilies.default};
    font-size: ${rem(10)};
    letter-spacing: ${rem(0.5)};
    margin-top: ${rem(4)};
    opacity: 0.85;
`;
