// @flow
import React, {useCallback, useRef, useState} from 'react';
import {useDropzone} from 'react-dropzone';
import {Field} from 'formik';
import filesize from 'filesize';
import bugsnagClient from '../../globals/bugsnag';
import CTAButton from '../CTAButton';
import * as FormStyles from '../Form/styles';
import * as S from './styles';

type Props = {
    isDocument?: boolean,
    labels?: Object,
    name?: string
};

const FileUploadField = ({
    isDocument = false,
    labels = {},
    name = 'file',
    ...fieldProps
}: Props) => {
    const [fileInfo, setFileInfo] = useState(null);

    const onDrop = useCallback(acceptedFiles => {
        const reader = new FileReader();
        const file = acceptedFiles[0];

        reader.readAsDataURL(file);
        reader.onerror = () => console.log('file reading has failed');
        reader.onload = event => {};

        fieldProps.setFieldValue(name, file);
        setFileInfo(`${file.name} ${isDocument ? '<br />' : ''} (${filesize(file.size)})`);

        if (fieldProps.handleFileSelect) {
            fieldProps.handleFileSelect();
        }
    }, []);

    const {getRootProps, getInputProps} = useDropzone({onDrop});

    const ctaLabels = {
        default: 'Attach a file',
        change: 'Change file',
        ...labels
    };

    return (
        <>
            <Field name={name}>
                {({field, form, ...props}) => (
                    <>
                        <div {...getRootProps()}>
                            <CTAButton
                                key="upload"
                                type="button"
                                disabled={props.isSubmitting || props.isValidating}
                                footnote={!isDocument ? fileInfo : null}
                                theme={field.value ? 'darkGreyBlue' : 'blueGrey'}
                                css={isDocument ? S.documentCTA({hasFile: field.value}) : null}
                                ghost={field.value ? true : false}
                                state={field.value ? 'default' : props.formState}
                                block={true}
                            >
                                {field.value
                                    ? ctaLabels.change
                                    : `${ctaLabels.default} ${
                                          !fieldProps.isRequired ? '(optional)' : ''
                                      }`}
                                {isDocument && fileInfo && (
                                    <S.FileInfo dangerouslySetInnerHTML={{__html: fileInfo}} />
                                )}
                            </CTAButton>
                        </div>
                        <input name={name} {...getInputProps()} />
                        {form.errors[name] && (
                            <span css={FormStyles.error}>{form.errors[name]}</span>
                        )}
                    </>
                )}
            </Field>
        </>
    );
};

export default FileUploadField;
