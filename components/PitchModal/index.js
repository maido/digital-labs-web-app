// @flow
import React from 'react';
import __ from '../../globals/strings';
import ButtonList from '../ButtonList';
import {ContentBlock} from '../ContentBlocks';
import CTAButton from '../CTAButton';
import * as UI from '../UI/styles';

type Props = {
    name: string,
    pitch: Object
};

const PitchModal = ({name, pitch}: Props) => (
    <>
        <UI.Heading3>{name}'s pitch</UI.Heading3>

        {pitch.video_embed_url && (
            <div
                style={{
                    background: '#000',
                    borderRadius: 5,
                    height: 0,
                    marginBottom: 30,
                    maxWidth: '100%',
                    overflow: 'hidden',
                    paddingBottom: '56.25%',
                    position: 'relative',
                    width: '100%'
                }}
            >
                <iframe
                    src={pitch.video_embed_url}
                    style={{
                        border: 0,
                        height: ' 100%',
                        left: 0,
                        position: 'absolute',
                        top: 0,
                        width: '100%'
                    }}
                />
            </div>
        )}

        {pitch.comments && (
            <ContentBlock
                block={{type: 'quote', citation: `Comments from ${name}`, body: pitch.comments}}
            />
        )}

        <ButtonList flex={true}>
            {[
                <CTAButton
                    key="pres"
                    href={pitch.presentation.url}
                    target="_blank"
                    rel="noopener"
                    wide={true}
                >
                    View presentation
                </CTAButton>,
                !pitch.video_embed_url ? (
                    <CTAButton
                        key="video"
                        href={pitch.video_url}
                        target="_blank"
                        rel="noopener"
                        wide={true}
                    >
                        Watch video pitch
                    </CTAButton>
                ) : null
            ]}
        </ButtonList>
    </>
);

export default PitchModal;
