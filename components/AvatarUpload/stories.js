// @flow
import React from 'react';
import {addDecorator, storiesOf} from '@storybook/react';
import AvatarUpload from './';
import Theme from '../Theme';

const stories = storiesOf('AvatarUpload', module);

stories.addDecorator(story => (
    <Theme theme="tertiary">
        <div style={{padding: 30}}>{story()}</div>
    </Theme>
));

stories.add('default', () => {
    return (
        <AvatarUpload apiUrl="" avatar="https://placehold.it/200x200" label="Edit your profile" />
    );
});

stories.add('with custom label', () => {
    return <AvatarUpload apiUrl="" avatar="https://placehold.it/200x200" label="Add an avatar" />;
});
