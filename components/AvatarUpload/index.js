// @flow
import React, {useCallback, useRef, useState} from 'react';
import axios from 'axios';
import Cookie from 'js-cookie';
import {useDropzone} from 'react-dropzone';
import {useSpring, animated} from 'react-spring/web.cjs';
import bugsnagClient from '../../globals/bugsnag';
import Spinner from '../Spinner';
import * as UI from '../UI/styles';
import * as S from './styles';

type Props = {
    apiUrl?: string,
    avatar?: string,
    handleSuccess?: Function,
    label?: string
};

const AvatarUpload = ({apiUrl, avatar, handleSuccess, label = 'Change photo'}: Props) => {
    const [isUploading, setIsUploading] = useState(false);
    const [stateLabel, setStateLabel] = useState(label);
    const [avatarAnimation, setAvatarAnimation] = useSpring(() => ({
        config: {tension: 60, mass: 1, friction: 10},
        from: {transform: `scale(0.5) translateY(20px)`, opacity: 0},
        to: {transform: `scale(1) translateY(0)`, opacity: 1}
    }));
    const labelAnimation = useSpring({
        config: {tension: 60, mass: 1, friction: 10},
        delay: 800,
        from: {transform: `translateY(5px)`, opacity: 0},
        to: {transform: `translateY(0)`, opacity: 1}
    });
    const $avatar = useRef(null);

    const onDrop = useCallback(async acceptedFiles => {
        setStateLabel('Preparing');
        setIsUploading(true);

        const reader = new FileReader();
        const file = acceptedFiles[0];

        reader.readAsDataURL(file);
        reader.onerror = () => console.log('file reading has failed');
        reader.onload = event => {
            if ($avatar.current) {
                $avatar.current.style.backgroundImage = `url('${event.target.result}')`;
            }

            setAvatarAnimation({
                transform: `scale(1.1)`,
                opacity: 0.8
            });
            setTimeout(() => setAvatarAnimation({transform: `scale(1) `, opacity: 1}), 400);
        };

        const formData = new FormData();

        formData.append('file', file);
        formData.append('token', Cookie.get('token'));

        if (apiUrl) {
            formData.append('apiUrl', apiUrl);
        }

        try {
            const response = await axios.post(
                `${process.env.NEXT_STATIC_UPLOADS_API_URL}api/avatar/`,
                formData,
                {
                    onUploadProgress: ({loaded, total}) => {
                        const percentCompleted = Math.round((loaded * 100) / total);

                        if (percentCompleted === 100) {
                            setStateLabel('Processing');
                        } else {
                            setStateLabel(`Uploading (${percentCompleted}%)`);
                        }
                    }
                }
            );

            setStateLabel('Success!');
            setTimeout(() => setStateLabel(label), 1000);

            if (handleSuccess) {
                handleSuccess(response);
            }

            setIsUploading(false);
        } catch (error) {
            if (error.status === 413) {
                setStateLabel('The file is too large, try a smaller file.');
            } else {
                if (bugsnagClient) {
                    bugsnagClient.notify(error);
                }

                setStateLabel('Error uploading!');
            }

            setTimeout(() => setStateLabel(label), 1000);

            if (avatar && $avatar.current) {
                $avatar.current.style.backgroundImage = `url('${avatar}')`;
            }

            setIsUploading(false);
        }
    }, []);

    const {getRootProps, getInputProps, isDragActive} = useDropzone({onDrop});

    return (
        <animated.div style={avatarAnimation} css={S.container}>
            <S.AvatarContainer {...getRootProps()}>
                <S.SpinnerContainer>
                    <UI.Avatar
                        image={avatar}
                        css={S.image({isActive: isUploading || isDragActive})}
                        ref={$avatar}
                    />
                    {isUploading && <Spinner />}
                    <br />
                    <br />
                    <br />
                </S.SpinnerContainer>
                <animated.span style={labelAnimation} css={S.label}>
                    {stateLabel}
                </animated.span>
                <input {...getInputProps()} />
            </S.AvatarContainer>
        </animated.div>
    );
};

export default AvatarUpload;
