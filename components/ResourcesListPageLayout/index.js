// @flow
import React from 'react';
import axios from 'axios';
import {getContentStatus} from '../../globals/functions';
import {logEvent} from '../../globals/analytics';
import __ from '../../globals/strings';
import {resourceCategories} from '../../globals/content';
import AnimateWhenVisible from '../AnimateWhenVisible';
import CardLayout from '../CardLayout';
import CardLayoutPlaceholder from '../Placeholder/CardLayout';
import Container from '../Container';
import CTAButton from '../CTAButton';
import TextCardPlaceholder from '../Placeholder/TextCard';
import TextCard from '../TextCard';
import Theme from '../Theme';
import TouchSwipeContainer from '../TouchSwipeContainer';
import * as UI from '../UI/styles';

type Props = {
    resources: Resources,
    handleTypeChange: Function,
    network: NetworkStatus
};

const ResourcesListPageLayout = ({resources, handleTypeChange, network}: Props) => {
    const filteredResources = resources.allIds
        .map(resourceId => resources.byId[resourceId])
        .filter(resource => {
            if (resources.type === 'All') {
                return true;
            } else {
                return resource.type === resources.type;
            }
        });
    let {showContent, showFallback, showPlaceholders} = getContentStatus(
        network.isLoading,
        resources.hasFetched,
        filteredResources.length > 0,
        true,
        true
    );

    return (
        <>
            <Container>
                <AnimateWhenVisible watch={false}>
                    <UI.Heading2 css={UI.centerText} as="h1">
                        Resources
                    </UI.Heading2>
                </AnimateWhenVisible>
                <TouchSwipeContainer>
                    {resourceCategories.map((type, index) => (
                        <CTAButton
                            key={type}
                            blockAtMobile={false}
                            onClick={() => {
                                handleTypeChange(type);
                                logEvent({
                                    action: 'Click',
                                    category: 'Resource Filter',
                                    label: type
                                });
                            }}
                            thin={true}
                            size="small"
                            theme={type === resources.type ? 'primary' : 'tertiary'}
                            css={index > 0 && UI.margin('left', 'xs')}
                        >
                            {type}
                        </CTAButton>
                    ))}
                </TouchSwipeContainer>
            </Container>

            <Theme theme="tertiary" overlap="secondary" overlapSize={100} grow={true}>
                <Container paddingVertical="l">
                    {showPlaceholders && (
                        <CardLayoutPlaceholder
                            columns={2}
                            size={6}
                            component={<TextCardPlaceholder />}
                        />
                    )}
                    {showContent && (
                        <CardLayout
                            animate={false}
                            items={filteredResources.map(resource => {
                                return {
                                    key: resource.id,
                                    component: (
                                        <TextCard
                                            icon={resource.type.toLowerCase()}
                                            title={resource.title}
                                            text={resource.type}
                                            url={resource.url}
                                            track={{
                                                action: 'Click',
                                                category: `Resource – ${resource.type}`,
                                                label: resource.url
                                            }}
                                        />
                                    )
                                };
                            })}
                        />
                    )}
                    {showFallback && (
                        <AnimateWhenVisible delay={200} watch={false}>
                            <UI.Box
                                css={UI.centerText}
                                style={{
                                    marginLeft: 'auto',
                                    marginRight: 'auto',
                                    padding: 30,
                                    maxWidth: 600
                                }}
                            >
                                <UI.Heading5 as="h2" style={{marginBottom: 10}}>
                                    {__('resources.emptyTitle')}
                                </UI.Heading5>
                                <p>
                                    {__(
                                        resources.type !== 'All'
                                            ? 'resources.emptyWithFilter'
                                            : 'resources.empty'
                                    )}
                                </p>
                            </UI.Box>
                        </AnimateWhenVisible>
                    )}
                </Container>
                <UI.Spacer />
            </Theme>
        </>
    );
};

export default ResourcesListPageLayout;
