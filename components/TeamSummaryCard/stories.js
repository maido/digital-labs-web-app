// @flow
import React from 'react';
import {addDecorator, storiesOf} from '@storybook/react';
import {withKnobs, object, select, text} from '@storybook/addon-knobs';
import TeamSummaryCard from './';
import Theme from '../Theme';

const stories = storiesOf('TeamSummaryCard', module);

stories.addDecorator(withKnobs);

stories.add('default', () => {
    return (
        <TeamSummaryCard
            title="Golden Retriever"
            photo="https://via.placeholder.com/70x70.jpg?text=Golden+Retriever"
            members={[
                {
                    first_name: 'Foo',
                    last_name: 'Bar',
                    avatar: {url: 'https://via.placeholder.com/70x70.jpg?text=Foo+Bar'}
                },
                {
                    first_name: 'Foo',
                    last_name: 'Bar',
                    avatar: {url: 'https://via.placeholder.com/70x70.jpg?text=Bar+Baz'}
                },
                {
                    first_name: 'Foo',
                    last_name: 'Bar',
                    avatar: {url: 'https://via.placeholder.com/70x70.jpg?text=Foo+Baz'}
                }
            ]}
        />
    );
});

stories.add('with a long title', () => {
    return (
        <TeamSummaryCard
            title="Golden Retriever is a good boy"
            photo="https://via.placeholder.com/70x70.jpg?text=Golden+Retriever"
            members={[
                {
                    first_name: 'Foo',
                    last_name: 'Bar',
                    avatar: {url: 'https://via.placeholder.com/70x70.jpg?text=Foo+Bar'}
                },
                {
                    first_name: 'Foo',
                    last_name: 'Bar',
                    avatar: {url: 'https://via.placeholder.com/70x70.jpg?text=Bar+Baz'}
                },
                {
                    first_name: 'Foo',
                    last_name: 'Bar',
                    avatar: {url: 'https://via.placeholder.com/70x70.jpg?text=Foo+Baz'}
                }
            ]}
        />
    );
});

stories.add('with 1 member', () => {
    return (
        <TeamSummaryCard
            title="Golden Retriever"
            photo="https://via.placeholder.com/70x70.jpg?text=Golden+Retriever"
            members={[
                {
                    first_name: 'Foo',
                    last_name: 'Bar',
                    avatar: {url: 'https://via.placeholder.com/70x70.jpg?text=Foo+Bar'}
                }
            ]}
        />
    );
});

stories.add('with max number of members', () => {
    return (
        <TeamSummaryCard
            title="Golden Retriever"
            photo="https://via.placeholder.com/70x70.jpg?text=Golden+Retriever"
            members={[
                {
                    first_name: 'Foo',
                    last_name: 'Bar',
                    avatar: {url: 'https://via.placeholder.com/70x70.jpg?text=Foo+Bar'}
                },
                {
                    first_name: 'Foo',
                    last_name: 'Bar',
                    avatar: {url: 'https://via.placeholder.com/70x70.jpg?text=Bar+Baz'}
                },
                {
                    first_name: 'Foo',
                    last_name: 'Bar',
                    avatar: {url: 'https://via.placeholder.com/70x70.jpg?text=Foo+Baz'}
                },
                {
                    first_name: 'Foo',
                    last_name: 'Bar',
                    avatar: {url: 'https://via.placeholder.com/70x70.jpg?text=Bar+Baz'}
                },
                {
                    first_name: 'Foo',
                    last_name: 'Bar',
                    avatar: {url: 'https://via.placeholder.com/70x70.jpg?text=Foo+Baz'}
                }
            ]}
        />
    );
});

stories.add('with detail view', () => {
    return (
        <TeamSummaryCard
            title="Golden Retriever"
            photo="https://via.placeholder.com/70x70.jpg?text=Golden+Retriever"
            type="detail"
            description="Elit esse culpa sunt dolore reprehenderit nisi Lorem. Do laborum Lorem cillum officia occaecat velit qui ex voluptate. Esse dolor eiusmod id reprehenderit."
            members={[
                {
                    first_name: 'Foo',
                    last_name: 'Bar',
                    avatar: {url: 'https://via.placeholder.com/70x70.jpg?text=Foo+Bar'}
                }
            ]}
        />
    );
});
