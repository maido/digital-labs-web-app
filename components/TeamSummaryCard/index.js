// @flow
import React from 'react';
import Link from 'next/link';
import * as UI from '../UI/styles';
import * as S from './styles';

const TeamSummaryCard = ({
    members = [],
    avatar,
    mission,
    footnote,
    handleClick = () => {},
    title
}: TeamSummaryUICard) => (
    <S.Container onClick={handleClick}>
        <S.Header>
            <UI.Avatar
                image={avatar && avatar.url ? avatar.url : ''}
                css={[UI.fadeIn, S.teamImage]}
            />
            <S.ContentContainer>
                {title && <S.Title css={UI.fadeInUp}>{title}</S.Title>}
                {mission && <S.Text css={UI.fadeInUp}>{mission}</S.Text>}
                {members.length > 0 && (
                    <S.MembersPreviewContainer>
                        {members.map(member => (
                            <UI.Avatar
                                key={`${member.first_name} ${member.last_name}`}
                                image={
                                    member.avatar && member.avatar.thumb_url
                                        ? member.avatar.thumb_url
                                        : ''
                                }
                                title={`${member.first_name} ${member.last_name}`}
                                css={S.memberImage}
                                size="small"
                            />
                        ))}
                        <span>
                            {members.length} member{members.length > 1 ? 's' : ''}
                        </span>
                    </S.MembersPreviewContainer>
                )}
                {footnote && <S.Footnote css={UI.fadeInUp}>{footnote}</S.Footnote>}
            </S.ContentContainer>
        </S.Header>
    </S.Container>
);

export default TeamSummaryCard;
