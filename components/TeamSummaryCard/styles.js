// @flow
import {css} from '@emotion/core';
import styled from '@emotion/styled';
import {rem} from 'polished';
import {responsiveRem} from '../../globals/functions';
import {
    breakpoints,
    colors,
    fontFamilies,
    fontSizes,
    shadows,
    spacing,
    themes,
    transitions
} from '../../globals/variables';

export const Container = styled.div`
    background-color: ${themes.white.background};
    box-shadow: ${shadows.default};
    border-radius: ${rem(5)};
    color: ${themes.white.text};
    cursor: pointer;
    display: block;
    padding: ${rem(spacing.s)};
    width: 100%;
    transition: ${transitions.default};
    -webkit-backface-visibility: hidden;

    &:hover,
    &:focus {
        box-shadow: ${shadows.hover};
        transform: translateY(-2px);
    }

    @media (min-width: ${rem(breakpoints.mobile)}) {
        padding: ${rem(spacing.m)};
    }
`;

export const Header = styled.header`
    align-items: center;
    display: flex;

    a {
        color: ${themes.white.text};
        font-family: ${fontFamilies.bold};
    }
`;

export const teamImage = css`
    margin-right: ${rem(spacing.s)};
`;

export const ContentContainer = styled.div`
    display: flex;
    flex-direction: column;
    text-align: left;
`;

export const Title = styled.span`
    color: ${colors.darkBlueGrey};
    display: block;
    font-size: ${rem(fontSizes.h4)};
    font-family: ${fontFamilies.heading};
    line-height: 1.3;
    margin-bottom: ${rem(spacing.xs)};
    text-align: left;

    ${Container}:hover &,
    ${Container}:focus & {
        color: ${colors.purplishBlue};
    }
`;

export const Text = styled.p`
    line-height: 1.4;
`;

export const Footnote = styled.small`
    color: ${colors.greyDark};
    font-size: ${rem(14)};
    margin-top: ${rem(spacing.xs)};
`;

export const memberImage = css`
    transition: ${transitions.default};

    & + div {
        margin-left: ${rem(10 * -1)};
    }
    &:last-of-type {
        margin-right: ${rem(spacing.xs)};
    }

    @media (min-width: ${rem(breakpoints.mobile)}) {
        & + div {
            margin-left: ${rem(15 * -1)};
        }
    }
`;

export const MembersPreviewContainer = styled.div`
    align-items: center;
    color: ${colors.greyDark};
    display: flex;
    cursor: default;
    font-family: ${fontFamilies.bold};
    margin-top: 0;

    span {
        transition: ${transitions.default};
    }

    &:hover div,
    &:focus div {
        margin-left: 0;
    }
    &:hover span,
    &:focus span {
        opacity: 0;
    }
`;
