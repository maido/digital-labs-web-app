// @flow
import React from 'react';
import get from 'lodash/get';
import __ from '../../globals/strings';
import {getContentStatus, canUserSubmitPitch} from '../../globals/functions';
import AnimateWhenVisible from '../AnimateWhenVisible';
import BannerNotification from '../BannerNotification';
import CardLayout from '../CardLayout';
import CardLayoutPlaceholder from '../Placeholder/CardLayout';
import LearningContentCard from '../LearningContentCard';
import Container from '../Container';
import CTAButton from '../CTAButton';
import HeroContentCard from '../HeroContentCard';
import LearningContentCardPlaceholder from '../Placeholder/LearningContentCard';
import SlackBanner from '../SlackBanner';
import TeamOverviewBanner from '../TeamOverviewBanner';
import Theme from '../Theme';
import * as UI from '../UI/styles';
import * as S from './styles';

type Props = {
    featuredLabs: Array<Lab>,
    network: NetworkStatus,
    nextTopic?: Topic | null,
    nextTopicIsLoading: boolean,
    user: User
};

const getPitchBannerText = daysRemaining => {
    if (daysRemaining <= 1) {
        return 'Your pitch is due today!';
    } else if (daysRemaining <= 7) {
        return 'Your pitch is due this week';
    } else if (daysRemaining <= 14) {
        return 'Your pitch is due soon';
    } else {
        return 'You can now submit your pitch';
    }
};

const DashboardPageLayout = ({
    featuredLabs = [],
    network,
    nextTopic,
    nextTopicIsLoading,
    user
}: Props) => {
    const canSubmitPitch = canUserSubmitPitch(user);
    const {showContent, showFallback, showPlaceholders} = getContentStatus(
        network.isLoading,
        featuredLabs.length,
        featuredLabs.length > 0,
        true,
        true
    );
    let heroContentCard = {
        image: '/topic-placeholder.png',
        isLoading: nextTopicIsLoading
    };

    if (!nextTopicIsLoading && nextTopic) {
        heroContentCard = {
            category: nextTopic.module.title || '',
            image: nextTopic.module.hero || '/topic-placeholder.png',
            intro: nextTopic.intro,
            title: nextTopic.title,
            primaryCTA: {
                label: 'Start Topic',
                url: {
                    href: `/labs/topics/[topicId]`,
                    as: `/labs/topics/${nextTopic.id}`
                }
            },
            secondaryCTA: {
                label: ' View all topics',
                url: {
                    href: `/labs/courses/[courseId]`,
                    as: `/labs/courses/${get(nextTopic, 'course.id')}`
                }
            },
            isLoading: false
        };
    }

    return (
        <>
            <Container css={S.heroContainer}>
                <AnimateWhenVisible delay={-1}>
                    <TeamOverviewBanner team={user.team} pitch={user.pitch} />
                </AnimateWhenVisible>
            </Container>

            {canSubmitPitch && (
                <Theme theme="darkGreyBlue">
                    <Container>
                        <BannerNotification
                            text={getPitchBannerText(get(user, 'pitch.daysRemaining'))}
                            cta={{url: '/team/pitch', label: 'Submit your pitch'}}
                        />
                    </Container>
                </Theme>
            )}

            <Theme
                theme="tertiary"
                overlap={canSubmitPitch ? 'darkGreyBlue' : 'secondary'}
                overlapSize={160}
                grow={true}
            >
                <Container paddingVertical="l" css={S.heroContainer}>
                    <AnimateWhenVisible delay={-1}>
                        <UI.Heading3 as="h2" css={UI.textColor('offWhite')}>
                            {__('dashboard.nextTopic.title')}
                        </UI.Heading3>
                        {nextTopic || nextTopicIsLoading ? (
                            <HeroContentCard {...heroContentCard} />
                        ) : (
                            <UI.Box css={UI.centerText}>
                                <Container paddingVertical="l">
                                    <UI.Heading3>
                                        {__('dashboard.nextTopic.empty.title')}
                                    </UI.Heading3>
                                    <p>{__('dashboard.nextTopic.empty.text')}</p>
                                </Container>
                            </UI.Box>
                        )}
                    </AnimateWhenVisible>

                    <UI.ResponsiveSpacer size="l" />

                    <S.HeaderWithLink>
                        <UI.Heading3>{__('dashboard.labs.title')}</UI.Heading3>
                        <CTAButton href="/labs" basic={true} css={UI.textColor('purplishBlue')}>
                            {__('dashboard.labs.ctaLabel')}
                            <svg
                                width="19"
                                height="8"
                                xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 19 8"
                                className="arrow-icon"
                            >
                                <path d="M19 4l-4-4v3H0v2h15v3z" fillRule="evenodd" />
                            </svg>
                        </CTAButton>
                    </S.HeaderWithLink>

                    {showPlaceholders && (
                        <CardLayoutPlaceholder
                            size={4}
                            component={
                                <LearningContentCardPlaceholder
                                    hasHeadline={false}
                                    hasImage={false}
                                />
                            }
                        />
                    )}
                    {showContent && (
                        <CardLayout
                            animate={false}
                            columns={2}
                            key={`featured-labs-${featuredLabs.length}`}
                            items={featuredLabs.map(lab => ({
                                key: lab.id,
                                component: (
                                    <LearningContentCard
                                        title={lab.title}
                                        headline={lab.headline}
                                        items={{
                                            all: lab.modules_count,
                                            topics_count: lab.topics_count,
                                            completed_topics_count: lab.completed_topics_count,
                                            type: 'Module'
                                        }}
                                        url={{
                                            href: `/labs/[labId]`,
                                            as: `/labs/${lab.id}`
                                        }}
                                    />
                                )
                            }))}
                        />
                    )}
                </Container>
            </Theme>
            <SlackBanner trackingLabel="Dashboard" />
        </>
    );
};

export default DashboardPageLayout;
