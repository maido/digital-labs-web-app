// @flow
import {css} from '@emotion/core';
import styled from '@emotion/styled';
import {rem} from 'polished';
import {breakpoints, spacing} from '../../globals/variables';

export const threeImagesHeroCard = css`
    bottom: ${rem(spacing.m * -1)};
    left: 60%;
    position: absolute;
    z-index: 2;
    width: ${rem(200)};
`;

export const heroContainer = css`
    @media (max-width: ${rem(breakpoints.tablet)}) {
        padding-top: 0 !important;
    }

    @media (max-width: ${rem(breakpoints.mobile)}) {
        margin-top: ${rem(spacing.m * -1)} !important;
        margin-bottom: ${rem(spacing.s)} !important;
    }
`;

export const HeaderWithLink = styled.header`
    a {
        display: inline-block !important;
        width: auto !important;
    }

    @media (max-width: ${rem(breakpoints.mobile)}) {
        a {
            margin: ${rem(spacing.s * -1)} 0 ${rem(spacing.m)};
        }
    }

    @media (min-width: ${rem(breakpoints.mobile)}) {
        align-items: flex-start;
        display: flex;

        h3 {
            flex-grow: 1;
        }
    }
`;
