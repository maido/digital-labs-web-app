// @flow
import {keyframes} from '@emotion/core';
import {rem} from 'polished';
import styled from '@emotion/styled';

const SIZE = 24;

const spin = keyframes`
    0% {
        transform: rotate(0deg);
    }
    100% {
        transform: rotate(360deg);
    }
  `;

export const Container = styled.div`
    display: inline-block;
    height: ${rem(SIZE)};
    position: relative;
    width: ${rem(SIZE)};

    div {
        animation: ${spin} 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
        border: 3px solid currentColor;
        border-color: currentColor transparent transparent transparent;
        border-radius: 50%;
        box-sizing: border-box;
        display: block;
        height: ${rem(SIZE)};
        position: absolute;
        width: ${rem(SIZE)};
    }
    div:nth-of-type(1) {
        animation-delay: -0.45s;
    }
    div:nth-of-type(2) {
        animation-delay: -0.3s;
    }
    div:nth-of-type(3) {
        animation-delay: -0.15s;
    }
`;
