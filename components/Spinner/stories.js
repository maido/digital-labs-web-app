// @flow
import React from 'react';
import {addDecorator, storiesOf} from '@storybook/react';
import {withKnobs, select} from '@storybook/addon-knobs';
import Spinner from './';
import Theme from '../Theme';

const themeOptions = {
    Primary: 'primary',
    Secondary: 'secondary',
    Tertiary: 'tertiary',
    White: 'white'
};
const stories = storiesOf('Spinner', module);

stories.addDecorator(withKnobs);

stories.addDecorator(story => (
    <Theme theme={select('Theme', themeOptions, 'secondary')}>{story()}</Theme>
));

stories.add('default', () => {
    return <Spinner />;
});
