// @flow
import styled from '@emotion/styled';
import {rem} from 'polished';
import {breakpoints, spacing} from '../../globals/variables';

export const Container = styled.div`
    ${props => props.center && 'text-align: center;'}

    div {
        display: inline-block;
    }
    div + div {
        margin-left: ${rem(spacing.s)};
    }

    @media (max-width: ${rem(breakpoints.mobileSmall)}) {
        div + div {
            margin-top: ${rem(spacing.s)};
        }

        ${props =>
            props.blockAtMobile &&
            `
                &,
                & * {
                    display: block;
                    margin-left: 0 !important;
                    width: 100%;
                }
        `}
    }

    @media (min-width: ${rem(breakpoints.mobileSmall)}) {
        ${props =>
            props.flex &&
            `
                display:flex;
                * {flex-grow:1;width:100%;}
        `}
    }
`;
