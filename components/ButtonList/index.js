// @flow
import * as React from 'react';
import {useTrail, animated} from 'react-spring/web.cjs';
import * as S from './styles';

type Props = {
    blockAtMobile?: boolean,
    center?: boolean,
    children: Array<React.Node>,
    delay?: number,
    flex?: boolean
};

const ButtonList = ({
    blockAtMobile = false,
    center = false,
    children = [],
    delay = 0,
    flex = false
}: Props) => {
    const trail = useTrail(children.length, {
        config: {tension: 40, mass: 1, friction: 10},
        delay,
        from: {transform: `translateY(20px)`, opacity: 0},
        to: {transform: `translateY(0)`, opacity: 1}
    });

    return (
        <S.Container center={center} blockAtMobile={blockAtMobile} flex={flex}>
            {trail.map((props, index) => (
                <animated.div key={index} style={props}>
                    {children[index]}
                </animated.div>
            ))}
        </S.Container>
    );
};

export default ButtonList;
