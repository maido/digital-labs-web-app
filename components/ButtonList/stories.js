// @flow
import React from 'react';
import {storiesOf} from '@storybook/react';
import CTAButton from '../CTAButton';
import ButtonList from './';

const stories = storiesOf('ButtonList', module);

stories.add('default', () => {
    return <ButtonList>{[<CTAButton>Foo</CTAButton>]}</ButtonList>;
});

stories.add('with two buttons', () => {
    return (
        <ButtonList>
            {[<CTAButton>Foo</CTAButton>, <CTAButton ghost={true}>Bar</CTAButton>]}
        </ButtonList>
    );
});

stories.add('with three buttons', () => {
    return (
        <ButtonList>
            {[
                <CTAButton>Foo</CTAButton>,
                <CTAButton>Bar</CTAButton>,
                <CTAButton ghost={true}>Bazs</CTAButton>
            ]}
        </ButtonList>
    );
});

stories.add('with centered buttons', () => {
    return (
        <ButtonList center={true}>
            {[
                <CTAButton>Foo</CTAButton>,
                <CTAButton>Bar</CTAButton>,
                <CTAButton ghost={true}>Bazs</CTAButton>
            ]}
        </ButtonList>
    );
});
