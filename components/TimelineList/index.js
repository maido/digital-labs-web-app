// @flow
import * as React from 'react';
import Link from 'next/link';
import dayjs from 'dayjs';
import map from 'lodash/map';
import AnimateWhenVisible from '../AnimateWhenVisible';
import TextCard from '../TextCard';
import * as UI from '../UI/styles';
import * as S from './styles';

type Props = {
    children: React.Node,
    items: Object,
    totalItems?: number,
    type: string
};

const TimelineList = ({children, items = {}, totalItems = 0, type = 'submission'}: Props) => (
    <>
        {Object.keys(items).length > 0 ? (
            <>
                <S.Heading>
                    <S.NotificationsNumber>{totalItems} </S.NotificationsNumber>
                    {type}
                    {totalItems === 1 ? '' : 's'} so far{totalItems === 0 ? '...' : ''}
                </S.Heading>
                <UI.Spacer />
                <S.Notifications>
                    {map(items, (itemGroup, date) => (
                        <S.DateContainer key={date}>
                            <S.Date>{date}</S.Date>

                            {itemGroup.map((item, index) => (
                                <div key={item.id}>
                                    <AnimateWhenVisible delay={index * 100}>
                                        <TextCard
                                            size="small"
                                            title={item.title}
                                            footnote={item.footnote}
                                            text={item.text ? item.text : null}
                                            url={item.url}
                                            icon={item.url ? 'download' : ''}
                                            urlLabel=""
                                        />
                                    </AnimateWhenVisible>
                                    {index < itemGroup.length - 1 && <UI.Spacer size="s" />}
                                </div>
                            ))}
                        </S.DateContainer>
                    ))}
                </S.Notifications>
            </>
        ) : (
            children
        )}
    </>
);

export default TimelineList;
