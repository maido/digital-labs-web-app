// @flow
import React from 'react';
import {addDecorator, storiesOf} from '@storybook/react';
import {withKnobs, object, select, text} from '@storybook/addon-knobs';
import TimelineList from '.';
import Theme from '../Theme';

const stories = storiesOf('TimelineList', module);

stories.addDecorator(withKnobs);

stories.add('default', () => {
    return <TimelineList />;
});

stories.add('with a different content type', () => {
    return <TimelineList type="submission" />;
});
