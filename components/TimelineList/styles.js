// @flow

import styled from '@emotion/styled';
import {rem} from 'polished';
import {responsiveRem} from '../../globals/functions';
import {
    breakpoints,
    colors,
    fontFamilies,
    fontSizes,
    shadows,
    spacing,
    themes
} from '../../globals/variables';

export const Heading = styled.h2`
    color: ${themes.tertiary.heading};
    font-size: ${rem(fontSizes.h5)};
    margin-top: ${rem(spacing.s * -1)};
    margin-bottom: 0;
`;

export const NotificationsNumber = styled.span`
    color: ${colors.secondary};
    font-size: ${rem(fontSizes.h5)};
    font-family: ${fontFamilies.heading};
`;

export const DateContainer = styled.div`
    & + & {
        margin-top: ${responsiveRem(spacing.m)};
    }
`;

export const Date = styled.div`
    background-color: ${colors.greyMid};
    border-radius: ${rem(2)};
    color: ${colors.white};
    display: inline-block;
    font-family: ${fontFamilies.bold};
    margin-bottom: ${rem(spacing.s)};
    padding: ${rem(2)} ${rem(spacing.s)};
`;

export const Notifications = styled.div`
    position: relative;

    &::before {
        background-color: ${colors.paleBlue};
        height: 100%;
        content: '';
        left: ${rem(spacing.l)};
        position: absolute;
        top: 0;
        width: 2px;
        z-index: 1;
    }

    /**
     * This divs are wrapped animations
     */
    > div {
        position: relative;
        z-index: 2;
    }

    > div + div {
        margin-top: ${rem(spacing.s)};
    }

    > div + ${Date} {
        margin-top: ${responsiveRem(spacing.m)};
    }
`;

export const Notification = styled.div`
    background-color: ${themes.white.background};
    box-shadow: ${shadows.default};
    border-radius: ${rem(5)};
    color: ${themes.white.text};
    padding: ${responsiveRem(spacing.s)};

    a {
        color: ${colors.purplishBlue};
    }
`;
