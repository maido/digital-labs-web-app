// @flow
import styled from '@emotion/styled';
import {rem} from 'polished';
import {breakpoints, colors, fontFamilies, fontSizes, spacing} from '../../globals/variables';

export const Container = styled.div`
    padding-bottom: ${rem(spacing.m)};
    padding-top: ${rem(spacing.m)};
    text-align: center;
`;

export const Title = styled.span`
    display: block;
    font-family: ${fontFamilies.heading};
    font-style: normal;
    font-size: ${rem(28)};
    line-height: 1.2;
    margin-bottom: ${rem(spacing.xs)};
`;
