// @flow
import React from 'react';
import {addDecorator, storiesOf} from '@storybook/react';
import {withKnobs, object, select, text} from '@storybook/addon-knobs';
import Container from '../Container';
import CTATextBanner from './';
import Theme from '../Theme';

const themeOptions = {
    Primary: 'primary',
    Secondary: 'secondary',
    Tertiary: 'tertiary',
    White: 'white'
};
const stories = storiesOf('CTATextBanner', module);

stories.addDecorator(withKnobs);

stories.addDecorator(story => (
    <Theme theme={select('Theme', themeOptions, 'secondary')}>
        <Container>{story()}</Container>
    </Theme>
));

stories.add('default', () => {
    return (
        <CTATextBanner
            title="Are you sure?"
            text="To submit a pitch you need a team of at least two people"
            cta={{label: "Yes, I'm sure", url: ''}}
        />
    );
});

stories.add('playground', () => {
    return (
        <CTATextBanner
            title={text('Title', 'Are you sure?')}
            text={text('Text', 'To submit a pitch you need a team of at least two people')}
            cta={object('Confirm CTA', {label: "Yes, I'm sure", url: ''})}
        />
    );
});
