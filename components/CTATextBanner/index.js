// @flow
import * as React from 'react';
import CTAButton from '../CTAButton';
import * as S from './styles';

type Props = {
    children?: React.Node,
    cta?: {
        label: string,
        url: string
    },
    text?: string,
    theme?: string,
    title?: string
};

const CTATextBanner = ({children, cta, text, theme, title}: Props) => (
    <S.Container>
        {title && <S.Title>{title}</S.Title>}
        {text && <p>{text}</p>}
        {cta && (
            <CTAButton
                href={cta.url}
                theme={theme}
                wide={true}
                track={cta.tracking ? cta.tracking : null}
            >
                {cta.label}
            </CTAButton>
        )}
        {children ? children : null}
    </S.Container>
);

export default CTATextBanner;
