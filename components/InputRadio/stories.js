// @flow
import React from 'react';
import {storiesOf} from '@storybook/react';
import {Formik, Form} from 'formik';
import accountCreationSchema from '../../schemas/create-account';
import InputButtonGroup from '../InputButtonGroup';
import InputRadio from './';
import {occupations} from '../../globals/content';

const stories = storiesOf('InputRadio', module);

stories.add('default', () => {
    return (
        <Formik
            initialValues={{}}
            onSubmit={() => {}}
            render={() => (
                <Form>
                    {' '}
                    <InputButtonGroup label="Your occupation" name="occupation">
                        {occupations.map((option, index) => (
                            <InputRadio
                                key={option}
                                name="occupation"
                                value={option}
                                tabIndex={index + 1}
                            />
                        ))}
                    </InputButtonGroup>
                </Form>
            )}
        />
    );
});

stories.add('with flexible full-widths', () => {
    return (
        <Formik
            initialValues={{}}
            onSubmit={() => {}}
            render={() => (
                <Form>
                    <InputButtonGroup label="Your occupation" name="occupation" grow={true}>
                        {occupations.map((option, index) => (
                            <InputRadio
                                key={option}
                                name="occupation"
                                value={option}
                                tabIndex={index + 1}
                            />
                        ))}
                    </InputButtonGroup>
                </Form>
            )}
        />
    );
});

stories.add('with initial value', () => {
    return (
        <Formik
            initialValues={{occupation: occupations[0]}}
            onSubmit={() => {}}
            render={() => (
                <Form>
                    <InputButtonGroup label="Your occupation" name="occupation">
                        {occupations.map((option, index) => (
                            <InputRadio
                                key={option}
                                name="occupation"
                                value={option}
                                tabIndex={index + 1}
                            />
                        ))}
                    </InputButtonGroup>
                </Form>
            )}
        />
    );
});

stories.add('has error', () => {
    return (
        <Formik
            initialValues={{occupation: 'foo'}}
            isInitialValid={false}
            validationSchema={accountCreationSchema.step2}
            onSubmit={() => {}}
            render={props => (
                <Form>
                    <InputButtonGroup label="Your occupation" name="occupation">
                        {[...occupations, 'foo'].map((option, index) => (
                            <InputRadio
                                key={option}
                                name="occupation"
                                value={option}
                                tabIndex={index + 1}
                            />
                        ))}
                    </InputButtonGroup>
                </Form>
            )}
        />
    );
});

stories.add('has field disabled', () => {
    return (
        <Formik
            initialValues={{}}
            onSubmit={() => {}}
            render={() => (
                <Form>
                    <InputButtonGroup label="Your occupation" name="occupation">
                        {occupations.map((option, index) => (
                            <InputRadio
                                key={option}
                                name="occupation"
                                value={option}
                                tabIndex={index + 1}
                            />
                        ))}
                    </InputButtonGroup>
                </Form>
            )}
        />
    );
});
