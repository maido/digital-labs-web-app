// @flow
import React from 'react';
import {Field} from 'formik';
import * as S from './styles';

type Props = {
    isWide?: boolean,
    name: string,
    value: string
};

const InputRadio = ({isWide = false, name, value, ...props}: Props) => (
    <Field name={name}>
        {({field}) => (
            <S.Container>
                <S.Input
                    id={`${name}-${value}`}
                    value={value}
                    name={field.name}
                    type="radio"
                    checked={field.value === value}
                    onChange={field.onChange}
                    onBlur={field.onBlur}
                    aria-labelledby={`${name}-label`}
                    {...props}
                />
                <S.Label htmlFor={`${name}-${value}`} isWide={isWide}>
                    {value}
                </S.Label>
            </S.Container>
        )}
    </Field>
);

export default InputRadio;
