// @flow
import styled from '@emotion/styled';
import {rem} from 'polished';
import {button} from '../CTAButton/styles';
import {responsiveRem} from '../../globals/functions';
import {spacing, themes} from '../../globals/variables';

export const Container = styled.div`
    display: flex;
    position: relative;
`;

export const Label = styled.label`
    ${button}
    background-color: ${themes.inactiveButton.background};
    color: ${themes.inactiveButton.text};
    padding: ${rem(spacing.s)};

    ${props =>
        props.isWide === true &&
        `
            padding-left: ${rem(spacing.m)};
            padding-right: ${rem(spacing.m)};
        `}
`;

export const Input = styled.input`
    -webkit-appearance: checkbox;
    left: 1px;
    margin: 0;
    outline: none;
    opacity: 0;
    position: absolute;
    top: 1px;
    z-index: 0;

    &:focus + ${Label} {
        outline: initial;
    }
    &:checked + ${Label} {
        background-color: ${themes.activeButton.background};
        color: ${themes.activeButton.text};

        ${props => props.type === 'radio' && `pointer-events: none;`}
    }
`;
