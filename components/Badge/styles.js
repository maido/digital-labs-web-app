// @flow
import {css, keyframes} from '@emotion/core';
import styled from '@emotion/styled';
import {rem} from 'polished';
import {colors, spacing} from '../../globals/variables';

const BADGE_SIZE = 140;

export const Container = styled.div`
    margin-left: auto;
    margin-right: auto;
    min-height: ${rem(BADGE_SIZE)};
    overflow: visible;
    perspective: 1000px;
    position: relative;
    width: ${rem(210)};
`;

export const confetti = css`
    &,
    & svg {
        height: auto;
        overflow: visible;
        width: 100%;
    }

    path {
        transform-origin: 50% 50%;
    }
`;

export const circleContainer = css`
    left: 50%;
    position: absolute;
    top: 50%;
`;

const shineAnimation = keyframes`
    from {
        opacity: 0;
        right: -100%;
    }
    to {
        opacity: 1;
        right: 100%;
    }
`;

export const Circle = styled.div`
    background-color: ${colors.white};
    background-image: url('/badges/${props => props.badge}.png');
    background-size:cover;
    background-repeat:no-repeat;
    border-radius: 100%;
    height: ${rem(BADGE_SIZE)};
    overflow: hidden;
    position:relative;
    width: ${rem(BADGE_SIZE)};
    will-change:transform;
    -webkit-backface-visibility: hidden;

    &::after {
        animation: ${shineAnimation} 1s;
        animation-delay: 1.5s;
        background: rgba(255, 255, 255, 0.3);
        background: linear-gradient(
            to right,
            rgba(255, 255, 255, 0.2) 0%,
            rgba(255, 255, 255, 0.2) 77%,
            rgba(255, 255, 255, 0.6) 92%,
            rgba(255, 255, 255, 0) 100%
        );
        content: '';
        height: 200%;
        opacity: 0;
        position: absolute;
        right: -200%;
        top: -30%;
        transform: rotate(30deg);
        width: 50%;
        z-index: 2;
    }
`;
