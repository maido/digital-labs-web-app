// @flow
import * as React from 'react';
import {useSpring, useTransition, animated} from 'react-spring/web.cjs';
import CTAButton from '../CTAButton';
import * as UI from '../UI/styles';
import * as S from './styles';

type Props = {
    badge: string
};

const FullScreenMessage = ({badge = 'generic-1'}: Props) => {
    const confettiZigZagAnimation = useSpring({
        config: {tension: 100, mass: 3, friction: 30},
        delay: 200,
        from: {transform: `scale(0)`, opacity: 0},
        to: {transform: `scale(1)`, opacity: 1}
    });
    const confettiDotAnimations = useTransition([1, 2, 3, 4, 5, 6, 7], items => items, {
        config: {mass: 1, tension: 150, friction: 10},
        delay: 200,
        from: {transform: `scale(0)`, opacity: 0},
        leave: {transform: `scale(0)`, opacity: 0},
        enter: {transform: `scale(1)`, opacity: 1}
    });
    const confettiLineAnimation = useSpring({
        config: {tension: 100, mass: 3, friction: 30},
        delay: 200,
        from: {transform: `rotate(80deg)`, opacity: 0},
        to: {transform: `rotate(0deg)`, opacity: 1}
    });
    const circleAnimation = useSpring({
        config: {tension: 200, mass: 5, friction: 30},
        from: {transform: `translateX(-50%) translateY(-50%) scale(0)`, opacity: 0},
        to: {transform: `translateX(-50%) translateY(-50%) scale(1)`, opacity: 1}
    });

    return (
        <S.Container>
            <animated.div css={S.confetti}>
                <svg
                    width="181"
                    height="148"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 181 148"
                >
                    <g fill="none" fillRule="evenodd">
                        <animated.path
                            style={confettiZigZagAnimation}
                            d="M64.81032 116.92737l-9.47169 1.61234c-1.01522.17207-1.876.8612-2.25732 1.80717l-3.56451 8.82417c-.59091 1.46175.12242 3.08136 1.59326 3.6175 1.47084.53613 3.14222-.21422 3.73312-1.67597l2.94702-7.29742 7.83294-1.3342c1.53917-.29728 2.59217-1.74098 2.37104-3.2508-.22113-1.5098-1.63542-2.53287-3.18455-2.30364l.0007.00085z"
                            fill="#EFB941"
                        />

                        <animated.path
                            style={confettiZigZagAnimation}
                            d="M56.02992 33.80066l-17.13755-9.99813c-1.83681-1.07061-4.09917-1.07-5.93542.0016-1.83625 1.0716-2.96764 3.0515-2.96826 5.1944v9.60897l-8.23386-4.80684c-2.82767-1.53755-6.35255-.52565-7.95813 2.28458-1.6056 2.81023-.71053 6.40124 2.02079 8.10745l17.13754 10.0005c1.83598 1.07575 4.10136 1.07575 5.93734 0 1.83726-1.07143 2.96902-3.05233 2.96868-5.19602V39.3882l8.23853 4.80449c1.83688 1.07058 4.09938 1.0695 5.93525-.00287C57.8707 43.11746 59.001 41.13675 59 38.99381c-.00101-2.14294-1.1332-4.12256-2.97008-5.19315z"
                            fill="#E4684F"
                        />
                        <animated.path
                            style={confettiLineAnimation}
                            d="M117.166 8.49502l6.99094 4.03524a2.41845 2.41845 0 0 0 1.83827.24278 2.41928 2.41928 0 0 0 1.47012-1.1304 2.42263 2.42263 0 0 0 .24147-1.83815 2.4218 2.4218 0 0 0-1.12879-1.47042l-6.99284-4.03619c-1.15476-.6322-2.60286-.22602-3.2607.9146-.65784 1.14061-.28453 2.598.84058 3.2816l.00095.00094z"
                            fill="#37B687"
                            fillRule="nonzero"
                        />
                        <animated.path
                            style={confettiZigZagAnimation}
                            d="M166.4454 70.21105l-11.17665-6.24884c-1.19792-.66913-2.67338-.66875-3.87093.001-1.19755.66975-1.93542 1.9072-1.93582 3.2465v6.00561l-5.36991-3.00427c-1.84414-.96098-4.14297-.32854-5.19009 1.42786-1.04712 1.7564-.46339 4.00077 1.3179 5.06715l11.17667 6.25031c1.19738.67235 2.6748.67235 3.87218 0 1.1982-.66964 1.93631-1.9077 1.93609-3.2475v-6.00561l5.37295 3.0028c1.19797.66912 2.67352.66844 3.87082-.00179 1.1973-.67022 1.93447-1.90817 1.9338-3.2475-.00065-1.33935-.73904-2.5766-1.937-3.24572z"
                            fill="#39BA8A"
                            fillRule="nonzero"
                        />
                        <animated.path
                            style={confettiLineAnimation}
                            d="M171.57137 100.28975c-1.1535-.62085-2.5914-.21226-3.24637.92249-.65497 1.13474-.28985 2.58475.82434 3.2737l7.55932 4.34476c1.15834.66932 2.63977.27256 3.30886-.8862.66909-1.15874.27246-2.64068-.88589-3.31l-7.56026-4.34475z"
                            fill="#E4684F"
                            fillRule="nonzero"
                        />
                        <animated.path
                            style={confettiZigZagAnimation}
                            d="M118.2111 27.52104l7.53361 4.33524c.7493.43185 1.67179.43185 2.42108 0l7.5098-4.33524c1.15836-.66879 1.5554-2.1503.88685-3.30904-.66856-1.15875-2.14957-1.55594-3.30792-.88715l-6.2988 3.6362-6.32354-3.6362c-1.15809-.66879-2.63888-.27181-3.30744.88667-.66856 1.15848-.27172 2.63978.88636 3.30857v.00095z"
                            fill="#F0BA41"
                            fillRule="nonzero"
                        />
                        <animated.path
                            style={confettiZigZagAnimation}
                            d="M126.5185 138.3303l-14.89037-8.63531c-1.59517-.92665-3.5616-.92665-5.15678 0l-14.89037 8.6353c-2.46723 1.42996-3.31338 4.5984-1.88994 7.07692 1.42345 2.47852 4.57746 3.32854 7.0447 1.89858l12.31299-7.1421 12.31502 7.14007c2.45958 1.35225 5.54397.48345 6.94514-1.95629 1.40117-2.43973.60604-5.55702-1.79039-7.01922v.00204z"
                            fill="#8FCEE2"
                            fillRule="nonzero"
                        />

                        {confettiDotAnimations.map(({item, props, key}, index) => {
                            switch (index) {
                                case 0:
                                    return (
                                        <animated.path
                                            key={index}
                                            style={props}
                                            d="M24.5 78c3.58985 0 6.5-2.91015 6.5-6.5S28.08985 65 24.5 65 18 67.91015 18 71.5s2.91015 6.5 6.5 6.5"
                                            fill="#92D2E6"
                                            fillRule="nonzero"
                                        />
                                    );
                                case 1:
                                    return (
                                        <animated.path
                                            key={index}
                                            style={props}
                                            d="M64 8c2.20914 0 4-1.79086 4-4s-1.79086-4-4-4-4 1.79086-4 4 1.79086 4 4 4"
                                            fill="#38B787"
                                            fillRule="nonzero"
                                        />
                                    );
                                case 2:
                                    <animated.path
                                        key={index}
                                        style={props}
                                        d="M2.38014 10.7619c1.31451 0 2.38013-1.06598 2.38013-2.38095C4.76027 7.066 3.69465 6 2.38014 6 1.06562 6 0 7.06599 0 8.38095c0 1.31497 1.06562 2.38095 2.38014 2.38095z"
                                        fill="#8CCADE"
                                        fillRule="nonzero"
                                    />;
                                case 3:
                                    <animated.path
                                        key={index}
                                        style={props}
                                        d="M2.38014 10.7619c1.31451 0 2.38013-1.06598 2.38013-2.38095C4.76027 7.066 3.69465 6 2.38014 6 1.06562 6 0 7.06599 0 8.38095c0 1.31497 1.06562 2.38095 2.38014 2.38095z"
                                        fill="#8CCADE"
                                        fillRule="nonzero"
                                    />;
                                case 4:
                                    return (
                                        <animated.path
                                            key={index}
                                            style={props}
                                            d="M160 35c-2.20914 0-4 1.79086-4 4s1.79086 4 4 4 4-1.79086 4-4-1.79086-4-4-4"
                                            fill="#E1664E"
                                        />
                                    );
                                case 5:
                                    return (
                                        <animated.path
                                            key={index}
                                            style={props}
                                            d="M10 104c-2.20914 0-4 1.79086-4 4s1.79086 4 4 4 4-1.79086 4-4-1.79086-4-4-4"
                                            fill="#37B687"
                                        />
                                    );
                                case 6:
                                    return (
                                        <animated.path
                                            key={index}
                                            style={props}
                                            d="M148 117c-2.20914 0-4 1.79086-4 4s1.79086 4 4 4 4-1.79086 4-4-1.79086-4-4-4"
                                            fill="#F1BB42"
                                        />
                                    );
                            }
                        })}
                    </g>
                </svg>
            </animated.div>
            <animated.div style={circleAnimation} css={S.circleContainer}>
                <S.Circle badge={badge} />
            </animated.div>
        </S.Container>
    );
};

export default FullScreenMessage;
