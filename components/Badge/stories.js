// @flow
import React from 'react';
import {addDecorator, storiesOf} from '@storybook/react';
import Badge from '.';
import Theme from '../Theme';

const stories = storiesOf('Badge', module);

stories.addDecorator(story => (
    <Theme theme="secondary">
        <div style={{padding: 40}}>{story()}</div>
    </Theme>
));

stories.add('generic 1', () => {
    return <Badge badge="generic-1" />;
});

stories.add('generic 2', () => {
    return <Badge badge="generic-2" />;
});

stories.add('generic 3', () => {
    return <Badge badge="generic-3" />;
});

stories.add('generic 4', () => {
    return <Badge badge="generic-4" />;
});
