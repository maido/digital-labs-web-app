// @flow
import React from 'react';
import map from 'lodash/map';
import startCase from 'lodash/startCase';
import filesize from 'filesize';
import pitchSchema from '../../schemas/pitch';
import __ from '../../globals/strings';
import ButtonList from '../ButtonList';
import CTAButton from '../CTAButton';
import * as FormStyles from '../Form/styles';
import * as UI from '../UI/styles';

type Props = {
    fields: Object,
    documents: Object,
    handleReviewClick: Function,
    handleSubmitClick: Function
};

const DOCUMENT_KEYS = [
    'presentation',
    'document_1',
    'document_2',
    'document_3',
    'document_4',
    'document_5',
    'video_url'
];

const isValidFormField = (key: string) => {
    const validKeys = Object.keys({...pitchSchema.files.fields, ...pitchSchema.details.fields});

    return validKeys.includes(key);
};

const PitchReview = ({fields, documents, handleReviewClick, handleSubmitClick}: Props) => (
    <>
        <UI.Heading3 as="h1" css={UI.margin('bottom', 'm')}>
            {__('pitch.review.title')}
        </UI.Heading3>
        <p css={UI.leadText} dangerouslySetInnerHTML={{__html: __('pitch.review.text')}} />

        <UI.Spacer size="s" />
        <UI.Divider size="xs" />

        {map(fields, (value, key) => {
            if (!value || !isValidFormField(key)) {
                return null;
            }

            const isDocument = DOCUMENT_KEYS.includes(key);
            const stringsKey = isDocument ? 'documents' : 'details';

            return (
                <div key={key}>
                    <strong css={FormStyles.label}>
                        {isDocument ? startCase(key) : __(`pitch.${stringsKey}.${key}.label`)}
                    </strong>
                    <div css={[UI.textColor('greyDarkest'), UI.padding('s')]}>
                        {isDocument && value.type ? (
                            <span>
                                {value.name} &nbsp; ({filesize(value.size)})
                            </span>
                        ) : (
                            <span>{value}</span>
                        )}
                    </div>
                    <UI.Divider size="xs" />
                </div>
            );
        })}

        <UI.ResponsiveSpacer />

        <ButtonList blockAtMobile={true} center={true} flex={true}>
            {[
                <CTAButton key="primary" theme="tertiary" onClick={handleReviewClick}>
                    {__('pitch.review.reviewLabel')}
                </CTAButton>,
                <CTAButton key="secondary" wide={true} onClick={handleSubmitClick}>
                    {__('pitch.review.confirmLabel')}
                </CTAButton>
            ]}
        </ButtonList>
    </>
);

export default PitchReview;
