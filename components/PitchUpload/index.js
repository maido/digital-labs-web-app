// @flow
import React, {useState} from 'react';
import dynamic from 'next/dynamic';
import Link from 'next/link';
import {connect} from 'react-redux';
import axios from 'axios';
import Cookie from 'js-cookie';
import {Formik, Field, Form} from 'formik';
import pitchSchema from '../../schemas/pitch';
import {updateForm} from '../../store/actions';
import bugsnagClient from '../../globals/bugsnag';
import __ from '../../globals/strings';
import CTAButton from '../CTAButton';
import FileUploadField from '../FileUploadField';
import InputCheckbox from '../InputCheckbox';
import InputHidden from '../InputHidden';
import InputText from '../InputText';
import PitchReview from '../PitchReview';
import * as FormStyles from '../Form/styles';
import * as UI from '../UI/styles';

const Modal = dynamic(() => import('../Modal'), {ssr: false});

type Props = {
    dispatch: Function,
    errors?: string,
    form: Object,
    handleSuccess: Function,
    pitch?: Object,
    pitchId: number
};

const getInitialValues = pitch => {
    const defaultValues = {
        presentation: pitch.presentation ? 'uploaded' : '',
        document_1: pitch.documents.length > 0 ? 'uploaded' : null,
        document_2: pitch.documents.length > 1 ? 'uploaded' : null,
        document_3: pitch.documents.length > 2 ? 'uploaded' : null,
        document_4: pitch.documents.length > 3 ? 'uploaded' : null,
        document_5: pitch.documents.length > 4 ? 'uploaded' : null,
        video_url: '',
        terms: false
    };
    const cache = localStorage.getItem('pitch.documents');
    const cachedValues = cache ? JSON.parse(cache) : {};

    return {
        ...defaultValues,
        ...cachedValues
    };
};

const isJson = string => {
    try {
        JSON.parse(string);
    } catch (e) {
        return false;
    }

    return true;
};

const PitchUpload = ({dispatch, errors, form, handleSuccess, pitch, pitchId}: Props) => {
    const [modalVisibility, setModalVisibility] = useState('hidden');
    const [formState, setFormState] = useState('default');
    const [stateLabel, setStateLabel] = useState(null);
    const uploadedDocuments = pitch.documents ? pitch.documents.length : 0;

    const handleOpenReview = async (documentsForm: Object) => {
        const formErrors = await documentsForm.validateForm();

        /**
         * Not sure why video and terms fields aren't validating. In the interest of _time_, we can
         * force validation on them.
         *
         * We must first set them as 'touched' and then 'error' if there is a validation error.
         */
        documentsForm.setTouched({
            presentation: true,
            video_url: true,
            terms: true
        });

        /**
         * We only check for _a_ value here. For example, we only check that the video URL is not empty.
         * This looks like it would validate a wrong URL, however, for *some* reason the proper valdation
         * will takeover after we set this initial error.
         */
        ['video_url', 'terms'].map(field => {
            if (field === false || field === '') {
                documentsForm.setFieldError(field, 'true');
            }
        });

        /**
         * If there are no errors, show the 'review' modal. The user will confirm their input before
         * we then submit the form.
         */
        if (Object.keys(formErrors).length === 0) {
            /**
             * We can't store files, so let's just store text values
             */
            localStorage.setItem(
                'pitch.documents',
                JSON.stringify({
                    terms: documentsForm.values.terms,
                    video_url: documentsForm.values.video_url
                })
            );
            dispatch(updateForm({fields: {...form.fields, ...documentsForm.values}}));
            setModalVisibility('visible');
        }
    };

    const handleCloseReview = () => {
        setModalVisibility('leaving');
        setTimeout(() => setModalVisibility('hidden'), 600);
    };

    const submitDocuments = async (file, type, typeLabel) => {
        const formData = new FormData();
        const token = Cookie.get('token');

        formData.append('file', file);
        formData.append('type', type);

        setStateLabel(`Uploading ${typeLabel}`);

        try {
            const response = await axios.post(
                `${process.env.NEXT_STATIC_API_URL}api/pitches/${pitchId}/documents`,
                formData,
                {
                    headers: {Authorization: `Bearer ${token}`},
                    onUploadProgress: ({loaded, total}) => {
                        const percentCompleted = Math.round((loaded * 100) / total);

                        if (percentCompleted === 100) {
                            setStateLabel('Processing');
                        } else {
                            setStateLabel(`Uploading ${typeLabel} (${percentCompleted}%)`);
                        }
                    }
                }
            );

            return response.data;
        } catch (error) {
            if (bugsnagClient) {
                bugsnagClient.notify(error);
            }

            const fieldKey = typeLabel.replace(' ', '_');

            throw new Error(JSON.stringify({[fieldKey]: error.message}));
        }
    };

    const handleError = (error, handlers) => {
        console.log({error});
        if (error.status === 413) {
            handlers.setErrors({presentation: 'The file is too large, try a smaller file.'});
        } else {
            if (bugsnagClient) {
                bugsnagClient.notify(error);
            }
            if (isJson(error.message)) {
                handlers.setErrors(JSON.parse(error.message));
            } else {
                handlers.setErrors({
                    presentation:
                        error.message && error.message.length > 0
                            ? error.message
                            : 'Error uploading. Please try again.'
                });
            }
        }

        handlers.setSubmitting(false);
        setStateLabel('');
        setFormState('default');
    };

    const handleSubmit = async (values, handlers) => {
        if (formState !== 'default') {
            return;
        }

        setFormState('pending');

        if (values.presentation && values.presentation !== 'uploaded') {
            try {
                await submitDocuments(values.presentation, 'presentation', 'presentation');
            } catch (error) {
                return handleError(error, handlers);
            }
        } else {
            handlers.setSubmitting(false);
            setStateLabel('');
            handleError('Please attach your PDF presentation', handlers);
        }

        await Promise.all(
            [...Array(5).keys()]
                .filter(i => values[`document_${i + 1}`])
                .map(async i => {
                    if (values[`document_${i + 1}`] === 'uploaded') {
                        return true;
                    }

                    return submitDocuments(
                        values[`document_${i + 1}`],
                        'supporting',
                        `document ${i + 1}`
                    );
                })
        ).catch(error => {
            return handleError(error, handlers);
        });

        handlers.setSubmitting(false);
        setStateLabel('Posting details');

        const finalFormValues = {...form.fields, video_url: values.video_url};

        dispatch(updateForm({fields: finalFormValues}));
        handleSuccess(finalFormValues, errors => {
            handlers.setSubmitting(false);
            setStateLabel('');
            handleError(errors, handlers);
        });
    };

    return (
        <Formik
            initialValues={getInitialValues(pitch)}
            onSubmit={handleSubmit}
            validationSchema={pitchSchema.files}
            validateOnChange={true}
            validateOnBlur={true}
            render={formProps => (
                <>
                    {modalVisibility !== 'hidden' && (
                        <Modal state={modalVisibility} handleClose={handleCloseReview}>
                            <PitchReview
                                documents={formProps.values}
                                fields={form.fields}
                                handleReviewClick={handleCloseReview}
                                handleSubmitClick={() => {
                                    handleCloseReview();
                                    formProps.submitForm();
                                }}
                            />
                        </Modal>
                    )}
                    <Form>
                        <strong css={FormStyles.label}>
                            {__('pitch.documents.presentation.title')}
                        </strong>
                        <p
                            css={FormStyles.description}
                            dangerouslySetInnerHTML={{
                                __html: __('pitch.documents.presentation.text')
                            }}
                        />

                        {pitch.presentation ? (
                            <p css={UI.textColor('purplishBlue')}>Your pitch has been uploaded</p>
                        ) : (
                            <FileUploadField
                                formState={formState}
                                isDocument={true}
                                labels={{default: __('pitch.documents.presentation.ctaLabel')}}
                                name="presentation"
                                isRequired={true}
                                setFieldValue={formProps.setFieldValue}
                            />
                        )}

                        <UI.Divider size="m" />

                        <strong css={FormStyles.label}>
                            {__('pitch.documents.document.title')}
                        </strong>
                        <p
                            css={FormStyles.description}
                            dangerouslySetInnerHTML={{
                                __html: __('pitch.documents.document.text')
                            }}
                        />

                        <UI.Flex style={{flexWrap: 'wrap'}}>
                            {[...Array(5 - uploadedDocuments).keys()].map(i => {
                                i = i + uploadedDocuments;

                                return (
                                    <div css={UI.margin('right', 'xs')} key={`file-${i}`}>
                                        <FileUploadField
                                            formState={formState}
                                            isDocument={true}
                                            labels={{
                                                default: __('pitch.documents.document.ctaLabel')
                                            }}
                                            name={`document_${i + 1}`}
                                            isRequired={true}
                                            setFieldValue={formProps.setFieldValue}
                                        />
                                        <UI.Spacer size="xs" />
                                    </div>
                                );
                            })}
                        </UI.Flex>

                        {uploadedDocuments > 0 && (
                            <p css={[UI.textColor('purplishBlue'), UI.margin('top', 's')]}>
                                {uploadedDocuments}{' '}
                                {uploadedDocuments === 1 ? 'document has' : 'documents have'} been
                                uploaed
                            </p>
                        )}

                        <UI.Divider size="m" />

                        <InputText
                            name="video_url"
                            label={__('pitch.documents.video.label')}
                            description={__('pitch.documents.video.description')}
                            placeholder="https://"
                        />

                        <UI.Divider size="m" />

                        <span css={FormStyles.label}>Terms &amp; Conditions</span>
                        <InputCheckbox name="terms" label="">
                            I have read and accept the{' '}
                            <Link href="/terms-and-conditions">
                                <a target="noopener">
                                    <strong
                                        css={UI.textColor('purplishBlue')}
                                        style={{marginLeft: 3}}
                                    >
                                        Terms &amp; Conditions
                                    </strong>
                                </a>
                            </Link>
                        </InputCheckbox>

                        <UI.Spacer size="l" />

                        <div css={UI.centerText}>
                            <CTAButton
                                wide={true}
                                state={formState}
                                disabled={formProps.isSubmitting || formProps.isValidating}
                                onClick={() => handleOpenReview(formProps)}
                                type="button"
                            >
                                {__('pitch.documents.submitLabel')}
                            </CTAButton>
                            <br />

                            {Object.keys(formProps.errors).length > 0 && (
                                <div css={[FormStyles.error, UI.margin('top', 's')]}>
                                    {__('pitch.documents.error')}
                                </div>
                            )}

                            {errors && <div css={FormStyles.error}>{errors}</div>}

                            {stateLabel && (
                                <div
                                    css={[
                                        UI.centerText,
                                        UI.margin('top', 's'),
                                        UI.textColor('purplishBlue')
                                    ]}
                                >
                                    {stateLabel}
                                </div>
                            )}
                        </div>
                    </Form>
                </>
            )}
        />
    );
};

const mapStateToProps = state => ({form: state.form, pitch: state.user.team.pitch});

export default connect(mapStateToProps)(PitchUpload);
