// @flow
import React from 'react';
import {addDecorator, storiesOf} from '@storybook/react';
import {withKnobs, object, select, text} from '@storybook/addon-knobs';
import Logo from './';
import Theme from '../Theme';

const themeOptions = {
    Primary: 'primary',
    Secondary: 'secondary',
    Tertiary: 'tertiary',
    White: 'white'
};
const stories = storiesOf('Logo', module);

stories.addDecorator(withKnobs);

stories.addDecorator(story => (
    <Theme theme={select('Theme', themeOptions, 'secondary')}>
        <div style={{padding: 30}}>{story()}</div>
    </Theme>
));

stories.add('default', () => {
    return <Logo />;
});

stories.add('playground', () => {
    return <Logo />;
});
