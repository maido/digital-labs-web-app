// @flow
import {keyframes} from '@emotion/core';
import styled from '@emotion/styled';
import {rem} from 'polished';
import {transitions} from '../../globals/variables';

const loadingAnimation = keyframes`
    0% {
        opacity: 1;
    }
    50% {
        opacity: 0.4;
    }
    100% {
        opacity: 1;
    }
`;

export const SVG = styled.svg`
    animation: ${loadingAnimation} 1s linear infinite;
    fill: currentColor;
    fill: ${props => props.theme.logoFill};
    height: ${rem(120)};
    vertical-align: top;
    transition: ${transitions.default};
    will-change: transform;
    width: auto;

    .u-fill {
        fill: currentColor;
        fill: ${props => props.theme.logoFill};
    }

    ${props => !props.isLoading && `animation:none;`}
    ${props => props.isSymbol && `height: ${rem(35)};`}
`;
