// @flow
import * as React from 'react';
import Link from 'next/link';

type Props = {
    children?: React.Node,
    href: string | Object,
    type?: string
};

const LinkType = ({href = '', children, type = 'button', ...props}: Props) => {
    const hrefPath = typeof href === 'string' ? href : href.href;

    if (hrefPath.includes('http') || hrefPath.includes('mailto:') || hrefPath.includes('tel:')) {
        return (
            <a href={href} target="noopener" {...props}>
                {children}
            </a>
        );
    } else if (href) {
        if (typeof href === 'string') {
            return (
                <Link href={href} passHref>
                    <a {...props}>{children}</a>
                </Link>
            );
        } else {
            return (
                <Link {...href} passHref>
                    <a {...props}>{children}</a>
                </Link>
            );
        }
    } else {
        return (
            <button type={type} {...props}>
                {children}
            </button>
        );
    }
};

export default LinkType;
