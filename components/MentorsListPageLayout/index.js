// @flow
import React from 'react';
import {useRouter} from 'next/router';
import axios from 'axios';
import get from 'lodash/get';
import size from 'lodash/size';
import sortBy from 'lodash/sortBy';
import {connect} from 'react-redux';
import {getContentStatus, getMentorTitle} from '../../globals/functions';
import {logEvent} from '../../globals/analytics';
import __ from '../../globals/strings';
import AnimateWhenVisible from '../AnimateWhenVisible';
import CardLayout from '../CardLayout';
import CardLayoutPlaceholder from '../Placeholder/CardLayout';
import Container from '../Container';
import CTAButton from '../CTAButton';
import TeamSummaryPlaceholder from '../Placeholder/TeamSummaryCard';
import TeamSummaryCard from '../TeamSummaryCard';
import Theme from '../Theme';
import * as UI from '../UI/styles';

type Props = {
    dispatch: Function,
    mentors: Mentors,
    network: NetworkStatus
};

const MentorsListPageLayout = ({dispatch, mentors, network}: Props) => {
    const router = useRouter();

    const {showContent, showFallback, showPlaceholders} = getContentStatus(
        network.isLoading,
        mentors.allIds.length > 0,
        mentors.allIds.length > 0,
        true,
        true
    );
    const sortedMentors = sortBy(mentors.byId, m => m.first_name);

    const handleCardClick = id => {
        router.push('/mentors/[mentorId]', `/mentors/${id}`);
    };

    return (
        <>
            <Container size="small">
                <AnimateWhenVisible watch={false}>
                    <UI.Heading2 css={[UI.centerText, UI.margin('bottom', 'none')]} as="h1">
                        Mentors
                    </UI.Heading2>
                </AnimateWhenVisible>
            </Container>

            <Theme theme="tertiary" overlap="secondary" overlapSize={120} grow={true}>
                <Container paddingVertical="l">
                    {showPlaceholders && (
                        <CardLayoutPlaceholder
                            columns={2}
                            size={12}
                            component={
                                <TeamSummaryPlaceholder hasImage={false} hasHeadline={false} />
                            }
                        />
                    )}
                    {showContent && (
                        <>
                            <CardLayout
                                animate={false}
                                items={sortedMentors.map(mentor => ({
                                    key: mentor.id,
                                    component: (
                                        <TeamSummaryCard
                                            avatar={mentor.avatar}
                                            handleClick={() => handleCardClick(mentor.id)}
                                            mission={getMentorTitle(
                                                mentor.job_title,
                                                mentor.company
                                            )}
                                            title={`${mentor.first_name} ${mentor.last_name}`}
                                        />
                                    )
                                }))}
                            />
                        </>
                    )}
                    {showFallback && (
                        <AnimateWhenVisible delay={200} watch={false}>
                            <UI.Box
                                css={UI.centerText}
                                style={{
                                    marginLeft: 'auto',
                                    marginRight: 'auto',
                                    padding: 30,
                                    maxWidth: 600
                                }}>
                                <UI.Heading5 as="h2" style={{marginBottom: 10}}>
                                    {__('mentors.emptyTitle')}
                                </UI.Heading5>
                                <p>{__('mentors.empty')}</p>
                            </UI.Box>
                        </AnimateWhenVisible>
                    )}
                </Container>
                <UI.Spacer />
            </Theme>
        </>
    );
};

export default connect()(MentorsListPageLayout);
