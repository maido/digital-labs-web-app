// @flow
import React from 'react';
import * as S from './styles';

type Props = {
    handleClick: Function,
    isActive: boolean
};

const Hamburger = ({handleClick, isActive, ...props}: Props) => (
    <S.Container
        type="button"
        isActive={isActive}
        className="u-link"
        onClick={handleClick}
        aria-expanded={isActive}
        {...props}
    >
        <S.Text>Menu</S.Text>
        <S.LineContainer>
            <S.Line />
            <S.Line />
            <S.Line />
            <S.Line />
        </S.LineContainer>
    </S.Container>
);

export default Hamburger;
