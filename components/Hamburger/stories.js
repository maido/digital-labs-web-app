// @flow
import React from 'react';
import {addDecorator, storiesOf} from '@storybook/react';
import {withKnobs, boolean} from '@storybook/addon-knobs';
import Hamburger from './';
import Theme from '../Theme';

const stories = storiesOf('Hamburger', module);

stories.addDecorator(withKnobs);

stories.addDecorator(story => (
    <Theme theme="secondary">
        <div style={{padding: 30}}>{story()}</div>
    </Theme>
));

stories.add('default', () => {
    return <Hamburger isActive={false} handleClick={() => {}} />;
});

stories.add('is active', () => {
    return <Hamburger isActive={true} handleClick={() => {}} />;
});

stories.add('playground', () => {
    return <Hamburger isActive={boolean('Is active?', false)} handleClick={() => {}} />;
});
