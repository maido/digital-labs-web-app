// @flow
import styled from '@emotion/styled';
import {rem, rgba} from 'polished';
import {breakpoints, colors, fontFamilies, spacing, transitions} from '../../globals/variables';

const SIZE = 24;
const LINE_HEIGHT = 2;

export const Container = styled.button`
    align-items: center;
    border: 1px solid;
    border-radius: ${rem(5)};
    display: flex;
    font-family: ${fontFamilies.bold};
    font-size: 14px;
    margin-top: 2px;
    padding: ${rem(spacing.xs * 1.25)} ${rem(spacing.xs)};
    transition: ${transitions.default};

    &:hover,
    &:active {
        background-color: ${rgba(colors.white, 0.1)};
    }
`;

export const Text = styled.span`
    line-height: 1;
    padding-right: ${rem(spacing.xs)};
`;

export const LineContainer = styled.div`
    cursor: pointer;
    height: ${SIZE * 0.5625}px;
    position: relative;
    transform: rotate(0deg);
    transition: ${transitions.default};
    vertical-align: middle;
    width: ${SIZE}px;
`;

export const Line = styled.span`
    background: currentColor;
    display: block;
    height: ${LINE_HEIGHT}px;
    left: 0;
    margin-top: 1px;
    opacity: 1;
    position: absolute;
    transform: rotate(0deg);
    transition: ${transitions.default};
    width: 100%;

    // [aria-expanded='true'] & {
    //     background: ${colors.black};
    // }

    &:nth-of-type(1) {
        top: 0px;
    }

    &:nth-of-type(2),
    &:nth-of-type(3) {
        top: calc(50% - ${LINE_HEIGHT / 2 + 1}px);
    }

    &:nth-of-type(4) {
        top: calc(100% - ${LINE_HEIGHT + 2}px);
    }

    @media (max-width: ${rem(breakpoints.mobile)}) {
        &:nth-of-type(4) {
            top: calc(100% - ${LINE_HEIGHT + 1}px);
        }
    }

    [aria-expanded='true'] &:nth-of-type(1) {
        top: ${LINE_HEIGHT * 2}px;
        width: 0%;
        left: 50%;
    }

    [aria-expanded='true'] &:nth-of-type(2) {
        transform: rotate(45deg);
    }

    [aria-expanded='true'] &:nth-of-type(3) {
        transform: rotate(-45deg);
    }

    [aria-expanded='true'] &:nth-of-type(4) {
        left: 50%;
        top: ${LINE_HEIGHT * 2}px;
        width: 0%;
    }
`;
