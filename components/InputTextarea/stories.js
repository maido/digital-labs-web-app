// @flow
import React from 'react';
import {storiesOf} from '@storybook/react';
import {Formik, Form} from 'formik';
import InputTextarea from './';

const stories = storiesOf('InputTextarea', module);

stories.add('default', () => {
    return (
        <Formik
            initialValues={{}}
            onSubmit={() => {}}
            render={() => (
                <Form>
                    <InputTextarea name="last_name" />
                </Form>
            )}
        />
    );
});

stories.add('with label', () => {
    return (
        <Formik
            initialValues={{}}
            onSubmit={() => {}}
            render={() => (
                <Form>
                    <InputTextarea name="last_name" label="Last name" />
                </Form>
            )}
        />
    );
});

stories.add('with placeholder', () => {
    return (
        <Formik
            initialValues={{}}
            onSubmit={() => {}}
            render={() => (
                <Form>
                    <InputTextarea name="last_name" placeholder="Doe" />
                </Form>
            )}
        />
    );
});

stories.add('with many rows', () => {
    return (
        <Formik
            initialValues={{}}
            onSubmit={() => {}}
            render={() => (
                <Form>
                    <InputTextarea name="last_name" placeholder="Doe" rows={10} />
                </Form>
            )}
        />
    );
});

stories.add('has error', () => {
    return (
        <Formik
            initialValues={{last_name: 1}}
            validate={{last_name: 1}}
            isInitialValid={false}
            onSubmit={() => {}}
            render={props => (
                <Form>
                    <InputTextarea name="last_name" label="Last name" placeholder="Doe" />
                </Form>
            )}
        />
    );
});

stories.add('has field disabled', () => {
    return (
        <Formik
            initialValues={{}}
            onSubmit={() => {}}
            render={() => (
                <Form>
                    <InputTextarea name="last_name" label="Last name" placeholder="Doe" />
                </Form>
            )}
        />
    );
});
