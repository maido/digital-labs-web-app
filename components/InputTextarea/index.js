// @flow
import React from 'react';
import {ErrorMessage, Field} from 'formik';
import * as FormStyles from '../Form/styles';
import * as S from '../InputText/styles';

type Props = {
    description?: string,
    label?: string,
    name: string,
    placeholder?: string,
    rows?: number
};

const InputTextarea = ({description, label, name, placeholder, rows = 2}: Props) => {
    return (
        <>
            <Field label={label} name={name} placeholder={placeholder}>
                {({field, form, ...props}) => (
                    <S.Container>
                        {label && (
                            <label htmlFor={name} css={FormStyles.label}>
                                {label}
                            </label>
                        )}
                        {description && (
                            <span
                                css={FormStyles.description}
                                dangerouslySetInnerHTML={{__html: description}}
                            />
                        )}
                        <S.InputContainer>
                            <textarea
                                css={[
                                    FormStyles.input,
                                    form.touched[field.name] && form.errors[field.name]
                                        ? FormStyles.inputError
                                        : null
                                ]}
                                id={name}
                                name={field.name}
                                placeholder={placeholder}
                                rows={rows}
                                style={{lineHeight: 1.7}}
                                {...field}
                                {...props}
                            />
                        </S.InputContainer>
                    </S.Container>
                )}
            </Field>
            <ErrorMessage
                component="span"
                name={name}
                css={FormStyles.error}
                data-testid={`error-${name}`}
            />
        </>
    );
};

export default InputTextarea;
