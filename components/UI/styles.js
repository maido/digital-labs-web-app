// @flow
import {css, keyframes} from '@emotion/core';
import styled from '@emotion/styled';
import {between, rem} from 'polished';
import {responsiveRem} from '../../globals/functions';
import {
    breakpoints,
    colors,
    fontFamilies,
    fontSizes,
    shadows,
    spacing,
    themes,
    transitions
} from '../../globals/variables';

type FadeInProps = {
    delay?: number
};
type FadeInUpProps = {
    delay?: number
};

export const centerText = css`
    text-align: center;
`;

export const centerTextAtMobile = css`
    @media (min-width: ${rem(breakpoints.mobile)}) {
        text-align: center;
    }
`;

export const displayBlock = css`
    display: block;
`;

export const textColor = (color: string) => css`
    color: ${colors[color]} !important;
`;

export const margin = (position?: string, size: string) => css`
    margin${position ? `-${position}` : ''}: ${rem(spacing[size])} !important;
`;

export const padding = (position?: string, size: string) => css`
    padding${position ? `-${position}` : ''}: ${rem(spacing[size])} !important;
`;

export const textSize = (size: number) => css`
    font-size: ${rem(size)} !important;
`;

export const leadText = css`
    font-size: ${rem(fontSizes.lead * 0.85)};
    line-height: 1.6;

    @media (min-width: ${rem(breakpoints.mobile)}) {
        font-size: ${rem(fontSizes.lead)};
        line-height: 1.6;
    }
`;

export const dangerLink = css`
    &:hover,
    &:focus {
        color: ${colors.red};
        text-decoration: underline;
    }
`;

//

export const ErrorMessage = styled.span`
    background-color: ${colors.red};
    border-radius: ${rem(30)};
    color: ${colors.white};
    display: inline-block;
    font-family: ${fontFamilies.bold};
    padding: ${rem(spacing.xs)} ${rem(spacing.m)};
`;

//

export const Flex = styled.div`
    display: flex;
`;

export const displayFlexAtMobile = css`
    @media (min-width: ${rem(breakpoints.mobile)}) {
        display: flex;
    }
`;

export const flexGrow1 = css`
    flex-grow: 1;
`;

export const flexAligncenter = css`
    align-items: center;
`;

//

const avatarSizes = {
    large: 120,
    default: 90,
    medium: 60,
    small: 40
};

export const Avatar = styled.div`
    background-color: ${themes.white.placeholder};
    background-image: url('${props => (props.image ? props.image : '/avatar-placeholder.png')}');
    background-repeat: no-repeat;
    background-size: cover;
    border: 2px solid ${props => (props.borderColor ? colors[props.borderColor] : colors.white)};
    border-radius: 100%;
    box-shadow: 0 2px 10px 0 rgba(255,255,255, 0.07);
    border-radius: 100%;
    display: block;
    flex: 0 0 ${props =>
        props.size ? rem(avatarSizes[props.size] * 0.8) : rem(avatarSizes.default * 0.8)};
    height: ${props =>
        props.size ? rem(avatarSizes[props.size] * 0.8) : rem(avatarSizes.default * 0.8)};
    object-fit: cover;
    transition: ${transitions.default};
    width: ${props =>
        props.size ? rem(avatarSizes[props.size] * 0.8) : rem(avatarSizes.default * 0.8)};
        will-change:transform;

    a&:hover,
    a&:focus,
    a:hover &,
    a:focus & {
        transform:scale(1.05);
    }

    @media (min-width: ${rem(breakpoints.mobile)}) {
        flex: 0 0 ${props =>
            props.size ? rem(avatarSizes[props.size]) : rem(avatarSizes.default)};
        height: ${props => (props.size ? rem(avatarSizes[props.size]) : rem(avatarSizes.default))};
        width: ${props => (props.size ? rem(avatarSizes[props.size]) : rem(avatarSizes.default))};
    }
`;

//

export const Box = styled.div`
    background-color: ${props =>
        props.theme && themes[props.theme]
            ? themes[props.theme].background
            : themes.white.background};
    box-shadow: ${shadows.hint};
    color: ${props =>
        props.theme && themes[props.theme] ? themes[props.theme].text : themes.white.text};
    display: block;
    padding: ${props => (props.padding ? rem(spacing[props.padding]) : rem(spacing.m))};

    ${props => `border-radius: ${rem(props.radius ? props.radius : 5)}`};
    ${props =>
        props.hasOwnProperty('paddingAtMobile') &&
        props.paddingAtMobile === false &&
        ` @media (max-width: ${rem(breakpoints.mobile)}) { padding: 0; }}`}
`;

//

export const Divider = styled.hr`
    border: 1px solid ${colors.greyLight};
    display: block;

    ${props => {
        if (props.size) {
            if (props.size === 0) {
                return `margin-bottom: 0;
                margin-top: 0;`;
            } else {
                return `margin-bottom: ${responsiveRem(spacing[props.size])};
                margin-top: ${responsiveRem(spacing[props.size])};`;
            }
        } else {
            return `margin-bottom: ${responsiveRem(spacing.s)};
            margin-top: ${responsiveRem(spacing.s)};`;
        }
    }}};
`;

//

const fadeInAnimation = keyframes`
    from { opacity: 0; }
    to { opacity: 1; }
`;

export const fadeIn = (props: FadeInProps) => css`
    animation: ${fadeInAnimation} 1s;
    animation-delay: ${props.delay ? `${props.delay / 1000}s` : 0};
    animation-fill-mode: forwards;
    opacity: 0;
    position: relative;
    transition: ${transitions.bezier};
    will-change: opacity;
`;

export const FadeIn = styled.div`
    ${props => fadeIn(props)};
`;

const fadeInUpAnimation = keyframes`
    from {
        opacity: 0;
        transform: translateY(${rem(spacing.s)});
    }
    to {
        opacity: 1;
        transform: translateY(0);
    }
`;

export const fadeInUp = (props: FadeInUpProps) => css`
    animation: ${fadeInUpAnimation} 1s;
    animation-delay: ${props.delay ? `${props.delay / 1000}s` : 0};
    animation-fill-mode: forwards;
    opacity: 0;
    position: relative;
    transform: translateY(${rem(spacing.m)});
    transition: ${transitions.bezier};
    will-change: transform;
`;

export const FadeInUp = styled.div`
    ${props => fadeInUp(props)};
`;

//

export const FlexMediaWrapper = styled.div`
    align-items: ${props => (props.align ? props.align : 'center')};
    display: flex;
`;

export const FlexMediaContent = styled.div`
    margin-left: ${props => (props.margin ? rem(spacing[props.margin]) : rem(spacing.s))};

    @media (min-width: ${rem(breakpoints.mobile)}) {
        margin-left: ${props => (props.margin ? rem(spacing[props.margin]) : rem(spacing.m))};
    }
`;

//

export const HorizontalList = styled.ul`
    list-style: none;
    margin: 0;
    padding: 0;

    > li {
        display: inline-block;
    }

    > li + li {
        margin-left: ${rem(spacing.m)};
    }
`;

//

export const formHeading = css`
    color: ${colors.greyDark};
    font-family: ${fontFamilies.default};
    font-weight: 300;
    margin-bottom: ${responsiveRem(spacing.s)};
`;

export const Subheading = styled.span`
    font-family: ${fontFamilies.heading};
    font-size: ${rem(16)};
`;

const Heading = css`
    font-family: ${fontFamilies.heading};
    font-style: normal;
    font-weight: normal;
    line-height: 1.2;
    margin-bottom: ${rem(spacing.m)};
    margin-top: 0;
`;

export const Heading1 = styled.h1`
    ${Heading};
    font-size: ${rem(fontSizes.h1 * 0.7)};
    letter-spacing: ${rem(-1)};

    @media (min-width: ${props => rem(breakpoints.tablet)}) {
        font-size: ${rem(fontSizes.h1)};
    }
`;

export const Heading2 = styled.h2`
    ${Heading};
    font-size: ${rem(fontSizes.h2 * 0.75)};
    letter-spacing: ${rem(-1)};

    @media (min-width: ${props => rem(breakpoints.mobile)}) {
        font-size: ${rem(fontSizes.h2)};
    }
`;

export const Heading3 = styled.h3`
    ${Heading};
    font-size: ${rem(fontSizes.h3)};
    letter-spacing: ${rem(-0.5)};
`;

export const Heading4 = styled.h4`
    ${Heading};
    font-size: ${rem(fontSizes.h4)};
    letter-spacing: ${rem(-0.5)};

    ${props => props.primary && `color: ${colors.primary};`}
`;

export const Heading5 = styled.h5`
    ${Heading};
    font-family: ${fontFamilies.bold};
    font-size: ${rem(fontSizes.h5)};
`;

export const Heading6 = styled.h6`
    ${Heading};
    font-family: ${fontFamilies.bold};
    font-size: ${rem(fontSizes.h6)};
`;

//

export const label = css`
    color: ${colors.grey};
    font-family: ${fontFamilies.bold};
    font-size: ${rem(13)};
    letter-spacing: ${rem(0.75)};
    text-transform: uppercase;
`;

export const Label = styled.span`
    ${label}
`;

//

export const LayoutContainer = styled.div`
    display: block;
    margin: 0;
    padding: 0;
    list-style: none;
    margin-left: ${rem(spacing.m * -1)};

    ${props =>
        props.type === 'matrix' &&
        `margin-bottom: ${rem(spacing.s * -1)};

        > div {
            margin-bottom: ${rem(spacing.s)};
        }

        @media (min-width: ${rem(breakpoints.mobile)}) {
            margin-bottom: ${rem(props.size ? spacing[props.size] * -1 : spacing.m * -1)};

            > div {
                margin-bottom: ${rem(props.size ? spacing[props.size] : spacing.m)};
            }
        }
        `};

    ${props =>
        props.size &&
        `@media (min-width: ${rem(breakpoints.mobile)}) {
            margin-left: ${rem(spacing[props.size] * -1)};

            > div {
                padding-left: ${rem(spacing[props.size])};
            }
        }
        `};

    ${props =>
        props.flush &&
        `margin-left: 0;

            > div {
                padding-left: 0;
            }
        `};

    ${props =>
        props.stack &&
        `@media (max-width: ${rem(breakpoints.mobile)}) {
            > div + div {
                margin-top: ${rem(spacing.m)};
            }
        }
        `};
`;

const layoutWidth = size => (size * 100).toFixed(2);

export const LayoutItem = styled.div`
    box-sizing: border-box;
    display: inline-block;
    vertical-align: top;
    padding-left: ${rem(spacing.m)};
    width: ${props => (props.size ? `${layoutWidth(props.size)}%` : '100%')};

    ${props =>
        props.sizeAtMobileSmall &&
        `@media (min-width: ${rem(breakpoints.mobileSmall)}) {
        width: ${layoutWidth(props.sizeAtMobileSmall)}%;
    }`}

    ${props =>
        props.sizeAtMobile &&
        `@media (min-width: ${rem(breakpoints.mobile)}) {
        width: ${layoutWidth(props.sizeAtMobile)}%;
    }`}

    ${props =>
        props.sizeAtTabletSmall &&
        `@media (min-width: ${rem(breakpoints.tabletSmall)}) {
        width: ${layoutWidth(props.sizeAtTabletSmall)}%;
    }`}


    ${props =>
        props.sizeAtTablet &&
        `@media (min-width: ${rem(breakpoints.tablet)}) {
        width: ${layoutWidth(props.sizeAtTablet)}%;
    }`}

    ${props =>
        props.sizeAtDesktop &&
        `@media (min-width: ${rem(breakpoints.desktop)}) {
        width: ${layoutWidth(props.sizeAtDesktop)}%;
    }`}

    ${props =>
        props.sizeAtDesktopLarge &&
        `@media (min-width: ${rem(breakpoints.desktopLarge)}) {
        width: ${layoutWidth(props.sizeAtDesktopLarge)}%;
    }`}

    ${props =>
        props.isBlank &&
        `@media (max-width: ${rem(breakpoints.tablet)}) {
        display: none;
    }`}
`;

//

export const List = styled.ul`
    display: inline-block;
    list-style: none;
    margin: 0;
    padding: 0;

    @media (min-width: ${rem(breakpoints.tablet)}) {
        align-items: center;
        display: flex;
    }
`;

export const ListItem = styled.li`
    & + & {
        amargin-top: ${rem(spacing.s)};
    }

    @media (min-width: ${rem(breakpoints.tablet)}) {
        & + & {
            margin-left: ${responsiveRem(spacing.m)};
            margin-top: 0;
        }

        ${props =>
            props.size &&
            `& + & {
            margin-left: ${responsiveRem(spacing[props.size])};
            margin-top: 0;
        }`}
    }
`;

//

export const OrderedList = styled.ol`
    list-style-position: outside;
    margin: 0;
    padding: 0;
    padding-left: ${rem(spacing.m)};

    li {
        margin-bottom: ${rem(spacing.s)};
    }
`;
export const UnorderedList = styled.ul`
    list-style-position: outside;
    margin: 0;
    padding: 0;
    padding-left: ${rem(spacing.m)};

    li {
        margin-bottom: ${rem(spacing.s)};
    }
`;

//

const shakeAnimation = keyframes`
    10%, 90% {
        transform: translate3d(-1px, 0, 0);
    }

    20%, 80% {
        transform: translate3d(2px, 0, 0);
    }

    30%, 50%, 70% {
        transform: translate3d(-4px, 0, 0);
    }

    40%, 60% {
        transform: translate3d(4px, 0, 0);
    }
`;

export const shake = css`
    animation: ${shakeAnimation} 0.82s cubic-bezier(0.36, 0.07, 0.19, 0.97) both;
    transform: translate3d(0, 0, 0);
    backface-visibility: hidden;
    perspective: 1000px;
`;

//

export const Spacer = styled.div`
    height: ${props => rem(spacing[props.size ? props.size : 'm'])};
    width: ${props => rem(spacing[props.size ? props.size : 'm'])};

    ${props =>
        props.sizeAtMobile &&
        `
        @media (min-width: ${rem(breakpoints.mobile)}) {
            height: ${rem(spacing[props.sizeAtMobile])};
            width: ${rem(spacing[props.sizeAtMobile])};
        }
        `}
`;

export const ResponsiveSpacer = styled.div`
    height: ${props => responsiveRem(spacing[props.size ? props.size : 'm'])};
    width: ${props => responsiveRem(spacing[props.size ? props.size : 'm'])};

    ${props =>
        props.sizeAtMobile &&
        `
        @media (min-width: ${rem(breakpoints.mobile)}) {
            height: ${responsiveRem(spacing[props.sizeAtMobile])};
            width: ${responsiveRem(spacing[props.sizeAtMobile])};
        }
        `}
`;

//

export const stickyAt = (breakpoint: string) => css`
    @media (min-width: ${rem(breakpoints[breakpoint])}) {
        position: sticky;
        top: ${rem(spacing.m)};
    }
`;

//

const scrollSwipeEntranceAnimation = keyframes`
    to {
        opacity: 1;
        transform: translateX(0);
    }
`;

export const ScrollSwipe = styled.div`
    ${props => {
        `@media (max-width: ${rem(breakpoints[props.breakpoint ? props.breakpoint : 'mobile'])}) {
        animation: ${scrollSwipeEntranceAnimation} 2s cubic-bezier(0.23, 1, 0.32, 1);
        animation-fill-mode: forwards;
        margin-left: ${rem(spacing.m * -1)};
        max-width: calc(100% + ${rem(spacing.m * 2)});
        overflow: hidden;
        position: relative;
        opacity: 0;
        touch-action: manipulation;
        transform: translateX(150px);
        white-space: nowrap;
        width: calc(100% + ${rem(spacing.m * 2)});
    }`;
    }}
`;

export const ScrollSwipeInner = styled.div`
    @media (max-width: ${rem(breakpoints.mobile)}) {
        overflow-x: auto;
        -webkit-overflow-scrolling: touch;
        padding-left: ${rem(spacing.m)};
        padding-right: ${rem(spacing.m)};

        * + * {
            margin-left: ${rem(spacing.xs)};
        }
    }
`;

//

export const textLink = css`
    cursor: pointer;
    font-family: ${fontFamilies.bold};
    position: relative;
    transition: ${transitions.default};

    &::before {
        background-color: currentColor;
        bottom: -5px;
        content: '';
        height: 2px;
        right: 0;
        opacity: 0;
        position: absolute;
        transition: ${transitions.default};
        transition-duration: 0.4s;
        width: 0;
    }

    &:hover::before,
    &:focus::before,
    &[aria-current='true']::before {
        left: 0;
        opacity: 1;
        right: auto;
        transition-duration: 0.2s;
        width: 100%;
    }
`;

export const TextLink = styled.a`
    ${textLink};

    & + & {
        margin-left: ${rem(spacing.m)};
    }

    ${props => {
        if (props.theme && colors[props.theme]) {
            return `
        & {
            color: ${colors[props.theme]} !important;
        }
        &:hover,
        &:focus {
            border-color: ${colors[props.theme]} !important;
            color: ${colors[props.theme]} !important;
        }
    `;
        } else {
            return `
            &,&:hover,&:focus {
                color: ${colors.purplishBlue};
            }`;
        }
    }};
`;

//

export const HideAt = styled.div`
    @media (min-width: ${props => rem(breakpoints[props.breakpoint])}) {
        display: none;
    }
`;

export const ShowAt = styled.div`
    @media (max-width: ${props => rem(breakpoints[props.breakpoint])}) {
        display: none;
    }
`;

export const VisuallyHidden = styled.div`
    border: 0 !important;
    clip: rect(0 0 0 0) !important;
    clip-path: inset(50%) !important;
    height: 1px !important;
    margin: -1px !important;
    overflow: hidden !important;
    padding: 0 !important;
    position: absolute !important;
    white-space: nowrap !important;
    width: 1px !important;
`;
