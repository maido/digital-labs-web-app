// @flow
import {css} from '@emotion/core';
import styled from '@emotion/styled';
import {rgba, shade, rem} from 'polished';
import {responsiveRem} from '../../globals/functions';
import {
    breakpoints,
    colors,
    fontFamilies,
    shadows,
    spacing,
    themes,
    transitions
} from '../../globals/variables';

const logoWidth = 100;

export const Nav = styled.nav`
    font-family: ${fontFamilies.bold};
    padding: ${rem(spacing.xs)} 0;
    position: relative;
    transition: background ${transitions.default};
    width: 100%;
    z-index: 201;

    @media (min-width: ${rem(breakpoints.mobile)}) {
        padding: ${rem(spacing.m)} 0;
    }

    @media (min-width: ${rem(breakpoints.mobile)}) {
        > div {
            align-items: center;
            display: flex;
        }
    }

    a {
        line-height: 1.2;
    }

    ${props => {
        /**
         * Workaround to not have 'green' nav links on 'secondary' theme backgrounds
         */
        if (props.theme && props.theme.background === colors.secondary) {
            return `a, a:hover, a:focus { color: ${colors.white}; }`;
        }
    }};

    ${props => props.isFixed && `z-index: 201;`};

    ${props => {
        if (props.isFixed && !props.isOverlayNavActive) {
            const fixedTheme = props.theme ? props.theme : themes.white;

            return `@media (max-width: ${rem(breakpoints.mobile)}) {
                background-color: ${rgba(fixedTheme.background, 0.95)};
                border-bottom: 2px solid ${shade(0.025, fixedTheme.background)};
                box-shadow: ${shadows.default};

                &,
                span,
                a,
                a:hover,
                a:focus,
                div:hover span,
                div:focus span {
                    color: ${fixedTheme.text} !important;
                }
                button {
                    border-color: ${fixedTheme.text} !important;
                }

                path {
                    fill: ${fixedTheme.logoFill};
                }
            }
        `;
        }
    }}

    ${props =>
        props.hasTallLogo &&
        `
        > div { align-items: flex-start; }
        > div > div:last-child { padding-top:${rem(10)} }
    `}
`;

export const LeftColumn = styled.div`
    @media (max-width: ${rem(breakpoints.tablet)}) {
        display: none;
    }

    @media (min-width: ${rem(breakpoints.tablet)}) {
        flex: 0 0 calc(50% - ${rem(logoWidth / 2)});
    }
`;

export const MiddleColumn = styled.div`
    display: inline-block;
    width: ${rem(logoWidth)};

    svg {
        height: ${responsiveRem(40)};
        margin-left: auto;
        margin-right: auto;
        transition: ${transitions.default};
        width: auto;
        will-change: transform;
        ${props => props.hasTallLogo && `height: ${rem(80)};`}
    }

    a:hover svg,
    a:focus svg {
        transform: scale(1.05);
    }

    @media (min-width: ${rem(breakpoints.tablet)}) {
        flex: 0 0 ${rem(logoWidth)};
        text-align: center;
    }
`;

export const RightColumn = styled.div`
    @media (max-width: ${rem(breakpoints.tablet)}) {
        align-items: center;
        color: ${colors.offWhite};
        display: flex;
        position: absolute;
        right: ${rem(spacing.s)};
        top: -10px;
    }

    @media (max-width: ${rem(breakpoints.mobile)}) {
        top: 4px;

        ${props => !props.hasHamburger && `top: 10px;`}
    }

    @media (min-width: ${rem(breakpoints.tablet)}) {
        align-items: center;
        display: flex;
        flex: 0 0 calc(50% - ${rem(logoWidth / 2)});
        justify-content: flex-end;
        text-align: right;
    }
`;

export const ProfileLink = styled.div`
    align-items: center;
    display: flex;
    margin-right: ${rem(spacing.m)};
    perspective: 1000px;

    padding: ${rem(6)} ${rem(spacing.s)};
    position: relative;
    transition: background 0.2s ease-in-out;

    span,
    &:hover span,
    &:focus span {
        border: 0;
    }

    &:hover > div,
    &:focus > div {
        border-width: 3px;
    }

    > span::before {
        display: none;
    }

    @media (max-width: ${rem(breakpoints.mobile)}) {
        display: none;
    }
`;

export const avatar = css`
    margin-right: ${rem(spacing.s)};
`;

export const ProfilePopOut = styled.div`
    background-color: ${colors.white};
    border-radius: ${rem(5)};
    box-shadow: ${shadows.hover};
    min-width: 200px;
    opacity: 0;
    position: absolute;
    left: 50%;
    top: 100%;
    transform: rotateX(-20deg) translateX(-50%) translateY(${rem(20)});
    transition: ${transitions.default};
    will-change: transform;
    visibility: hidden;
    z-index: 100;

    ${ProfileLink}:hover &,
    ${ProfileLink}:focus & {
        opacity: 1;
        transform: rotateX(0) translateX(-50%) translateY(4px);
        transition-duration: 0.2s;
        visibility: visible;
    }
`;

export const popoutLink = css`
    &,
    &:hover,
    &:focus {
        color: ${colors.darkGreyBlue} !important;
        display: block;
        margin-left: 0 !important;
        padding: ${rem(spacing.s)} ${rem(spacing.s)};
        text-align: center;
    }
    &::before {
        display: none;
    }
    &:hover,
    &:focus {
        background-color: ${colors.greyLight};
    }
    & + & {
        border-top: 1px solid ${colors.blueGrey} !important;
    }
    &:first-of-type {
        border-top-left-radius: ${rem(5)};
        border-top-right-radius: ${rem(5)};
    }
    &:last-of-type {
        background-color: ${colors.greyLight};
        border-bottom-left-radius: ${rem(5)};
        border-bottom-right-radius: ${rem(5)};
        color: ${colors.greyDark} !important;
    }
    &:last-of-type:hover,
    &:last-of-type:focus {
        background-color: ${colors.red};
        color: ${colors.white} !important;
    }
`;

export const logoutLink = css`
    color: ${colors.greyDark} !important;
`;

export const ProfilePopOutArrow = styled.span`
    bottom: 100%;
    height: 12px;
    left: 50%;
    margin-left: -12px;
    overflow: hidden;
    position: absolute;
    width: 24px;

    &::after {
        background-color: ${colors.white};
        box-shadow: 0px 2px 30px rgba(0, 0, 0, 0.1);
        content: '';
        height: 12px;
        left: 50%;
        position: absolute;
        transform: translate(-50%, 50%) rotate(45deg);
        width: 12px;
    }
`;

export const NotificationsLink = styled.a`
    cursor: pointer;
    position: relative;

    ${props =>
        props.notifications &&
        props.notifications > 0 &&
        `
        ::after {
            align-items: center;
            background-color: ${colors.aquaMarine};
            border-radius: 100%;
            color: ${colors.darkBlueGrey};
            content: '${props.notifications}';
            display: flex;
            font-size: ${rem(12)};
            height: ${rem(17)};
            left: ${rem(15)};
            justify-content: center;
            overflow: hidden;
            padding-top: 2px;
            position: absolute;
            top: 50%;
            transform: translateY(-50%);
            transition: ${transitions.default};
            width: ${rem(16)};
        }

        &:hover::after,
        &:focus::after {
            transform: scale(1.2) translateY(-50%);
        }
    `};
`;

export const BellIcon = styled.svg`
    height: ${rem(22)};
    margin-right: ${rem(spacing.s * 1.5)};
    position: relative;
    transition: ${transitions.default};
    width: auto;
    vertical-align: sub;

    path {
        fill: currentColor;
    }

    a:hover &,
    a:focus & {
        transform: scale(1.1);
    }
`;

export const externalLink = css`
    font-family: ${fontFamilies.default};
    margin-left: ${rem(spacing.m)};
`;

export const ExternalIcon = styled.svg`
    height: auto;
    margin-left: ${rem(spacing.xs)};
    vertical-align: top;
    width: ${rem(18)};

    path {
        fill: currentColor;
    }
`;
