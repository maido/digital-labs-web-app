// @flow
import React from 'react';
import {addDecorator, storiesOf} from '@storybook/react';
import SiteHeader from './';
import Theme from '../Theme';

const stories = storiesOf('SiteHeader', module);

stories.addDecorator(story => (
    <Theme theme="secondary">
        <div style={{padding: 30}}>{story()}</div>
    </Theme>
));

stories.add('default', () => {
    return <SiteHeader />;
});
