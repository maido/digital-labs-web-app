// @flow
import React, {useState} from 'react';
import {connect} from 'react-redux';
import {useRouter} from 'next/router';
import Link from 'next/link';
import get from 'lodash/get';
import startsWith from 'lodash/startsWith';
import {logEvent} from '../../globals/analytics';
import {canUserSubmitPitch, getUnreadNotifications} from '../../globals/functions';
import Container from '../Container';
import Hamburger from '../Hamburger';
import Logo, {LogoSymbol} from '../Logo';
import * as UI from '../UI/styles';
import * as S from './styles';
import {INTEGRATIONS} from '../../globals/config';

type Props = {
    handleHamburgerClick: Function,
    isFixed: boolean,
    isOverlayNavActive: boolean,
    network: NetworkStatus,
    notifications: Notifications,
    user: User
};
type AuthenticatedUserLinksProps = {
    notificationsCount: number,
    user: User
};

const AuthenticatedSiteLinks = ({pathname = ''}) => (
    <>
        <Link href="/dashboard" passHref>
            <UI.TextLink aria-current={startsWith(pathname, '/dashboard')}>Dashboard</UI.TextLink>
        </Link>
        <Link href="/labs" passHref>
            <UI.TextLink aria-current={startsWith(pathname, '/labs')}>Labs</UI.TextLink>
        </Link>
        <Link href="/resources" passHref>
            <UI.TextLink aria-current={startsWith(pathname, '/resources')}>Resources</UI.TextLink>
        </Link>
        <Link href="/teams" passHref>
            <UI.TextLink aria-current={startsWith(pathname, '/teams')}>Teams</UI.TextLink>
        </Link>
        <Link href="/mentors" passHref>
            <UI.TextLink aria-current={startsWith(pathname, '/mentors')}>Mentors</UI.TextLink>
        </Link>
        <UI.TextLink href={INTEGRATIONS.wikifactory.url} css={S.externalLink}>
            {INTEGRATIONS.wikifactory.name}
            <S.ExternalIcon
                xmlns="http://www.w3.org/2000/svg"
                width="24"
                height="24"
                viewBox="0 0 24 24"
            >
                <path d="M9 5v2h6.59L4 18.59 5.41 20 17 8.41V15h2V5z" />
            </S.ExternalIcon>
        </UI.TextLink>
    </>
);

const AuthenticatedUserLinks = ({notificationsCount, user}: AuthenticatedUserLinksProps) => {
    const canSubmitPitch = canUserSubmitPitch(user);
    let userAvatar;

    if (user.avatar) {
        userAvatar = user.avatar.thumb_url ? user.avatar.thumb_url : user.avatar.url;
    }

    return (
        <>
            <Link href="/notifications">
                <S.NotificationsLink
                    aria-label={`${notificationsCount} notifications`}
                    notifications={notificationsCount}
                    css={notificationsCount > 0 ? UI.shake : null}
                >
                    <S.BellIcon
                        width="16"
                        height="20"
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 16 20"
                    >
                        <path
                            d="M8 20c1.1 0 2-.9 2-2H6c0 1.1.89 2 2 2zm6-6V9c0-3.07-1.64-5.64-4.5-6.32V2C9.5 1.17 8.83.5 8 .5S6.5 1.17 6.5 2v.68C3.63 3.36 2 5.92 2 9v5l-2 2v1h16v-1l-2-2z"
                            fillRule="evenodd"
                        />
                    </S.BellIcon>
                </S.NotificationsLink>
            </Link>

            <S.ProfileLink>
                <UI.Avatar image={userAvatar} size="small" css={S.avatar} />
                <UI.TextLink as="span" className="u-link">
                    Hi, {user.first_name}
                </UI.TextLink>
                <S.ProfilePopOut>
                    <Link href="/account/edit" passHref>
                        <UI.TextLink css={S.popoutLink}>Edit profile</UI.TextLink>
                    </Link>
                    <Link href="/teams/[teamId]" as={`/teams/${user.team.id}`} passHref>
                        <UI.TextLink css={S.popoutLink}>View team</UI.TextLink>
                    </Link>
                    {canSubmitPitch && (
                        <Link href="/team/pitch" passHref>
                            <UI.TextLink
                                onClick={() => {
                                    logEvent({
                                        action: 'click',
                                        category: 'Pitch',
                                        label: 'Start - Header'
                                    });
                                }}
                                css={S.popoutLink}
                            >
                                Submit pitch
                            </UI.TextLink>
                        </Link>
                    )}
                    <Link href="/logout" passHref>
                        <UI.TextLink css={S.popoutLink}>Logout</UI.TextLink>
                    </Link>
                    <S.ProfilePopOutArrow />
                </S.ProfilePopOut>
            </S.ProfileLink>
        </>
    );
};

const hasNavigation = urlPathname => {
    const routesWithNoNavigation = ['/signup/onboarding', '/team/create'];

    if (!routesWithNoNavigation.includes(urlPathname)) {
        return true;
    }
};

const SiteHeader = ({
    handleHamburgerClick,
    isFixed,
    isOverlayNavActive,
    network,
    notifications,
    user
}: Props) => {
    const {pathname} = useRouter();

    const showNavigation = hasNavigation(pathname);
    let notificationsCount = 0;

    if (notifications.allIds.unread.length > 0) {
        notificationsCount = getUnreadNotifications(notifications).length;
    }

    return (
        <S.Nav
            isFixed={isFixed}
            isOverlayNavActive={isOverlayNavActive}
            hasTallLogo={user.isAuthenticated ? false : true}
        >
            <Container paddingVertical={0}>
                <S.LeftColumn>
                    {user.isAuthenticated && showNavigation && (
                        <AuthenticatedSiteLinks pathname={pathname} />
                    )}
                </S.LeftColumn>
                <S.MiddleColumn hasTallLogo={user.isAuthenticated ? false : true}>
                    <Link href={user.isAuthenticated ? '/dashboard' : '/'}>
                        <a id="header-logo" style={{cursor: 'pointer'}}>
                            {user.isAuthenticated ? (
                                <LogoSymbol isLoading={network.isLoading} />
                            ) : (
                                <Logo />
                            )}
                        </a>
                    </Link>
                </S.MiddleColumn>
                <S.RightColumn hasHamburger={user.isAuthenticated}>
                    {!user.isAuthenticated && (
                        <>
                            <Link href="/signup" passHref>
                                <UI.TextLink
                                    onClick={() => {
                                        logEvent({
                                            action: 'click',
                                            category: 'Sign up',
                                            label: 'General - Header'
                                        });
                                    }}
                                >
                                    Signup
                                </UI.TextLink>
                            </Link>
                            <Link href="/login" passHref>
                                <UI.TextLink>Login</UI.TextLink>
                            </Link>
                        </>
                    )}
                    {user.isAuthenticated && showNavigation && (
                        <>
                            <AuthenticatedUserLinks
                                notificationsCount={notificationsCount}
                                user={user}
                            />

                            <UI.HideAt breakpoint="tablet">
                                <Hamburger
                                    isActive={isOverlayNavActive}
                                    handleClick={handleHamburgerClick}
                                />
                            </UI.HideAt>
                        </>
                    )}
                </S.RightColumn>
            </Container>
        </S.Nav>
    );
};

const mapStateToProps = state => ({
    network: state.network,
    notifications: state.notifications,
    user: state.user
});

export default connect(mapStateToProps)(SiteHeader);
