// @flow
import React from 'react';
import {ErrorMessage, Field} from 'formik';
import * as FormStyles from '../Form/styles';
import * as S from './styles';

type Props = {
    handleChange: Function,
    label?: string,
    name: string
};

const InputFile = ({handleChange, label, name}: Props) => (
    <>
        <Field label={label} name={name}>
            {({field, form, ...props}) => (
                <S.Container>
                    <S.InputContainer>
                        <input
                            css={[
                                FormStyles.input,
                                form.touched[field.name] && form.errors[field.name]
                                    ? FormStyles.inputError
                                    : null
                            ]}
                            id={name}
                            name={field.name}
                            type="file"
                            onChange={event => handleChange(event, field, form)}
                        />
                    </S.InputContainer>
                </S.Container>
            )}
        </Field>
        <ErrorMessage
            component="span"
            name={name}
            css={FormStyles.error}
            data-testid={`error-${name}`}
        />
    </>
);

export default InputFile;
