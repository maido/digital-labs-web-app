// @flow
import React from 'react';
import {storiesOf} from '@storybook/react';
import {Formik, Form} from 'formik';
import InputFile from './';

const stories = storiesOf('InputFile', module);

stories.add('default', () => {
    return (
        <Formik
            initialValues={{}}
            onSubmit={() => {}}
            render={() => (
                <Form>
                    <InputFile name="avatar" handleChange={() => {}} />
                </Form>
            )}
        />
    );
});
