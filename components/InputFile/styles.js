// @flow
import styled from '@emotion/styled';
import {rem} from 'polished';
import {spacing} from '../../globals/variables';

export const Container = styled.div`
    position: relative;
`;

export const InputContainer = styled.div`
    display: flex;
    flex-wrap: wrap;
    position: relative;
`;

export const PasswordToggle = styled.button`
    position: absolute;
    right: ${rem(spacing.s)};
    top: 50%;
    transform: translateY(-50%);
`;

// .custom-file-input::-webkit-file-upload-button {
//     visibility: hidden;
//   }
//   .custom-file-input::before {
//     content: 'Select some files';
//     display: inline-block;
//     background: linear-gradient(top, #f9f9f9, #e3e3e3);
//     border: 1px solid #999;
//     border-radius: 3px;
//     padding: 5px 8px;
//     outline: none;
//     white-space: nowrap;
//     -webkit-user-select: none;
//     cursor: pointer;
//     text-shadow: 1px 1px #fff;
//     font-weight: 700;
//     font-size: 10pt;
//   }
//   .custom-file-input:hover::before {
//     border-color: black;
//   }
//   .custom-file-input:active::before {
//     background: -webkit-linear-gradient(top, #e3e3e3, #f9f9f9);
//   }
