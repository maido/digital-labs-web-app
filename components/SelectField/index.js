// @flow
import React from 'react';
import get from 'lodash/get';
import {ErrorMessage, Field} from 'formik';
import Select from 'react-select';
import * as FormStyles from '../Form/styles';
import * as UI from '../UI/styles';
import * as S from './styles';

type Props = {
    description?: string,
    label?: string,
    isMulti?: boolean,
    maxOptions?: number,
    name: string,
    noOptionsMessage?: string,
    options: Array<{
        label: string,
        options?: Array<{
            label: string,
            value: string
        }>,
        value?: string
    }>,
    placeholder?: string,
    tip?: string,
    value?: string
};

const SelectField = ({
    description,
    label,
    isMulti = false,
    name,
    maxOptions,
    noOptionsMessage,
    placeholder = 'Select...',
    options,
    tip,
    value
}: Props) => {
    const getValue = (options, field, value) => {
        const defaultValue = field.value || value;

        if (options) {
            if (isMulti) {
                // console.log({isMulti, options, field, value, defaultValue});
                // return options.filter(option => defaultValue.includes(option.value));
                if (
                    defaultValue &&
                    defaultValue.length > 0 &&
                    typeof defaultValue !== 'string' &&
                    typeof defaultValue[0] !== 'object'
                ) {
                    return defaultValue.map(v => ({
                        label: v,
                        value: v
                    }));
                } else {
                    return defaultValue;
                }
            } else {
                if (options[0].options) {
                    return {label: defaultValue, value: defaultValue};
                } else {
                    return options.find(option => option.value === defaultValue);
                }
            }
        }
    };

    return (
        <>
            {label && (
                <label htmlFor={name} css={FormStyles.label}>
                    {label}
                    {tip && <span css={[UI.label, FormStyles.tip]}>{tip}</span>}
                </label>
            )}
            {description && (
                <span
                    css={FormStyles.description}
                    dangerouslySetInnerHTML={{__html: description}}
                />
            )}
            <Field name={name}>
                {({field, form}) => {
                    if (isMulti && field.value && field.value.length === maxOptions) {
                        options = [];
                    }

                    return (
                        <Select
                            options={options}
                            id={field.name}
                            name={field.name}
                            isMulti={isMulti}
                            noOptionsMessage={() => (isMulti ? noOptionsMessage : null)}
                            onChange={option => {
                                if (isMulti) {
                                    form.setFieldValue(
                                        field.name,
                                        option ? option.map(o => o.value) : ''
                                    );
                                } else {
                                    form.setFieldValue(field.name, option ? option.value : '');
                                }
                            }}
                            onBlur={field.onBlur}
                            placeholder={placeholder}
                            styles={S.select(form.touched[field.name] && form.errors[field.name])}
                            value={getValue(options, field, value)}
                        />
                    );
                }}
            </Field>
            <ErrorMessage
                component="span"
                name={name}
                css={FormStyles.error}
                data-testid={`error-${name}`}
            />
        </>
    );
};

export default SelectField;
