// @flow
import React from 'react';
import {storiesOf} from '@storybook/react';
import {Formik, Form} from 'formik';
import SelectField from './';
import accountCreationSchema from '../../schemas/create-account';
import {countryList, areasForLearning} from '../../globals/content';

const stories = storiesOf('SelectField', module);

stories.add('default', () => {
    return (
        <Formik
            render={() => (
                <Form>
                    <SelectField
                        name="foo"
                        label="Foo"
                        options={[
                            {label: 'Foo', value: 'foo'},
                            {label: 'Bar', value: 'bar'},
                            {label: 'Baz', value: 'baz'}
                        ]}
                    />
                </Form>
            )}
        />
    );
});

stories.add('without a label', () => {
    return (
        <Formik
            render={() => (
                <Form>
                    {' '}
                    <SelectField
                        name="foo"
                        options={[
                            {label: 'Foo', value: 'foo'},
                            {label: 'Bar', value: 'bar'},
                            {label: 'Baz', value: 'baz'}
                        ]}
                    />
                </Form>
            )}
        />
    );
});

stories.add('with medium range of options', () => {
    return (
        <Formik
            render={() => (
                <Form>
                    <SelectField
                        name="learning"
                        label="Areas for learning"
                        options={areasForLearning.map(c => ({
                            label: c,
                            value: c
                        }))}
                    />
                </Form>
            )}
        />
    );
});

stories.add('with large range of options', () => {
    return (
        <Formik
            render={() => (
                <Form>
                    <SelectField
                        name="country"
                        label="Your country"
                        options={Object.values(countryList).map(c => ({
                            label: c,
                            value: c
                        }))}
                    />
                </Form>
            )}
        />
    );
});

stories.add('has error', () => {
    return (
        <Formik
            initialValues={{country_origin: 'foo'}}
            validationSchema={accountCreationSchema.step2}
            onSubmit={values => console.log(values)}
            render={props => {
                return (
                    <Form>
                        <SelectField
                            name="country_origin"
                            label="Your country"
                            options={[
                                ...countryList.map(c => ({
                                    label: c,
                                    value: c
                                })),
                                {value: 'foo', label: 'foo'}
                            ]}
                            defaultValue="foo"
                        />
                    </Form>
                );
            }}
        />
    );
});

stories.add('has field disabled', () => {
    return (
        <Formik
            render={() => (
                <Form>
                    <SelectField
                        name="country"
                        label="Your country"
                        options={Object.values(countryList).map(c => ({
                            label: c,
                            value: c
                        }))}
                    />
                </Form>
            )}
        />
    );
});

stories.add('has multiple options (no limit)', () => {
    return (
        <Formik
            render={() => (
                <Form>
                    <SelectField
                        name="country"
                        label="Your country"
                        options={Object.values(countryList).map(c => ({
                            label: c,
                            value: c
                        }))}
                        isMulti={true}
                    />
                </Form>
            )}
        />
    );
});

stories.add('has multiple options (with limit)', () => {
    return (
        <Formik
            render={() => (
                <Form>
                    <SelectField
                        name="country"
                        label="Your country"
                        options={Object.values(countryList).map(c => ({
                            label: c,
                            value: c
                        }))}
                        isMulti={true}
                        maxOptions={3}
                        noOptionsMessage="3 of 3 selected"
                        tip="Select up to 3"
                    />
                </Form>
            )}
        />
    );
});

stories.add('has grouped options', () => {
    return (
        <Formik
            render={() => (
                <Form>
                    <SelectField
                        name="country"
                        label="Your country"
                        options={[
                            {
                                label: 'Foo',
                                options: [
                                    {value: 'Foo', label: 'Foo'},
                                    {value: 'Bar', label: 'Bar'},
                                    {value: 'Baz', label: 'Baz'}
                                ]
                            },
                            {
                                label: 'Bar',
                                options: [
                                    {value: 'Foo', label: 'Foo'},
                                    {value: 'Bar', label: 'Bar'},
                                    {value: 'Baz', label: 'Baz'}
                                ]
                            },
                            {
                                label: 'Baz',
                                options: [
                                    {value: 'Foo', label: 'Foo'},
                                    {value: 'Bar', label: 'Bar'},
                                    {value: 'Baz', label: 'Baz'}
                                ]
                            }
                        ]}
                    />
                </Form>
            )}
        />
    );
});
