// @flow
import {css} from '@emotion/core';
import {rem} from 'polished';
import {colors, fontSizes, spacing, themes} from '../../globals/variables';

const getBorderColor = (isSelected, hasError) => {
    if (hasError) {
        return colors.orangeyRed;
    } else if (isSelected) {
        return themes.activeFormField.border;
    } else {
        return themes.inactiveFormField.border;
    }
};

export const select = (hasError = false) => ({
    control: (provided: Object, state: Object) => ({
        ...provided,
        borderColor: getBorderColor(state.isSelected, hasError),
        borderRadius: rem(4),
        cursor: 'pointer',
        fontSize: rem(fontSizes.default),
        minHeight: 46,
        padding: rem(3.5)
    }),
    dropdownIndicator: (provided: Object, state: Object) => ({
        ...provided,
        color: themes.selectedFormField.background
    }),
    groupHeading: (provided: Object, state: Object) => ({
        ...provided,
        color: themes.selectedFormField.heading,
        fontWeight: 700
    }),
    indicatorSeparator: (provided: Object, state: Object) => ({
        display: 'none'
    }),
    multiValue: (provided: Object, state: Object) => ({
        ...provided,
        alignItems: 'center',
        backgroundColor: themes.selectedFormField.background,
        borderRadius: rem(50),
        color: themes.selectedFormField.text,
        fontSize: rem(fontSizes.default),
        margin: `${rem(2)} ${rem(4)} ${rem(2)} 0`,
        padding: `${rem(2)} ${rem(8)}`
    }),
    multiValueRemove: (provided: Object, state: Object) => ({
        ...provided,
        borderRadius: '100%',
        cursor: 'pointer',
        height: 20,
        width: 20
    }),
    option: (provided: Object, state: Object) => {
        let backgroundColor = themes.inactiveFormField.background;
        let color = themes.selectedFormField.text;

        if (state.isFocused) {
            backgroundColor = themes.activeFormField.background;
            color = themes.activeFormField.text;
        } else if (state.isSelected) {
            backgroundColor = themes.selectedFormField.background;
            color = themes.selectedFormField.text;
        }

        return {
            ...provided,
            backgroundColor,
            color,
            fontSize: rem(fontSizes.default),
            cursor: 'pointer',
            zIndex: 100
        };
    }
});
