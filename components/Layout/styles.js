// @flow
import {css, keyframes} from '@emotion/core';
import styled from '@emotion/styled';
import {rem} from 'polished';
import {responsiveRem} from '../../globals/functions';
import {breakpoints, spacing} from '../../globals/variables';

type ContainerProps = {
    flex?: boolean
};

export const container = (props: ContainerProps) => css`
    ${props.flex &&
        `display: flex;
        flex-direction: column;
        flex-wrap: wrap;
        width: 100%;`}
`;

export const stickyHeaderWrapper = css`
    position: relative;
    width: 100%;
    z-index: 201;

    @media (min-width: ${rem(breakpoints.mobile)}) {
        /**
         * Disable sticky compensation on desktop
         */
        > div > div {
            padding: 0 !important;
        }
    }
`;

const stickyEntranceAnimation = keyframes`
    to {
        transform: translateZ(0) translateY(0);
    }
`;

export const StickyHeaderContainer = styled.div`
    z-index: 201;

    ${props =>
        props.isSticky &&
        `
        // animation: ${stickyEntranceAnimation} 1s;
        // animation-fill-mode: forwards;
        left: 0;
        position: fixed;
        top: 0;
        // transform: translateZ(0) translateY(-100px);
        transform: translateZ(0) translateY(0);
        width: 100%;
    `};

    @media (min-width: ${rem(breakpoints.mobile)}) {
        position: relative !important;
    }
`;

export const Main = styled.main`
    display: flex;
    flex-direction: column;
    flex-grow: 1;
    max-width: 100%;
`;

export const footer = css`
    padding-bottom: ${responsiveRem(spacing.m)};
    padding-top: ${responsiveRem(spacing.m)};
    width: 100%;
`;
