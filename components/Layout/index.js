// @flow
import * as React from 'react';
import {ThemeProvider} from 'emotion-theming';
import {connect} from 'react-redux';
import {initGA, logPageView} from '../../globals/analytics';
import {themes} from '../../globals/variables';
import Container from '../Container';
import OverlayNavigation from '../OverlayNavigation';
import SiteFooter from '../SiteFooter';
import SiteHeader from '../SiteHeader';
import Theme from '../Theme';
import * as S from './styles';

type Props = {
    children: React.Node,
    hasSiteFooter?: boolean,
    hasSiteHeader?: boolean,
    theme?: string,
    type?: string,
    user: User
};

const Layout = ({
    children,
    hasSiteFooter = true,
    hasSiteHeader = true,
    theme = 'secondary',
    type,
    user
}: Props) => {
    const [isOverlayNavActive, setIsOverlayNavActive] = React.useState(false);

    const handleHamburgerClick = () => setIsOverlayNavActive(!isOverlayNavActive);

    React.useEffect(() => {
        if (!window.GA_INITIALIZED) {
            initGA();
            window.GA_INITIALIZED = true;
        }

        logPageView();
    }, []);

    return (
        <ThemeProvider theme={themes[theme]}>
            <Theme theme={theme} isPageTheme={true} css={S.container({flex: type !== 'topic'})}>
                {hasSiteHeader && (
                    <SiteHeader
                        handleHamburgerClick={handleHamburgerClick}
                        isOverlayNavActive={isOverlayNavActive}
                    />
                )}

                {user.isAuthenticated && isOverlayNavActive && (
                    <OverlayNavigation
                        user={user}
                        isActive={isOverlayNavActive}
                        handleClick={handleHamburgerClick}
                    />
                )}

                <S.Main>{children}</S.Main>

                {hasSiteFooter && (
                    <Theme theme="white" css={S.footer}>
                        <Container paddingVertical={0}>
                            <SiteFooter user={user} />
                        </Container>
                    </Theme>
                )}
            </Theme>
        </ThemeProvider>
    );
};
const mapStateToProps = state => ({
    notifications: state.notifications,
    user: state.user
});

export default connect(mapStateToProps)(Layout);
