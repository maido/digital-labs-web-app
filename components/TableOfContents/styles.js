// @flow
import {css} from '@emotion/core';
import styled from '@emotion/styled';
import {rem} from 'polished';
import {colors, fontFamilies, fontSizes, spacing, transitions} from '../../globals/variables';

export const Container = styled.nav``;

export const Label = styled.span`
    display: inline-block;
    margin-bottom: ${rem(spacing.s)};
`;

export const Link = styled.a`
    border-left: 3px solid ${colors.primary};
    display: block;
    font-family: ${fontFamilies.bold};
    font-size: ${rem(fontSizes.default)};
    font-style: normal;
    font-weight: normal;
    line-height: 1.2;
    margin-top: 0;
    padding-left: ${rem(spacing.s)};
    padding-top: ${rem(3)};

    &:hover,
    &:active {
        transform: translateX(4px);
    }

    & + & {
        margin-top: ${rem(spacing.s)};
    }
`;
