// @flow
import React, {useState} from 'react';
import * as S from './styles';
import __ from '../../globals/strings';

type Props = {
    items: Array<{
        heading: string,
        slug: string
    }>
};

const TableOfContents = ({items}: Props) => {
    const [$headings, set$headings] = useState({});

    const handleScrollToContent = (event, slug: string) => {
        const $heading = $headings[slug] || document.querySelector(`#${slug}`);

        if ($heading) {
            event.preventDefault();
            window.scrollTo({
                behavior: 'smooth',
                top: $heading.getBoundingClientRect().top + window.scrollY - 20
            });

            if (!$headings[slug]) {
                set$headings({
                    ...$headings,
                    [slug]: $heading
                });
            }
        }
    };

    return (
        <S.Container>
            <S.Label>{__('learningContent.assignment.tableOfContents')}</S.Label>

            {items.map(item => (
                <S.Link
                    key={item.slug}
                    href={`#${item.slug}`}
                    onClick={event => handleScrollToContent(event, item.slug)}
                >
                    {item.heading}
                </S.Link>
            ))}
        </S.Container>
    );
};

export default TableOfContents;
