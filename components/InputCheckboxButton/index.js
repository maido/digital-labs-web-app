// @flow
import React from 'react';
import {Field} from 'formik';
import * as S from '../InputRadio/styles';

type Props = {
    name: string,
    max?: number,
    value: string
};

const InputCheckbox = ({name, max, value, ...props}: Props) => (
    <Field name={name}>
        {({field, form}) => (
            <S.Container>
                <S.Input
                    id={`${name}-${value}`}
                    type="checkbox"
                    name={name}
                    value={value}
                    checked={field.value && field.value.includes(value)}
                    onChange={e => {
                        let nextValue;

                        if (field.value && field.value.includes(value)) {
                            nextValue = field.value.filter(v => v !== value);
                        } else {
                            if (max && field.value && typeof field.value !== 'string') {
                                if (field.value.length === max) {
                                    return;
                                }
                            }

                            nextValue = [...field.value, value];
                        }

                        form.setFieldValue(name, nextValue);
                    }}
                    {...props}
                />
                <S.Label htmlFor={`${name}-${value}`}>{value}</S.Label>
            </S.Container>
        )}
    </Field>
);

export default InputCheckbox;
