// @flow
import React from 'react';
import axios from 'axios';
import {connect} from 'react-redux';
import {Formik, Form} from 'formik';
import __ from '../../globals/strings';
import {updateForm, updateUser} from '../../store/actions';
import accountEditSchema from '../../schemas/edit-account';
import {getSchemaDefaultFieldValues} from '../../globals/functions';
import {areasOfInterest, countryList, heardVia, prizes} from '../../globals/content';
import AnimateWhenVisible from '../AnimateWhenVisible';
import AvatarUpload from '../AvatarUpload';
import BreadcrumbNav from '../BreadcrumbNav';
import Container from '../Container';
import CTAButton from '../CTAButton';
import {label} from '../Form/styles';
import InputButtonGroup from '../InputButtonGroup';
import InputCheckbox from '../InputCheckbox';
import InputRadio from '../InputRadio';
import InputText from '../InputText';
import SelectField from '../SelectField';
import * as UI from '../UI/styles';
import * as S from './styles';

type Props = {
    dispatch: Function,
    form: Form,
    user: Object
};

const AccountEditPageLayout = ({dispatch, form, user}: Props) => {
    const handleFormSubmit = async (values, handlers) => {
        try {
            const response = await axios.post(`/api/account/update?id=${user.id}`, values);

            dispatch(updateForm({state: 'success', fields: values}));
            dispatch(updateUser(response.data));
            handlers.setSubmitting(false);

            setTimeout(() => {
                dispatch(updateForm({state: 'default'}));
            }, 1500);
        } catch (error) {
            dispatch(updateForm({state: 'default'}));

            const errors = error.errors
                ? error.errors
                : {
                      email_notifications: 'Sorry, we could not update your user. Please try again.'
                  };
            handlers.setErrors(errors);
            handlers.setSubmitting(false);
        }
    };

    const handleAvatarChange = response => {
        if (response.data) {
            dispatch(updateUser({avatar: response.data}));
        }
    };

    const initialValues = {...getSchemaDefaultFieldValues(accountEditSchema.fields), ...user};

    if (!initialValues.heard_via) {
        initialValues.heard_via = 'n/a';
    }

    return (
        <Container paddingVertical="l">
            <BreadcrumbNav
                links={[
                    {label: 'Account', url: {href: '/users/[userId]', as: `/users/${user.id}`}},
                    {label: 'Edit account', url: '/account/edit'}
                ]}
            />

            <AvatarUpload
                apiUrl={`api/users/${user.id}/avatars`}
                avatar={user.avatar ? user.avatar.url : ''}
                handleSuccess={handleAvatarChange}
                label="Change profile photo"
            />

            <UI.Spacer />

            <Formik
                initialValues={initialValues}
                onSubmit={handleFormSubmit}
                validationSchema={accountEditSchema}
                render={formProps => (
                    <Form encType="multipart/form-data" autoComplete="off">
                        <AnimateWhenVisible delay={150}>
                            <UI.Box css={S.container}>
                                <UI.Heading5 as="h2" css={UI.formHeading}>
                                    Account details
                                </UI.Heading5>
                                <UI.LayoutContainer type="matrix">
                                    <UI.LayoutItem sizeAtMobile={6 / 12}>
                                        <InputText
                                            name="first_name"
                                            label="First name"
                                            placeholder="John"
                                        />
                                    </UI.LayoutItem>
                                    <UI.LayoutItem sizeAtMobile={6 / 12}>
                                        <InputText
                                            name="last_name"
                                            label="Last name"
                                            placeholder="Doe"
                                        />
                                    </UI.LayoutItem>
                                    <UI.LayoutItem sizeAtMobile={6 / 12}>
                                        <InputText
                                            name="email"
                                            label="Email"
                                            placeholder="johndoe@gmail.com"
                                        />
                                    </UI.LayoutItem>
                                    <UI.LayoutItem sizeAtMobile={6 / 12}>
                                        <InputText
                                            name="birthdate"
                                            label="Date of Birth"
                                            type="date"
                                        />
                                    </UI.LayoutItem>
                                    <UI.LayoutItem sizeAtMobile={6 / 12}>
                                        <SelectField
                                            label="Country of origin"
                                            name="country_origin"
                                            options={Object.values(countryList).map(c => ({
                                                label: c,
                                                value: c
                                            }))}
                                            placeholder="Select a country..."
                                            value={user.country_origin}
                                        />
                                    </UI.LayoutItem>
                                    <UI.LayoutItem sizeAtMobile={6 / 12}>
                                        <SelectField
                                            label="Country of residence"
                                            name="country_residence"
                                            options={Object.values(countryList).map(c => ({
                                                label: c,
                                                value: c
                                            }))}
                                            placeholder="Select a country..."
                                            value={user.country_residence}
                                        />
                                    </UI.LayoutItem>
                                    <UI.LayoutItem sizeAtMobile={6 / 12}>
                                        <InputText name="university" label="University" />
                                    </UI.LayoutItem>
                                    <UI.LayoutItem sizeAtMobile={6 / 12}>
                                        <InputText
                                            name="field_of_expertise"
                                            label="Field of expertise"
                                        />
                                    </UI.LayoutItem>
                                    <UI.LayoutItem sizeAtMobile={6 / 12}>
                                        <InputText name="linkedin" label="LinkedIn Profile" />
                                    </UI.LayoutItem>
                                    <UI.LayoutItem sizeAtMobile={6 / 12}>
                                        <InputText name="slack" label="Slack username" />
                                    </UI.LayoutItem>
                                </UI.LayoutContainer>

                                <UI.ResponsiveSpacer />

                                <UI.Heading5 as="h2" css={UI.formHeading}>
                                    Learning content
                                </UI.Heading5>

                                <UI.LayoutContainer type="matrix">
                                    <UI.LayoutItem>
                                        <SelectField
                                            label="What are your areas of interest?"
                                            name="areas_of_interest"
                                            options={areasOfInterest.map(option => ({
                                                label: option.label,
                                                options: option.options.map(o => ({
                                                    value: o,
                                                    label: o
                                                }))
                                            }))}
                                            placeholder="Select an option..."
                                            isMulti={true}
                                            maxOptions={3}
                                            noOptionsMessage="3 of 3 selected"
                                            tip="Select up to 3"
                                            value={user.areas_of_interest}
                                        />
                                    </UI.LayoutItem>
                                    <UI.LayoutItem>
                                        <SelectField
                                            label="What are you looking to learn more about?"
                                            name="areas_of_learning"
                                            options={areasOfInterest.map(option => ({
                                                label: option.label,
                                                options: option.options.map(o => ({
                                                    value: o,
                                                    label: o
                                                }))
                                            }))}
                                            placeholder="Select an option..."
                                            isMulti={true}
                                            maxOptions={3}
                                            noOptionsMessage="3 of 3 selected"
                                            tip="Select up to 3"
                                            value={user.areas_of_learning}
                                        />
                                    </UI.LayoutItem>
                                </UI.LayoutContainer>

                                <UI.ResponsiveSpacer />

                                <UI.Heading5 as="h2" css={UI.formHeading}>
                                    Update your password
                                </UI.Heading5>

                                <UI.LayoutContainer>
                                    <UI.LayoutItem sizeAtMobile={6 / 12}>
                                        <InputText
                                            name="password"
                                            label="Password"
                                            type="password"
                                            autoComplete="off"
                                        />
                                    </UI.LayoutItem>
                                    <UI.LayoutItem sizeAtMobile={6 / 12}>
                                        <InputText
                                            name="password_confirmation"
                                            label="Confirm password"
                                            type="password"
                                            autoComplete="off"
                                        />
                                    </UI.LayoutItem>
                                </UI.LayoutContainer>

                                <UI.ResponsiveSpacer />

                                <UI.Heading5 as="h2" css={UI.formHeading}>
                                    Your preferences
                                </UI.Heading5>

                                <UI.LayoutContainer type="matrix">
                                    <UI.LayoutItem>
                                        <span css={label}>Email preferences</span>
                                        <InputCheckbox
                                            name="accepts_marketing"
                                            label="Receive email updates on team progress and new content"
                                            onClick={() => {
                                                formProps.setFieldValue('accepts_marketing', true);
                                            }}
                                        />
                                    </UI.LayoutItem>
                                </UI.LayoutContainer>

                                <UI.ResponsiveSpacer />
                                <div css={UI.centerText}>
                                    <CTAButton
                                        disabled={formProps.isSubmitting || formProps.isValidating}
                                        type="submit"
                                        state={
                                            formProps.isSubmitting || formProps.isValidating
                                                ? 'pending'
                                                : form.state
                                        }
                                        wide={true}
                                    >
                                        Update your profile
                                    </CTAButton>
                                </div>
                            </UI.Box>
                        </AnimateWhenVisible>
                    </Form>
                )}
            />
        </Container>
    );
};

const mapStateToProps = state => ({form: state.form, user: state.user});

export default connect(mapStateToProps)(AccountEditPageLayout);
