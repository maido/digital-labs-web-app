// @flow
import {css} from '@emotion/core';

export const container = css`
    margin-left: auto;
    margin-right: auto;
    max-width: 700px;
`;
