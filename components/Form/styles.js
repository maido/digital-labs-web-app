// @flow
import {css} from '@emotion/core';
import {between, rem} from 'polished';
import {
    breakpoints,
    colors,
    fontFamilies,
    fontSizes,
    spacing,
    themes,
    transitions
} from '../../globals/variables';

export const label = css`
    display: block;
    font-family: ${fontFamilies.bold};
    font-weight: normal;
    font-size: ${rem(16)};
    line-height: 1.2;
    margin-bottom: ${between(`${spacing.xs}px`, `${spacing.xs * 1.2}px`)};

    @media (max-width: ${rem(breakpoints.mobile)}) {
        margin-top: ${rem(spacing.xs)};
    }
`;

export const description = css`
    color: ${colors.greyDarkest};
    display: block;
    line-height: 1.4;
    margin-bottom: ${rem(spacing.s)};
    margin-top: ${rem(spacing.xs)};

    a,
    b,
    strong {
        font-family: ${fontFamilies.bold};
        font-weight: 500 !important;
    }

    b,
    strong {
        color: ${colors.darkGreyBlue};
    }
`;

export const input = css`
    appearance: none;
    background-image: none !important;
    border: 1px solid ${themes.inactiveFormField.border};
    border-radius: ${rem(4)};
    color: ${themes.activeFormField.text};
    font-family: ${fontFamilies.default};
    font-size: ${rem(fontSizes.default)};
    line-height: 1.4;
    margin: 0;
    padding: ${rem(spacing.s)};
    transition: ${transitions.default};
    width: 100%;

    &::placeholder {
        color: ${themes.inactiveFormField.text};
    }

    &:focus {
        border-color: ${themes.activeFormField.border};
        color: ${themes.activeFormField.text};
        outline: none;

        + button {
            border-color: ${themes.activeFormField.border};
        }
    }
`;

export const inputError = css`
    border-color: ${colors.red};

    + button {
        border-color: ${colors.red};
    }
`;

export const error = css`
    color: ${colors.red};
    display: block;
    font-style: italic;
    line-height: 1.3;
    margin-top: ${rem(spacing.xs)};
`;

export const tip = css`
    color: ${colors.greyDark};
    float: right;

    @media (max-width: ${rem(breakpoints.mobile)}) {
        display: block;
        float: none;
        padding-top: ${rem(spacing.s)};
    }
`;
