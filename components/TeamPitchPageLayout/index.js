// @flow
import React, {useRef, useState} from 'react';
import dynamic from 'next/dynamic';
import axios from 'axios';
import isArray from 'lodash/isArray';
import flatMap from 'lodash/flatMap';
import get from 'lodash/get';
import bugsnagClient from '../../globals/bugsnag';
import {scrollToTop} from '../../globals/functions';
import __ from '../../globals/strings';
import AnimateWhenVisible from '../AnimateWhenVisible';
import Container from '../Container';
import CTAButton from '../CTAButton';
import FullScreenMessage from '../FullScreenMessage';
import * as FormStyles from '../Form/styles';
import * as UI from '../UI/styles';
import * as S from './styles';

const Modal = dynamic(() => import('../Modal'), {ssr: false});
const TypeForm = dynamic(() => import('../ContentBlocks/TypeFormBlockPopup'), {ssr: false});

type Props = {
    team: Team,
    user: User
};

const TeamPitchPageLayout = ({team, user}: Props) => {
    const [modalVisibility, setModalVisibility] = useState('hidden');

    const handleComplete = () => {
        setModalVisibility('visible');
    };

    return (
        <>
            {modalVisibility !== 'hidden' && (
                <FullScreenMessage
                    badge="medal"
                    cta={{
                        label: 'Return to your dashboard',
                        url: '',
                        onClick: () => (window.top.location = '/dashboard')
                    }}
                    state={modalVisibility}
                    title={__('pitch.success.title')}
                    text={__('pitch.success.text')}
                />
            )}
            <Container css={S.pageContainer} paddingVertical="l">
                <UI.Heading3 as="h1" css={[UI.centerText, UI.margin('bottom', 'm')]}>
                    Rising Star Agri-Food Innovation Challenge 2021
                </UI.Heading3>

                <section css={S.contentContainer}>
                    <div css={S.formContentContainer}>
                        <AnimateWhenVisible watch={false}>
                            <UI.Box data-testid="pitch-details">
                                <p>
                                    It's time to submit your pitch! Pitching is certainly an art,
                                    and when it is done well it will win over the minds and hearts
                                    of your key audiences, and help to ensure that investments roll
                                    in. The most important thing to remember in mastering this skill
                                    is to be simple, clear, and focused.
                                </p>
                                <p>
                                    To keep things as straightforward as possible for you and for
                                    us, we ask that you submit all of your pitch materials according
                                    to the instructions indicated below.
                                </p>
                                <p>
                                    <strong>Before you begin:</strong>
                                    <ul>
                                        <li>
                                            Review your Team settings: all details must be correct{' '}
                                        </li>
                                        <li>
                                            Have your pitch prepared (you may want to submit via
                                            your desktop or laptop computer)
                                        </li>
                                        <li>
                                            Remember you have until May 28th to submit your pitch{' '}
                                        </li>
                                        <li>
                                            However, once you submit your pitch, you can’t change it
                                        </li>
                                        <li>
                                            Review the{' '}
                                            <UI.TextLink
                                                href="/terms-and-conditions"
                                                target="_blank"
                                            >
                                                terms and conditions
                                            </UI.TextLink>
                                        </li>
                                    </ul>
                                </p>
                                <p>
                                    <strong>Ready? Let's get going!</strong>
                                </p>
                                <TypeForm
                                    handleComplete={handleComplete}
                                    label="Start pitch"
                                    url="https://form.typeform.com/to/xK04f4HS"
                                />
                                <UI.Spacer />
                                <small
                                    dangerouslySetInnerHTML={{
                                        __html: __('pitch.intro.footnote')
                                    }}
                                    css={UI.textColor('greyDark')}
                                />
                            </UI.Box>
                        </AnimateWhenVisible>
                    </div>
                </section>
            </Container>
        </>
    );
};

export default TeamPitchPageLayout;
