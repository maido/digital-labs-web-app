// @flow
import {css} from '@emotion/core';
import styled from '@emotion/styled';
import {rem, shade} from 'polished';
import {breakpoints, fontFamilies, spacing, themes, transitions} from '../../globals/variables';

export const pageContainer = css`
    @media (max-width: ${rem(breakpoints.mobile)}) {
        padding-top: ${rem(spacing.m)};
    }

    @media (min-width: ${rem(breakpoints.mobile)}) {
        display: flex;
        flex-direction: column;
        flex-grow: 1;
    }
`;

export const contentContainer = css`
    display: flex;
    flex-direction: column;
    flex-grow: 1;
    height: 100%;
    margin-bottom: ${rem(spacing.m)};
    margin-left: auto;
    margin-right: auto;
    max-width: ${rem(700)};
    width: 100%;
`;

export const formContentContainer = css`
    display: flex;
    flex-direction: column;
    flex-grow: 1;
    margin-top: ${rem(spacing.m)};

    > [data-testid='pitch-details'] {
        display: flex;
        flex-grow: 1;

        iframe {
            min-height: 100%;
            min-width: 100%;
        }
    }
`;

export const informationContainer = css`
    @media (min-width: ${rem(breakpoints.mobile)}) {
        margin: 0 auto;
        max-width: ${rem(500)};
        text-align: center;
        width: 100%;

        /**
         * Target the wrapping container div used for checkboxes
         */
        > div {
            justify-content: center;
        }
    }
`;

export const PlusIcon = styled.svg`
    height: ${rem(spacing.m)};
    margin-right: ${rem(spacing.xs)};
    transition: ${transitions.default};
    width: ${rem(spacing.m)};
    will-change: transform;

    path {
        fill: ${themes.tertiary.text};
    }
`;

export const AddDocumentsButton = styled.button`
    align-items: center;
    background-color: ${themes.tertiary.background};
    border-radius: ${rem(5)};
    color: ${themes.tertiary.text};
    display: flex;
    font-family: ${fontFamilies.bold};
    height: 100%;
    min-height: 80px;
    justify-content: center;
    padding: ${rem(spacing.m)};
    transition: ${transitions.slow};
    width: 100%;

    &:hover,
    &:focus {
        background-color: ${shade(0.05, themes.tertiary.background)};

        ${PlusIcon} {
            transform: rotate(90deg) scale(1.05);
        }
    }
`;
