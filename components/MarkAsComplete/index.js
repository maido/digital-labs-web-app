// @flow
import React, {useState} from 'react';
import axios from 'axios';
import {connect} from 'react-redux';
import __ from '../../globals/strings';
import {updateSyllabusContent} from '../../store/actions';
import CTAButton from '../CTAButton';
import CTATextBanner from '../CTATextBanner';
import FullScreenMessage from '../FullScreenMessage';
import * as UI from '../UI/styles';
import * as S from './styles';

type Props = {
    completedAt?: string,
    courseId: number,
    dispatch: Function,
    topicId: number,
    theme: string
};

const MarkAsComplete = ({completedAt, courseId, dispatch, topicId, theme}: Props) => {
    const [buttonState, setButtonState] = useState('default');
    const [modalVisibility, setModalVisibility] = useState('hidden');
    const [error, setError] = useState(null);

    const handleMarkAsComplete = async () => {
        try {
            setButtonState('pending');

            const response = await axios.post(`/api/labs/read-topic?id=${topicId}`);

            setButtonState('success');
            setModalVisibility('visible');

            setTimeout(() => {
                dispatch(
                    updateSyllabusContent('topics', [{id: topicId, completed_at: new Date()}])
                );
            }, 1500);
        } catch (e) {
            setError(__('learningContent.markAsComplete.errors.complete'));
            setButtonState('default');

            setTimeout(() => setError(null), 3000);
        }
    };

    if (completedAt) {
        return (
            <>
                <FullScreenMessage
                    badge="generic-3"
                    cta={{
                        label: 'Back to all tasks',
                        onClick: () => {
                            /**
                             * Force a refresh to reset cached syllabus data
                             */
                            window.location = `/labs/courses/${courseId}`;
                        }
                    }}
                    key={topicId}
                    state={modalVisibility}
                    title={__('learningContent.markAsComplete.success.title')}
                    text={__('learningContent.markAsComplete.success.text')}
                />

                <CTATextBanner
                    theme={theme}
                    title={__('learningContent.markAsComplete.success.title')}
                    text={__('learningContent.markAsComplete.success.text')}
                />
            </>
        );
    } else {
        return (
            <CTATextBanner
                theme={theme}
                title={__('learningContent.markAsComplete.title')}
                text={__('learningContent.markAsComplete.text')}
            >
                <CTAButton theme="white" onClick={handleMarkAsComplete} state={buttonState}>
                    <span css={UI.textColor('black')}>
                        {__('learningContent.markAsComplete.ctaLabel')}
                    </span>
                </CTAButton>

                {error && (
                    <>
                        <UI.Spacer size="s" />
                        <UI.ErrorMessage css={UI.shake}>{error}</UI.ErrorMessage>
                    </>
                )}
            </CTATextBanner>
        );
    }
};

export default connect()(MarkAsComplete);
