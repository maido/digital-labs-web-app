// @flow
import React from 'react';
import {storiesOf} from '@storybook/react';
import MarkAsComplete from './';

const stories = storiesOf('MarkAsComplete', module);

stories.add('default', () => {
    return <MarkAsComplete theme="primary" topicId={1} />;
});

stories.add('with completed topic', () => {
    return <MarkAsComplete completedAt={new Date()} theme="primary" topicId={1} />;
});
