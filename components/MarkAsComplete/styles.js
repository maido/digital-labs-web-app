// @flow
import {css} from '@emotion/core';
import styled from '@emotion/styled';
import {rem} from 'polished';
import {colors, fontFamilies, spacing, transitions} from '../../globals/variables';

type ImageProps = {
    isActive?: boolean
};

export const container = css`
    display: flex;
    flex-direction: row;
    justify-content: center;
`;

export const image = (props: ImageProps) => css`
    background-repeat: no-repeat;
    background-size: cover;
    border-radius: 100%;
    margin-bottom: ${rem(spacing.s)};
    margin-left: auto;
    margin-right: auto;
    overflow: hidden;
    transition: ${transitions.default};
    vertical-align: top;

    ${props.isActive
        ? `background-color: ${colors.aquaMarine}; transform:scale(1.025); opacity: 0.75;`
        : ''}
`;

export const label = css`
    color: ${colors.brightSkyBlue};
    font-family: ${fontFamilies.bold};
    text-align: center;
`;

export const AvatarContainer = styled.div`
    cursor: pointer;
    color: ${colors.purplishBlue};
    display: flex;
    flex-direction: column;
    font-family: ${fontFamilies.bold};
    justify-content: center;
    outline: none;
    text-align: center;

    &:hover img,
    &:focus img {
        background-color: ${colors.aquaMarine};
        transform: scale(1.025);
    }

    &:hover span,
    &:focus span {
        opacity: 0.6;
    }
`;

export const SpinnerContainer = styled.div`
    color: ${colors.white};
    position: relative;

    > div {
        left: 50%;
        position: absolute;
        top: 45%;
        transform: translateX(-50%) translateY(-50%);
    }
`;
