// @flow
import React from 'react';
import {addDecorator, storiesOf} from '@storybook/react';
import {withKnobs, boolean, object, select, text} from '@storybook/addon-knobs';
import TeamOverviewBanner from './';
import Theme from '../Theme';

const themeOptions = {
    Primary: 'primary',
    Secondary: 'secondary',
    Tertiary: 'tertiary',
    White: 'white'
};
const stories = storiesOf('TeamOverviewBanner', module);

stories.addDecorator(withKnobs);

stories.addDecorator(story => (
    <Theme theme={select('Theme', themeOptions, 'secondary')}>
        <div style={{padding: 30}}>{story()}</div>
    </Theme>
));

stories.add('default', () => {
    return (
        <TeamOverviewBanner
            team={{
                name: 'Safi Organics',
                avatar: {url: 'https://placehold.it/200x200'},
                members: [
                    {
                        first_name: 'Foo',
                        last_name: 'Bar',
                        avatar: {url: 'https://placehold.it/50x50'}
                    }
                ]
            }}
        />
    );
});

stories.add('with completed profile', () => {
    return (
        <TeamOverviewBanner
            team={{
                name: 'Safi Organics',
                mission: 'foo',
                avatar: {url: 'https://placehold.it/200x200'},
                members: [
                    {
                        first_name: 'Foo',
                        last_name: 'Bar',
                        avatar: {url: 'https://placehold.it/50x50'}
                    }
                ]
            }}
        />
    );
});

stories.add('with additional team members', () => {
    return (
        <TeamOverviewBanner
            team={{
                name: 'Safi Organics',
                mission: 'foo',
                avatar: {url: 'https://placehold.it/200x200'},
                members: [
                    {
                        first_name: 'Foo',
                        last_name: 'Bar',
                        avatar: {url: 'https://placehold.it/50x50'}
                    },
                    {
                        first_name: 'Foo',
                        last_name: 'Bar',
                        avatar: {url: 'https://placehold.it/50x50'}
                    },
                    {
                        first_name: 'Foo',
                        last_name: 'Bar',
                        avatar: {url: 'https://placehold.it/50x50'}
                    },
                    {
                        first_name: 'Foo',
                        last_name: 'Bar',
                        avatar: {url: 'https://placehold.it/50x50'}
                    }
                ]
            }}
        />
    );
});

stories.add('playground', () => {
    return (
        <TeamOverviewBanner
            team={object('Team', {
                name: 'Safi Organics',
                mission: 'foo',
                avatar: {url: 'https://placehold.it/200x200'},
                members: [
                    {
                        first_name: 'Foo',
                        last_name: 'Bar',
                        avatar: {url: 'https://placehold.it/50x50'}
                    },
                    {
                        first_name: 'Foo',
                        last_name: 'Bar',
                        avatar: {url: 'https://placehold.it/50x50'}
                    },
                    {
                        first_name: 'Foo',
                        last_name: 'Bar',
                        avatar: {url: 'https://placehold.it/50x50'}
                    },
                    {
                        first_name: 'Foo',
                        last_name: 'Bar',
                        avatar: {url: 'https://placehold.it/50x50'}
                    }
                ]
            })}
        />
    );
});
