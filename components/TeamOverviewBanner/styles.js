// @flow
import {css} from '@emotion/core';
import styled from '@emotion/styled';
import {rem, rgba} from 'polished';
import {
    breakpoints,
    colors,
    fontFamilies,
    fontSizes,
    shadows,
    spacing,
    themes,
    transitions
} from '../../globals/variables';

export const Container = styled.div`
    border-radius: ${rem(5)};
    justify-content: center;
    margin: ${rem(spacing.s)} 0;
    padding: ${rem(spacing.s)} 0;

    @media (min-width: ${rem(breakpoints.mobileSmall)}) {
        align-items: center;
        display: flex;
    }

    @media (min-width: ${rem(breakpoints.mobile)}) {
        margin: ${rem(spacing.m)} 0 0;
    }
`;

export const ContentContainer = styled.div`
    align-items: center;
    display: flex;
    flex-direction: row;

    @media (max-width: ${rem(breakpoints.mobileSmall)}) {
        width: 100%;
    }

    @media (max-width: ${rem(breakpoints.mobileSmall)}) {
        flex-direction: column;
        justify-content: center;
        text-align: center;
    }
`;

export const TitleContainer = styled.div`
    display: block;
    padding-left: ${rem(spacing.s)};

    @media (min-width: ${rem(breakpoints.mobile)}) {
        padding-left: ${rem(spacing.m)};
    }

    @media (max-width: ${rem(breakpoints.mobile)}) {
        margin-top: ${rem(spacing.s)};
        width: 100%;
    }
`;

export const title = css`
    font-size: ${rem(26)};
    font-family: ${fontFamilies.heading};
    line-height: 1.2;

    @media (min-width: ${rem(breakpoints.mobile)}) {
        font-size: ${rem(fontSizes.h2)};
    }
`;

export const teamLinksContainer = css`
    margin-top: ${rem(spacing.xs)};
    white-space: nowrap;

    @media (max-width: ${rem(breakpoints.mobile)}) {
        font-size: ${rem(14)};
    }
`;

export const memberImage = css`
    display: inline-block;
    margin-right: 0;

    & + * {
        margin-left: ${rem(15 * -1)};
        transition: ${transitions.default};
    }
`;

export const addMemberCTA = css`
    border: 2px solid ${themes.secondary.background};
    height: ${rem(40)};
    margin-left: ${rem(15 * -1)};
    padding: 0;
    position: relative;
    width: ${rem(40)};

    @media (max-width: ${rem(breakpoints.mobile)}) {
        border-color: ${themes.white.background};
    }
`;

export const MembersContainer = styled.div`
    align-items: center;
    display: flex;
    font-size: ${rem(fontSizes.h6)};
    font-family: ${fontFamilies.bold};
    position: relative;

    @media (min-width: ${rem(breakpoints.mobile)}) {
        margin-left: ${rem(spacing.l)};

        &::before {
            background-color: ${rgba(themes.white.background, 0.1)};
            content: '';
            height: 150%;
            margin-left: ${rem(spacing.m * -1)};
            position: absolute;
            top: 50%;
            transform: translateY(-50%);
            width: 1px;
        }

        &:hover *,
        &:focus * {
            margin-left: ${rem(2)};
        }
    }

    @media (max-width: ${rem(breakpoints.mobile)}) {
        display: none;
    }
`;

export const AddMemberIcon = styled.svg`
    height: ${rem(14)};
    left: 50%;
    margin-left: 0 !important;
    position: absolute;
    top: 50%;
    width: ${rem(14)};

    &,
    &:hover,
    &:focus {
        transform: translateX(-50%) translateY(-50%) !important;
    }
`;

export const ViewProgressIcon = styled.svg`
    margin-left: ${rem(spacing.xs)};
    transition: ${transitions.default};

    path {
        fill: currentColor;
    }
`;

export const viewProgressCTA = css`
    color: ${colors.aquaMarine};
    font-family: ${fontFamilies.bold};
    text-align: right;

    &:hover,
    &:focus {
        color: ${colors.aquaMarine};
        text-decoration: underline;

        ${ViewProgressIcon} {
            transform: translateX(${rem(3)});
        }
    }

    @media (max-width: ${rem(breakpoints.mobile)}) {
        &,
        &:hover,
        &:focus {
            color: ${colors.purplishBlue};
        }
    }
`;
