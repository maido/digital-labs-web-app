// @flow
import * as React from 'react';
import get from 'lodash/get';
import map from 'lodash/map';
import size from 'lodash/size';
import Link from 'next/link';
import {useSpring, animated} from 'react-spring/web.cjs';
import CTAButton from '../CTAButton';
import {isTeamProfileComplete} from '../../globals/functions';
import * as UI from '../UI/styles';
import * as S from './styles';

type Props = {
    team: Team
};

const TeamOverviewBanner = ({team}: Props) => {
    const avatarAnimation = useSpring({
        config: {tension: 300, mass: 2, friction: 20},
        delay: 100,
        from: {transform: `perspective(600px) rotateX(90deg) `, opacity: 0},
        to: {transform: `perspective(600px) rotateX(0) `, opacity: 1}
    });
    const titleAnimation = useSpring({
        config: {tension: 50, mass: 1, friction: 10},
        delay: 200,
        from: {transform: `translateY(50%)`, opacity: 0},
        to: {transform: `translateY(0)`, opacity: 1}
    });
    const linksAnimation = useSpring({
        config: {tension: 60, mass: 1, friction: 10},
        delay: 300,
        from: {transform: `translateY(50%)`, opacity: 0},
        to: {transform: `translateY(0)`, opacity: 1}
    });

    return (
        <S.Container>
            <S.ContentContainer>
                <animated.div style={avatarAnimation}>
                    <Link href="/teams/[teamId]" as={`/teams/${team.id}`} passHref>
                        <UI.Avatar as="a" image={team.avatar ? team.avatar.url : null} />
                    </Link>
                </animated.div>

                {team.name && (
                    <S.TitleContainer>
                        <animated.div css={S.title} style={titleAnimation}>
                            {team.name}
                        </animated.div>
                        <animated.div css={S.teamLinksContainer} style={linksAnimation}>
                            {!isTeamProfileComplete(team) ? (
                                <CTAButton href="/team/edit" size="small">
                                    Complete your profile
                                </CTAButton>
                            ) : (
                                <>
                                    <Link href="/teams/[teamId]" as={`/teams/${team.id}`}>
                                        <UI.TextLink theme="primary">View team</UI.TextLink>
                                    </Link>
                                    <Link href="/team/edit">
                                        <UI.TextLink theme="primary">Edit team</UI.TextLink>
                                    </Link>
                                </>
                            )}
                        </animated.div>
                    </S.TitleContainer>
                )}
            </S.ContentContainer>
        </S.Container>
    );
};

export default TeamOverviewBanner;
