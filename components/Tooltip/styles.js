// @flow
import styled from '@emotion/styled';
import Tippy from '@tippy.js/react';

export const Tooltip = styled(Tippy)`
    position: relative;

    &::after,
    &::before {
        line-height: 1;
        user-select: none;
        pointer-events: none;
        position: absolute;
        opacity: 0;
        display: block;
        text-transform: none;
    }
    &::before {
        content: '';
        z-index: 9999;
        border: 5px solid transparent;
    }
    &::after {
        content: '';
        z-index: 9998;
        font-size: 14px;
        font-weight: 500;
        min-width: 3em;
        max-width: 21em;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
        padding: 8px 12px;
        border-radius: 8px;
        background: rgba(0, 0, 0, 0.9);
        color: #fff;
    }

    &:hover::after,
    &:hover::before {
        opacity: 1;
        transition: opacity 0.1s ease-in 0.1s;
    }
`;
