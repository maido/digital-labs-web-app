// @flow
import React from 'react';
import Tippy from '@tippy.js/react';
import * as S from './styles';

type Props = {
    content: string,
    style?: Object
};

/**
 * https://github.com/withspectrum/spectrum/src/components/tooltip/index.js
 */
const Tooltip = (props: Props) => {
    const {style = {}, content, ...rest} = props;

    return (
        <S.Tooltip
            placement="top"
            touch={false}
            arrow={true}
            arrowType={'round'}
            content={<span style={{fontSize: '14px', fontWeight: '600', ...style}}>{content}</span>}
            // https://github.com/FezVrasta/popper.js/issues/535
            popperOptions={{
                modifiers: {
                    preventOverflow: {
                        boundariesElement: 'window'
                    }
                }
            }}
            {...rest}
        />
    );
};

export default Tooltip;
