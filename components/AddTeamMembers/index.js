// @flow
import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import axios from 'axios';
import get from 'lodash/get';
import {Formik, Form} from 'formik';
import teamCreationSchema from '../../schemas/create-team';
import __ from '../../globals/strings';
import Accordion from '../Accordion';
import AssignmentUpload from '../AssignmentUpload';
import CardLayout from '../CardLayout';
import Container from '../Container';
import CTAButton from '../CTAButton';
import InputTextarea from '../InputTextarea';
import TextCard from '../TextCard';
import * as UI from '../UI/styles';
import * as S from './styles';

type Props = {
    team: Team
};

const AddTeamMembers = ({team}: Props) => {
    const [formState, setFormState] = useState('default');

    const handleFormSubmit = async (values, handlers) => {
        if (values.emails) {
            const emails = values.emails.split(',');

            try {
                const response = await axios.post(`/api/team/invite-member?id=${team.id}`, {
                    emails
                });
                const {data} = response;

                if (data.success) {
                    setFormState('success');
                    handlers.setSubmitting(false);

                    setTimeout(() => setFormState('complete'), 1000);
                    // dispatch(
                    //     updateForm({
                    //         state: 'success',
                    //         fields: {...form.fields, emails: values.emails}
                    //     })
                    // );
                    // Cookie.remove('name_draft');
                    // setTimeout(() => dispatch(updateForm({step: 3})), 1000);
                } else {
                    setFormState('default');
                    handlers.setErrors({emails: 'An unknown error occurred.'});
                    handlers.setSubmitting(false);
                }
            } catch (error) {
                const errorResponse = get(error, 'response.data');
                const errors = errorResponse || {
                    emails: __('team.errors.invite')
                };

                // dispatch(updateForm({state: 'default'}));
                setFormState('default');
                handlers.setErrors(errors);
                handlers.setSubmitting(false);
            }
        }
    };

    useEffect(() => {
        /**
         * TODO: How can we pass this into yup as context?
         */
        window.teamMembers = team.members.length || 0;
    }, []);

    return (
        <Container css={UI.centerText} paddingVertical="l">
            <UI.Heading3 as="h2" css={UI.margin('bottom', 's')}>
                {__('team.edit.addMembers.title')}
            </UI.Heading3>

            <div style={{margin: '0 auto', maxWidth: 550}}>
                {formState !== 'complete' && (
                    <>
                        <p>{__('team.edit.addMembers.text')}</p>
                        <small css={UI.textColor('greyDark')}>{__('team.invite.tip')}</small>

                        <UI.Spacer />

                        <Formik
                            initialValues={{emails: ''}}
                            onSubmit={handleFormSubmit}
                            validationSchema={teamCreationSchema.step2}
                            validateOnBlur={false}
                            validateOnChange={false}
                            render={formProps => (
                                <Form autoComplete="off">
                                    <InputTextarea
                                        name="emails"
                                        placeholder="john@doe.com"
                                        autoComplete="off"
                                        autoFocus
                                    />
                                    <UI.Spacer size="s" />
                                    <CTAButton
                                        disabled={formProps.isSubmitting || formProps.isValidating}
                                        type="submit"
                                        state={
                                            formProps.isSubmitting || formProps.isValidating
                                                ? 'pending'
                                                : formState
                                        }
                                        wide={true}
                                    >
                                        {__('team.invite.ctaLabel')}
                                    </CTAButton>
                                </Form>
                            )}
                        />
                    </>
                )}
                {formState === 'complete' && (
                    <p css={UI.fadeInUp}>{__('team.edit.addMembers.success')}</p>
                )}
            </div>
        </Container>
    );
};

export default connect()(AddTeamMembers);
