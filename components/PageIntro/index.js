// @flow
import * as React from 'react';
import {useSpring, animated} from 'react-spring/web.cjs';
import ButtonList from '../ButtonList';
import CTAButton from '../CTAButton';
import * as UI from '../UI/styles';
import * as S from './styles';

type Props = {
    animate?: boolean,
    children?: React.Node,
    image?: string,
    primaryCTA?: CTA | null,
    secondaryCTA?: CTA | null,
    subtitle?: string,
    text?: string,
    title?: string
};

const PageIntro = ({
    animate = true,
    children,
    image,
    primaryCTA,
    secondaryCTA,
    subtitle,
    text,
    title
}: Props) => {
    const config = {
        config: {tension: 60, mass: 1, friction: 10},
        from: {transform: `translateY(${animate ? 20 : 0}px)`, opacity: 0},
        immediate: !animate,
        to: {transform: `translateY(0)`, opacity: 1}
    };
    const badgeAnimation = useSpring({
        config: {tension: 200, mass: 5, friction: 30},
        delay: 100,
        from: {transform: `scale(${animate ? 0 : 1})`, opacity: 1},
        to: {transform: `scale(1)`, opacity: 1}
    });
    const titleAnimation = useSpring(config);
    const subtitleAnimation = useSpring({...config, delay: animate ? 200 : 0});
    const textAnimation = useSpring({...config, delay: animate ? 400 : 0});
    const CTAAnimation = useSpring({...config, delay: animate ? 600 : 0});
    const childrenAnimation = useSpring({...config, delay: animate ? 800 : 0});

    const buttons = [];

    if (primaryCTA) {
        buttons.push(
            <CTAButton
                href={primaryCTA.url}
                key="primary"
                track={primaryCTA.tracking ? primaryCTA.tracking : null}
            >
                {primaryCTA.label}
            </CTAButton>
        );
    }

    if (secondaryCTA) {
        buttons.push(
            <CTAButton
                href={secondaryCTA.url}
                ghost={true}
                key="secondary"
                track={secondaryCTA.tracking ? secondaryCTA.tracking : null}
            >
                {secondaryCTA.label}
            </CTAButton>
        );
    }

    return (
        <S.Container>
            {image && <animated.img style={badgeAnimation} src={image} css={S.image} />}
            {subtitle && (
                <animated.div style={subtitleAnimation}>
                    <UI.Subheading>{subtitle}</UI.Subheading>
                </animated.div>
            )}
            {title && (
                <animated.div style={titleAnimation}>
                    <UI.Heading1 css={UI.margin('bottom', 's')}>{title}</UI.Heading1>
                </animated.div>
            )}
            {text && (
                <animated.div style={textAnimation} dangerouslySetInnerHTML={{__html: text}} />
            )}

            {(primaryCTA || secondaryCTA) && (
                <React.Fragment>
                    <UI.ResponsiveSpacer />

                    <animated.div style={CTAAnimation}>
                        <ButtonList blockAtMobile={true}>{buttons}</ButtonList>
                    </animated.div>
                </React.Fragment>
            )}

            {children && (
                <React.Fragment>
                    <UI.Spacer />
                    <animated.div style={childrenAnimation}>{children}</animated.div>
                </React.Fragment>
            )}
        </S.Container>
    );
};

export default PageIntro;
