// @flow
import {css} from '@emotion/core';
import styled from '@emotion/styled';
import {rem} from 'polished';
import {breakpoints, spacing} from '../../globals/variables';

export const Container = styled.section`
    margin-left: auto;
    margin-right: auto;
    max-width: ${rem(700)};
    padding: ${rem(spacing.m * 0.85)} ${rem(spacing.m)};
    text-align: center;

    @media (max-width: ${rem(breakpoints.desktop)}) {
        margin-top: ${rem(spacing.s)};
    }

    @media (min-width: ${rem(breakpoints.desktop)}) {
        padding-top: calc(${rem(spacing.m)} + 2vmax);
        padding-bottom: calc(${rem(spacing.m)} + 2vmax);
    }
`;

export const image = css`
    height: ${rem(100)};
    width: ${rem(100)};
`;
