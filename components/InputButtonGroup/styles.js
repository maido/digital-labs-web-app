// @flow
import styled from '@emotion/styled';
import {rem} from 'polished';
import {label} from '../Form/styles';
import {Container as InputRadio} from '../InputRadio/styles';
import {breakpoints, colors, spacing} from '../../globals/variables';

export const Container = styled.fieldset`
    border: 0;
    margin: 0;
    padding: 0;
    position: relative;
`;

export const Legend = styled.legend(label);

export const Info = styled.span`
    color: ${colors.greyMid};
    position: absolute;
    right: 0;
    top: 0;
`;

export const ButtonsContainer = styled.div`
    display: flex;
    flex-wrap: wrap;

    ${InputRadio} {
        display: flex;
        margin-bottom: ${rem(spacing.s)};
        margin-right: ${rem(spacing.xs)};

        label {
            align-self: flex-start;
        }
    }

    ${props =>
        props.grow &&
        `${InputRadio} {
        flex-grow: 1;

        label {
            flex-grow: 1;
        }
    }
    `}
`;
