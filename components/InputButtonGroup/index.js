// @flow
import * as React from 'react';
import {ErrorMessage} from 'formik';
import * as FormStyles from '../Form/styles';
import * as S from './styles';

type Props = {
    children: React.Node,
    grow?: boolean,
    info?: string,
    label: string,
    name: string
};

const InputButtonGroup = ({children, grow = false, info, label, name}: Props) => (
    <S.Container>
        {info && <S.Info>{info}</S.Info>}
        <S.Legend id={`${name}-label`}>{label}</S.Legend>
        <S.ButtonsContainer grow={grow}>{children}</S.ButtonsContainer>
        <ErrorMessage
            component="span"
            name={name}
            css={FormStyles.error}
            data-testid={`error-${name}`}
        />
    </S.Container>
);

export default InputButtonGroup;
