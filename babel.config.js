module.exports = api => {
    const isTest = api.env('test');

    return {
        presets: [
            'next/babel',
            [
                '@babel/preset-env',
                {
                    targets: {
                        browsers: ['last 5 versions', 'safari >= 7', 'ie 11'],
                        node: 'current'
                    },
                    modules: isTest ? false : 'auto'
                }
            ],
            '@babel/preset-react',
            '@emotion/babel-preset-css-prop'
        ],
        env: {
            test: {
                presets: ['@babel/preset-react']
            }
        },
        plugins: [
            '@babel/plugin-transform-flow-comments',
            '@babel/plugin-proposal-class-properties'
        ]
    };
};
