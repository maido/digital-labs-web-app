// @flow
import {boolean, mixed, object, string} from 'yup';
import filesize from 'filesize';

export const MAX_SIZE = 10485760;
export const ALLOWED_EXTENSIONS = [
    'jpg',
    'jpeg',
    'gif',
    'png',
    'heic',
    'mp3',
    'mp4',
    'doc',
    'docx',
    'ppt',
    'pptx',
    'xls',
    'xlsx',
    'pdf',
    'csv'
];
export const ALLOWED_TYPES = [
    // jpeg
    'image/jpg',
    'image/jpeg',
    // png
    'image/png',
    // gif
    'image/gif',
    // heic/heif
    'image/heic',
    'image/heif',
    // mp3
    'audio/mp3',
    'audio/mpeg3',
    'audio/x-mpeg-3',
    'audio/mp4',
    'video/mpeg',
    'video/x-mpeg',
    'video/mp4',
    // pdf
    'application/pdf',
    // csv
    'application/csv',
    'application/x-csv',
    'text/csv',
    'text/comma-separated-values',
    'text/x-comma-separated-values',
    'text/tab-separated-values',
    // spreadsheet
    'application/vnd.ms-excel',
    'application/xls',
    'application/xlsx',
    'application/vnd.ms-excel',
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    'application/vnd.openxmlformats-officedocument.spreadsheetml.template',
    // document
    'application/docx',
    'application/msword',
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    // presentation
    'application/ppt',
    'application/vnd.ms-powerpoint',
    'application/vnd.openxmlformats-officedocument.presentationml.presentation'
];

export const isValidFileType = (type = '', allowedTypes: Array<string> = ALLOWED_TYPES) => {
    return allowedTypes.includes(type);
};

const schema = object().shape({
    isFileRequired: boolean(),
    file: mixed().when('isFileRequired', {
        is: true,
        then: mixed()
            .required('You must include an attachment with your submission')
            .test(
                'fileSize',
                `Sorry, your file must be ${filesize(MAX_SIZE)} or smaller`,
                value => value && value.size <= MAX_SIZE
            )
            .test(
                'fileType',
                `Sorry, your file type must be one of: ${ALLOWED_EXTENSIONS.join(', ')}`,
                value => isValidFileType(value ? value.type : '')
            ),
        otherwise: mixed()
            .test(
                'fileSize',
                `Sorry, your file must be ${filesize(MAX_SIZE)} or smaller`,
                value => {
                    if (value && value.size) {
                        return value.size <= MAX_SIZE;
                    } else {
                        return true;
                    }
                }
            )
            .test(
                'fileType',
                `Sorry, your file type must be one of: ${ALLOWED_EXTENSIONS.join(', ')}`,
                value => {
                    if (value && value.type) {
                        return isValidFileType(value.type);
                    } else {
                        return true;
                    }
                }
            )
    }),
    reflections: string()
        .min(2, 'Please provide more detail')
        .max(200, 'Please provide less detail (200 characters max)')
        .required('Please add a response')
});

export default schema;
