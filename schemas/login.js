// @flow
import {object, string} from 'yup';

const schema = object().shape({
    email: string()
        .email('Your email address is not valid')
        .required('Please provide your email address'),
    password: string().when('type', {
        is: 'login',
        then: string()
            .min(8, 'Sorry, your password must be 8 characters or longer')
            .max(40, 'Sorry, your password must be less than 40 characters')
            .required('Please provide your password')
    })
});

export default schema;
