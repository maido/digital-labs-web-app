// @flow
import {mixed, object, string, test} from 'yup';
import {countryList} from '../globals/content';

const schema = object().shape({
    name: string()
        .required('Please provide your team name')
        .min(4, 'Sorry, your team name is too short')
        .max(60, 'Sorry, please shorten your team name')
        .matches(/^[a-z 0-9+]+$/i, 'Sorry, team names can only include letters or numbers'),
    mission: string()
        .min(10, 'Sorry, your mission statement is too short')
        .max(255, 'Sorry, please shorten your mission statement'),
    country: mixed()
        .nullable()
        .oneOf(Object.values(countryList)),
    about: string()
        .min(10, 'Sorry, please provide more detail')
        .max(1500, 'Sorry, please provide less detail'),
    story: string()
        .min(10, 'Sorry, please provide more detail')
        .max(1500, 'Sorry, please provide less detail'),
    category: string().required('Please select at least one option'),
    website_url: string()
        .nullable()
        .url('Please provide a valid URL'),
    facebook_url: string()
        .nullable()
        .url('Please provide a valid URL'),
    twitter_url: string()
        .nullable()
        .url('Please provide a valid URL'),
    instagram_url: string()
        .nullable()
        .url('Please provide a valid URL'),
    linkedin_url: string()
        .nullable()
        .url('Please provide a valid URL')
});

export default schema;
