// @flow
import {boolean, mixed, object, string} from 'yup';
import filesize from 'filesize';
import {isValidYouTubeUrl, isValidVimeoUrl} from '../globals/functions';
import {ALLOWED_EXTENSIONS, MAX_SIZE, isValidFileType, ALLOWED_TYPES} from './assignment';

const fileSchema = (
    isRequired: boolean = true,
    maxSize: number = MAX_SIZE,
    allowedExtensions: Array<string> = ALLOWED_EXTENSIONS,
    allowedFileTypes: Array<string> = ALLOWED_TYPES
) => {
    return mixed()
        .test('isEmpty', 'You must select a file', value => (isRequired && value) || !isRequired)
        .test('fileSize', `Sorry, the file must be ${filesize(maxSize)} or smaller`, value => {
            if (value === 'uploaded') {
                return true;
            }

            if (!isRequired && !value) {
                return true;
            }

            return value && value.size <= maxSize;
        })
        .test(
            'fileType',
            `Sorry, the file type must be: ${allowedExtensions.join(', ')}`,
            value => {
                if (value === 'uploaded') {
                    return true;
                }

                if (!isRequired && !value) {
                    return true;
                }

                return isValidFileType(value ? value.type : '', allowedFileTypes);
            }
        );
};

const files = object().shape({
    terms: boolean().oneOf([true], 'You must read and accept the T&Cs'),
    presentation: fileSchema(true, MAX_SIZE, ['pdf'], ['application/pdf']),
    document_1: fileSchema(false),
    document_2: fileSchema(false),
    document_3: fileSchema(false),
    document_4: fileSchema(false),
    document_5: fileSchema(false),
    video_url: string()
        .required('Please provide a link to your video pitch')
        .max(191, 'Please provide a valid URL')
        .url('Please provide a valid URL')
        .test('video_url', `Sorry, your video must be hosted on YouTube or Vimeo`, value => {
            if (!value || value === '') {
                return false;
            }

            return isValidYouTubeUrl(value) || isValidVimeoUrl(value);
        })
});

const details = object().shape({
    about: string()
        .min(100, 'Please provide more detail')
        .max(2800, 'Please provide less detail (2800 characters max)')
        .required('Please tell us about your project'),
    use_case: string()
        .min(100, 'Please provide more detail')
        .max(960, 'Please provide less detail (960 characters max)')
        .required('Please tell us about the use case'),
    problem_summary: string()
        .min(100, 'Please provide more detail')
        .max(960, 'Please provide less detail (960 characters max)')
        .required('Please provide a summary of the problem'),
    solution_summary: string()
        .min(100, 'Please provide more detail')
        .max(960, 'Please provide less detail (960 characters max)')
        .required('Please provide a summary of the solution'),
    marketing_summary: string()
        .min(100, 'Please provide more detail')
        .max(960, 'Please provide less detail (960 characters max)')
        .required('Please provide a summary of the market'),
    business_model_summary: string()
        .min(100, 'Please provide more detail')
        .max(960, 'Please provide less detail (960 characters max)')
        .required('Please provide a summary of the business model'),
    challenges_summary: string()
        .min(100, 'Please provide more detail')
        .max(960, 'Please provide less detail (960 characters max)')
        .required('Please provide a summary of the potential challenges'),
    competitors_summary: string()
        .min(100, 'Please provide more detail')
        .max(960, 'Please provide less detail (960 characters max)')
        .required('Please provide a summary of your competitors'),
    uproot_status_quo: string()
        .min(100, 'Please provide more detail')
        .max(1680, 'Please provide less detail (1680 characters max)')
        .required('Please tell us how you will uproot the status quo'),
    team_why_summary: string()
        .min(100, 'Please provide more detail')
        .max(960, 'Please provide less detail (960 characters max)')
        .required('Please provide an answer'),
    impact_for_people: string()
        .min(100, 'Please provide more detail')
        .max(960, 'Please provide less detail (960 characters max)')
        .required('Please tell us about the impact on people'),
    impact_for_environment: string()
        .min(100, 'Please provide more detail')
        .max(960, 'Please provide less detail (960 characters max)')
        .required('Please tell us the impact on the environment'),
    category: string().required('Please select an option'),
    prize_interest: string().required('Please select an option'),
    is_incorporated: string().required('Please select an option'),
    money_raised: string().when('is_incorporated', {
        is: 'Yes',
        then: string()
            .min(2, 'Please provide your funding and currency')
            .max(15, 'Only 15 characters max')
            .required('Please provide your funding and currency')
    }),
    money_raised_source: string().when('is_incorporated', {
        is: 'Yes',
        then: string()
            .min(2, 'Please provide your funding source(s)')
            .max(280, 'Only 280 characters max')
            .required('Please provide your funding source(s)')
    }),
    innovation_attitudes_examples: string().when('is_incorporated', {
        is: 'Yes',
        then: string().max(1500, 'Please provide less detail')
    }),
    comments: string().max(1500, 'Please provide less detail (1500 characters max)')
});

export default {files, details};
