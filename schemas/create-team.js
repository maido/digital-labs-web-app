// @flow
import {mixed, object, string, test} from 'yup';
import get from 'lodash/get';
import axios from 'axios';

const step1 = object().shape({
    name: string()
        .required('Please provide your team name')
        .min(4, 'Sorry, your team name is too short')
        .max(60, 'Sorry, please shorten your team name')
        .matches(/^[a-z 0-9+]+$/i, 'Sorry, team names can only include letters or numbers')
});

const step2 = object().shape({
    emails: string()
        .test('are-valid', 'Sorry, please check the emails are valid', async (value = '') => {
            const emails = value.split(',');

            const emailValidations = await Promise.all(
                emails.map((email = '') => {
                    const schema = string().email('Please enter a valid email address');
                    return schema.isValid(email.trim());
                })
            );

            return emailValidations.filter(v => !v).length === 0;
        })
});

export default {step1, step2};
