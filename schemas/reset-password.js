// @flow
import {mixed, object, ref, string} from 'yup';

const schema = object().shape({
    password: string()
        .min(8, 'Your new password must be at least 8 characters long')
        .max(40, 'Sorry, your password must be less than 40 characterss')
        .required('Please enter your new password'),
    password_confirmation: string()
        .oneOf([ref('password'), null], 'Your password confirmation does not match')
        .required('Please confirm your password'),
    token: string().required('A valid token must be provided')
});

export default schema;
