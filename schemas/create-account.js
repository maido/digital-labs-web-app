// @flow
import {array, boolean, date, mixed, object, ref, string} from 'yup';
import dayjs from 'dayjs';
import {countryList, occupations} from '../globals/content';

const step1 = object().shape({
    first_name: string()
        .min(2, 'Please provide your first name')
        .max(255, 'Sorry, please shorten your name')
        .required('Please provide your first name'),
    last_name: string()
        .min(2, 'Please provide your last name')
        .max(255, 'Sorry, please shorten your name')
        .required('Please provide your last name'),
    email: string()
        .email('Your email address is not valid')
        .required('Please provide your email address'),
    birthdate: date()
        .min(new Date(1900, 0, 1), "You're surely too old for this")
        .max(
            dayjs()
                .subtract(16, 'year')
                .toDate(),
            'Sorry, you must be at least 16 to sign up'
        )
        .typeError("Sorry, we don't recognise that format. Please enter as MM/DD/YYYY")
        .required('Please provide your date of birth'),
    password: string()
        .min(8, 'Sorry, your password must be 8 characters or longer')
        .max(40, 'Sorry, your password must be less than 40 characters')
        .required("Please add your password – it's important!"),
    password_confirmation: string()
        .oneOf([ref('password'), null], 'Your password confirmation does not match')
        .required('Please confirm your password')
});

const step2 = object().shape({
    country_origin: mixed()
        .oneOf(Object.values(countryList))
        .required('Please confirm your country of origin'),
    country_residence: mixed()
        .oneOf(Object.values(countryList))
        .required('Please confirm your current country of residence'),
    occupation: mixed()
        .oneOf(occupations)
        .required('Please select one option'),
    degree_date: date().when('occupation', {
        is: 'Student',
        then: date().required('Please provide your expected degree date')
    }),
    graduation_date: date().when('occupation', {
        is: 'Student',
        then: date().required('Please provide your expected graduation date')
    }),
    areas_of_interest: array()
        .max(3, 'You can only select a max of 3 options')
        .required('Please select at least one option'),
    areas_of_learning: array()
        .max(3, 'You can only select a max of 3 options')
        .required('Please select at least one option'),
    heard_via: string().required('Please let us know how you heard about the challenge'),
    terms: boolean().oneOf([true], 'You must read and accept our T&Cs')
});

export default {
    step1,
    step2
};
