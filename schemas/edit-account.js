// @flow
import {array, boolean, date, mixed, object, ref, string} from 'yup';
import dayjs from 'dayjs';
import {countryList, occupations} from '../globals/content';

const schema = object().shape({
    first_name: string()
        .min(2, 'Please provide your first name')
        .max(255, 'Sorry, please shorten your name')
        .required('Please provide your first name'),
    last_name: string()
        .min(2, 'Please provide your last name')
        .max(255, 'Sorry, please shorten your name')
        .required('Please provide your last name'),
    email: string()
        .email('Your email address is not valid')
        .required('Please provide your email address'),
    birthdate: date()
        .min(new Date(1900, 0, 1), "You're surely too old for this")
        .max(
            dayjs()
                .subtract(16, 'year')
                .toDate(),
            'Sorry, you must be at least 16 to sign up'
        )
        .required('Please provide your date of birth'),
    password: string()
        .min(8, 'Sorry, your password must be 8 characters or longer')
        .max(40, 'Sorry, your password must be less than 40 characterss')
        .nullable(),
    password_confirmation: string()
        .oneOf([ref('password'), null], 'Your password confirmation does not match')
        .nullable(),
    country_origin: mixed()
        .oneOf(Object.values(countryList))
        .required('Please confirm your country of origin'),
    country_residence: mixed()
        .oneOf(Object.values(countryList))
        .required('Please confirm your current country of residence'),
    occupation: mixed()
        .oneOf(occupations)
        .required('Please select one option'),
    areas_of_interest: array()
        .max(3, 'You can only select a max of 3 options')
        .required('Please select at least one option'),
    areas_of_learning: array()
        .max(3, 'You can only select a max of 3 options')
        .required('Please select at least one option'),
    university: string()
        .min(2, 'Please provide your university name')
        .max(255, 'Sorry, please shorten your university name')
        .nullable(),
    slack: string()
        .min(2, 'Please provide your Slack name')
        .max(255, 'Sorry, please shorten your Slack name')
        .nullable(),
    field_of_expertise: string()
        .min(2, 'Please provide your field of expertise')
        .max(255, 'Sorry, please shorten your field of expertise')
        .nullable(),
    linkedin: string()
        .url('Please provide a valid URL')
        .nullable()
});

export default schema;
