import {Fragment} from 'react';
import {configure} from '@storybook/react';
import {addDecorator, addParameters} from '@storybook/react';
import {Global} from '@emotion/core';
import {styles} from '../globals/styles';

const newViewports = {
    largeIphone: {
        name: 'Large iPhone (8+, X)',
        styles: {
            width: '416px',
            height: '896px'
        }
    },
    smallIphone: {
        name: 'Small iPhone (8, 5, XR)',
        styles: {
            width: '320px',
            height: '480px'
        }
    },
    iPad: {
        name: 'iPad',
        styles: {
            width: '1080px',
            height: '760px'
        }
    },

    laptop: {
        name: 'Laptop',
        styles: {
            width: '1440px',
            height: '960px'
        }
    }
};

addParameters({viewport: {viewports: newViewports}});

const loadStories = () => {
    const req = require.context('../components', true, /stories\.js$/);
    req.keys().forEach(filename => req(filename));
};

const withGlobal = cb => (
    <Fragment>
        <Global styles={styles} />
        {cb()}
    </Fragment>
);

addDecorator(withGlobal);
configure(loadStories, module);
