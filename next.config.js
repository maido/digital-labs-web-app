const nextEnv = require('next-env');
const withBundleAnalyzer = require('@next/bundle-analyzer')({
    enabled: process.env.ANALYZE === 'true'
});
const withFonts = require('next-fonts');
const withImages = require('next-images');
const withPlugins = require('next-compose-plugins');
require('dotenv').config();
const withNextEnv = nextEnv();

module.exports = withPlugins([withBundleAnalyzer, withFonts, withImages, withNextEnv], {
    poweredByHeader: false,
    target: 'serverless'
});
