import React from 'react';
import bugsnag from '@bugsnag/js';
import bugsnagReact from '@bugsnag/plugin-react';

const bugsnagClient = bugsnag({
    apiKey:
        process.env.NEXT_SERVER_BUGSNAG_CLIENT_ID ||
        process.env.NEXT_STATIC_BUGSNAG_REACT_CLIENT_ID,
    beforeSend: report =>
        new Promise((resolve, reject) => {
            if (process.env.NODE_ENV === 'development') {
                resolve(false);
            } else {
                resolve();
            }
        }),
    releaseStage: process.env.NODE_ENV
});
bugsnagClient.use(bugsnagReact, React);

export default bugsnagClient;
