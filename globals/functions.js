// @flow
import {rem} from 'polished';
import dayjs from 'dayjs';
import get from 'lodash/get';
import map from 'lodash/map';
import zipObject from 'lodash/zipObject';

export const responsiveRem = (unit: number = 0, calculation: string = '+') =>
    `calc(${rem(unit)} ${calculation} var(--responsive-spacing))`;

export const bytesToSize = (bytes: number, seperator: string = '') => {
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];

    if (bytes !== 0) {
        const i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)), 10);

        if (i === 0) {
            return `${bytes}${seperator}${sizes[i]}`;
        } else {
            return `${(bytes / 1024 ** i).toFixed(1)}${seperator}${sizes[i]}`;
        }
    }
};

export const isTeamProfileComplete = (team: Object) => {
    if (team.avatar && team.avatar.url !== '' && team.mission && team.name) {
        return true;
    }

    return false;
};

export const isUserProfileComplete = (user: Object) => {
    if (user.avatar && user.avatar.url !== '' && user.name && user.field_of_expertise) {
        return true;
    }

    return false;
};

export const getDaysRemaining = (date: string) => {
    const deadlineDate = dayjs(date);
    const diff = deadlineDate.diff(dayjs(), 'day', true);

    return Math.ceil(diff);
};

export const getPitchActions = async (pitchDeadline: string) => {
    const daysRemaining = getDaysRemaining(pitchDeadline);
    const actions = {
        canSubmit: daysRemaining >= 0,
        daysRemaining: daysRemaining < 1 ? 0 : daysRemaining,
        deadline: pitchDeadline
    };
    console.log(actions);
    return actions;
};

export const canUserSubmitPitch = (user: Object) => {
    if (user.pitch && user.pitch.canSubmit) {
        return true;
    }
};

export const getSyllabusSubtitle = (type: string, items: number) => {
    if (items) {
        return `${items} ${type}${items > 1 ? 's' : ''}`;
    }
};

export const isContentNew = (date: string) => {
    const creationDate = dayjs(date);
    const diff = creationDate.diff(dayjs(), 'day');

    if (diff > 0 && diff <= 7) {
        return true;
    }
};

export const getSchemaDefaultFieldValues = (fields: Object) => {
    const keys = Object.keys(fields);
    const values = map(fields, f => {
        if (f._type === 'boolean') {
            return false;
        } else {
            return '';
        }
    });

    // $FlowFixMe
    return zipObject(keys, values);
};

export const getLearningProgress = (items: Object = {}) => {
    const {topics_count, completed_topics_count} = items;
    let progress = 0;

    // Note: `topics_count` MUST be greater than 0 to prevent 0 division error
    if (topics_count > 0 && completed_topics_count >= 0) {
        progress = Math.abs((completed_topics_count / topics_count) * 100);

        if (progress > 100) {
            progress = 100;
        }
    }

    return progress;
};

export const getYouTubeIdFromUrl = (url: string = '') => {
    const regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
    const match = url.match(regExp);

    return match && match[7].length == 11 ? match[7] : null;
};

export const handleFetchError = (error: Object, res: Object) => {
    if (error.response) {
        let data = {error: error.response.statusText};

        if (error.response.data.errors) {
            data = error.response.data.errors;
        } else if (error.response.data.message) {
            data = {error: error.response.data.message};
        }

        res.status(error.response.status).json(data);
    } else {
        res.status(500).json({serverError: true});
    }
};

export const absoluteUrl = (req: Object, setLocalhost?: string) => {
    let protocol = 'https:';
    let host;

    if (req) {
        host = req.headers['x-forwarded-host'] || req.headers.host;
    } else if (typeof window !== 'undefined') {
        host = window.location.host;
    }

    if (host && host.indexOf('localhost') > -1) {
        if (setLocalhost) {
            host = setLocalhost;
        }

        protocol = 'http:';
    }

    return {
        protocol,
        host,
        origin: `${protocol}//${host ? host : ''}`
    };
};

export const getFileTypeFromMimeType = (mimeType: string = '') => {
    const fileType = mimeType.split('/');

    if (fileType.length === 2) {
        return fileType[1].toUpperCase();
    } else {
        return mimeType;
    }
};

export const getCardPropsForLearningContent = (content: Object, contentType: string) => {
    let cardSize = 'default';
    let difficulty;
    let completed;
    let image;
    let childItems;
    let headline;
    let url;
    let title;

    if (content) {
        headline = content.headline;
        title = content.title;

        if (contentType === 'syllabus') {
            image = get(content, 'hero.conversions.default_2x', null);
            childItems = {
                all: content.modules_count,
                topics_count: content.topics_count,
                completed_topics_count: content.completed_topics_count,
                type: 'Module'
            };
            url = {
                href: `/labs/[labId]`,
                as: `/labs/${content.id}`
            };
        } else if (contentType === 'lab') {
            image = get(content, 'hero.conversions.default_2x', null);
            childItems = {
                all: content.courses_count,
                topics_count: content.topics_count,
                completed_topics_count: content.completed_topics_count,
                type: 'Course'
            };
            url = {
                href: `/labs/modules/[moduleId]`,
                as: `/labs/modules/${content.id}`
            };
        } else if (contentType === 'module') {
            childItems = {
                all: content.topics_count,
                topics_count: content.topics_count,
                completed_topics_count: content.completed_topics_count,
                type: 'Topic'
            };
            url = {
                href: `/labs/courses/[courseId]`,
                as: `/labs/courses/${content.id}`
            };
        } else if (contentType === 'course') {
            cardSize = 'small';
            completed = get(content, 'completed_at');
            difficulty = get(content, 'difficulty');
            url = {
                href: `/labs/topics/[topicId]`,
                as: `/labs/topics/${content.id}`
            };
        }
    }

    return {
        completed,
        difficulty,
        headline,
        image,
        items: childItems,
        size: cardSize,
        title,
        url
    };
};

export const getUrlForNotification = (content: Notification) => {
    switch (content.model_type) {
        case 'lab':
            return `/labs/${content.model_id}`;
        case 'module':
            return `/labs/modules/${content.model_id}`;
        case 'course':
            return `/labs/courses/${content.model_id}`;
        case 'topic':
            return `/labs/topics/${content.model_id}`;
        case 'resource':
            return `/resources/${content.model_id}`;
        case 'team':
            return `/teams/${content.model_id}`;
        default:
            return '';
    }
};

export const getUnreadNotifications = (notifications: Notifications): Array<number> => {
    return notifications.allIds.unread.filter(id => !notifications.allIds.read.includes(id));
};

export const getContentStatus = (
    isLoading: boolean,
    hasFetched: boolean,
    hasContent: boolean,
    loadingByDefault: boolean = false,
    alwaysShowPlaceholders: boolean = false
) => {
    const status = {
        showContent: false,
        showFallback: false,
        showPlaceholders: loadingByDefault
    };

    if (isLoading && !hasFetched) {
        status.showPlaceholders = true;
    } else if (isLoading && hasFetched && alwaysShowPlaceholders) {
        status.showPlaceholders = true;
    } else if (!isLoading && hasFetched) {
        status.showContent = hasContent;
        status.showFallback = !hasContent;
        status.showPlaceholders = false;
    } else if (hasFetched) {
        status.showPlaceholders = false;
    }

    return status;
};

export const filterAllowedStrings = (string: strings, allowedStrings: Array<string>) => {
    const isAllowed = allowedStrings.includes(string);

    return isAllowed ? string : '';
};

export const getMentorTitle = (jobTitle: string, company: string) => {
    let title = '';

    if (jobTitle) {
        title = `${jobTitle}`;
    }

    if (company) {
        title = `${title} ${jobTitle ? ',' : ''} ${company}`;
    }

    return title;
};

export const scrollToTop = $containerRef => {
    if (typeof window !== 'undefined') {
        const top =
            $containerRef && $containerRef.current
                ? $containerRef.current.getBoundingClientRect().top
                : 0;

        window.scrollTo({
            behavior: 'smooth',
            top: window.scrollY - top * -1
        });
    }
};

export const isValidYouTubeUrl = (url: string = '') => {
    const regex = /(?:youtube\.com\/\S*(?:(?:\/e(?:mbed))?\/|watch\/?\?(?:\S*?&?v\=))|youtu\.be\/)([a-zA-Z0-9_-]{6,11})/g;

    return regex.test(url);
};

export const isValidVimeoUrl = (url: string = '') => {
    const regex = /(http|https)?:\/\/(www\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|)(\d+)(?:|\/\?)/;

    return regex.test(url);
};
