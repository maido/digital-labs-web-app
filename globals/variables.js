// @flow
export const breakpoints = {
    mobileSmall: 540,
    mobile: 741,
    tabletSmall: 769,
    tablet: 1025,
    desktop: 1300,
    desktopLarge: 1800
};
export const colors = {
    white: '#fff',
    black: '#000',
    grey: '#d1d1d6',
    greyLight: '#f1f2f5',
    greyLightest: '#f6f7fa',
    greyMid: '#8e94a4',
    greyDark: '#74747a',
    greyDarkest: '#54545a',
    blueGrey: '#e4e5e7',
    red: '#ff3b30',
    orangeyRed: '#ff3b30',
    offWhite: '#fffde5',
    greenBlue: '#09bc8f',
    aquaMarine: '#1de8b5',
    lightAqua: '#8efadf',
    weirdGreen: '#4cd964',
    purplishBlue: '#651fff',
    brightSkyBlue: '#03c2f1',
    darkBlueGrey: '#1e2a4a',
    darkGreyBlue: '#2c3f6c',
    mediumPink: '#ff5d9e',
    neonBlue: '#00ebff',
    violet: '#F0E9FF',
    paleGrey: '#efeff4',
    paleBlue: '#e4e5e7',
    lightTan: '#f2e3ad',

    primary: '#1de8b5',
    secondary: '#1e2a4a',
    tertiary: '#f1f2f5'
};

export const themes = {
    primary: {
        background: colors.aquaMarine,
        buttonText: colors.darkBlueGrey,
        logoFill: colors.purplishBlue,
        links: colors.purplishBlue,
        text: colors.darkBlueGrey
    },
    secondary: {
        background: colors.darkBlueGrey,
        buttonText: colors.white,
        logoFill: colors.offWhite,
        links: colors.white,
        text: colors.offWhite
    },
    tertiary: {
        accent: colors.greyMid,
        background: colors.greyLight,
        buttonText: colors.darkBlueGrey,
        heading: colors.greyDark,
        links: colors.purplishBlue,
        logoFill: colors.purplishBlue,
        text: colors.darkBlueGrey
    },
    white: {
        accent: colors.blueGrey,
        background: colors.white,
        buttonText: colors.purplishBlue,
        links: colors.darkGreyBlue,
        logoFill: colors.purplishBlue,
        placeholder: colors.grey,
        text: colors.darkBlueGrey
    },
    darkGreyBlue: {
        background: colors.darkGreyBlue,
        links: colors.white,
        placeholder: colors.grey,
        text: colors.white
    },
    greyDark: {
        background: colors.greyDark,
        text: colors.white
    },
    brightSkyBlue: {
        background: colors.brightSkyBlue,
        buttonBackground: colors.white,
        buttonText: colors.darkBlueGrey,
        links: colors.offWhite,
        logoFill: colors.offWhite,
        placeholder: colors.grey,
        text: colors.offWhite
    },
    purplishBlue: {
        background: colors.purplishBlue,
        buttonBackground: colors.white,
        buttonText: colors.darkBlueGrey,
        links: colors.white,
        logoFill: colors.white,
        placeholder: colors.grey,
        text: colors.white
    },
    hotPurple: {
        background: colors.purplishBlue,
        buttonBackground: colors.white,
        buttonText: colors.darkBlueGrey,
        links: colors.white,
        logoFill: colors.white,
        placeholder: colors.grey,
        text: colors.white
    },
    lightTan: {
        background: colors.lightTan,
        buttonBackground: colors.white,
        buttonText: colors.darkBlueGrey,
        links: colors.darkBlueGrey,
        logoFill: colors.darkBlueGrey,
        placeholder: colors.grey,
        text: colors.darkBlueGrey
    },
    blueGrey: {
        background: colors.blueGrey,
        buttonBackground: colors.blueGrey,
        buttonText: colors.darkBlueGrey,
        text: colors.darkBlueGrey
    },
    inactiveFormField: {
        background: colors.white,
        border: colors.grey,
        text: colors.greyDark
    },
    activeFormField: {
        background: colors.greyLightest,
        border: colors.greyMid,
        text: colors.darkBlueGrey
    },
    selectedFormField: {
        background: colors.aquaMarine,
        border: colors.greyMid,
        heading: colors.purplishBlue,
        text: colors.darkBlueGrey
    },
    inactiveButton: {
        background: colors.blueGrey,
        text: colors.greyDark
    },
    activeButton: {
        background: colors.aquaMarine,
        text: colors.darkBlueGrey
    },
    modal: {
        background: colors.darkBlueGrey,
        text: colors.darkBlueGrey
    },
    error: {
        background: colors.purplishBlue,
        text: colors.white
    },
    'Theme 1': {},
    'Theme 2': {}
};

themes['Theme 1'] = themes.primary;
themes['Theme 2'] = themes.brightSkyBlue;

export const pageThemes = [
    'primary',
    'secondary',
    'tertiary',
    'brightSkyBlue',
    'purplishBlue',
    'hotPurple',
    'lightTan'
];

export const fontFamilies = {
    default: "'museo-sans-regular', Arial, sans-serif",
    bold: "'museo-sans-bold', Arial, sans-serif",
    italic: "'museo-sans-italic', Arial, sans-serif",
    heading: "'museo-sans-heading', Arial, sans-serif"
};

export const fontSizes = {
    default: 16,
    lead: 18,
    h1: 48,
    h2: 36,
    h3: 28,
    h4: 24,
    h5: 18,
    h6: 12
};

export const radius = 0;

export const shadows = {
    default: '0 6px 18px 0 rgba(0, 0, 0, 0.06)',
    hint: '0 10px 12px 0 rgba(0, 0, 0, 0.01)',
    hover: '0 6px 24px 0 rgba(0, 0, 0, 0.1)'
};

export const spacing = {
    none: 0,
    xs: 6,
    s: 12,
    m: 24,
    l: 48,
    xl: 72
};

export const transitions = {
    default: 'all .2s ease-in-out',
    slow: 'all .3s ease-out',
    bezier: 'all 2s cubic-bezier(.23,1,.32,1)'
};
