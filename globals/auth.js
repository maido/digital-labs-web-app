import React, {Component} from 'react';
import Router from 'next/router';
import nextCookie from 'next-cookies';
import cookie from 'js-cookie';
import axios from 'axios';
import get from 'lodash/get';
import bugSnagClient from '../globals/bugsnag';
import {absoluteUrl} from '../globals/functions';
import {updateUser} from '../store/actions';

const login = ({token}) => {
    cookie.set('token', token, {expires: 1});
    Router.push('/dashboard');
};

const logout = () => {
    cookie.remove('token');
    window.localStorage.setItem('logout', Date.now());
    Router.push('/login');
};

// Gets the display name of a JSX component for dev tools
const getDisplayName = Component => Component.displayName || Component.name || 'Component';

const auth = (ctx, isRouteBlacklisted) => {
    const {token} = nextCookie(ctx);

    if (!token && isRouteBlacklisted) {
        const from = get(ctx, 'req.originalUrl');
        const redirect = `/login${from ? `?redirect=${from}` : ''}`;

        /*
         * If `ctx.req` is available it means we are on the server.
         * Additionally if there's no token it means the user is not logged in.
         */
        if (ctx.req) {
            ctx.res.writeHead(302, {Location: redirect});
            ctx.res.end();
        } else {
            // We already checked for server. This should only happen on client.
            Router.push(redirect);
        }
    }

    return token;
};

function withAuthSync(WrappedComponent, isRouteBlacklisted = true) {
    return class extends Component {
        static displayName = `withAuthSync(${getDisplayName(WrappedComponent)})`;

        static async getInitialProps(ctx) {
            const token = auth(ctx, isRouteBlacklisted);
            const {origin} = absoluteUrl(ctx.req);
            const state = ctx.reduxStore.getState();
            const {isAuthenticated} = get(state, 'user', {});

            const redirect = (redirectUrl: string = '/login') => {
                if (isRouteBlacklisted) {
                    if (ctx.req) {
                        ctx.res.writeHead(302, {Location: redirectUrl});
                        ctx.res.end();
                    } else {
                        Router.push(redirectUrl);
                    }
                }
            };

            if (token && isAuthenticated !== true) {
                try {
                    const response = await axios.get(`${origin}/api/account/me?token=${token}`);
                    const {data} = response;

                    if (data) {
                        data.isAuthenticated = true;
                        ctx.reduxStore.dispatch(updateUser(data));

                        /**
                         * We have an authenticated user. We need to first check they have a
                         * valid team before continuing.
                         */
                        if (!data.team.name) {
                            const currentPath = ctx.req ? ctx.req.url : Router.router.pathname;
                            const whiteListedPaths = [
                                '/logout',
                                '/team/create',
                                '/signup/success',
                                '/signup/onboarding'
                            ];

                            if (!whiteListedPaths.includes(currentPath)) {
                                redirect('/team/create');
                            }
                        } else {
                            const componentProps =
                                WrappedComponent.getInitialProps &&
                                (await WrappedComponent.getInitialProps(ctx));

                            return {...componentProps};
                        }
                    }
                } catch (error) {
                    if (bugSnagClient) {
                        bugSnagClient.notify(error);
                    }

                    redirect();
                }
            } else if (!token) {
                redirect();
            }
        }

        render() {
            return <WrappedComponent {...this.props} />;
        }
    };
}

export {login, logout, withAuthSync, auth};
