// @flow
import {css} from '@emotion/core';
import {rem} from 'polished';
import {breakpoints, colors, fontFamilies, spacing, transitions} from './variables';

export const styles = css`
    :root {
        --responsive-spacing: 0.1px;
        --reach-menu-button: 1;
    }

    @media (min-width: ${rem(breakpoints.mobile)}) {
        :root {
            --responsive-spacing: ${rem(spacing.xs)};
        }
    }

    @media (min-width: ${rem(breakpoints.desktop)}) {
        :root {
            --responsive-spacing: ${rem(spacing.s)};
        }
    }

    *,
    *::before,
    *::after {
        box-sizing: inherit;
    }

    html {
        background-color: ${colors.white};
        box-sizing: border-box;
        color: ${colors.black};
        font-family: ${fontFamilies.default};
        font-style: normal;
        font-weight: 300;
        overflow-x: hidden;
        -moz-osx-font-smoothing: grayscale;
        -webkit-font-smoothing: antialiased;
        -webkit-text-size-adjust: 100%;
    }

    body {
        font-size: 1em;
        line-height: 1.7;
        margin: 0;
        max-width: 100%;
    }

    @media (max-width: ${rem(breakpoints.mobile)}) {
        body {
            font-size: 0.95em;
        }
    }

    button {
        background: none;
        border: 0;
        cursor: pointer;
        outline: none;
        padding: 0;
    }

    i,
    em,
    b,
    strong {
        font-family: ${fontFamilies.default};
    }

    img {
        max-width: 100%;
    }

    ::selection {
        background-color: ${colors.primary};
        color: ${colors.white};
    }

    a,
    button {
        color: inherit;
        font-family: inherit;
        font-size: inherit;
        text-decoration: none;
        transition: ${transitions.default};
    }

    a {
        color: ${colors.primary};
    }
    a:hover,
    a:focus {
        color: ${colors.greyDark};
    }

    p {
        margin: 0;
    }

    p + * {
        margin-top: ${rem(spacing.m)};
    }

    @media (prefers-reduced-motion: reduce) {
        * {
            animation: none !important;
        }
    }

    /* Fonts */
    @font-face {
        font-family: 'museo-sans-regular';
        src: url('/fonts/museo-sans-300-regular.woff2') format('woff2'),
            url('/fonts/museo-sans-300-regular.woff') format('woff');
        font-display: swap;
        font-weight: normal;
        font-style: normal;
    }
    @font-face {
        font-family: 'museo-sans-italic';
        src: url('/fonts/museo-sans-300-italic.woff2') format('woff2'),
            url('/fonts/museo-sans-300-italic.woff') format('woff');
        font-display: swap;
        font-weight: normal;
        font-style: normal;
    }
    @font-face {
        font-family: 'museo-sans-bold';
        src: url('/fonts/museo-sans-700-regular.woff2') format('woff2'),
            url('/fonts/museo-sans-700-regular.woff') format('woff');
        font-display: swap;
        font-weight: normal;
        font-style: normal;
    }
    @font-face {
        font-family: 'museo-sans-heading';
        src: url('/fonts/museo-sans-900-regular.woff2') format('woff2'),
            url('/fonts/museo-sans-900-regular.woff') format('woff');
        font-display: swap;
        font-weight: normal;
        font-style: normal;
    }

    /* Cookiebot */

    #CybotCookiebotDialog,
    #CybotCookiebotDialog * {
        border-width: 2px !important;
        font-family: inherit !important;
        font-size: 14px !important;
        font-weight: 300 !important;
        line-height: 1.4 !important;
    }
    #CybotCookiebotDialog {
        -webkit-box-shadow: 0 20px 80px rgba(0, 0, 0, 0.4) !important;
        box-shadow: 0 20px 80px rgba(0, 0, 0, 0.4) !important;
        border-top: none;
        padding-bottom: 0 !important;
    }
    #CybotCookiebotDialogPoweredbyLink {
        display: none !important;
    }
    #CybotCookiebotDialogBody {
        max-width: 1000px !important;
        padding-bottom: 30px !important;
        padding-right: 0 !important;
        padding-top: 30px !important;
        width: 90% !important;
    }
    #CybotCookiebotDialogBodyContent {
        color: #000 !important;
        margin-bottom: 15px !important;
        padding: 0 !important;
    }
    #CybotCookiebotDialogBodyContentText {
        color: #000 !important;
    }
    #CybotCookiebotDialogBodyContentTitle {
        color: #1e2a4a !important;
        font-family: ${fontFamilies.default};
        font-size: 23px !important;
        font-weight: 300 !important;
        margin-bottom: 15px !important;
        margin-top: 0;
    }
    #CybotCookiebotDialogBodyButtons {
        margin: 0 !important;
        padding-left: 0 !important;
    }
    #CybotCookiebotDialogBodyButtonAccept,
    #CybotCookiebotDialogBodyLevelButtonAccept {
        background-color: #1e2a4a !important;
        border: 0 !important;
        border-radius: 40px !important;
        margin: 0 !important;
        padding: 8px 20px !important;
    }
    #CybotCookiebotDialogBodyButtonDetails {
        float: none !important;
        margin-left: 30px !important;
    }
    #CybotCookiebotDialogDetailBody {
        max-width: 1000px !important;
        width: 90% !important;
    }
    #CybotCookiebotDialogDetailBodyContentTabs .CybotCookiebotDialogDetailBodyContentTab {
        border: 2px solid #f3f6f6 !important;
        border-radius: 0 !important;
        margin-right: 15px;
        padding: 7.5px 15px !important;
        top: 2px !important;
    }
    #CybotCookiebotDialogDetailBodyContentTabs
        .CybotCookiebotDialogDetailBodyContentTabsItemSelected {
        border-bottom-color: transparent !important;
        color: #1e2a4a !important;
        font-family: ${fontFamilies.default};
        font-weight: 300 !important;
        top: 2px !important;
    }
    #CybotCookiebotDialogDetailBodyContent {
        border-color: #f3f6f6 !important;
        border-width: 2px !important;
    }
    #CybotCookiebotDialogDetailBodyContent
        .CybotCookiebotDialogDetailBodyContentCookieContainerTypes,
    #CybotCookiebotDialogDetailBodyContent
        .CybotCookiebotDialogDetailBodyContentCookieContainerTypesSelected {
        padding: 7.5px 30px 7.5px 15px !important;
    }
    #CybotCookiebotDialogDetailBodyContent
        .CybotCookiebotDialogDetailBodyContentCookieContainerTypesSelected {
        color: #1e2a4a !important;
        font-family: ${fontFamilies.default};
        font-weight: 300 !important;
    }
    #CybotCookiebotDialogDetailBodyContent {
        height: 200px !important;
    }
    #CybotCookiebotDialogDetailBodyContentCookieContainerTypeDetails,
    #CybotCookiebotDialogDetailBodyContentTextAbout {
        color: #000 !important;
        height: 170px !important;
        max-height: 170px !important;
    }
    #CybotCookiebotDialogDetailBodyContentCookieContainerTypeDetails {
        margin-right: 0 !important;
        margin-bottom: 15px !important;
        margin-top: 15px !important;
        padding-left: 15px !important;
        padding-right: 15px !important;
    }
    #CybotCookiebotDialogDetailBodyContentCookieContainerTypeDetails::-webkit-scrollbar {
        width: 4px;
        height: 4px;
        -webkit-appearance: none;
        -webkit-border-radius: 100px;
    }
    #CybotCookiebotDialogDetailBodyContentCookieContainerTypeDetails::-webkit-scrollbar-track {
        background: #fff;
        -webkit-border-radius: 100px;
    }
    #CybotCookiebotDialogDetailBodyContentCookieContainerTypeDetails::-webkit-scrollbar-thumb {
        background: #f3f6f6;
        border: none;
        -webkit-border-radius: 100px;
    }
    #CybotCookiebotDialogDetailBodyContentCookieContainerTypes {
        margin-top: 7.5px !important;
    }
    #CybotCookiebotDialogDetailBodyContentCookieContainer
        .CybotCookiebotDialogDetailBodyContentCookieTypeTable {
        margin-top: 15px !important;
    }
    #CybotCookiebotDialogDetailBodyContentCookieContainer
        .CybotCookiebotDialogDetailBodyContentCookieTypeTable
        thead
        td {
        background-color: transparent !important;
        color: #1e2a4a !important;
        font-family: ${fontFamilies.default};
        font-weight: 300 !important;
        padding: 7.5px !important;
    }
    #CybotCookiebotDialogDetailBodyContentCookieContainer
        .CybotCookiebotDialogDetailBodyContentCookieTypeTable
        tbody
        td:first-of-type {
        font-weight: 300 !important;
    }
    #CybotCookiebotDialogDetailBodyContentCookieContainer
        .CybotCookiebotDialogDetailBodyContentCookieTypeTable
        td {
        color: #000 !important;
        font-size: 13px !important;
        padding: 7.5px !important;
    }
    #CybotCookiebotDialogDetailFooter {
        margin-top: 7.5px !important;
    }
    #CybotCookiebotDialogDetailFooter,
    #CybotCookiebotDialogDetailFooter a {
        font-size: 10px !important;
    }
    .CybotCookiebotDialogDetailBodyContentCookieTypeIntro {
        color: #000;
    }
    .CookieDeclarationDialogText,
    .CookieDeclarationIntro,
    .CookieDeclarationLastUpdated {
        margin: 0 0 15px 0;
    }
    .CookieDeclarationDialogText,
    .CookieDeclarationIntro,
    .CookieDeclarationIntro + p,
    #CookieDeclarationUserStatusPanel,
    .CookieDeclarationLastUpdated {
        color: #000;
    }
    .CookieDeclarationUserStatusPanel + br,
    .CookieDeclarationUserStatusLabelOff {
        display: none;
    }
    .CookieDeclarationLastUpdated {
        margin-bottom: 60px;
    }
    #CookieDeclarationUserStatusPanel a {
        border: 1px solid;
        border-color: #1e2a4a;
        color: #1e2a4a;
        border-radius: 40px;
        cursor: pointer;
        display: inline-block;
        font: inherit;
        font-family: ${fontFamilies.bold};
        font-weight: 700;
        line-height: 1;
        margin-top: 15px;
        padding: 8px 20px;
        text-align: center;
        -webkit-transition: all 0.15s ease-in;
        transition: all 0.15s ease-in;
        vertical-align: middle;
    }
    #CookieDeclarationUserStatusPanel a:hover,
    #CookieDeclarationUserStatusPanel a:active {
        background-color: #1e2a4a;
        color: #fff;
    }
    .CookieDeclarationType {
        background-color: #fff !important;
        border: 0 !important;
        display: block !important;
        padding: 45px !important;
        vertical-align: top !important;
    }
    .CookieDeclarationType + .CookieDeclarationType {
        margin-top: 30px;
    }
    #CookieDeclarationUserStatusPanel {
        display: block !important;
        margin: 0 !important;
    }
    .CookieDeclarationTypeHeader {
        font-size: 16px !important;
        font-size: 1rem !important;
        color: #1e2a4a !important;
        font-family: ${fontFamilies.bold};
        font-weight: 700;
        font-weight: inherit !important;
        line-height: 1.1 !important;
        margin-bottom: 30px !important;
    }
    .CookieDeclarationTypeDescription {
        margin-bottom: 30px;
        opacity: 0.6;
    }
    .CookieDeclarationTable {
        table-layout: fixed;
        width: 100%;
        border-collapse: collapse;
        border-spacing: 0;
        margin: 0 0 18px 0;
        padding: 0;
        border: 0;
        font-size: 100%;
        font: inherit;
        vertical-align: baseline;
    }
    .CookieDeclarationTableHeader {
        border-bottom: 2px solid #1e2a4a !important;
        font-weight: 300;
        padding-bottom: 15px;
        text-align: left;
    }
    .CookieDeclarationTableHeader[dir='rtl'] {
        text-align: right;
    }
    .CookieDeclarationTableCell {
        border-color: #1e2a4a !important;
        color: #000 !important;
        font-size: small;
        text-overflow: ellipsis;
        word-wrap: break-word;
        border-top: 2px solid #f3f6f6;
        vertical-align: top;
        padding: 15px 7.5px;
    }
    @media (max-width: 840px) {
        #CybotCookiebotDialogDetailBody {
            width: 100% !important;
        }
        #CybotCookiebotDialogDetailBodyContent
            .CybotCookiebotDialogDetailBodyContentCookieContainerTypes,
        #CybotCookiebotDialogDetailBodyContent
            .CybotCookiebotDialogDetailBodyContentCookieContainerTypesSelected {
            font-size: small !important;
            padding: 7.5px !important;
        }
        #CybotCookiebotDialogDetailBodyContent
            .CybotCookiebotDialogDetailBodyContentCookieTypeIntro,
        #CybotCookiebotDialogDetailBodyContent
            .CybotCookiebotDialogDetailBodyContentCookieTypeTableContainer {
            font-size: small !important;
        }
        #CybotCookiebotDialogDetailBodyContentCookieContainerTypeDetails {
            overflow-x: scroll;
            -webkit-overflow-scrolling: touch;
            padding-right: 7.5px !important;
        }
        #CybotCookiebotDialogDetailBodyContentCookieContainerTypeDetails,
        #CybotCookiebotDialogDetailBodyContentTextAbout {
            height: 155px !important;
            max-height: 155px !important;
        }
        #CybotCookiebotDialogDetailFooter {
            display: none !important;
        }
        .CookieDeclarationType {
            margin-left: -30px !important;
            padding: 30px !important;
            width: calc(100% + 60px) !important;
        }
        .CybotCookiebotDialogDetailBodyContentCookieTypeTable,
        .CookieDeclarationTable {
            position: relative;
        }
        .CybotCookiebotDialogDetailBodyContentCookieTypeTable *,
        .CybotCookiebotDialogDetailBodyContentCookieTypeTable tr,
        .CybotCookiebotDialogDetailBodyContentCookieTypeTable td,
        .CookieDeclarationTable *,
        .CookieDeclarationTable tr,
        .CookieDeclarationTable td {
            display: block !important;
        }
        .CybotCookiebotDialogDetailBodyContentCookieTypeTable > tbody > tr:nth-of-type(n + 2),
        .CookieDeclarationTable > tbody > tr:nth-of-type(n + 2) {
            margin-top: 45px;
        }
        .CybotCookiebotDialogDetailBodyContentCookieTypeTable thead,
        .CookieDeclarationTable thead {
            display: none !important;
        }
        #CybotCookiebotDialogDetailBodyContentCookieContainer
            .CybotCookiebotDialogDetailBodyContentCookieTypeTable
            td,
        .CookieDeclarationTableCell {
            padding: 0 !important;
            padding-left: 75px !important;
            position: relative !important;
            max-width: none !important;
        }
        .CybotCookiebotDialogDetailBodyContentCookieTypeTable td:first-of-type,
        .CybotCookiebotDialogDetailBodyContentCookieTypeTable td:last-of-type,
        .CookieDeclarationTableCell:first-of-type,
        .CookieDeclarationTableCell:last-of-type {
            border: 0 !important;
        }
        .CybotCookiebotDialogDetailBodyContentCookieTypeTable td::before,
        .CookieDeclarationTableCell::before {
            color: #1e2a4a !important;
            left: 0;
            font-family: ${fontFamilies.bold};
            font-weight: 700;
            margin-right: 15px;
            position: absolute;
        }
        .CybotCookiebotDialogDetailBodyContentCookieTypeTable td:nth-of-type(1)::before,
        .CookieDeclarationTableCell:nth-of-type(1)::before {
            content: 'Name';
        }
        .CybotCookiebotDialogDetailBodyContentCookieTypeTable td:nth-of-type(2)::before,
        .CookieDeclarationTableCell:nth-of-type(2)::before {
            content: 'Provider';
        }
        .CybotCookiebotDialogDetailBodyContentCookieTypeTable td:nth-of-type(3)::before,
        .CookieDeclarationTableCell:nth-of-type(3)::before {
            content: 'Purpose';
        }
        .CybotCookiebotDialogDetailBodyContentCookieTypeTable td:nth-of-type(4)::before,
        .CookieDeclarationTableCell:nth-of-type(4)::before {
            content: 'Expiry';
        }
        .CybotCookiebotDialogDetailBodyContentCookieTypeTable td:nth-of-type(5)::before,
        .CookieDeclarationTableCell:nth-of-type(5)::before {
            content: 'Type';
        }
    }
`;
