const WHITELISTED_ROUTES = [
    '/',
    '/login',
    '/forgot-password',
    '/register',
    '/terms-and-conditions',
    '/privacy-policy',
    '/cookie-policy',
    '/contact',
    '/pitch'
];

const INTEGRATIONS = {
    wikifactory: {
        name: 'Wikifactory',
        url: process.env.NEXT_STATIC_WIKIFACTORY_URL
    }
};

module.exports = {
    WHITELISTED_ROUTES,
    INTEGRATIONS
};
