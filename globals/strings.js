import get from 'lodash/get';

export const strings = {
    account: {
        login: {
            ctaLabel: 'Sign in',
            resetPassword: {
                ctaLabel: 'Return to login',
                text:
                    'Your password reset has been requested and is on its way to your email address.',
                title: 'Reset requested'
            },
            signupLabel: 'Not a member? <strong>Sign up for free</strong>',
            title: 'Welcome to the TFF Digital Labs',
            passwordResetCtaLabel: 'Request a password reset',
            passwordResetLabel: 'Forgot your password?',
            errors: {
                login: 'Sorry, we could not sign you in. Please try again.',
                passwordReset: 'Sorry, we could not submit your request. Please try again.',
                signup: 'Sorry, we could not complete your signup. Please try again.'
            }
        },
        onboarding: {
            title: "Welcome, you've just joined the TFF Digital Labs",
            subtitle: 'What to expect',
            text:
                '<p>By signing up, you gain access to the TFF Digital Labs – a new digital startup accelerator and collaboration platform for food and agricultural innovators – and have the chance to win travel, perks and cash prizes.</p><p>The TFF Digital Labs is a new digital startup accelerator and collaboration platform for food and agricultural innovators, featuring modular learning across five key areas, as well as peer-to-peer and expert mentoring. It empowers you with the skills, mindsets and connections you need to build a successful food and agricultural businesses wherever you are in the world...</p>',

            ctaLabel: 'Create your team',
            ctaLabelInvite: 'Go to your dashboard'
        },
        resetPassword: {
            title: 'Reset requested',
            text: 'We have sent a reset link to your email.',
            ctaLabel: 'Return to login',
            success: {
                title: 'Password reset',
                text: 'Your password has successfully been reset and you can now login.',
                ctaLabel: 'Return to login'
            },
            errors: {
                reset: 'Sorry, we could not submit your request. Please try again.'
            }
        },
        signup: {
            title: 'TFF Digital Labs',
            step1Title: 'Sign up for free',
            step1Text: 'Where next-gen innovation happens',
            step2Title: 'Tell us a bit more about yourself',
            step2Text: 'Where next-gen innovation happens',
            sameCountry: 'Same as country of origin',
            degreeDate: 'Anticipated degree date',
            graduationDate: 'Anticipated graduation date',
            heardVia: 'How did you learn about the TFF Digital Labs?',
            prizes: 'What prize are you most interested in?',
            prizesDescription:
                'Please indicate if you are also interested in taking part in one of the topical challenges offered by TFF partners.',
            success: {
                title: 'Hooray!',
                text: "You're almost ready to get started. Let's get your team set up.",
                textInvitation: "You're almost ready to get started..."
            }
        }
    },
    dashboard: {
        nextTopic: {
            title: 'Start your next topic',
            empty: {
                title: 'Awaiting next topic...',
                text: 'Check back soon for your next topic!'
            }
        },
        labs: {
            title: 'Explore the Labs',
            ctaLabel: 'View all labs'
        }
    },
    errors: {
        notFound: "Sorry, we couldn't find the page you were looking for.",
        other:
            "We're sorry for the interruption, but an error occurred while trying to render this page. Please refresh and try again and, if the problem persists, please let us know."
    },
    learningContent: {
        emptyTitle: 'Coming soon!',
        empty: "This content will be available soon. We will notify you when it's ready.",
        ctaBackToAll: 'Back to all Labs',
        markAsComplete: {
            title: 'Mark as complete',
            text:
                'There are no assignments for this topic. Mark as complete and then move on to your next topic.',
            ctaLabel: 'Mark as complete',
            success: {
                title: 'Topic complete',
                text: 'You have completed this topic. Nice one!'
            },
            errors: {
                complete:
                    'Sorry, we are unable to mark this topic as read at this time. Please try again or come back later.'
            }
        },
        assignment: {
            tableOfContents: 'In this topic:',
            title: 'Submit your work',
            text: 'You have assignments to complete for this topic.',
            textAllCompleted: 'You have completed all assignments for this topic. Hooray!',
            continue: 'Continue with topic assignments',
            view: 'View topic assignments',
            success: {
                title: 'Assignment complete',
                titleAll: 'Topic complete',
                text: 'You have completed this assignment. Way to go!',
                textAllCompleted: 'You have now completed all assignments for this topic. Hooray!'
            },
            errors: {
                complete:
                    'Sorry, we are unable to complete this assignment. Please try again or come back later.'
            }
        },
        relatedTopics: {
            title: 'Explore other topics from this course'
        },
        syllabus: {
            title: 'Curriculum',
            subtitle: 'TFF Digital Labs',
            headline:
                'The TFF Digital Labs cover the 5 key areas: entrepreneurship, food & agriculture knowledge, personal growth, inspiration and creativity.'
        }
    },
    notifications: {
        titleUnread: 'Notifications',
        titleRead: 'Older notifications',
        emptyUnread: "No new notifications, you're all caught up!",
        emptyRead: 'No notifications.'
    },
    pitch: {
        blocked: {
            title: 'Sorry, you cannot pitch at this time',
            text:
                'You cannot submit your pitch right now. Remember, you nead at least <strong>3</strong> members in your team to be able to submit a pitch.'
        },
        passed: {
            title: "Unfortunately, you've missed the pitch deadline",
            text: `We\'re sorry but you\'ve missed the deadline to submit your pitch for TFF Challenge. But, don\'t worry, plenty more challenges await you! The Digital Labs will be updated with new content and live sessions very soon.
                <br /><br />
                Hang out with us here and stay alert for the next pitch deadlines.`
        },
        other: {
            title: 'Sorry, you cannot pitch at this time',
            text:
                'You cannot submit your pitch right now, please try again later. Please contact us if the issue does not resolve and you need further assistance.'
        },
        intro: {
            title: 'Submit your pitch',
            text: `<strong>It's time to submit your pitch!</strong> Pitching is certainly an art, and when it is done well it will win over the minds and hearts of your key audiences, and help to ensure that investments roll in. The most important thing to remember in mastering this skill is to be <strong>simple</strong>, <strong>clear</strong>, and <strong>focused</strong>.
                <br /><br />
                To keep things as straightforward as possible for you and for us, we ask that you submit all of your pitch materials according to the instructions indicated below.
                <br /><br />
                Please take your time and complete your submission properly. To make sure you don’t lose your answers, we suggest you write them out in a separate document and then cut and paste into the fields here.
                <br /><br />
                <strong>Before you begin:</strong>
                <ul>
                    <li>Review your <a href="/team/edit">Team settings</a>; all details must be correct</li>
                    <li>Have your pitch prepared (you may want to submit via your desktop or laptop computer)</li>
                    <li>Remember that you have until <strong>the end of January</strong> to submit your pitch</li>
                    <li>However, once you submit your pitch, you can't change it</li>
                </ul>
                Ready? Let's get going!`,
            footnote:
                'If you have any questions, please reach out to us on Slack or at <a href="mailto:hi@tffdigitallabs.org" target="_blank" rel="noopener">hi@tffdigitallabs.org</a>.',
            exampleUrl: '',
            exampleLabel: 'View example pitch',
            ctaLabel: 'Start your submission'
        },
        team: {
            title: 'Your team',
            text:
                'Before you continue, please confirm your team details are correct. Your details cannot be changed after you submit your pitch.',
            confirmation: 'My team details are correct'
        },
        details: {
            title: 'Your project',
            about: {
                label: 'About your project',
                description: `Describe your project in five <strong>simple</strong>, <strong>clear</strong> and <strong>precise</strong> sentences. (2,800 characters max) <br /><br />

            Sentence 1: Tell us about the problem you are solving. <br />
            Sentence 2: Explain how you are solving it. <br />
            Sentence 3: Describe the innovation you have developed. <br />
            Sentence 4: Tell us about the business model you are using. <br />
            Sentence 5: Tell us how your project is unique.
            <br /><br />
            <strong>Do not</strong> tell us about needing to feed 10 billion by 2050. Please, be more specific. Use these sentences wisely! `
            },
            category: {
                label: 'Category',
                description:
                    'Select <strong>one</strong>. We understand that multiple categories might apply, please choose the one that best fits your concept.'
            },
            use_case: {
                label: 'Use case (960 characters)',
                description:
                    'How will your product or service potentially be used and by whom? Describe a specific situation in two <strong>clear</strong> and <strong>concise</strong> sentences.'
            },
            executive_summary: {
                label: 'Executive summary',
                description: `Answer the questions below <strong>simply</strong>, <strong>clearly</strong> and <strong>concisely</strong>.
            <br />
            You can add more detail and context in your pitch deck. This is just to summarize what you show us in your pitch deck.`
            },
            problem_summary: {
                label: 'Summary of the problem (960 characters)',
                description:
                    'What specific problem are you solving? Describe in two <strong>clear</strong> and <strong>concise</strong> sentences. Use data/ statistics where possible to make your point.'
            },
            solution_summary: {
                label: 'Summary of the solution (960 characters)',
                description:
                    'In two <strong>clear</strong> and <strong>concise</strong> sentences, describe your specific solution. Tell us about the innovation you have developed. '
            },
            marketing_summary: {
                label: 'Summary of the market (960 characters)',
                description:
                    'In two <strong>clear</strong> and <strong>concise</strong> sentences, describe your customer and the market you are targeting.'
            },
            business_model_summary: {
                label: 'Summary of the business model (960 characters)',
                description:
                    'In two <strong>clear</strong> and <strong>concise</strong> sentences, describe your business model. Include details about how you generate revenues and how you will scale in 5 years time.'
            },
            challenges_summary: {
                label: 'Summary of the potential challenges (960 characters)',
                description:
                    'In two <strong>clear</strong> and <strong>concise</strong> sentences, describe challenges you will or may potentially face (e.g. regulatory, customer acceptance, too easily copied, competition, etc).'
            },
            competitors_summary: {
                label: 'Summary of competitors (960 characters)',
                description: `Who are your competitors? Name specific companies wherever possible, even if they are in different parts of the world.
            <br /><br />
            What makes you different or better than them? `
            },
            uproot_status_quo: {
                label: 'Uproot the status quo (1680 characters)',
                description:
                    'In two <strong>clear</strong> and <strong>concise</strong> sentences, tell us how your concept improves the status quo in the market you are targeting.'
            },
            team_why_summary: {
                label: "Summary of your team's Why (960 characters)",
                description: `What is your team’s Why or purpose? <br />
            Tell us why you are doing what you are doing.
            <br />
            How did you come up with your idea in the first place?`
            },
            impact_for_people: {
                label: 'Impact on people (960 characters)',
                description: `Tell us who your concept positively impacts, and how. <br />
            Use specific metrics and data wherever possible.`
            },
            impact_for_environment: {
                label: 'Impact on the environment (960 characters)',
                description: `Tell us how your concept positively impacts the environment. <br />
            Use specific metrics and data wherever possible. `
            },
            prize_interest: {
                label: 'What prize are you most interested in?',
                description:
                    'All entries that meet Thought For Food’s <a href="/terms-and-conditions" target="_blank" rel="noopener">Terms and Conditions</a> will be eligible for the prizes offered by Thought For Food. Please select below if you would like your project to be considered for one of this year\'s special partner prizes (you can only select one!)'
            },
            is_incorporated: {label: 'Are you already incorporated as a business? '},
            money_raised: {
                label: 'Have you raised any money to date?',
                description: 'If so, please specify currency and amount'
            },
            money_raised_source: {
                label: 'In what form have you raised money (investments, grants, other)?'
            },
            innovation_attitudes_examples: {
                label:
                    'Have you integrated any of Thought For Food’s 6 next-gen innovation attitudes into your ways of working and/ or how you do your business?',
                description:
                    'Describe how with <strong>specific</strong> examples. Read about our <a href="https://thoughtforfood.org/content-hub/the-6-essential-attitudes-for-next-generation-innovation/" target="_blank" rel="noopener">innovation attitudes</a>.'
            },
            comments: {
                label: 'Additional comments',
                description:
                    'Please leave any additional comments here. This is <strong>optional</strong>.'
            },
            submitLabel: 'Submit and continue'
        },
        documents: {
            title: 'Your presentation',
            presentation: {
                title: 'Upload your pitch deck',
                text: `Be sure to include all slides from this <a href="https://drive.google.com/open?id=1GYhSeYqPfyZbdslvJofGgB1-vPGIZf3I" target="_blank" rel="noopener">checklist</a>. You can also add additional slides — but keep your deck to under 20 slides total.
                <br /><br />
                <strong>If you are applying for one of the topical prizes, please make sure you include 1-2 slides
explaining your project’s relevance for the prize.</strong>
                <br /><br />
                If you are applying for Circular Economy of Food prize <a href="https://drive.google.com/file/d/1nyMl4UJSbQtWoC8F5_lmcIzyUqCgWMPz/view" target="_blank" rel="noopener">please include this criteria</a> as a slide with your relevant information`,
                ctaLabel: 'Choose file'
            },
            document: {
                title: 'Upload supporting documents',
                text: `Here you can upload legal documents, images or other to support your pitch. This is <strong>optional</strong>. This will be confidential, it won’t appear on your public page.`,
                ctaLabel: 'Choose file'
            },
            video: {
                label: 'Video pitch',
                description: `Please limit your pitch video to 3 minutes max. Anything that exceed 3 minutes will be disregarded.<br /><br />
                Please upload your pitch video to YouTube or Vimeo and provide the link below. Make sure that anyone with the link can access the video.
                <br /><br />
                Market your concept – this is your elevator pitch to win us over. Do <strong>not</strong> tell us about the fact that the world's population is rising to 10bn people, we know this.
                <br />
                <ul>
                    <li>Start with the specific problem you are solving.</li>
                    <li>Explain what is unique about your approach.</li>
                    <li>Show us your prototype.</li>
                    <li>Explain the business model and growth potential.</li>
                    <li>Provide testimonials.</li>
                </ul>

                Show us your team's energy, make us believe in what you are doing!`
            },
            submitLabel: 'Continue to submission review',
            error: 'There were validation errors, please review the form.'
        },
        review: {
            title: 'Review your submission',
            text:
                'Review your details and documents carefully. Remember, you cannot change the details after you submit your pitch!',
            confirmLabel: 'Submit my pitch',
            reviewLabel: 'Edit details'
        },
        success: {
            title: 'Pitch submitted',
            text: 'Your pitch has successfully been submitted. Horray!'
        }
    },
    resources: {
        emptyTitle: 'No resources found',
        empty: 'Please check back soon when there shoud be new resources available.',
        emptyWithFilter:
            'Please check back soon when there shoud be new resources available. You could also try a different filter.'
    },
    team: {
        create: {
            title: 'Create your team',
            text:
                'People will recognise you for your team name. Choose a bold and creative name. If you don’t have a team yet, you can simply use your own name. Don’t worry, you can change this later if you need to.',
            ctaLabel: 'Create your team',
            tip:
                "If you can't think of the perfect team name right now, don't worry. You can change your name at any point from your team's page."
        },
        created: {
            title: "You've got your team all set up",
            text:
                'Your team has been created and the members have been invited. You can now go explore your dashboard.',
            ctaLabel: 'Go to dashboard'
        },
        edit: {
            avatarLabel: 'Change team photo',
            ctaLabel: 'Update your team',
            members: {
                title: 'Team members',
                text: 'To remove someone from your team, or to leave a team, please contact us.',
                addLabel: 'Add team members'
            },
            addMembers: {
                title: 'Add team members',
                text:
                    'You still have space on your team to invite new members. Once they have accepted your invite you will see them on your team.',
                success:
                    'Your invites have been sent. You will see your new team mates once they have accepted their invites.'
            }
        },
        invite: {
            title: 'Invite your team members',
            text:
                'You will only see the members of your team once they have accepted their invite.',
            tip: 'Separate each email address with a comma.',
            restartLabel: 'Change team name',
            ctaLabel: 'Send invites',
            skipLabel: 'Invite team members later',
            skipInformation: 'You can create a team later',
            slack:
                'We can help connect you with our passionate global community of next-gen changemakers; on Slack you can join #findateam channel or reach out to us at <a href="mailto:hi@tffdigitallabs.org">hi@tffdigitallabs.org</a>.',
            skipConfirm: {
                title: 'Are you sure?',
                text:
                    'To submit a pitch and be considered a finalist, you will need to form a team of at least three.',
                confirmLabel: 'Yes, invite members later',
                cancelLabel: 'No, invite members now'
            },
            winningTeams: {
                title: "Last year's winning teams"
            }
        },
        errors: {
            create: 'Sorry, we could not create your team. Please try again.',
            edit: 'Sorry, we could not update your team. Please try again.',
            invite:
                'Sorry, we could not send the invitations. Please check the emails and try again.'
        }
    },
    teams: {
        emptyTitle: 'Sorry, there is currently no teams right now.',
        empty: 'Please check back soon when there shoud be new teams signed-up.',
        profile: {},
        submissions: {
            title: 'Submissions',
            empty: "This team haven't submitted any assignments yet. Check back soon.",
            emptyUser:
                "You haven't submitted any assignments yet. Once you have submitted assignments for a topic they will appear here."
        }
    },
    mentors: {
        emptyTitle: 'Sorry, there is currently no mentors right now.',
        empty: 'Please check back soon when there shoud be new mentors available.'
    },
    footer: {
        links: {
            emailLabel: 'Email us',
            emailUrl: 'mailto:hi@tffdigitallabs.org',
            websiteLabel: 'Visit main website',
            websiteUrl: 'https://thoughtforfood.org'
        }
    },
    slackBanner: {
        title: 'Join the TFF Network on Slack',
        text:
            'A space to continue the discussion and exchange ideas with other innovators from around the world.',
        ctaLabel: 'Join',
        ctaUrl: 'http://tffnetwork.slack.com/'
    }
};

const __ = (key: string | Array<string>) => get(strings, key, '');

export default __;
