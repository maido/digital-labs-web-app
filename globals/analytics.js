// @flow
import ReactGA from 'react-ga';

type EventProps = {
    action?: string,
    category?: string,
    label?: string
};
type ExceptionProps = {
    action?: string,
    category?: string,
    label?: string
};

export const initGA = () => {
    ReactGA.initialize(process.env.NEXT_STATIC_GOOGLE_ANALYTICS_ID);
};

export const logPageView = () => {
    ReactGA.set({page: window.location.pathname});
    ReactGA.pageview(window.location.pathname);
};

export const logEvent = ({action = '', category = '', label = ''}: EventProps) => {
    if (window.GA_INITIALIZED && category && action) {
        ReactGA.event({category, action, label});
    }
};

export const logException = ({description = '', fatal = false}: ExceptionProps) => {
    if (window.GA_INITIALIZED && description) {
        ReactGA.exception({description, fatal});
    }
};
